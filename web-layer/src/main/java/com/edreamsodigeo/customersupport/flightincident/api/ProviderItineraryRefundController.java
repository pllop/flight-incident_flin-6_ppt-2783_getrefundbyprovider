package com.edreamsodigeo.customersupport.flightincident.api;

import com.edreamsodigeo.customersupport.flightincident.RefundRequestService;
import com.edreamsodigeo.customersupport.flightincident.exception.DatabaseInconsistency;
import com.edreamsodigeo.customersupport.flightincident.exception.InsertFailed;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.mapper.RefundRequestMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.product.itinerary.provideritineraryrefundapi.AcceptedAndRefundedAmountsNotEqualException;
import com.odigeo.product.itinerary.provideritineraryrefundapi.BspLinkRefundRequest;
import com.odigeo.product.itinerary.provideritineraryrefundapi.BspLinkTicketAlreadyProcessedException;
import com.odigeo.product.itinerary.provideritineraryrefundapi.DataBaseException;
import com.odigeo.product.itinerary.provideritineraryrefundapi.ProviderItineraryRefundService;
import com.odigeo.product.itinerary.provideritineraryrefundapi.SaveBspLinkRefundRequest;
import com.odigeo.product.itinerary.provideritineraryrefundapi.SaveRefundReceived;
import com.odigeo.product.itinerary.provideritineraryrefundapi.UpdateAcceptedRefundRequest;
import com.odigeo.product.itinerary.provideritineraryrefundapi.UpdateRejectedRefundRequest;

import java.time.ZonedDateTime;
import java.util.List;

@Singleton
public class ProviderItineraryRefundController implements ProviderItineraryRefundService {
    private final RefundRequestMapper mapper;
    private final RefundRequestService service;

    @Inject
    public ProviderItineraryRefundController(RefundRequestMapper mapper, RefundRequestService service) {
        this.mapper = mapper;
        this.service = service;
    }

    @Override
    public void saveBspLinkRequest(SaveBspLinkRefundRequest saveBspLinkRefundRequest) throws BspLinkTicketAlreadyProcessedException {
        try {
            service.saveBspLinkRequest(mapper.mapSaveRequest(saveBspLinkRefundRequest));
        } catch (InsertFailed insertFailed) {
            insertFailed.printStackTrace();
            throw new BspLinkTicketAlreadyProcessedException(insertFailed.getMessage(), insertFailed);
        }
    }

    @Override
    public void saveBspLinkRefundReceived(Long ticketId, SaveRefundReceived saveRefundReceived) throws AcceptedAndRefundedAmountsNotEqualException, DataBaseException {
        try {
            service.saveBspLinkRefundReceived(ticketId, mapper.mapSaveRefundReceived(saveRefundReceived));
        } catch (RegisterNotFound registerNotFound) {
            registerNotFound.printStackTrace();
            throw new DataBaseException(registerNotFound.getMessage(), registerNotFound);
        } catch (DatabaseInconsistency databaseInconsistency) {
            throw new AcceptedAndRefundedAmountsNotEqualException(databaseInconsistency.getMessage());
        }
    }

    @Override
    public void updateBspLinkAcceptedRequest(Long ticketId, UpdateAcceptedRefundRequest request) throws DataBaseException {
        try {
            service.updateBspLinkAcceptedRequest(ticketId, request.getAmountAccepted(), request.getAcceptedTimestamp());
        } catch (RegisterNotFound registerNotFound) {
            registerNotFound.printStackTrace();
            throw new DataBaseException(registerNotFound.getMessage(), registerNotFound);
        }
    }

    @Override
    public void updateBspLinkRejectedRequest(Long ticketId, UpdateRejectedRefundRequest request) throws DataBaseException {
        try {
            service.updateBspLinkRejectedRequest(ticketId, request.getRejectionTimestamp());
        } catch (RegisterNotFound registerNotFound) {
            registerNotFound.printStackTrace();
            throw new DataBaseException(registerNotFound.getMessage(), registerNotFound);
        }
    }

    @Override
    public BspLinkRefundRequest getBspLinkRequest(Long ticketId) throws DataBaseException {
        try {
            return mapper.mapRequest(service.getBspLinkRequest(ticketId));
        } catch (RegisterNotFound registerNotFound) {
            registerNotFound.printStackTrace();
            throw new DataBaseException(registerNotFound.getMessage(), registerNotFound);
        }
    }

    @Override
    public BspLinkRefundRequest getBspLinkRequestByProviderBookingItemId(Long providerBookingItemId) throws DataBaseException {
        try {
            return mapper.mapRequest(service.getBspLinkRequestByProviderBookingItemId(providerBookingItemId));
        } catch (RegisterNotFound registerNotFound) {
            registerNotFound.printStackTrace();
            throw new DataBaseException(registerNotFound.getMessage(), registerNotFound);
        }
    }

    @Override
    public List<BspLinkRefundRequest> getBspLinkRequests(String firstDate, String lastDate) throws DataBaseException {
        ZonedDateTime start = ZonedDateTime.parse(firstDate);
        ZonedDateTime end = ZonedDateTime.parse(lastDate);
        return mapper.mapRequestDTOs(service.getBspLinkRequests(start, end));
    }
}
