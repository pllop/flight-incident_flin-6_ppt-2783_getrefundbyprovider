package com.edreamsodigeo.customersupport.flightincident.mapper;

import com.edreamsodigeo.customersupport.flightincident.model.BspLinkRefundRequest;
import com.edreamsodigeo.customersupport.flightincident.model.RefundMethod;
import com.edreamsodigeo.customersupport.flightincident.model.RefundReceived;
import com.edreamsodigeo.customersupport.flightincident.model.RefundStatus;
import com.odigeo.product.itinerary.provideritineraryrefundapi.RefundRequestStatus;
import com.odigeo.product.itinerary.provideritineraryrefundapi.SaveBspLinkRefundRequest;
import com.odigeo.product.itinerary.provideritineraryrefundapi.SaveRefundReceived;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

public class RefundRequestMapper {

    public BspLinkRefundRequest mapSaveRequest(SaveBspLinkRefundRequest request) {
        BspLinkRefundRequest bspLinkRefundRequest = new BspLinkRefundRequest();
        bspLinkRefundRequest.setProviderBookingItemId(request.getProviderBookingItemId());
        bspLinkRefundRequest.setTicketId(request.getTicketNumber());
        bspLinkRefundRequest.setStatus(RefundStatus.REQUESTED);
        bspLinkRefundRequest.setRequestInstant(Instant.now());
        bspLinkRefundRequest.setExpectedRefundAmount(request.getExpectedRefundAmount());
        bspLinkRefundRequest.setProviderRefundRequestId(request.getDocumentId());
        bspLinkRefundRequest.setAirlineResponseInstant(request.getRequestTimestamp());
        return bspLinkRefundRequest;
    }

    public com.odigeo.product.itinerary.provideritineraryrefundapi.BspLinkRefundRequest mapRequest(BspLinkRefundRequest bspLinkRequest) {
        com.odigeo.product.itinerary.provideritineraryrefundapi.BspLinkRefundRequest bspLinkRefundRequest = new com.odigeo.product.itinerary.provideritineraryrefundapi.BspLinkRefundRequest();
        bspLinkRefundRequest.setId(bspLinkRequest.getId().getId());
        bspLinkRefundRequest.setProviderBookingItemId(bspLinkRequest.getProviderBookingItemId());
        bspLinkRefundRequest.setExpectedRefundAmount(bspLinkRequest.getExpectedRefundAmount());
        bspLinkRefundRequest.setRequestTimestamp(bspLinkRequest.getRequestInstant());
        bspLinkRefundRequest.setAirlineResponseTimestamp(bspLinkRequest.getAirlineResponseInstant());
        bspLinkRefundRequest.setStatus(RefundRequestStatus.valueOf(bspLinkRequest.getStatus().name()));
        bspLinkRefundRequest.setTicketId(bspLinkRequest.getTicketId());
        bspLinkRefundRequest.setDocumentId(bspLinkRequest.getProviderRefundRequestId());
        return bspLinkRefundRequest;
    }

    public List<com.odigeo.product.itinerary.provideritineraryrefundapi.BspLinkRefundRequest> mapRequestDTOs(List<BspLinkRefundRequest> bspLinkRequests) {
        return bspLinkRequests.stream().map(this::mapRequest).collect(Collectors.toList());
    }

    public RefundReceived mapSaveRefundReceived(SaveRefundReceived saveRefundReceived) {
        RefundReceived refundReceived = new RefundReceived();
        refundReceived.setProviderAmountRefunded(saveRefundReceived.getAmountRefunded());
        refundReceived.setRefundInstant(saveRefundReceived.getRefundTimestamp());
        refundReceived.setMethod(RefundMethod.valueOf(saveRefundReceived.getMethod()));
        refundReceived.setProviderBookingItemId(saveRefundReceived.getProviderBookingItemId());
        refundReceived.setProviderRefundRequestId(saveRefundReceived.getDocumentId());
        refundReceived.setTicketId(saveRefundReceived.getTicketNumber());
        return refundReceived;
    }
}
