package com.edreamsodigeo.customersupport.flightincident.bootstrap;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.api.CancellationRestController;
import com.edreamsodigeo.customersupport.cancellation.api.ProviderItemDiscountRestController;
import com.edreamsodigeo.customersupport.cancellation.api.ProviderItemManualTaskRestController;
import com.edreamsodigeo.customersupport.cancellation.api.ProviderItemMerchantRefundController;
import com.edreamsodigeo.customersupport.cancellation.api.ProviderItemNonMerchantRefundController;
import com.edreamsodigeo.customersupport.flightincident.api.FlightIncidentRestController;
import com.edreamsodigeo.customersupport.flightincident.api.ProviderItineraryRefundController;
import com.edreamsodigeo.customersupport.flightincident.exceptionmapper.DataBaseExceptionMapper;
import com.edreamsodigeo.customersupport.flightincident.exceptionmapper.IncidentNotFoundExceptionMapper;
import com.edreamsodigeo.customersupport.flightincident.exceptionmapper.RuntimeExceptionMapper;
import com.edreamsodigeo.customersupport.refund.RefundController;

import com.edreamsodigeo.customersupport.itinerary.api.FlexibleTicketRestController;
import com.edreamsodigeo.customersupport.refund.customermovement.api.CustomerMovementRestController;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ApplicationPath("")
public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication() {
        super();
        singletons.addAll(buildRestControllersAndProviders());
    }

    private List<?> buildRestControllersAndProviders() {
        return Arrays.asList(ConfigurationEngine.getInstance(FlightIncidentRestController.class),
                ConfigurationEngine.getInstance(CancellationRestController.class),
                ConfigurationEngine.getInstance(FlexibleTicketRestController.class),
                ConfigurationEngine.getInstance(RefundController.class),
                ConfigurationEngine.getInstance(ProviderItemManualTaskRestController.class),
                ConfigurationEngine.getInstance(ProviderItemDiscountRestController.class),
                ConfigurationEngine.getInstance(ProviderItemMerchantRefundController.class),
                ConfigurationEngine.getInstance(ProviderItemNonMerchantRefundController.class),
                ConfigurationEngine.getInstance(CustomerMovementRestController.class),
                ConfigurationEngine.getInstance(ProviderItineraryRefundController.class),
                new IncidentNotFoundExceptionMapper(),
                new DataBaseExceptionMapper(),
                new RuntimeExceptionMapper(),

                new com.edreamsodigeo.customersupport.cancellation.exceptionmapper.CancellationNotFoundExceptionMapper(),
                new com.edreamsodigeo.customersupport.cancellation.exceptionmapper.ProviderItemMerchantNotFoundExceptionMapper(),
                new com.edreamsodigeo.customersupport.cancellation.exceptionmapper.ProviderItemNonMerchantNotFoundExceptionMapper(),
                new com.edreamsodigeo.customersupport.cancellation.exceptionmapper.DataBaseExceptionMapper(),
                new com.edreamsodigeo.customersupport.cancellation.exceptionmapper.RuntimeExceptionMapper(),
                new com.edreamsodigeo.customersupport.cancellation.exceptionmapper.RuntimeExceptionMapper(),
                new com.edreamsodigeo.customersupport.refund.customermovement.exceptionmapper.CustomerMovementResourceExceptionMapper(),

                new com.edreamsodigeo.customersupport.itinerary.exceptionmapper.FlexibleTicketNotFoundExceptionMapper(),
                new com.edreamsodigeo.customersupport.itinerary.exceptionmapper.DataBaseExceptionMapper(),
                new com.edreamsodigeo.customersupport.itinerary.exceptionmapper.RuntimeExceptionMapper()
        );
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
