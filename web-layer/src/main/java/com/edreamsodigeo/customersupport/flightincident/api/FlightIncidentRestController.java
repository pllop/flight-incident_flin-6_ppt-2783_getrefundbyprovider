package com.edreamsodigeo.customersupport.flightincident.api;

import com.edreamsodigeo.customersupport.flightincident.CashRefundService;
import com.edreamsodigeo.customersupport.flightincident.ConsentDTO;
import com.edreamsodigeo.customersupport.flightincident.FlightIncidentService;
import com.edreamsodigeo.customersupport.flightincident.IncidentDTO;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.mappers.CashRefundMapper;
import com.edreamsodigeo.customersupport.flightincident.v1.FlightIncidentResource;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.IncidentNotFoundException;
import com.edreamsodigeo.customersupport.flightincident.v1.model.BookingId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.BookingItemCashRefund;
import com.edreamsodigeo.customersupport.flightincident.v1.model.CancellationId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Consent;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.v1.model.IncidentId;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.uuid.UUIDGenerator;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import java.util.List;

@Singleton
public class FlightIncidentRestController implements FlightIncidentResource {

    private final FlightIncidentService flightIncidentService;
    private final CashRefundService cashRefundService;

    @Inject
    public FlightIncidentRestController(FlightIncidentService flightIncidentService, CashRefundService cashRefundService) {
        this.flightIncidentService = flightIncidentService;
        this.cashRefundService = cashRefundService;
    }

    @Override
    public List<Incident> getIncidents(BookingId bookingId) throws IncidentNotFoundException {
        List<Incident> incidents;
        try {
            incidents = flightIncidentService.getIncidents(Long.parseLong(bookingId.toString()));
        } catch (RegisterNotFound | EJBException registerNotFound) {
            throw new IncidentNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
        return incidents;
    }

    @Override
    public Incident getIncident(IncidentId incidentId) throws IncidentNotFoundException {
        try {
            return flightIncidentService.getIncident(incidentId.getId());
        } catch (RegisterNotFound | EJBException registerNotFound) {
            throw new IncidentNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
    }

    @Override
    public void updateConsent(IncidentId incidentId, Consent consent, String channel) throws IncidentNotFoundException {
        try {
            flightIncidentService.updateConsent(incidentId.getId(), new ConsentDTO(consent.getFlexibleTicketAccepted(),
                    consent.getVoucherAccepted(),
                    consent.getCashRefundAccepted(),
                    consent.getEmdAccepted()), channel);
        } catch (RegisterNotFound | EJBException registerNotFound) {
            throw new IncidentNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
    }

    @Override
    public void addIncident(Incident incident) throws DataBaseException {
        try {
            flightIncidentService.addIncident(incidentToDTO(incident));
        } catch (PersistenceException | EJBException insertFailed) {
            throw new DataBaseException(insertFailed.getMessage(), insertFailed);
        }
    }

    @Override
    public void updateIncident(Incident incident) throws IncidentNotFoundException {
        try {
            flightIncidentService.updateIncident(incidentToDTOUpdate(incident));
        } catch (RegisterNotFound | EJBException registerNotFound) {
            throw new IncidentNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
    }

    @Override
    public List<BookingItemCashRefund> getCashRefundsStatus(IncidentId incidentId) throws IncidentNotFoundException {
        try {
            com.edreamsodigeo.customersupport.flightincident.model.IncidentId internalId = new com.edreamsodigeo.customersupport.flightincident.model.IncidentId(incidentId.getId());
            return CashRefundMapper.INSTANCE.map(cashRefundService.getRefundsStatus(internalId));
        } catch (RegisterNotFound e) {
            throw new IncidentNotFoundException(e.getMessage(), e);
        }
    }

    @Override
    public void addCancellationId(IncidentId incidentId, CancellationId cancellationId) throws IncidentNotFoundException {
        try {
            flightIncidentService.addCancellationID(incidentId.getId(), cancellationId.getId());
        } catch (RegisterNotFound | EJBException registerNotFound) {
            throw new IncidentNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
    }

    @Override
    public List<Incident> getIncidentsWithoutCancellation(int pageNumber) throws DataBaseException {
        return flightIncidentService.getIncidentsWithoutCancellation(pageNumber);
    }

    private IncidentDTO incidentToDTO(Incident incident) {
        IncidentDTO incidentDTO = new IncidentDTO();
        incidentDTO.setId(UUIDGenerator.getInstance().generateUUID());
        incidentDTO.setCancellationId(incident.getCancellationId() != null ? incident.getCancellationId().getId() : null);
        incidentDTO.setDepartureLocalDate(incident.getDepartureLocalDate() != null ? incident.getDepartureLocalDate().atStartOfDay() : incident.getDepartureLocalDateTime());
        incidentDTO.setFlightNumber(incident.getFlightNumber());
        return getIncidentDTO(incident, incidentDTO);
    }

    private IncidentDTO incidentToDTOUpdate(Incident incident) {
        IncidentDTO incidentDTO = new IncidentDTO();
        return getIncidentDTO(incident, incidentDTO);
    }

    private IncidentDTO getIncidentDTO(Incident incident, IncidentDTO incidentDTO) {
        ConsentDTO consentDTO = new ConsentDTO(incident.getConsent().getFlexibleTicketAccepted(),
                incident.getConsent().getVoucherAccepted(),
                incident.getConsent().getCashRefundAccepted(),
                incident.getConsent().getEmdAccepted());
        incidentDTO.setBookingId(incident.getBookingId().getId());
        incidentDTO.setProviderBookingItemId(incident.getProviderBookingItemId());
        incidentDTO.setStatus(incident.getStatus() != null ? incident.getStatus().name() : null);
        incidentDTO.setConsentDTO(consentDTO);
        incidentDTO.setId(incident.getId().getId());
        return incidentDTO;
    }

}
