package com.edreamsodigeo.customersupport.flightincident.mappers;

import com.edreamsodigeo.customersupport.cashrefund.BookingItemCashRefund;
import com.edreamsodigeo.customersupport.cashrefund.ProviderBookingItemCashRefund;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.util.List;

@Mapper
@SuppressFBWarnings
public interface CashRefundMapper {

    CashRefundMapper INSTANCE = Mappers.getMapper(CashRefundMapper.class);

    com.edreamsodigeo.customersupport.flightincident.v1.model.ProviderBookingItemCashRefund toContract(ProviderBookingItemCashRefund providerBookingItemCashRefund);

    List<com.edreamsodigeo.customersupport.flightincident.v1.model.BookingItemCashRefund> map(List<BookingItemCashRefund> bookingItemCashRefunds);
}
