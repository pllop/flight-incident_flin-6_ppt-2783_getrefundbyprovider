package com.edreamsodigeo.customersupport.flightincident.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.v1.client.CancellationServiceModule;
import com.edreamsodigeo.customersupport.flightincident.configuration.ServiceLayerModule;
import com.edreamsodigeo.customersupport.flightincident.configuration.StoreLayerModule;
import com.edreamsodigeo.customersupport.refund.v1.client.CustomerMovementServiceModule;
import com.edreamsodigeo.customersupport.refund.v1.client.RefundServiceModule;
import com.odigeo.commons.config.files.ConfigurationFilesManager;
import com.odigeo.commons.monitoring.metrics.MetricsManager;
import com.odigeo.commons.monitoring.metrics.reporter.ReporterStatus;
import com.odigeo.commons.rest.bootstrap.EdoContextListener;
import com.odigeo.crm.mailer.EmailApi;
import com.odigeo.crm.mailer.EmailApiModule;
import com.odigeo.product.itinerary.provideritineraryrefundapi.ProviderItineraryRefundApiDefaultModule;
import java.io.IOException;
import java.net.URL;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.vfs.VFS;
import org.jboss.vfs.VFSUtils;
import org.jboss.vfs.VirtualFile;

public class ContextListener extends EdoContextListener {

    private static final String LOG4J_FILENAME = "/log4j.properties";
    private static final long LOG4J_WATCH_DELAY_MS = 1800000L;
    private final HealthCheckRegister healthcheckRegister = new HealthCheckRegister();

    private static String getAppName() {
        try {
            return InitialContext.doLookup("java:app/AppName");
        } catch (NamingException e) {
            throw new IllegalStateException("cannot get AppName", e);
        }
    }

    @Override
    protected void startUpApplication(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();

        servletContext.log("Bootstrapping .....");
        Logger logger = initLog4J(servletContext);

        initConfigurationEngine();

        initMetrics();

        healthcheckRegister.initHealthChecks(event, new HealthCheckRegistry());

        logger.info("Bootstrapping finished!");
    }

    @Override
    protected void endApplication(ServletContextEvent event) {
        event.getServletContext().log("context destroyed");
    }

    private void initConfigurationEngine() {
        // Add any other dependencies' modules here
        ConfigurationEngine.init(
                new StoreLayerModule(),
                new ServiceLayerModule(),
                new BootstrapModule(),
                new CancellationServiceModule(),
                new CustomerMovementServiceModule(),
                new RefundServiceModule(),
                new EmailApiModule(EmailApi.class),
                new ProviderItineraryRefundApiDefaultModule()
        );
    }

    private Logger initLog4J(ServletContext servletContext) {
        VirtualFile virtualFile = VFS.getChild(new ConfigurationFilesManager().getConfigurationFileUrl(LOG4J_FILENAME, this.getClass()).getFile());
        URL fileRealURL;
        try {
            fileRealURL = VFSUtils.getPhysicalURL(virtualFile);
            PropertyConfigurator.configureAndWatch(fileRealURL.getFile(), LOG4J_WATCH_DELAY_MS);
            servletContext.log("Log4j has been initialized with config file " + fileRealURL.getFile() + " and watch delay of " + (LOG4J_WATCH_DELAY_MS / 1000) + " seconds");
        } catch (IOException e) {
            throw new IllegalStateException("Log4j cannot be initialized: file " + LOG4J_FILENAME + " cannot be load", e);
        }

        return Logger.getLogger(ContextListener.class);
    }

    private void initMetrics() {
        MetricsManager.getInstance().addMetricsReporter(getAppName());
        MetricsManager.getInstance().changeReporterStatus(ReporterStatus.STARTED);
    }
}
