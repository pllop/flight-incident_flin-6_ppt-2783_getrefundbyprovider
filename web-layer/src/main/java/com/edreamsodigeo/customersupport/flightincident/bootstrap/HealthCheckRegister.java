package com.edreamsodigeo.customersupport.flightincident.bootstrap;

import com.codahale.metrics.health.HealthCheckRegistry;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckException;
import com.odigeo.commons.monitoring.healthcheck.HealthCheckServlet;
import com.odigeo.commons.rest.monitoring.healthcheck.ServiceHealthCheck;
import com.odigeo.crm.mailer.EmailApi;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;

public class HealthCheckRegister {
    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass());

    void initHealthChecks(ServletContextEvent servletContextEvent, HealthCheckRegistry registry) {
        servletContextEvent.getServletContext().setAttribute(HealthCheckServlet.REGISTRY_KEY, registry);
        autoRegisterServiceHealthCheck(EmailApi.class, registry);
    }

    void autoRegisterServiceHealthCheck(Class<?> clazz, HealthCheckRegistry registry) {
        try {
            registry.register(clazz.getSimpleName(), new ServiceHealthCheck(clazz));
        } catch (IllegalAccessException | IOException | InvocationTargetException e) {
            LOGGER.error(clazz.getCanonicalName() + " health check is throwing exceptions");
            throw new HealthCheckException("Can not initialize " + clazz.getCanonicalName() + "healthcheck", e);
        }
    }
}
