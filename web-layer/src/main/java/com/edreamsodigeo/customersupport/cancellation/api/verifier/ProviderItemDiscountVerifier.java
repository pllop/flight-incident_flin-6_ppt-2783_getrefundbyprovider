package com.edreamsodigeo.customersupport.cancellation.api.verifier;

import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscount;

import java.util.Objects;

public class ProviderItemDiscountVerifier {
    public void verify(ProviderItemDiscount providerItemDiscount) throws IllegalArgumentException {
        switch (providerItemDiscount.getDiscountType().name()) {
        case "VOUCHER_REFUND":
        case "TOP_UP_VOUCHER":
            if (isVoucher(providerItemDiscount)) {
                throw new IllegalArgumentException();
            }
            break;
        case "EMD":
            if (isEmd(providerItemDiscount)) {
                throw new IllegalArgumentException();
            }
            break;
        case "FLEXIBLE_TICKET":
            if (isFlexibleTicket(providerItemDiscount)) {
                throw new IllegalArgumentException();
            }
            break;
        }
    }

    private boolean isFlexibleTicket(ProviderItemDiscount providerItemDiscount) {
        return Objects.isNull(providerItemDiscount.getFlexibleTicketId()) || Objects.nonNull(providerItemDiscount.getVoucherId()) || Objects.nonNull(providerItemDiscount.getEmdId());
    }

    private boolean isEmd(ProviderItemDiscount providerItemDiscount) {
        return Objects.isNull(providerItemDiscount.getEmdId()) || Objects.nonNull(providerItemDiscount.getFlexibleTicketId()) || Objects.nonNull(providerItemDiscount.getVoucherId());
    }

    private boolean isVoucher(ProviderItemDiscount providerItemDiscount) {
        return Objects.isNull(providerItemDiscount.getVoucherId()) || Objects.nonNull(providerItemDiscount.getFlexibleTicketId()) || Objects.nonNull(providerItemDiscount.getEmdId());
    }
}
