package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantRefundService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.ProviderItemNonMerchantApiMapper;
import com.edreamsodigeo.customersupport.cancellation.v1.ProviderItemNonMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.ProviderItemNonMerchantNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemNonMerchant;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemNonMerchantId;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;

@Singleton
public class ProviderItemNonMerchantRefundController implements ProviderItemNonMerchantRefund {

    private final ProviderItemNonMerchantApiMapper mapper;
    private final ProviderItemNonMerchantRefundService service;

    @Inject
    public ProviderItemNonMerchantRefundController(ProviderItemNonMerchantApiMapper providerItemNonMerchantApiMapper, ProviderItemNonMerchantRefundService providerItemNonMerchantRefundService) {
        this.mapper = providerItemNonMerchantApiMapper;
        this.service = providerItemNonMerchantRefundService;
    }

    @Override
    public ProviderItemNonMerchant getProviderItemNonMerchant(ProviderItemNonMerchantId id) throws ProviderItemNonMerchantNotFoundException {
        try {
            return mapper.mapDtoToProviderItemNonMerchant(service.getProviderItemNonMerchant(id.getId()));
        } catch (RegisterNotFound registerNotFound) {
            throw new ProviderItemNonMerchantNotFoundException(registerNotFound.getMessage(), registerNotFound);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public ProviderItemNonMerchant getProviderItemNonMerchantByProviderItemId(ProviderItemId providerItemId) throws ProviderItemNonMerchantNotFoundException {
        try {
            return mapper.mapDtoToProviderItemNonMerchant(service.getProviderItemNonMerchantByProviderItem(providerItemId.getId()));
        } catch (RegisterNotFound registerNotFound) {
            throw new ProviderItemNonMerchantNotFoundException(registerNotFound.getMessage(), registerNotFound);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public ProviderItemNonMerchant addProviderItemNonMerchant(ProviderItemNonMerchant providerItemNonMerchant) throws DataBaseException {
        try {
            ProviderItemNonMerchantDTO itemToCancel = mapper.mapProviderItemNonMerchantToDto(providerItemNonMerchant);
            ProviderItemNonMerchantDTO result = service.addProviderItemNonMerchant(itemToCancel);
            return mapper.mapDtoToProviderItemNonMerchant(result);
        } catch (PersistenceException | EJBException insertFailed) {
            throw new DataBaseException(insertFailed.getMessage(), insertFailed);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void updateProviderItemNonMerchant(ProviderItemNonMerchantId providerItemNonMerchantId, ProviderItemNonMerchant providerItemNonMerchant) throws ProviderItemNonMerchantNotFoundException {
        try {
            providerItemNonMerchant.setId(providerItemNonMerchantId);
            ProviderItemNonMerchantDTO dto = mapper.mapProviderItemNonMerchantToDto(providerItemNonMerchant);
            service.updateProviderItemNonMerchant(dto);
        } catch (RegisterNotFound registerNotFound) {
            throw new ProviderItemNonMerchantNotFoundException(registerNotFound.getMessage(), registerNotFound);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }
}
