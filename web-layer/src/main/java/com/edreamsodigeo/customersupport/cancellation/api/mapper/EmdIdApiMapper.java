package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.v1.model.EmdId;

public class EmdIdApiMapper {
    public EmdId map(Long id) {
        EmdId emdId = new EmdId();
        emdId.setId(id);
        return emdId;
    }
}
