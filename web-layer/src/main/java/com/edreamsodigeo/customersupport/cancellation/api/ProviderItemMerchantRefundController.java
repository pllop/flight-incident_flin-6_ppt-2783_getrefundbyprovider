package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantRefundService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.ProviderItemMerchantApiMapper;
import com.edreamsodigeo.customersupport.cancellation.v1.ProviderItemMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.ProviderItemMerchantNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemMerchant;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemMerchantId;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;

@Singleton
public class ProviderItemMerchantRefundController implements ProviderItemMerchantRefund {

    private final ProviderItemMerchantApiMapper mapper;
    private final ProviderItemMerchantRefundService service;

    @Inject
    public ProviderItemMerchantRefundController(ProviderItemMerchantApiMapper providerItemMerchantApiMapper, ProviderItemMerchantRefundService providerItemMerchantRefundService) {
        this.mapper = providerItemMerchantApiMapper;
        this.service = providerItemMerchantRefundService;
    }

    @Override
    public ProviderItemMerchant getProviderItemMerchant(ProviderItemMerchantId id) throws ProviderItemMerchantNotFoundException {
        try {
            return mapper.mapDtoToProviderItemMerchant(service.getProviderItemMerchant(id.getId()));
        } catch (RegisterNotFound registerNotFound) {
            throw new ProviderItemMerchantNotFoundException(registerNotFound.getMessage(), registerNotFound);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }

    public ProviderItemMerchant getProviderItemMerchantByProviderItemId(ProviderItemId providerItemId) throws ProviderItemMerchantNotFoundException {
        try {
            return mapper.mapDtoToProviderItemMerchant(service.getProviderItemMerchantByProviderItem(providerItemId.getId()));
        } catch (RegisterNotFound registerNotFound) {
            throw new ProviderItemMerchantNotFoundException(registerNotFound.getMessage(), registerNotFound);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public ProviderItemMerchant addProviderItemMerchant(ProviderItemMerchant providerItemMerchant) throws DataBaseException {
        try {
            ProviderItemMerchantDTO itemToCancel = mapper.mapProviderItemMerchantToDto(providerItemMerchant);
            ProviderItemMerchantDTO result = service.addProviderItemMerchant(itemToCancel);
            return mapper.mapDtoToProviderItemMerchant(result);
        } catch (PersistenceException | EJBException insertFailed) {
            throw new DataBaseException(insertFailed.getMessage(), insertFailed);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void updateProviderItemMerchant(ProviderItemMerchantId id, ProviderItemMerchant providerItemMerchant) throws ProviderItemMerchantNotFoundException {
        try {
            providerItemMerchant.setId(id);
            ProviderItemMerchantDTO dto = mapper.mapProviderItemMerchantToDto(providerItemMerchant);
            service.updateProviderItemMerchant(dto);
        } catch (RegisterNotFound registerNotFound) {
            throw new ProviderItemMerchantNotFoundException(registerNotFound.getMessage(), registerNotFound);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }
}
