package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemDiscountDTO;
import com.edreamsodigeo.customersupport.cancellation.v1.model.DiscountType;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscount;
import com.google.inject.Inject;

import java.util.Objects;

public class ProviderItemDiscountApiMapper {

    private final ProviderItemIdApiMapper providerItemIdApiMapper;
    private final ProviderItemDiscountIdApiMapper providerItemDiscountIdApiMapper;
    private final FlexibleTicketIdApiMapper flexibleTicketIdApiMapper;
    private final VoucherIdApiMapper voucherIdApiMapper;
    private final EmdIdApiMapper emdIdApiMapper;

    @Inject
    public ProviderItemDiscountApiMapper(ProviderItemIdApiMapper providerItemIdApiMapper, ProviderItemDiscountIdApiMapper providerItemDiscountIdApiMapper, FlexibleTicketIdApiMapper flexibleTicketIdApiMapper, VoucherIdApiMapper voucherIdApiMapper, EmdIdApiMapper emdIdApiMapper) {
        this.providerItemIdApiMapper = providerItemIdApiMapper;
        this.providerItemDiscountIdApiMapper = providerItemDiscountIdApiMapper;
        this.flexibleTicketIdApiMapper = flexibleTicketIdApiMapper;
        this.voucherIdApiMapper = voucherIdApiMapper;
        this.emdIdApiMapper = emdIdApiMapper;
    }

    public ProviderItemDiscountDTO map(ProviderItemDiscount providerItemDiscount) {
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        if (Objects.nonNull(providerItemDiscount.getId())) {
            providerItemDiscountDTO.setId(providerItemDiscount.getId().getId());
        }
        providerItemDiscountDTO.setProviderItemId(providerItemDiscount.getProviderItemId().getId());
        providerItemDiscountDTO.setDiscountType(providerItemDiscount.getDiscountType().name());
        if (Objects.nonNull(providerItemDiscount.getFlexibleTicketId())) {
            providerItemDiscountDTO.setFlexibleTicketId(providerItemDiscount.getFlexibleTicketId().getId());
        }
        if (Objects.nonNull(providerItemDiscount.getVoucherId())) {
            providerItemDiscountDTO.setVoucherId(providerItemDiscount.getVoucherId().getId());
        }
        if (Objects.nonNull(providerItemDiscount.getEmdId())) {
            providerItemDiscountDTO.setEmdId(providerItemDiscount.getEmdId().getId());
        }
        return providerItemDiscountDTO;
    }

    public ProviderItemDiscount map(ProviderItemDiscountDTO providerItemDiscountDTO) {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        providerItemDiscount.setId(providerItemDiscountIdApiMapper.map(providerItemDiscountDTO.getId()));
        providerItemDiscount.setProviderItemId(providerItemIdApiMapper.map(providerItemDiscountDTO.getProviderItemId()));
        providerItemDiscount.setDiscountType(DiscountType.valueOf(providerItemDiscountDTO.getDiscountType()));
        if (Objects.nonNull(providerItemDiscountDTO.getFlexibleTicketId())) {
            providerItemDiscount.setFlexibleTicketId(flexibleTicketIdApiMapper.map(providerItemDiscountDTO.getFlexibleTicketId()));
        }
        providerItemDiscount.setVoucherId(voucherIdApiMapper.map(providerItemDiscountDTO.getVoucherId()));
        providerItemDiscount.setEmdId(emdIdApiMapper.map(providerItemDiscountDTO.getEmdId()));
        return providerItemDiscount;
    }
}
