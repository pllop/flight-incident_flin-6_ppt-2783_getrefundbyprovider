package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.v1.model.FlexibleTicketId;

import java.util.UUID;

public class FlexibleTicketIdApiMapper {
    public FlexibleTicketId map(UUID id) {
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(id);
        return flexibleTicketId;
    }
}
