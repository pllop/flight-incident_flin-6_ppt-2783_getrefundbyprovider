package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Money;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemNonMerchant;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemNonMerchantId;

public class ProviderItemNonMerchantApiMapper {


    public ProviderItemNonMerchantDTO mapProviderItemNonMerchantToDto(ProviderItemNonMerchant cancelledItemNonMerchant) {
        ProviderItemNonMerchantDTO result = new ProviderItemNonMerchantDTO();
        result.setId(cancelledItemNonMerchant.getId().getId());
        result.setProviderItemId(cancelledItemNonMerchant.getProviderItemId().getId());
        result.setProviderRefundAmountExpected(new MoneyDTO(
                cancelledItemNonMerchant.getProviderRefundAmountExpected().getAmount(),
                cancelledItemNonMerchant.getProviderRefundAmountExpected().getCurrency()));
        result.setRefunder(cancelledItemNonMerchant.getRefunder());
        return result;
    }

    public ProviderItemNonMerchant mapDtoToProviderItemNonMerchant(ProviderItemNonMerchantDTO dto) {
        ProviderItemNonMerchant result = new ProviderItemNonMerchant();
        ProviderItemNonMerchantId id = new ProviderItemNonMerchantId();
        id.setId(dto.getId());
        result.setId(id);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(dto.getProviderItemId());
        result.setProviderItemId(providerItemId);
        result.setProviderRefundAmountExpected(new Money(dto.getProviderRefundAmountExpected().getAmount(), dto.getProviderRefundAmountExpected().getCurrency()));
        result.setRefunder(dto.getRefunder());
        return result;
    }

}
