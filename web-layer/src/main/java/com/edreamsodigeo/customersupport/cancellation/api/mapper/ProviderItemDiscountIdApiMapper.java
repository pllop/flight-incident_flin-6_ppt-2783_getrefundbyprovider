package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscountId;

import java.util.UUID;

public class ProviderItemDiscountIdApiMapper {
    public ProviderItemDiscountId map(UUID id) {
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(id);
        return providerItemDiscountId;
    }
}
