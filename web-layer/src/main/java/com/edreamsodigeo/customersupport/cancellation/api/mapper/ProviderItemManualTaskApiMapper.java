package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskDTO;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemManualTaskId;

import java.util.Objects;

public class ProviderItemManualTaskApiMapper {

    public ProviderItemManualTaskDTO mapProviderItemManualTaskToDto(ProviderItemManualTask providerItemManualTask) {
        ProviderItemManualTaskDTO result = new ProviderItemManualTaskDTO();
        if (Objects.nonNull(providerItemManualTask.getId())) {
            result.setId(providerItemManualTask.getId().getId());
        }
        result.setProviderItemId(providerItemManualTask.getProviderItemId().getId());
        result.setTaskOnDemandId(providerItemManualTask.getTaskOnDemandId());
        return result;
    }

    public ProviderItemManualTask mapDtoToProviderItemManualTask(ProviderItemManualTaskDTO dto) {
        ProviderItemManualTask result = new ProviderItemManualTask();
        ProviderItemManualTaskId id = new ProviderItemManualTaskId();
        id.setId(dto.getId());
        result.setId(id);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(dto.getProviderItemId());
        result.setProviderItemId(providerItemId);
        result.setTaskOnDemandId(dto.getTaskOnDemandId());
        return result;
    }

}
