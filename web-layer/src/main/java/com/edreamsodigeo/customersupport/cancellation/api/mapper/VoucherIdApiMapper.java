package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.v1.model.VoucherId;

public class VoucherIdApiMapper {
    public VoucherId map(Long id) {
        VoucherId voucherId = new VoucherId();
        voucherId.setId(id);
        return voucherId;
    }
}
