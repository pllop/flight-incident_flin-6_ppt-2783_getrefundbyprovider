package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;

import java.util.UUID;

public class ProviderItemIdApiMapper {
    public ProviderItemId map(UUID id) {
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(id);
        return providerItemId;
    }
}
