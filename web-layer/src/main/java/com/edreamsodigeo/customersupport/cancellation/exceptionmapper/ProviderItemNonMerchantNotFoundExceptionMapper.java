package com.edreamsodigeo.customersupport.cancellation.exceptionmapper;

import com.edreamsodigeo.customersupport.cancellation.v1.exception.ProviderItemMerchantNotFoundException;
import com.odigeo.commons.rest.error.SimpleExceptionBean;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ProviderItemNonMerchantNotFoundExceptionMapper implements ExceptionMapper<ProviderItemMerchantNotFoundException> {
    private static final boolean NOT_INCLUDE_CAUSE = false;

    @Override
    public Response toResponse(ProviderItemMerchantNotFoundException e) {
        return Response
                .status(Response.Status.NOT_FOUND)
                .type(MediaType.APPLICATION_JSON)
                .entity(new SimpleExceptionBean(e, NOT_INCLUDE_CAUSE))
                .build();
    }
}
