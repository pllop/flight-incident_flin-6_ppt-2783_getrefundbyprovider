package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.ProviderItemManualTaskApiMapper;
import com.edreamsodigeo.customersupport.cancellation.v1.ProviderItemManualTaskResource;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.ProviderItemManualTaskNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemManualTaskId;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;

@Singleton
public class ProviderItemManualTaskRestController implements ProviderItemManualTaskResource {

    private final ProviderItemManualTaskApiMapper mapper;
    private final ProviderItemManualTaskService service;

    @Inject
    public ProviderItemManualTaskRestController(ProviderItemManualTaskApiMapper mapper,
                                                ProviderItemManualTaskService service) {
        this.mapper = mapper;
        this.service = service;
    }

    @Override
    public ProviderItemManualTask getProviderItemManualTask(ProviderItemManualTaskId id) throws ProviderItemManualTaskNotFoundException {
        ProviderItemManualTask result;
        try {
            result = mapper.mapDtoToProviderItemManualTask(service.getProviderItemManualTask(id.getId()));
        } catch (RegisterNotFound registerNotFound) {
            throw new ProviderItemManualTaskNotFoundException(registerNotFound.getMessage(), registerNotFound);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public ProviderItemManualTask addProviderItemManualTask(ProviderItemManualTask providerItemManualTask) throws DataBaseException {
        ProviderItemManualTask result;
        try {
            ProviderItemManualTaskDTO dto = mapper.mapProviderItemManualTaskToDto(providerItemManualTask);
            result = mapper.mapDtoToProviderItemManualTask(service.addProviderItemManualTask(dto));
        } catch (PersistenceException | EJBException insertFailed) {
            throw new DataBaseException(insertFailed.getMessage(), insertFailed);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public void updateProviderItemManualTask(ProviderItemManualTaskId id, ProviderItemManualTask providerItemManualTask) throws ProviderItemManualTaskNotFoundException {
        try {
            ProviderItemManualTaskDTO dto = mapper.mapProviderItemManualTaskToDto(providerItemManualTask);
            service.updateProviderItemManualTask(id.getId(), dto);
        } catch (RegisterNotFound registerNotFound) {
            throw new ProviderItemManualTaskNotFoundException(registerNotFound.getMessage(), registerNotFound);
        } catch (Throwable e) {
            e.printStackTrace();
            throw e;
        }
    }
}
