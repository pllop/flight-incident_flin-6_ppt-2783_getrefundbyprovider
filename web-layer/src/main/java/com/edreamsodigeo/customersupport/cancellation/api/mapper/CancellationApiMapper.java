package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.CancellationDTO;
import com.edreamsodigeo.customersupport.cancellation.ItemDTO;
import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemDTO;
import com.edreamsodigeo.customersupport.cancellation.v1.model.BookingItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Item;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Money;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderBookingItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.UserId;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CancellationApiMapper {

    public CancellationDTO mapCancellationToCancellationDTO(Cancellation cancellation) {
        CancellationDTO cancellationDTO = new CancellationDTO();

        if (Objects.nonNull(cancellation.getId())) {
            cancellationDTO.setId(cancellation.getId().getId());
        }
        cancellationDTO.setIncidentId(cancellation.getIncidentId().getId());
        cancellationDTO.setDiscarded(cancellation.getDiscarded());
        if (Objects.nonNull(cancellation.getUserId())) {
            cancellationDTO.setUserId(cancellation.getUserId().getId());
        }
        cancellationDTO.setBuyerEmail(cancellation.getBuyerEmail());
        cancellationDTO.setTimestamp(cancellation.getTimestamp());
        cancellationDTO.setItems(mapItemsToItemsDTO(cancellation.getItems()));
        return cancellationDTO;
    }

    private List<ItemDTO> mapItemsToItemsDTO(List<Item> cancelledItems) {
        return cancelledItems.stream().map(this::mapItemToItemDTO).collect(Collectors.toList());
    }

    private ItemDTO mapItemToItemDTO(Item cancelledItem) {
        ItemDTO itemDTO = new ItemDTO();

        if (Objects.nonNull(cancelledItem.getId())) {
            itemDTO.setId(cancelledItem.getId().getId());
        }
        itemDTO.setBookingItemId(cancelledItem.getBookingItemId().getId());
        itemDTO.setProviderItems(mapProviderItemsToProvierItemsDTO(cancelledItem.getProviderItems()));
        return itemDTO;
    }

    private List<ProviderItemDTO> mapProviderItemsToProvierItemsDTO(List<ProviderItem> providerItems) {
        return providerItems.stream().map(this::mapProviderItemToProviderItemDTO).collect(Collectors.toList());
    }

    private ProviderItemDTO mapProviderItemToProviderItemDTO(ProviderItem providerItem) {
        ProviderItemDTO providerItemDTO = new ProviderItemDTO();

        if (Objects.nonNull(providerItem.getId())) {
            providerItemDTO.setId(providerItem.getId().getId());
        }
        providerItemDTO.setProviderBookingItemId(providerItem.getProviderBookingItemId().getId());
        providerItemDTO.setResolutionType(providerItem.getResolutionType());
        if (Objects.nonNull(providerItem.getTopUpVoucherAmount())) {
            providerItemDTO.setTopUpVoucherAmount(mapMoneyToMoneyDTO(providerItem.getTopUpVoucherAmount()));
        }
        if (Objects.nonNull(providerItem.getTopUpVoucherMinBasketAmount())) {
            providerItemDTO.setTopUpVoucherMinBasketAmount(mapMoneyToMoneyDTO(providerItem.getTopUpVoucherMinBasketAmount()));
        }
        return providerItemDTO;
    }

    private MoneyDTO mapMoneyToMoneyDTO(Money topUpVoucherAmount) {
        return new MoneyDTO(topUpVoucherAmount.getAmount(), topUpVoucherAmount.getCurrency());
    }

    public Cancellation mapCancellationDTOToCancellation(CancellationDTO cancellationDTO) {
        Cancellation cancellation = new Cancellation();

        cancellation.setId(new CancellationId(cancellationDTO.getId().toString()));
        cancellation.setIncidentId(new IncidentId(cancellationDTO.getIncidentId().toString()));
        cancellation.setDiscarded(cancellationDTO.isDiscarded());
        cancellation.setUserId(new UserId(cancellationDTO.getUserId()));
        cancellation.setBuyerEmail(cancellationDTO.getBuyerEmail());
        cancellation.setTimestamp(cancellationDTO.getTimestamp());
        cancellation.setItems(mapItemsDTOToItems(cancellation.getId().getAsString(), cancellationDTO.getItems()));
        return cancellation;
    }

    private List<Item> mapItemsDTOToItems(String cancellationId, List<ItemDTO> itemsDTO) {
        return itemsDTO.stream().map(itemDTO -> mapItemDTOToItem(cancellationId, itemDTO)).collect(Collectors.toList());
    }

    private Item mapItemDTOToItem(String cancellationId, ItemDTO itemDTO) {
        Item item = new Item();

        item.setId(new ItemId(itemDTO.getId().toString()));
        item.setCancellationId(new CancellationId(cancellationId));
        item.setBookingItemId(new BookingItemId(String.valueOf(itemDTO.getBookingItemId())));
        item.setProviderItems(mapProviderItemsDTOToProviderItems(item.getId().getAsString(), itemDTO.getProviderItems()));
        return item;
    }

    private List<ProviderItem> mapProviderItemsDTOToProviderItems(String itemId, List<ProviderItemDTO> providerItems) {
        return providerItems.stream().map(providerItemDTO -> mapProviderItemDTOToProviderItem(itemId, providerItemDTO)).collect(Collectors.toList());
    }

    private ProviderItem mapProviderItemDTOToProviderItem(String itemId, ProviderItemDTO providerItemDTO) {
        ProviderItem providerItem = new ProviderItem();

        providerItem.setId(new ProviderItemId(providerItemDTO.getId().toString()));
        providerItem.setItemId(new ItemId(itemId));
        providerItem.setProviderBookingItemId(new ProviderBookingItemId(String.valueOf(providerItemDTO.getProviderBookingItemId())));
        providerItem.setResolutionType(providerItemDTO.getResolutionType());
        if (Objects.nonNull(providerItemDTO.getTopUpVoucherAmount())) {
            providerItem.setTopUpVoucherAmount(mapMoneyDTOToMoney(providerItemDTO.getTopUpVoucherAmount()));
        }
        if (Objects.nonNull(providerItemDTO.getTopUpVoucherMinBasketAmount())) {
            providerItem.setTopUpVoucherMinBasketAmount(mapMoneyDTOToMoney(providerItemDTO.getTopUpVoucherMinBasketAmount()));
        }
        return providerItem;
    }

    private Money mapMoneyDTOToMoney(MoneyDTO moneyDTO) {
        return new Money(moneyDTO.getAmount(), moneyDTO.getCurrency());
    }
}
