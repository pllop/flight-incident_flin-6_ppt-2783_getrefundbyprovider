package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.CancellationDTO;
import com.edreamsodigeo.customersupport.cancellation.CancellationService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.CancellationApiMapper;
import com.edreamsodigeo.customersupport.cancellation.v1.CancellationResource;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.CancellationNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class CancellationRestController implements CancellationResource {

    private final CancellationService cancellationService;
    private final CancellationApiMapper cancellationApiMapper;

    @Inject
    public CancellationRestController(CancellationService cancellationService, CancellationApiMapper cancellationApiMapper) {
        this.cancellationService = cancellationService;
        this.cancellationApiMapper = cancellationApiMapper;
    }

    @Override
    public Cancellation getCancellation(CancellationId id) throws CancellationNotFoundException {
        Cancellation cancellation;
        try {
            cancellation = cancellationApiMapper.mapCancellationDTOToCancellation(
                    cancellationService.getCancellation(id.getId())
            );
        } catch (RegisterNotFound registerNotFound) {
            throw new CancellationNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
        return cancellation;
    }

    @Override
    public List<Cancellation> getCancellationsByIncident(IncidentId incidentId) throws CancellationNotFoundException {
        List<Cancellation> result;
        try {
            List<CancellationDTO> cancellationsByIncident = cancellationService.getCancellationsByIncident(incidentId.getId());
            result = cancellationsByIncident.stream().map(cancellationApiMapper::mapCancellationDTOToCancellation).collect(Collectors.toList());
        } catch (RegisterNotFound registerNotFound) {
            throw new CancellationNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
        return result;
    }

    @Override
    public Cancellation addCancellation(Cancellation cancellation) throws DataBaseException {
        Cancellation cancellationAdded;
        try {
            cancellationAdded = cancellationApiMapper.mapCancellationDTOToCancellation(
                    cancellationService.addCancellation(cancellationApiMapper.mapCancellationToCancellationDTO(cancellation))
            );
        } catch (PersistenceException | EJBException insertFailed) {
            throw new DataBaseException(insertFailed.getMessage(), insertFailed);
        }
        return cancellationAdded;
    }


    @Override
    public void updateCancellation(CancellationId id, Cancellation cancellation) throws CancellationNotFoundException {
        try {
            cancellation.setId(id);
            cancellationService.updateCancellation(cancellationApiMapper.mapCancellationToCancellationDTO(cancellation));
        } catch (RegisterNotFound registerNotFound) {
            throw new CancellationNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
    }


    @Override
    public void discardCancellation(CancellationId cancellationId) throws CancellationNotFoundException {
        try {
            cancellationService.discardCancellation(cancellationId.getId());
        } catch (RegisterNotFound registerNotFound) {
            throw new CancellationNotFoundException(registerNotFound.getMessage(), registerNotFound);
        }
    }
}
