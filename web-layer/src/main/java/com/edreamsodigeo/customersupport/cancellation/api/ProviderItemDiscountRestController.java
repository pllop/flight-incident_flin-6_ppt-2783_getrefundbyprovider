package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemDiscountService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.ProviderItemDiscountApiMapper;
import com.edreamsodigeo.customersupport.cancellation.api.verifier.ProviderItemDiscountVerifier;
import com.edreamsodigeo.customersupport.cancellation.v1.ProviderItemDiscountResource;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscountId;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;

@Singleton
public class ProviderItemDiscountRestController implements ProviderItemDiscountResource {
    private final ProviderItemDiscountApiMapper providerItemDiscountApiMapper;
    private final ProviderItemDiscountService providerItemDiscountService;
    private final ProviderItemDiscountVerifier providerItemDiscountVerifier;

    @Inject
    public ProviderItemDiscountRestController(ProviderItemDiscountApiMapper providerItemDiscountApiMapper, ProviderItemDiscountService providerItemDiscountService, ProviderItemDiscountVerifier providerItemDiscountVerifier) {
        this.providerItemDiscountApiMapper = providerItemDiscountApiMapper;
        this.providerItemDiscountService = providerItemDiscountService;
        this.providerItemDiscountVerifier = providerItemDiscountVerifier;
    }

    @Override
    public ProviderItemDiscount addProviderItemDiscount(ProviderItemDiscount providerItemDiscount) throws DataBaseException {
        try {
            providerItemDiscountVerifier.verify(providerItemDiscount);
            return providerItemDiscountApiMapper.map(
                    providerItemDiscountService.addproviderItemDiscount(providerItemDiscountApiMapper.map(providerItemDiscount))
            );
        } catch (PersistenceException | EJBException | IllegalArgumentException insertFailed) {
            throw new DataBaseException(insertFailed.getMessage(), insertFailed);
        }
    }

    @Override
    public ProviderItemDiscount getProviderItemDiscount(ProviderItemDiscountId providerItemDiscountId) throws DataBaseException {
        return providerItemDiscountApiMapper.map(
                providerItemDiscountService.getProviderItemDiscount(providerItemDiscountId.getId())
        );
    }
}
