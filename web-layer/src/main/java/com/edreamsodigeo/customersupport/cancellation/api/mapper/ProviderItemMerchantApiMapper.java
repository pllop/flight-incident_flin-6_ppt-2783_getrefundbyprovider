package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.model.RefundMethod;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Money;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemMerchant;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemMerchantId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.RefundId;

public class ProviderItemMerchantApiMapper {


    public ProviderItemMerchantDTO mapProviderItemMerchantToDto(ProviderItemMerchant providerItemMerchant) {
        ProviderItemMerchantDTO result = new ProviderItemMerchantDTO();
        result.setId(providerItemMerchant.getId().getId());
        result.setProviderItemId(providerItemMerchant.getProviderItemId().getId());
        result.setCustomerRefundAmount(new MoneyDTO(
                providerItemMerchant.getCustomerRefundAmount().getAmount(),
                providerItemMerchant.getCustomerRefundAmount().getCurrency()));
        result.setProviderRefundAmountExpected(new MoneyDTO(
                providerItemMerchant.getProviderRefundAmountExpected().getAmount(),
                providerItemMerchant.getProviderRefundAmountExpected().getCurrency()));
        result.setCustomerRefundMethod(RefundMethod.valueOf(providerItemMerchant.getCustomerRefundMethod().name()));
        result.setRefundId(providerItemMerchant.getRefundId().getId());
        return result;
    }

    public ProviderItemMerchant mapDtoToProviderItemMerchant(ProviderItemMerchantDTO dto) {
        ProviderItemMerchant result = new ProviderItemMerchant();
        ProviderItemMerchantId id = new ProviderItemMerchantId();
        id.setId(dto.getId());
        result.setId(id);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(dto.getProviderItemId());
        result.setProviderItemId(providerItemId);
        result.setCustomerRefundAmount(new Money(dto.getCustomerRefundAmount().getAmount(), dto.getCustomerRefundAmount().getCurrency()));
        result.setProviderRefundAmountExpected(new Money(dto.getProviderRefundAmountExpected().getAmount(), dto.getProviderRefundAmountExpected().getCurrency()));
        result.setCustomerRefundMethod(com.edreamsodigeo.customersupport.cancellation.v1.model.RefundMethod.valueOf(dto.getCustomerRefundMethod().name()));
        RefundId refundId = new RefundId();
        refundId.setId(dto.getRefundId());
        result.setRefundId(refundId);
        return result;
    }

}
