package com.edreamsodigeo.customersupport.refund;

import com.edreamsodigeo.customersupport.refund.mapper.RefundApiMapper;
import com.edreamsodigeo.customersupport.refund.v1.RefundResource;
import com.edreamsodigeo.customersupport.refund.v1.model.Refund;
import com.edreamsodigeo.customersupport.refund.v1.model.RefundId;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class RefundController implements RefundResource {

    private RefundService refundService;
    private RefundApiMapper refundApiMapper;

    @Inject
    public RefundController(RefundService refundService, RefundApiMapper refundApiMapper) {
        this.refundService = refundService;
        this.refundApiMapper = refundApiMapper;
    }

    @Override
    public Refund getRefund(RefundId id) {
        return refundApiMapper.mapRefundDTOToRefund(refundService.getRefundById(id.getId()));
    }

    @Override
    public Refund addRefund(Refund refund) {
        return refundApiMapper.mapRefundDTOToRefund(
                refundService.addRefund(refundApiMapper.mapRefundToRefundDTO(refund)));
    }

    @Override
    public void updateRefund(Refund refund) {
        refundService.updateRefund(refundApiMapper.mapRefundToRefundDTO(refund));
    }
}
