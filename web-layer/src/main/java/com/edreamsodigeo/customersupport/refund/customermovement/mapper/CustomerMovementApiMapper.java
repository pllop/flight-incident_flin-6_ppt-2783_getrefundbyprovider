package com.edreamsodigeo.customersupport.refund.customermovement.mapper;

import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementDTO;
import com.edreamsodigeo.customersupport.refund.customermovement.MovementMoneyDTO;
import com.edreamsodigeo.customersupport.refund.v1.model.Money;
import com.edreamsodigeo.customersupport.refund.v1.model.RefundId;
import com.edreamsodigeo.customersupport.refund.v1.model.request.CustomerMovement;
import com.edreamsodigeo.customersupport.refund.v1.model.request.CustomerMovementId;
import com.edreamsodigeo.customersupport.refund.v1.model.request.CustomerMovementWavePspId;
import com.odigeo.commons.uuid.UUIDGenerator;

import java.util.Objects;

public class CustomerMovementApiMapper {

    public CustomerMovementDTO mapCustomerMovementToCustomerMovementDTO(CustomerMovement customerMovement) {
        CustomerMovementDTO customerMovementDTO = new CustomerMovementDTO();

        if (Objects.isNull(customerMovement.getId())) {
            customerMovementDTO.setId(UUIDGenerator.getInstance().generateUUID());
        }
        if (Objects.nonNull(customerMovement.getRefundId())) {
            customerMovementDTO.setRefundId(customerMovement.getRefundId().getId());
        }
        customerMovementDTO.setAmount(mapMoneyToMoneyDTO(customerMovement.getAmount()));
        customerMovementDTO.setCurrencyCode(customerMovement.getAmount().getCurrency());
        customerMovementDTO.setCurrencyRateProviderCustomer(customerMovement.getCurrencyRateProviderCustomer());
        customerMovementDTO.setAmountInEur(mapMoneyToMoneyDTO(customerMovement.getAmountInEur()));
        customerMovementDTO.setCurrencyRateProviderEur(customerMovement.getCurrencyRateProviderEur());
        customerMovementDTO.setPsp(customerMovement.getPsp());
        customerMovementDTO.setPspAccount(customerMovement.getPspAccount());
        customerMovementDTO.setStatus(customerMovement.getStatus());
        if (Objects.isNull(customerMovement.getCustomerMovementWavePspId())) {
            customerMovementDTO.setCustomerMovementWavePspId(UUIDGenerator.getInstance().generateUUID());
        }
        return customerMovementDTO;
    }

    public CustomerMovement mapCustomerMovementDTOToCustomerMovement(CustomerMovementDTO customerMovementDTO) {
        CustomerMovement customerMovement = new CustomerMovement();
        customerMovement.setId(new CustomerMovementId(customerMovementDTO.getId().toString()));
        customerMovement.setRefundId(new RefundId(customerMovementDTO.getRefundId().toString()));
        customerMovement.setAmount(mapMoneyDTOToMoney(customerMovementDTO.getAmount()));
        customerMovement.setCurrencyRateProviderCustomer(customerMovementDTO.getCurrencyRateProviderCustomer());
        customerMovement.setAmountInEur(mapMoneyDTOToMoney(customerMovementDTO.getAmountInEur()));
        customerMovement.setCurrencyRateProviderEur(customerMovementDTO.getCurrencyRateProviderEur());
        customerMovement.setPsp(customerMovementDTO.getPsp());
        customerMovement.setPspAccount(customerMovementDTO.getPspAccount());
        customerMovement.setStatus(customerMovementDTO.getStatus());
        customerMovement.setCustomerMovementWavePspId(new CustomerMovementWavePspId(customerMovementDTO.getCustomerMovementWavePspId().toString()));
        return customerMovement;
    }

    private MovementMoneyDTO mapMoneyToMoneyDTO(Money amount) {
        return new MovementMoneyDTO(amount.getAmount(), amount.getCurrency());
    }

    private Money mapMoneyDTOToMoney(MovementMoneyDTO movementMoneyDTO) {
        return new Money(movementMoneyDTO.getAmount(), movementMoneyDTO.getCurrency());
    }
}
