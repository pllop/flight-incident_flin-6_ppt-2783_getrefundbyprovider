package com.edreamsodigeo.customersupport.refund.customermovement.exceptionmapper;

import com.edreamsodigeo.customersupport.refund.v1.exception.CustomerMovementResourceException;
import com.odigeo.commons.rest.error.SimpleExceptionBean;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class CustomerMovementResourceExceptionMapper implements ExceptionMapper<CustomerMovementResourceException> {
    private static final boolean NOT_INCLUDE_CAUSE = false;

    @Override
    public Response toResponse(CustomerMovementResourceException e) {
        return Response
                .status(Response.Status.NOT_FOUND)
                .type(MediaType.APPLICATION_JSON)
                .entity(new SimpleExceptionBean(e, NOT_INCLUDE_CAUSE))
                .build();
    }
}
