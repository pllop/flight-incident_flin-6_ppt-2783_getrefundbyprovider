package com.edreamsodigeo.customersupport.refund.customermovement.api;

import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementDTO;
import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementService;
import com.edreamsodigeo.customersupport.refund.customermovement.mapper.CustomerMovementApiMapper;
import com.edreamsodigeo.customersupport.refund.v1.CustomerMovementResource;
import com.edreamsodigeo.customersupport.refund.v1.exception.CustomerMovementResourceException;
import com.edreamsodigeo.customersupport.refund.v1.model.request.CustomerMovement;
import com.edreamsodigeo.customersupport.refund.v1.model.request.CustomerMovementId;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class CustomerMovementRestController implements CustomerMovementResource {
    private final CustomerMovementService customerMovementService;
    private final CustomerMovementApiMapper customerMovementApiMapper;

    @Inject
    public CustomerMovementRestController(CustomerMovementService customerMovementService, CustomerMovementApiMapper customerMovementApiMapper) {
        this.customerMovementService = customerMovementService;
        this.customerMovementApiMapper = customerMovementApiMapper;
    }

    @Override
    public CustomerMovementId addCustomerMovement(CustomerMovement customerMovement) throws CustomerMovementResourceException {
        try {
            CustomerMovementDTO customerMovementDTO = customerMovementApiMapper.mapCustomerMovementToCustomerMovementDTO(customerMovement);
            return new CustomerMovementId(customerMovementService.createCustomerMovement(customerMovementDTO).toString());
        } catch (RegisterNotFound registerNotFound) {
            throw new CustomerMovementResourceException(registerNotFound.getMessage(), registerNotFound.getCause());
        }
    }
}


