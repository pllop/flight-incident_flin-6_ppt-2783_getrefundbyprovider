package com.edreamsodigeo.customersupport.refund.mapper;

import com.edreamsodigeo.customersupport.refund.ProviderRefundDTO;
import com.edreamsodigeo.customersupport.refund.RefundDTO;
import com.edreamsodigeo.customersupport.refund.v1.model.BookingItemId;
import com.edreamsodigeo.customersupport.refund.v1.model.Money;
import com.edreamsodigeo.customersupport.refund.v1.model.ProviderBookingItemId;
import com.edreamsodigeo.customersupport.refund.v1.model.ProviderRefund;
import com.edreamsodigeo.customersupport.refund.v1.model.ProviderRefundId;
import com.edreamsodigeo.customersupport.refund.v1.model.Refund;
import com.edreamsodigeo.customersupport.refund.v1.model.RefundId;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class RefundApiMapper {

    public Refund mapRefundDTOToRefund(RefundDTO refundDTO) {

        Refund refund = new Refund();
        if (refundDTO == null) {
            return refund;
        }

        if (refundDTO.getId() != null) {
            refund.setId(new RefundId(refundDTO.getId().toString()));
        }

        if (refundDTO.getProviderRefunds() != null) {
            refund.setProviderRefunds(mapProviderRefundsDTOToProviderRefunds(refundDTO.getProviderRefunds()));
        }

        refund.setCustomerRefundMethod(refundDTO.getCustomerRefundMethod());

        if (refundDTO.getCustomerRefundAmount() != null) {
            refund.setCustomerRefundAmount(new Money(new BigDecimal(refundDTO.getCustomerRefundAmount()), refundDTO.getCustomerRefundCurrency()));
        }

        BookingItemId bookingItemId = new BookingItemId();
        bookingItemId.setId(refundDTO.getBookingItemId());
        refund.setBookingItemId(bookingItemId);

        refund.setStatus(refundDTO.getStatus());

        return refund;
    }

    public RefundDTO mapRefundToRefundDTO(Refund refund) {
        RefundDTO refundDTO = new RefundDTO();

        if (refund == null) {
            return refundDTO;
        }

        if (refund.getId() != null) {
            refundDTO.setId(refund.getId().getId());
        }

        if (refund.getProviderRefunds() != null) {
            refundDTO.setProviderRefunds(mapProviderRefundsToProviderRefundsDTO(refund.getProviderRefunds()));
        }

        refundDTO.setCustomerRefundMethod(refund.getCustomerRefundMethod());

        if (refund.getCustomerRefundAmount() != null) {
            refundDTO.setCustomerRefundAmount(refund.getCustomerRefundAmount().getAmount().doubleValue());
            refundDTO.setCustomerRefundCurrency(refund.getCustomerRefundAmount().getCurrency());
        }

        if (refund.getBookingItemId() != null) {
            refundDTO.setBookingItemId(refund.getBookingItemId().getId());
        }

        refundDTO.setStatus(refund.getStatus());

        return refundDTO;
    }

    private List<ProviderRefundDTO> mapProviderRefundsToProviderRefundsDTO(List<ProviderRefund> providerRefunds) {
        return providerRefunds.stream().map(this::mapProviderRefundToProviderRefundDTO).collect(Collectors.toList());
    }

    private ProviderRefundDTO mapProviderRefundToProviderRefundDTO(ProviderRefund providerRefund) {
        ProviderRefundDTO providerRefundDTO = new ProviderRefundDTO();
        providerRefundDTO.setId(providerRefund.getId().getId());

        if (providerRefund.getProviderBookingItemId() != null) {
            providerRefundDTO.setProviderBookingItemId(providerRefund.getProviderBookingItemId().getId());
        }

        if (providerRefund.getExpectedProviderRefundAmount() != null) {
            providerRefundDTO.setExpectedProviderRefundAmount(providerRefund.getExpectedProviderRefundAmount().getAmount().doubleValue());
            providerRefundDTO.setExpectedProviderRefundCurrency(providerRefund.getExpectedProviderRefundAmount().getCurrency());
        }

        return providerRefundDTO;
    }

    private List<ProviderRefund> mapProviderRefundsDTOToProviderRefunds(List<ProviderRefundDTO> providerRefundDTOS) {
        return providerRefundDTOS.stream().map(this::mapProviderRefundDTOToProviderRefund).collect(Collectors.toList());
    }

    private ProviderRefund mapProviderRefundDTOToProviderRefund(ProviderRefundDTO providerRefundDTO) {
        ProviderRefund providerRefund = new ProviderRefund();

        if (providerRefundDTO.getId() != null) {
            providerRefund.setId(new ProviderRefundId(providerRefundDTO.getId().toString()));
        }

        if (providerRefundDTO.getProviderBookingItemId() != null) {
            providerRefund.setProviderBookingItemId(new ProviderBookingItemId(providerRefundDTO.getProviderBookingItemId()));
        }

        if (providerRefundDTO.getExpectedProviderRefundAmount() != null) {
            providerRefund.setExpectedProviderRefundAmount(new Money(new BigDecimal(providerRefundDTO.getExpectedProviderRefundAmount()), providerRefundDTO.getExpectedProviderRefundCurrency()));
        }

        return providerRefund;
    }
}
