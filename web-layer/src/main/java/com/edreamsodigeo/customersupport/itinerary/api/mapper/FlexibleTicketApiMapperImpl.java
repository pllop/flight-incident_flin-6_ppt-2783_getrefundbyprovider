package com.edreamsodigeo.customersupport.itinerary.api.mapper;

import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketDTO;
import com.edreamsodigeo.customersupport.itinerary.MoneyDTO;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.itinerary.v1.model.Money;
import com.edreamsodigeo.customersupport.itinerary.v1.model.Status;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class FlexibleTicketApiMapperImpl implements FlexibleTicketApiMapper {

    @Override
    public FlexibleTicketDTO mapFlexibleTicketToFlexibleTicketDTO(@NotNull FlexibleTicket flexibleTicket) {
        Objects.requireNonNull(flexibleTicket);
        Objects.requireNonNull(flexibleTicket.getId());
        return FlexibleTicketDTO.builder()
                .id(flexibleTicket.getId().getId())
                .customerId(flexibleTicket.getCustomerId())
                .providerBookingItemId(flexibleTicket.getProviderBookingItemId())
                .status(mapEnumToString(flexibleTicket.getStatus()))
                .monetaryValue(mapMoneyToMoneyDTO(flexibleTicket.getMonetaryValue()))
                .build();
    }

    @Override
    public FlexibleTicket mapFlexibleTicketDTOToFlexibleTicket(@NotNull FlexibleTicketDTO flexibleTicketDTO) {
        Objects.requireNonNull(flexibleTicketDTO);
        FlexibleTicket flexibleTicket = new FlexibleTicket(new FlexibleTicketId(flexibleTicketDTO.getId()));
        flexibleTicket.setCustomerId(flexibleTicketDTO.getCustomerId());
        flexibleTicket.setProviderBookingItemId(flexibleTicketDTO.getProviderBookingItemId());
        flexibleTicket.setStatus(mapStringToStatus(flexibleTicketDTO.getStatus()));
        flexibleTicket.setMonetaryValue(mapMoneyDTOToMoney(flexibleTicketDTO.getMonetaryValue()));
        return flexibleTicket;
    }

    private String mapEnumToString(Enum<?> status) {
        if (Objects.isNull(status)) {
            return null;
        }
        return status.toString();
    }

    private MoneyDTO mapMoneyToMoneyDTO(Money money) {
        if (Objects.isNull(money)) {
            return null;
        }
        return new MoneyDTO(money.getAmount(), money.getCurrencyCode());
    }

    private Status mapStringToStatus(String status) {
        if (Objects.isNull(status)) {
            return null;
        }
        return Status.valueOf(status);
    }

    private Money mapMoneyDTOToMoney(MoneyDTO moneyDTO) {
        if (Objects.isNull(moneyDTO) || Objects.isNull(moneyDTO.getAmount()) || Objects.isNull(moneyDTO.getCurrencyCode())) {
            return null;
        }
        return new Money(moneyDTO.getAmount(), moneyDTO.getCurrencyCode());
    }
}
