package com.edreamsodigeo.customersupport.itinerary.api.mapper;

import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketDTO;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicket;
import com.google.inject.ImplementedBy;

@ImplementedBy(FlexibleTicketApiMapperImpl.class)
public interface FlexibleTicketApiMapper {

    FlexibleTicketDTO mapFlexibleTicketToFlexibleTicketDTO(FlexibleTicket flexibleTicket);

    FlexibleTicket mapFlexibleTicketDTOToFlexibleTicket(FlexibleTicketDTO flexibleTicketDTO);

}
