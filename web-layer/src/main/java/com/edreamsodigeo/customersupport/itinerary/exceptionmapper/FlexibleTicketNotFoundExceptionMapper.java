package com.edreamsodigeo.customersupport.itinerary.exceptionmapper;

import com.edreamsodigeo.customersupport.itinerary.v1.exception.FlexibleTicketNotFoundException;
import com.odigeo.commons.rest.error.SimpleExceptionBean;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class FlexibleTicketNotFoundExceptionMapper implements ExceptionMapper<FlexibleTicketNotFoundException> {
    private static final boolean NOT_INCLUDE_CAUSE = false;

    @Override
    public Response toResponse(FlexibleTicketNotFoundException e) {
        return Response
                .status(Response.Status.NOT_FOUND)
                .type(MediaType.APPLICATION_JSON)
                .entity(new SimpleExceptionBean(e, NOT_INCLUDE_CAUSE))
                .build();
    }
}
