package com.edreamsodigeo.customersupport.itinerary.api;

import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketDTO;
import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketService;
import com.edreamsodigeo.customersupport.itinerary.api.mapper.FlexibleTicketApiMapper;
import com.edreamsodigeo.customersupport.itinerary.exception.RegisterNotFoundException;
import com.edreamsodigeo.customersupport.itinerary.v1.FlexibleTicketResource;
import com.edreamsodigeo.customersupport.itinerary.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.itinerary.v1.exception.FlexibleTicketNotFoundException;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicketId;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;

@Singleton
public class FlexibleTicketRestController implements FlexibleTicketResource {

    private final FlexibleTicketService service;
    private final FlexibleTicketApiMapper mapper;

    @Inject
    public FlexibleTicketRestController(FlexibleTicketService service, FlexibleTicketApiMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Override
    public FlexibleTicket getFlexibleTicket(FlexibleTicketId flexibleTicketId) throws FlexibleTicketNotFoundException {
        try {
            final FlexibleTicketDTO flexibleTicketDTO = service.getFlexibleTicket(flexibleTicketId.getId());
            return mapper.mapFlexibleTicketDTOToFlexibleTicket(flexibleTicketDTO);
        } catch (RegisterNotFoundException exception) {
            throw new FlexibleTicketNotFoundException(exception.getMessage(), exception);
        }
    }

    @Override
    public void addFlexibleTicket(FlexibleTicket flexibleTicket) throws DataBaseException {
        try {
            service.addFlexibleTicket(
                    mapper.mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket));
        } catch (PersistenceException | EJBException insertFailed) {
            throw new DataBaseException(insertFailed.getMessage(), insertFailed);
        }
    }

    @Override
    public void updateFlexibleTicket(FlexibleTicket flexibleTicket) throws FlexibleTicketNotFoundException {
        try {
            service.updateFlexibleTicket(mapper.mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket));
        } catch (RegisterNotFoundException | EJBException exception) {
            throw new FlexibleTicketNotFoundException(exception.getMessage(), exception);
        }
    }
}
