package com.edreamsodigeo.customersupport.itinerary.api.mapper;

import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketDTO;
import com.edreamsodigeo.customersupport.itinerary.MoneyDTO;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.itinerary.v1.model.Money;
import com.edreamsodigeo.customersupport.itinerary.v1.model.Status;
import com.flextrade.jfixture.JFixture;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class FlexibleTicketApiMapperImplTest {

    private FlexibleTicketApiMapper mapper;
    private JFixture fixture = new JFixture();

    @BeforeMethod
    public  void reset() {
        mapper = new FlexibleTicketApiMapperImpl();
    }


    @Test
    public void testMapFlexibleTicketToFlexibleTicketDTO() {
        FlexibleTicket source = new FlexibleTicket(new FlexibleTicketId(UUID.randomUUID()));
        source.setProviderBookingItemId(fixture.create(Long.class));
        source.setCustomerId(fixture.create(Long.class));
        source.setStatus(fixture.create(Status.class));
        source.setMonetaryValue(new Money(fixture.create(BigDecimal.class), fixture.create(String.class)));

        final FlexibleTicketDTO actual = mapper.mapFlexibleTicketToFlexibleTicketDTO(source);

        assertNotNull(actual);
        assertEquals(actual.getId(), source.getId().getId());
        assertEquals(actual.getProviderBookingItemId(), source.getProviderBookingItemId());
        assertEquals(actual.getCustomerId(), source.getCustomerId());
        assertEquals(actual.getStatus(), source.getStatus().toString());
        assertEquals(actual.getMonetaryValue().getAmount(), source.getMonetaryValue().getAmount());
        assertEquals(actual.getMonetaryValue().getCurrencyCode(), source.getMonetaryValue().getCurrencyCode());
    }

    @Test
    public void testMapFlexibleTicketDTOToFlexibleTicket() {
        FlexibleTicketDTO source = FlexibleTicketDTO.builder()
                .id(UUID.randomUUID())
                .providerBookingItemId(fixture.create(Long.class))
                .customerId(fixture.create(Long.class))
                .status(fixture.create(Status.class).toString())
                .monetaryValue(fixture.create(MoneyDTO.class))
                .build();

        final FlexibleTicket actual = mapper.mapFlexibleTicketDTOToFlexibleTicket(source);

        assertNotNull(actual);
        assertEquals(actual.getId().getId(), source.getId());
        assertEquals(actual.getProviderBookingItemId(), source.getProviderBookingItemId());
        assertEquals(actual.getCustomerId(), source.getCustomerId());
        assertNotNull(actual.getMonetaryValue().getAmount());
        assertNotNull(actual.getMonetaryValue().getCurrencyCode());
        assertEquals(actual.getMonetaryValue().getAmount(), source.getMonetaryValue().getAmount());
        assertEquals(actual.getMonetaryValue().getCurrencyCode(), source.getMonetaryValue().getCurrencyCode());
    }
}
