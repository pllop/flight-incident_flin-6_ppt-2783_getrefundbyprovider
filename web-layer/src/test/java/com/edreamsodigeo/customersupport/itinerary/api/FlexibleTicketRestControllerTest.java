package com.edreamsodigeo.customersupport.itinerary.api;

import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketDTO;
import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketService;
import com.edreamsodigeo.customersupport.itinerary.api.mapper.FlexibleTicketApiMapper;
import com.edreamsodigeo.customersupport.itinerary.exception.RegisterNotFoundException;
import com.edreamsodigeo.customersupport.itinerary.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.itinerary.v1.exception.FlexibleTicketNotFoundException;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.v1.model.FlexibleTicketId;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.PersistenceException;
import java.util.UUID;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class FlexibleTicketRestControllerTest {


    private FlexibleTicketRestController controller;

    @Mock
    private FlexibleTicketApiMapper mapper;
    @Mock
    private FlexibleTicketService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new FlexibleTicketRestController(service, mapper);
    }

    @Test
    public void testGetFlexibleTicket() throws FlexibleTicketNotFoundException, RegisterNotFoundException {
        final FlexibleTicketId flexibleTicketId = new FlexibleTicketId(UUID.randomUUID());
        final FlexibleTicketDTO flexibleTicketDTO = Mockito.mock(FlexibleTicketDTO.class);
        when(service.getFlexibleTicket(flexibleTicketId.getId())).thenReturn(flexibleTicketDTO);
        final FlexibleTicket expected = new FlexibleTicket(flexibleTicketId);
        when(mapper.mapFlexibleTicketDTOToFlexibleTicket(flexibleTicketDTO)).thenReturn(expected);

        final FlexibleTicket actual = controller.getFlexibleTicket(flexibleTicketId);
        assertSame(actual, expected);
        verify(service).getFlexibleTicket(flexibleTicketId.getId());
        verify(mapper).mapFlexibleTicketDTOToFlexibleTicket(flexibleTicketDTO);
    }

    @Test(expectedExceptions = FlexibleTicketNotFoundException.class)
    public void testGetFlexibleTicketThrowsException() throws RegisterNotFoundException, FlexibleTicketNotFoundException {
        final FlexibleTicketId flexibleTicketId = new FlexibleTicketId(UUID.randomUUID());
        Mockito.mock(FlexibleTicketDTO.class);
        when(service.getFlexibleTicket(flexibleTicketId.getId())).thenThrow(new RegisterNotFoundException("mes", new Exception()));
        controller.getFlexibleTicket(flexibleTicketId);
    }

    @Test
    public void testAddFlexibleTicket() throws DataBaseException {
        final FlexibleTicketId flexibleTicketId = new FlexibleTicketId(UUID.randomUUID());
        final FlexibleTicketDTO flexibleTicketDTO = Mockito.mock(FlexibleTicketDTO.class);
        final FlexibleTicket flexibleTicket = new FlexibleTicket(flexibleTicketId);
        when(mapper.mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket)).thenReturn(flexibleTicketDTO);

        controller.addFlexibleTicket(flexibleTicket);

        verify(service).addFlexibleTicket(flexibleTicketDTO);
        verify(mapper).mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket);
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddFlexibleTicketException() throws DataBaseException {
        final FlexibleTicketId flexibleTicketId = new FlexibleTicketId(UUID.randomUUID());
        final FlexibleTicketDTO flexibleTicketDTO = Mockito.mock(FlexibleTicketDTO.class);
        final FlexibleTicket flexibleTicket = new FlexibleTicket(flexibleTicketId);
        when(mapper.mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket)).thenReturn(flexibleTicketDTO);
        doThrow(new PersistenceException()).when(service).addFlexibleTicket(flexibleTicketDTO);
        controller.addFlexibleTicket(flexibleTicket);
    }

    @Test
    public void testUpdateFlexibleTicket() throws FlexibleTicketNotFoundException, RegisterNotFoundException {
        final FlexibleTicketId flexibleTicketId = new FlexibleTicketId(UUID.randomUUID());
        final FlexibleTicketDTO flexibleTicketDTO = Mockito.mock(FlexibleTicketDTO.class);
        final FlexibleTicket flexibleTicket = new FlexibleTicket(flexibleTicketId);
        when(mapper.mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket)).thenReturn(flexibleTicketDTO);

        controller.updateFlexibleTicket(flexibleTicket);

        verify(service).updateFlexibleTicket(flexibleTicketDTO);
        verify(mapper).mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket);
    }

    @Test(expectedExceptions = FlexibleTicketNotFoundException.class)
    public void testUpdateFlexibleTicketException() throws FlexibleTicketNotFoundException, RegisterNotFoundException {
        final FlexibleTicketId flexibleTicketId = new FlexibleTicketId(UUID.randomUUID());
        final FlexibleTicketDTO flexibleTicketDTO = Mockito.mock(FlexibleTicketDTO.class);
        final FlexibleTicket flexibleTicket = new FlexibleTicket(flexibleTicketId);
        when(mapper.mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket)).thenReturn(flexibleTicketDTO);
        doThrow(new RegisterNotFoundException("Msg")).when(service).updateFlexibleTicket(flexibleTicketDTO);
        controller.updateFlexibleTicket(flexibleTicket);
    }
}
