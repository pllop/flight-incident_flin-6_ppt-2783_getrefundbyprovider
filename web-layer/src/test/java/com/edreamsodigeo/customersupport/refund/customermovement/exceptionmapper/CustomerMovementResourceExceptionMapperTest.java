package com.edreamsodigeo.customersupport.refund.customermovement.exceptionmapper;

import com.edreamsodigeo.customersupport.refund.v1.exception.CustomerMovementResourceException;
import com.odigeo.commons.rest.error.SimpleExceptionBean;
import javax.ws.rs.core.Response;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustomerMovementResourceExceptionMapperTest {
    private CustomerMovementResourceExceptionMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new CustomerMovementResourceExceptionMapper();
    }

    @Test
    public void testMapper() {
        String mockMessage = "Mock Message";
        Throwable throwable = new Throwable();
        Response response = mapper.toResponse(new CustomerMovementResourceException(mockMessage, throwable));
        assertEquals(response.getStatus(), Response.Status.NOT_FOUND.getStatusCode());
        assertNotNull(response.getEntity());
        SimpleExceptionBean responseEntity = (SimpleExceptionBean) response.getEntity();
        assertEquals(responseEntity.getMessage(), mockMessage);
    }
}
