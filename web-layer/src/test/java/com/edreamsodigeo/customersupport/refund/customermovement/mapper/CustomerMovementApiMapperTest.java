package com.edreamsodigeo.customersupport.refund.customermovement.mapper;


import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementDTO;
import com.edreamsodigeo.customersupport.refund.customermovement.MovementMoneyDTO;
import com.edreamsodigeo.customersupport.refund.v1.model.Money;
import com.edreamsodigeo.customersupport.refund.v1.model.RefundId;
import com.edreamsodigeo.customersupport.refund.v1.model.request.CustomerMovement;
import java.math.BigDecimal;
import java.util.UUID;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustomerMovementApiMapperTest {
    public static final Money AMOUNT = new Money(BigDecimal.valueOf(7), "GBP");
    public static final Money AMOUNT_EUR = new Money(BigDecimal.valueOf(8), "EUR");
    public static final MovementMoneyDTO AMOUNT_DTO = new MovementMoneyDTO(BigDecimal.valueOf(10), "GBP");
    public static final MovementMoneyDTO AMOUNT_EUR_DTO = new MovementMoneyDTO(BigDecimal.valueOf(7), "EUR");
    public static final String ID = "b31f8180-fa5b-494f-8f03-4a996e2eaab4";
    public static final String PSP = "WORLDPAY";
    public static final String PSP_ACCOUNT = "WPEUROLTD";
    public static final String STATUS = "OK";

    private CustomerMovementApiMapper customerMovementApiMapper;

    @BeforeMethod
    public void setUpd() {
        customerMovementApiMapper = new CustomerMovementApiMapper();
    }

    @Test
    public void testMapCustomerMovementToCustomerMovementDTO() {
        CustomerMovement customerMovement = buildCustomerMovement();
        CustomerMovementDTO customerMovementDTO = customerMovementApiMapper.mapCustomerMovementToCustomerMovementDTO(customerMovement);
        assertNotNull(customerMovementDTO);
        assertNotNull(customerMovementDTO.getId());
        assertEquals(customerMovement.getRefundId().getId(), customerMovementDTO.getRefundId());
        assertEquals(customerMovement.getAmount().getAmount(), customerMovementDTO.getAmount().getAmount());
        assertEquals(customerMovement.getAmount().getCurrency(), customerMovementDTO.getAmount().getCurrency());
        assertEquals(customerMovement.getAmount().getCurrency(), customerMovementDTO.getCurrencyCode());
        assertEquals(customerMovement.getCurrencyRateProviderCustomer(), customerMovementDTO.getCurrencyRateProviderCustomer());
        assertEquals(customerMovement.getAmountInEur().getAmount(), customerMovementDTO.getAmountInEur().getAmount());
        assertEquals(customerMovement.getAmountInEur().getCurrency(), customerMovementDTO.getAmountInEur().getCurrency());
        assertEquals(customerMovement.getCurrencyRateProviderEur(), customerMovementDTO.getCurrencyRateProviderEur());
        assertEquals(customerMovement.getPsp(), customerMovementDTO.getPsp());
        assertEquals(customerMovement.getPspAccount(), customerMovementDTO.getPspAccount());
        assertEquals(customerMovement.getStatus(), customerMovementDTO.getStatus());
        assertNotNull(customerMovementDTO.getCustomerMovementWavePspId());
    }

    private CustomerMovement buildCustomerMovement() {
        CustomerMovement customerMovement = new CustomerMovement();
        customerMovement.setRefundId(new RefundId(ID));
        customerMovement.setAmount(AMOUNT);
        customerMovement.setCurrencyRateProviderCustomer(BigDecimal.TEN);
        customerMovement.setAmountInEur(AMOUNT_EUR);
        customerMovement.setCurrencyRateProviderEur(BigDecimal.ONE);
        customerMovement.setPsp(PSP);
        customerMovement.setPspAccount(PSP_ACCOUNT);
        customerMovement.setStatus(STATUS);
        return customerMovement;
    }

    @Test
    public void testMapCustomerMovementDTOToCustomerMovement() {
        CustomerMovementDTO customerMovementDTO = buildCustomerMovementDTO();
        CustomerMovement customerMovement = customerMovementApiMapper.mapCustomerMovementDTOToCustomerMovement(customerMovementDTO);
        assertNotNull(customerMovement);
        assertEquals(customerMovementDTO.getId(), customerMovement.getId().getId());
        assertEquals(customerMovementDTO.getRefundId(), customerMovement.getRefundId().getId());
        assertEquals(customerMovementDTO.getAmount().getAmount(), customerMovement.getAmount().getAmount());
        assertEquals(customerMovementDTO.getAmount().getCurrency(), customerMovement.getAmount().getCurrency());
        assertEquals(customerMovementDTO.getCurrencyCode(), customerMovement.getAmount().getCurrency());
        assertEquals(customerMovementDTO.getCurrencyRateProviderCustomer(), customerMovement.getCurrencyRateProviderCustomer());
        assertEquals(customerMovementDTO.getAmountInEur().getAmount(), customerMovement.getAmountInEur().getAmount());
        assertEquals(customerMovementDTO.getAmountInEur().getCurrency(), customerMovement.getAmountInEur().getCurrency());
        assertEquals(customerMovementDTO.getCurrencyRateProviderEur(), customerMovement.getCurrencyRateProviderEur());
        assertEquals(customerMovementDTO.getPsp(), customerMovement.getPsp());
        assertEquals(customerMovementDTO.getPspAccount(), customerMovement.getPspAccount());
        assertEquals(customerMovementDTO.getStatus(), customerMovement.getStatus());
        assertEquals(customerMovementDTO.getCustomerMovementWavePspId(), customerMovement.getCustomerMovementWavePspId().getId());

    }

    private CustomerMovementDTO buildCustomerMovementDTO() {
        CustomerMovementDTO customerMovementDTO = new CustomerMovementDTO();
        customerMovementDTO.setId(UUID.fromString(ID));
        customerMovementDTO.setRefundId(UUID.fromString(ID));
        customerMovementDTO.setAmount(AMOUNT_DTO);
        customerMovementDTO.setCurrencyCode(AMOUNT.getCurrency());
        customerMovementDTO.setCurrencyRateProviderCustomer(BigDecimal.TEN);
        customerMovementDTO.setAmountInEur(AMOUNT_EUR_DTO);
        customerMovementDTO.setCurrencyRateProviderEur(BigDecimal.ONE);
        customerMovementDTO.setPsp(PSP);
        customerMovementDTO.setPspAccount(PSP_ACCOUNT);
        customerMovementDTO.setStatus(STATUS);
        customerMovementDTO.setCustomerMovementWavePspId(UUID.fromString(ID));
        return customerMovementDTO;
    }
}
