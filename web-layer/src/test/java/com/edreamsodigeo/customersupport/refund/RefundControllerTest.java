package com.edreamsodigeo.customersupport.refund;

import com.edreamsodigeo.customersupport.refund.mapper.RefundApiMapper;
import com.edreamsodigeo.customersupport.refund.v1.model.Refund;
import com.edreamsodigeo.customersupport.refund.v1.model.RefundId;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class RefundControllerTest {

    @Mock
    private RefundService refundService;

    @Mock
    private RefundApiMapper refundApiMapper;

    @BeforeClass
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetRefund() {
        RefundId id = new RefundId("abdb61be-dac9-486f-99ed-e2b9d376fc4d");
        Refund refund = new Refund();
        RefundDTO refundDTO = new RefundDTO();

        when(refundService.getRefundById(id.getId())).thenReturn(refundDTO);
        when(refundApiMapper.mapRefundDTOToRefund(refundDTO)).thenReturn(refund);

        RefundController refundController = new RefundController(refundService, refundApiMapper);
        Refund response = refundController.getRefund(id);

        verify(refundService).getRefundById(id.getId());
        verify(refundApiMapper).mapRefundDTOToRefund(refundDTO);
        assertEquals(response, refund);
    }

    @Test
    public void testAddRefund() {
        Refund refund = new Refund();
        RefundDTO refundDTO = new RefundDTO();

        when(refundApiMapper.mapRefundToRefundDTO(refund)).thenReturn(refundDTO);

        RefundController refundController = new RefundController(refundService, refundApiMapper);
        refundController.addRefund(refund);

        verify(refundApiMapper).mapRefundToRefundDTO(refund);
        verify(refundService).addRefund(refundDTO);
    }

    @Test
    public void testUpdateRefund() {
        Refund refund = new Refund();
        RefundDTO refundDTO = new RefundDTO();

        when(refundApiMapper.mapRefundToRefundDTO(refund)).thenReturn(refundDTO);

        RefundController refundController = new RefundController(refundService, refundApiMapper);
        refundController.updateRefund(refund);

        verify(refundApiMapper).mapRefundToRefundDTO(refund);
        verify(refundService).updateRefund(refundDTO);
    }
}
