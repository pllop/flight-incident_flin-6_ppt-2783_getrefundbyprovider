package com.edreamsodigeo.customersupport.refund.mapper;

import com.edreamsodigeo.customersupport.refund.ProviderRefundDTO;
import com.edreamsodigeo.customersupport.refund.RefundDTO;
import com.edreamsodigeo.customersupport.refund.v1.model.BookingItemId;
import com.edreamsodigeo.customersupport.refund.v1.model.Money;
import com.edreamsodigeo.customersupport.refund.v1.model.ProviderBookingItemId;
import com.edreamsodigeo.customersupport.refund.v1.model.ProviderRefund;
import com.edreamsodigeo.customersupport.refund.v1.model.ProviderRefundId;
import com.edreamsodigeo.customersupport.refund.v1.model.Refund;
import com.edreamsodigeo.customersupport.refund.v1.model.RefundId;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class RefundApiMapperTest {

    @Test
    public void testMapRefundDTOToRefund() {
        RefundDTO refundDTO = createRefundDTO();

        RefundApiMapper refundApiMapper = new RefundApiMapper();
        Refund response = refundApiMapper.mapRefundDTOToRefund(refundDTO);

        assertEquals(refundDTO.getId().toString(), response.getId().getId().toString());
        assertEquals(refundDTO.getCustomerRefundAmount(), response.getCustomerRefundAmount().getAmount().doubleValue());
        assertEquals(refundDTO.getCustomerRefundCurrency(), response.getCustomerRefundAmount().getCurrency());
        assertEquals(refundDTO.getProviderRefunds().size(), response.getProviderRefunds().size());
        assertEquals(refundDTO.getProviderRefunds().get(0).getId().toString(), response.getProviderRefunds().get(0).getId().getId().toString());
        assertEquals(refundDTO.getProviderRefunds().get(0).getProviderBookingItemId(), response.getProviderRefunds().get(0).getProviderBookingItemId().getId());
        assertEquals(refundDTO.getProviderRefunds().get(0).getExpectedProviderRefundAmount(), response.getProviderRefunds().get(0).getExpectedProviderRefundAmount().getAmount().doubleValue());
        assertEquals(refundDTO.getProviderRefunds().get(0).getExpectedProviderRefundCurrency(), response.getProviderRefunds().get(0).getExpectedProviderRefundAmount().getCurrency());
        assertEquals(refundDTO.getBookingItemId(), response.getBookingItemId().getId());
        assertEquals(refundDTO.getCustomerRefundMethod(), response.getCustomerRefundMethod());
        assertEquals(refundDTO.getStatus(), response.getStatus());
    }

    @Test
    public void testMapRefundDTOToRefundNull() {
        RefundDTO refundDTO = null;

        RefundApiMapper refundApiMapper = new RefundApiMapper();
        Refund response = refundApiMapper.mapRefundDTOToRefund(refundDTO);

        assertNull(response.getId());
        assertNull(response.getCustomerRefundAmount());
        assertNull(response.getProviderRefunds());
        assertNull(response.getBookingItemId());
        assertNull(response.getCustomerRefundMethod());
        assertNull(response.getStatus());
    }

    @Test
    public void testMapRefundToRefundDTO() {
        Refund refund = createRefund();

        RefundApiMapper refundApiMapper = new RefundApiMapper();
        RefundDTO response = refundApiMapper.mapRefundToRefundDTO(refund);

        assertEquals(refund.getId().getId().toString(), response.getId().toString());
        assertEquals(refund.getCustomerRefundAmount().getAmount().doubleValue(), response.getCustomerRefundAmount());
        assertEquals(refund.getCustomerRefundAmount().getCurrency(), response.getCustomerRefundCurrency());
        assertEquals(refund.getProviderRefunds().size(), response.getProviderRefunds().size());
        assertEquals(refund.getProviderRefunds().get(0).getId().getId().toString(), response.getProviderRefunds().get(0).getId().toString());
        assertEquals(refund.getProviderRefunds().get(0).getProviderBookingItemId().getId(), response.getProviderRefunds().get(0).getProviderBookingItemId());
        assertEquals(refund.getProviderRefunds().get(0).getExpectedProviderRefundAmount().getAmount().doubleValue(), response.getProviderRefunds().get(0).getExpectedProviderRefundAmount());
        assertEquals(refund.getProviderRefunds().get(0).getExpectedProviderRefundAmount().getCurrency(), response.getProviderRefunds().get(0).getExpectedProviderRefundCurrency());
        assertEquals(refund.getBookingItemId().getId(), response.getBookingItemId());
        assertEquals(refund.getCustomerRefundMethod(), response.getCustomerRefundMethod());
        assertEquals(refund.getStatus(), response.getStatus());
    }

    @Test
    public void testMapRefundToRefundDTONull() {
        Refund refund = null;

        RefundApiMapper refundApiMapper = new RefundApiMapper();
        RefundDTO response = refundApiMapper.mapRefundToRefundDTO(refund);

        assertNull(response.getId());
        assertNull(response.getProviderRefunds());
        assertNull(response.getCustomerRefundAmount());
        assertNull(response.getBookingItemId());
        assertNull(response.getCustomerRefundCurrency());
        assertNull(response.getCustomerRefundMethod());
        assertNull(response.getStatus());
    }

    private Refund createRefund() {
        Refund refund = new Refund();
        refund.setId(new RefundId("abdb61be-dac9-486f-99ed-e2b9d376fc4d"));
        refund.setCustomerRefundAmount(new Money(new BigDecimal(123.23), "EUR"));
        refund.setProviderRefunds(createProviderRefunds());

        BookingItemId bookingItemId = new BookingItemId();
        bookingItemId.setId(9L);
        refund.setBookingItemId(bookingItemId);

        refund.setCustomerRefundMethod("method");
        refund.setStatus("status");

        return refund;
    }

    private List<ProviderRefund> createProviderRefunds() {
        List<ProviderRefund> providerRefunds = new ArrayList<>();

        ProviderRefund providerRefund = new ProviderRefund();
        providerRefund.setId(new ProviderRefundId("abdb61be-dac9-486f-99ed-e2b9d376fc13"));
        providerRefund.setProviderBookingItemId(new ProviderBookingItemId(8L));
        providerRefund.setExpectedProviderRefundAmount(new Money(new BigDecimal(789), "EUR"));

        providerRefunds.add(providerRefund);

        return providerRefunds;
    }

    private RefundDTO createRefundDTO() {
        RefundDTO refundDTO = new RefundDTO();
        refundDTO.setId(UUID.fromString("abdb61be-dac9-486f-99ed-e2b9d376fc4d"));
        refundDTO.setCustomerRefundAmount(123.5);
        refundDTO.setCustomerRefundCurrency("EUR");
        refundDTO.setProviderRefunds(createProviderRefundsDTO());
        refundDTO.setBookingItemId(5L);
        refundDTO.setStatus("status");

        return refundDTO;
    }

    private List<ProviderRefundDTO> createProviderRefundsDTO() {
        List<ProviderRefundDTO> providerRefundDTOS = new ArrayList<>();

        ProviderRefundDTO providerRefundDTO = new ProviderRefundDTO();
        providerRefundDTO.setId(UUID.fromString("abdb61be-dac9-486f-99ed-e2b9d376fc13"));
        providerRefundDTO.setProviderBookingItemId(7L);
        providerRefundDTO.setExpectedProviderRefundAmount(789.45);
        providerRefundDTO.setExpectedProviderRefundCurrency("EUR");

        providerRefundDTOS.add(providerRefundDTO);

        return providerRefundDTOS;
    }
}
