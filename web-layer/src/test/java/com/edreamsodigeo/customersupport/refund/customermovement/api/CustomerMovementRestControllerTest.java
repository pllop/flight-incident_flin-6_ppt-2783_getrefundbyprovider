package com.edreamsodigeo.customersupport.refund.customermovement.api;

import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementDTO;
import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementService;
import com.edreamsodigeo.customersupport.refund.customermovement.mapper.CustomerMovementApiMapper;
import com.edreamsodigeo.customersupport.refund.v1.exception.CustomerMovementResourceException;
import com.edreamsodigeo.customersupport.refund.v1.model.request.CustomerMovement;
import com.edreamsodigeo.customersupport.refund.v1.model.request.CustomerMovementId;
import java.util.UUID;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustomerMovementRestControllerTest {
    public static final String ID = "b31f8180-fa5b-494f-8f03-4a996e2eaab4";

    private CustomerMovementRestController restController;

    @Mock
    private CustomerMovementApiMapper mapper;

    @Mock
    private CustomerMovementService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        restController = new CustomerMovementRestController(service, mapper);
    }

    @Test
    public void testAddCustomerMovement() throws RegisterNotFound, CustomerMovementResourceException {
        CustomerMovement customerMovement = new CustomerMovement();
        customerMovement.setId(new CustomerMovementId(ID));
        CustomerMovementDTO dto = new CustomerMovementDTO();
        dto.setId(UUID.fromString(ID));
        when(mapper.mapCustomerMovementToCustomerMovementDTO(customerMovement)).thenReturn(dto);
        when(mapper.mapCustomerMovementDTOToCustomerMovement(dto)).thenReturn(customerMovement);
        when(service.createCustomerMovement(dto)).thenReturn(dto.getId());
        CustomerMovementId result = restController.addCustomerMovement(customerMovement);
        assertEquals(result, customerMovement.getId());
        verify(mapper).mapCustomerMovementToCustomerMovementDTO(customerMovement);
        verify(service).createCustomerMovement(dto);
    }

    @Test(expectedExceptions = CustomerMovementResourceException.class)
    public void testUpdateCancelledItemNotFound() throws RegisterNotFound, CustomerMovementResourceException {
        Mockito.doThrow(new RegisterNotFound("Mock Exception")).when(service).createCustomerMovement(any());
        restController.addCustomerMovement(new CustomerMovement());
    }
}
