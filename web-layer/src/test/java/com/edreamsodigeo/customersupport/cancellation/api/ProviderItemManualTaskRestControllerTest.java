package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.ProviderItemManualTaskApiMapper;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.ProviderItemManualTaskNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemManualTaskId;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class ProviderItemManualTaskRestControllerTest {

    private ProviderItemManualTaskRestController controller;

    @Mock
    private ProviderItemManualTaskApiMapper mapper;

    @Mock
    private ProviderItemManualTaskService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new ProviderItemManualTaskRestController(mapper, service);
    }

    @Test
    public void testGetProviderItemManualTaskl() throws RegisterNotFound, ProviderItemManualTaskNotFoundException {
        UUID uuid = UUID.randomUUID();
        ProviderItemManualTaskId id = new ProviderItemManualTaskId(uuid.toString());
        uuid = id.getId();
        ProviderItemManualTaskDTO dto = new ProviderItemManualTaskDTO();
        ProviderItemManualTask response = new ProviderItemManualTask();
        when(service.getProviderItemManualTask(uuid)).thenReturn(dto);
        when(mapper.mapDtoToProviderItemManualTask(dto)).thenReturn(response);

        ProviderItemManualTask result = controller.getProviderItemManualTask(id);
        assertSame(result, response);
        verify(service).getProviderItemManualTask(uuid);
        verify(mapper).mapDtoToProviderItemManualTask(dto);
    }


    @Test(expectedExceptions = ProviderItemManualTaskNotFoundException.class)
    public void testGetProviderItemManualTaskNotFound() throws RegisterNotFound, ProviderItemManualTaskNotFoundException {
        when(service.getProviderItemManualTask(any())).thenThrow(new RegisterNotFound("Mock Excpetion"));
        try {
            controller.getProviderItemManualTask(new ProviderItemManualTaskId(UUID.randomUUID().toString()));
        } catch (ProviderItemManualTaskNotFoundException e) {
            verify(service).getProviderItemManualTask(any());
            throw e;
        }
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testGetProviderItemManualTaskThrowable() throws RegisterNotFound, ProviderItemManualTaskNotFoundException {
        when(service.getProviderItemManualTask(any())).thenThrow(new NullPointerException("Mock Excpetion"));
        controller.getProviderItemManualTask(new ProviderItemManualTaskId(UUID.randomUUID().toString()));
    }

    @Test
    public void testAddProviderItemManualTask() throws DataBaseException {
        ProviderItemManualTask providerItemManualTask = new ProviderItemManualTask();
        ProviderItemManualTaskDTO dto = new ProviderItemManualTaskDTO();
        when(mapper.mapProviderItemManualTaskToDto(providerItemManualTask)).thenReturn(dto);
        when(mapper.mapDtoToProviderItemManualTask(dto)).thenReturn(providerItemManualTask);
        when(service.addProviderItemManualTask(dto)).thenReturn(dto);
        ProviderItemManualTask result = controller.addProviderItemManualTask(providerItemManualTask);
        assertSame(result, providerItemManualTask);
        verify(mapper).mapDtoToProviderItemManualTask(dto);
        verify(mapper).mapProviderItemManualTaskToDto(providerItemManualTask);
        verify(service).addProviderItemManualTask(dto);
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddProviderItemManualTaskPersistenceException() throws DataBaseException {
        when(service.addProviderItemManualTask(any())).thenThrow(new PersistenceException("Mock Exception"));
        controller.addProviderItemManualTask(new ProviderItemManualTask());
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddProviderItemManualTaskEJBException() throws DataBaseException {
        when(service.addProviderItemManualTask(any())).thenThrow(new EJBException("Mock Exception"));
        controller.addProviderItemManualTask(new ProviderItemManualTask());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testAddProviderItemManualTaskThrowable() throws DataBaseException {
        when(service.addProviderItemManualTask(any())).thenThrow(new NullPointerException("Mock Exception"));
        controller.addProviderItemManualTask(new ProviderItemManualTask());
    }

    @Test
    public void testUpdateProviderItemManualTask() throws ProviderItemManualTaskNotFoundException, RegisterNotFound {
        UUID uuid = UUID.randomUUID();
        ProviderItemManualTaskId id = new ProviderItemManualTaskId(uuid.toString());
        uuid = id.getId();
        ProviderItemManualTask providerItemManualTask = new ProviderItemManualTask();
        ProviderItemManualTaskDTO dto = new ProviderItemManualTaskDTO();
        when(mapper.mapProviderItemManualTaskToDto(providerItemManualTask)).thenReturn(dto);
        controller.updateProviderItemManualTask(id, providerItemManualTask);
        verify(mapper).mapProviderItemManualTaskToDto(providerItemManualTask);
        verify(service).updateProviderItemManualTask(uuid, dto);
    }

    @Test(expectedExceptions = ProviderItemManualTaskNotFoundException.class)
    public void testUpdateCancelledItemNotFound() throws ProviderItemManualTaskNotFoundException, RegisterNotFound {
        Mockito.doThrow(new RegisterNotFound("Mock Exception")).when(service).updateProviderItemManualTask(any(), any());
        controller.updateProviderItemManualTask(new ProviderItemManualTaskId(), new ProviderItemManualTask());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testUpdateCancelledItemThrowable() throws ProviderItemManualTaskNotFoundException, RegisterNotFound {
        Mockito.doThrow(new NullPointerException("Mock Exception")).when(service).updateProviderItemManualTask(any(), any());
        controller.updateProviderItemManualTask(new ProviderItemManualTaskId(), new ProviderItemManualTask());
    }


}
