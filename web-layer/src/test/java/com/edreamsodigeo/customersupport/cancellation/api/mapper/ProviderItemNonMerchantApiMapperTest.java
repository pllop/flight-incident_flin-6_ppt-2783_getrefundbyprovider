package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Money;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemNonMerchant;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemNonMerchantId;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class ProviderItemNonMerchantApiMapperTest {

    private ProviderItemNonMerchantApiMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new ProviderItemNonMerchantApiMapper();
    }

    @Test
    public void testMapDtoToProviderItemManual() {
        ProviderItemNonMerchantDTO dto = new ProviderItemNonMerchantDTO();
        UUID providerItemId = UUID.randomUUID();
        UUID refundId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        dto.setProviderItemId(providerItemId);
        dto.setId(id);
        dto.setProviderRefundAmountExpected(new MoneyDTO(BigDecimal.TEN, "EUR"));
        dto.setRefunder("TEST-REFUND");

        ProviderItemNonMerchant result = mapper.mapDtoToProviderItemNonMerchant(dto);

        assertEquals(result.getProviderItemId().getId(), providerItemId);
        assertEquals(result.getId().getId(), id);
        assertEquals(result.getProviderRefundAmountExpected().getAmount(), BigDecimal.TEN);
        assertEquals(result.getProviderRefundAmountExpected().getCurrency(), "EUR");
        assertEquals(result.getRefunder(), "TEST-REFUND");
    }

    @Test
    public void testMapProviderItemManualToDto() {
        ProviderItemNonMerchant itemManual = new ProviderItemNonMerchant();

        UUID providerItemId = UUID.randomUUID();
        UUID refundId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        itemManual.setProviderRefundAmountExpected(new Money(BigDecimal.TEN, "EUR"));
        itemManual.setId(new ProviderItemNonMerchantId(id.toString()));
        itemManual.setProviderItemId(new ProviderItemId(providerItemId.toString()));
        itemManual.setRefunder("TEST-REFUND");

        ProviderItemNonMerchantDTO result = mapper.mapProviderItemNonMerchantToDto(itemManual);
        assertEquals(result.getId(), id);
        assertEquals(result.getProviderItemId(), providerItemId);
        assertEquals(result.getProviderRefundAmountExpected().getAmount(), BigDecimal.TEN);
        assertEquals(result.getProviderRefundAmountExpected().getCurrency(), "EUR");
        assertEquals(result.getRefunder(), "TEST-REFUND");
    }

}
