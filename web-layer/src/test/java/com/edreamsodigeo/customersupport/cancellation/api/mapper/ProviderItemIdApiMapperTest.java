package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class ProviderItemIdApiMapperTest {
    @Test
    public void testMap() {
        ProviderItemIdApiMapper providerItemIdApiMapper = new ProviderItemIdApiMapper();
        UUID uuid = new UUID(1L, 2L);
        ProviderItemId providerItemId = providerItemIdApiMapper.map(uuid);
        assertEquals(providerItemId.getId(), uuid);
    }

}
