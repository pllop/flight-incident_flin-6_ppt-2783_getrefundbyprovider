package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.model.RefundMethod;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Money;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemMerchant;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemMerchantId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.RefundId;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class ProviderItemMerchantApiMapperTest {

    private ProviderItemMerchantApiMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new ProviderItemMerchantApiMapper();
    }

    @Test
    public void testMapDtoToProviderItemManual() {
        ProviderItemMerchantDTO dto = new ProviderItemMerchantDTO();
        UUID providerItemId = UUID.randomUUID();
        UUID refundId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        dto.setRefundId(refundId);
        dto.setProviderItemId(providerItemId);
        dto.setId(id);
        dto.setProviderRefundAmountExpected(new MoneyDTO(BigDecimal.TEN, "EUR"));
        dto.setCustomerRefundAmount(new MoneyDTO(BigDecimal.ONE, "EUR"));
        dto.setCustomerRefundMethod(RefundMethod.CASH);

        ProviderItemMerchant result = mapper.mapDtoToProviderItemMerchant(dto);

        assertEquals(result.getProviderItemId().getId(), providerItemId);
        assertEquals(result.getId().getId(), id);
        assertEquals(result.getProviderRefundAmountExpected().getAmount(), BigDecimal.TEN);
        assertEquals(result.getProviderRefundAmountExpected().getCurrency(), "EUR");
        assertEquals(result.getCustomerRefundAmount().getAmount(), BigDecimal.ONE);
        assertEquals(result.getCustomerRefundAmount().getCurrency(), "EUR");
        assertEquals(result.getCustomerRefundMethod(), com.edreamsodigeo.customersupport.cancellation.v1.model.RefundMethod.CASH);
    }

    @Test
    public void testMapProviderItemManualToDto() {
        ProviderItemMerchant itemManual = new ProviderItemMerchant();

        UUID providerItemId = UUID.randomUUID();
        UUID refundId = UUID.randomUUID();
        UUID id = UUID.randomUUID();

        itemManual.setProviderRefundAmountExpected(new Money(BigDecimal.TEN, "EUR"));
        itemManual.setCustomerRefundAmount(new Money(BigDecimal.ONE, "EUR"));
        itemManual.setCustomerRefundMethod(com.edreamsodigeo.customersupport.cancellation.v1.model.RefundMethod.CASH);
        itemManual.setId(new ProviderItemMerchantId(id.toString()));
        itemManual.setProviderItemId(new ProviderItemId(providerItemId.toString()));
        itemManual.setRefundId(new RefundId(refundId.toString()));

        ProviderItemMerchantDTO result = mapper.mapProviderItemMerchantToDto(itemManual);
        assertEquals(result.getId(), id);
        assertEquals(result.getProviderItemId(), providerItemId);
        assertEquals(result.getRefundId(), refundId);
        assertEquals(result.getProviderRefundAmountExpected().getAmount(), BigDecimal.TEN);
        assertEquals(result.getProviderRefundAmountExpected().getCurrency(), "EUR");
        assertEquals(result.getCustomerRefundAmount().getAmount(), BigDecimal.ONE);
        assertEquals(result.getCustomerRefundAmount().getCurrency(), "EUR");
        assertEquals(result.getCustomerRefundMethod(), RefundMethod.CASH);
    }

}
