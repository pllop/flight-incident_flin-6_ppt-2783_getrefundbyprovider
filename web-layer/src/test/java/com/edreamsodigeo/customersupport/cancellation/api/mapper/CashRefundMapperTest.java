package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cashrefund.BookingItemCashRefund;
import com.edreamsodigeo.customersupport.cashrefund.ProviderBookingItemCashRefund;
import com.edreamsodigeo.customersupport.flightincident.mappers.CashRefundMapper;

import com.edreamsodigeo.customersupport.flightincident.v1.model.CashRefundStatus;
import com.flextrade.jfixture.annotations.Fixture;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertEquals;

import static com.flextrade.jfixture.FixtureAnnotations.initFixtures;

public class CashRefundMapperTest {

    private static CashRefundMapper mapper = CashRefundMapper.INSTANCE;

    @Fixture
    private ProviderBookingItemCashRefund modelPBI;
    @Fixture
    private BookingItemCashRefund modelBI;

    @BeforeMethod
    public void setup() {
        initFixtures(this);
    }

    @Test
    public void testToContract() {
        com.edreamsodigeo.customersupport.flightincident.v1.model.ProviderBookingItemCashRefund result = mapper.toContract(modelPBI);
        com.edreamsodigeo.customersupport.flightincident.v1.model.ProviderBookingItemCashRefund expected = new com.edreamsodigeo.customersupport.flightincident.v1.model.ProviderBookingItemCashRefund();
        expected.setProviderBookingItemId(modelPBI.getProviderBookingItemId());
        expected.setCashRefundStatus(CashRefundStatus.valueOf(modelPBI.getCashRefundStatus().toString()));
        assertEquals(result.getProviderBookingItemId(), expected.getProviderBookingItemId());
        assertEquals(result.getCashRefundStatus(), expected.getCashRefundStatus());
    }

    @Test
    public void testMap() {
        modelBI.setProviderBookingItemCashRefunds(Collections.singletonList(modelPBI));
        com.edreamsodigeo.customersupport.flightincident.v1.model.BookingItemCashRefund expectedBI = new com.edreamsodigeo.customersupport.flightincident.v1.model.BookingItemCashRefund();
        expectedBI.setBookingItemId(modelBI.getBookingItemId());

        com.edreamsodigeo.customersupport.flightincident.v1.model.ProviderBookingItemCashRefund expectedPBI = new com.edreamsodigeo.customersupport.flightincident.v1.model.ProviderBookingItemCashRefund();
        expectedPBI.setProviderBookingItemId(modelBI.getProviderBookingItemCashRefunds().get(0).getProviderBookingItemId());
        expectedPBI.setCashRefundStatus(CashRefundStatus.valueOf(modelBI.getProviderBookingItemCashRefunds().get(0).getCashRefundStatus().toString()));
        expectedBI.setProviderBookingItemCashRefunds(Collections.singletonList(expectedPBI));

        List<com.edreamsodigeo.customersupport.flightincident.v1.model.BookingItemCashRefund> result = mapper.map(Collections.singletonList(modelBI));
        assertEquals(result.get(0).getBookingItemId(), expectedBI.getBookingItemId());
        assertEquals(result.get(0).getProviderBookingItemCashRefunds().get(0).getProviderBookingItemId(), expectedPBI.getProviderBookingItemId());
        assertEquals(result.get(0).getProviderBookingItemCashRefunds().get(0).getCashRefundStatus(), expectedPBI.getCashRefundStatus());
    }
}
