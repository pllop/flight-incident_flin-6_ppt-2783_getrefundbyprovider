package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscountId;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class ProviderItemDiscountIdApiMapperTest {
    @Test
    public void testMap() {
        ProviderItemDiscountIdApiMapper providerItemDiscountIdApiMapper = new ProviderItemDiscountIdApiMapper();
        UUID uuid = new UUID(1L, 2L);
        ProviderItemDiscountId providerItemDiscountId = providerItemDiscountIdApiMapper.map(uuid);
        assertEquals(providerItemDiscountId.getId(), uuid);
    }

}
