package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantRefundService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.ProviderItemMerchantApiMapper;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.ProviderItemMerchantNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemMerchant;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemMerchantId;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class ProviderItemMerchantRefundControllerTest {

    private ProviderItemMerchantRefundController controller;

    @Mock
    private ProviderItemMerchantApiMapper mapper;

    @Mock
    private ProviderItemMerchantRefundService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new ProviderItemMerchantRefundController(mapper, service);
    }

    @Test
    public void testGetProviderItemMerchant() throws RegisterNotFound, ProviderItemMerchantNotFoundException {
        UUID uuid = UUID.randomUUID();
        ProviderItemMerchantId id = new ProviderItemMerchantId(uuid.toString());
        uuid = id.getId();
        ProviderItemMerchantDTO dto = new ProviderItemMerchantDTO();
        ProviderItemMerchant response = new ProviderItemMerchant();
        when(service.getProviderItemMerchant(uuid)).thenReturn(dto);
        when(mapper.mapDtoToProviderItemMerchant(dto)).thenReturn(response);

        ProviderItemMerchant result = controller.getProviderItemMerchant(id);
        assertSame(result, response);
        verify(service).getProviderItemMerchant(uuid);
        verify(mapper).mapDtoToProviderItemMerchant(dto);
    }


    @Test(expectedExceptions = ProviderItemMerchantNotFoundException.class)
    public void testGetProviderItemMerchantNotFound() throws RegisterNotFound, ProviderItemMerchantNotFoundException {
        when(service.getProviderItemMerchant(any())).thenThrow(new RegisterNotFound("Mock Excpetion"));
        try {
            controller.getProviderItemMerchant(new ProviderItemMerchantId(UUID.randomUUID().toString()));
        } catch (ProviderItemMerchantNotFoundException e) {
            verify(service).getProviderItemMerchant(any());
            throw e;
        }
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testGetProviderItemMerchantThrowable() throws RegisterNotFound, ProviderItemMerchantNotFoundException {
        when(service.getProviderItemMerchant(any())).thenThrow(new NullPointerException("Mock Excpetion"));
        controller.getProviderItemMerchant(new ProviderItemMerchantId(UUID.randomUUID().toString()));
    }

    @Test
    public void testAddProviderItemMerchant() throws DataBaseException {
        ProviderItemMerchant providerItemMerchant = new ProviderItemMerchant();
        ProviderItemMerchantDTO dto = new ProviderItemMerchantDTO();
        when(mapper.mapProviderItemMerchantToDto(providerItemMerchant)).thenReturn(dto);
        when(mapper.mapDtoToProviderItemMerchant(dto)).thenReturn(providerItemMerchant);
        when(service.addProviderItemMerchant(dto)).thenReturn(dto);
        ProviderItemMerchant result = controller.addProviderItemMerchant(providerItemMerchant);
        assertSame(result, providerItemMerchant);
        verify(mapper).mapDtoToProviderItemMerchant(dto);
        verify(mapper).mapProviderItemMerchantToDto(providerItemMerchant);
        verify(service).addProviderItemMerchant(dto);
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddProviderItemMerchantPersistenceException() throws DataBaseException {
        when(service.addProviderItemMerchant(any())).thenThrow(new PersistenceException("Mock Exception"));
        controller.addProviderItemMerchant(new ProviderItemMerchant());
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddProviderItemMerchantEJBException() throws DataBaseException {
        when(service.addProviderItemMerchant(any())).thenThrow(new EJBException("Mock Exception"));
        controller.addProviderItemMerchant(new ProviderItemMerchant());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testAddProviderItemMerchantThrowable() throws DataBaseException {
        when(service.addProviderItemMerchant(any())).thenThrow(new NullPointerException("Mock Exception"));
        controller.addProviderItemMerchant(new ProviderItemMerchant());
    }

    @Test
    public void testUpdateProviderItemMerchant() throws ProviderItemMerchantNotFoundException, RegisterNotFound {
        UUID uuid = UUID.randomUUID();
        ProviderItemMerchantId id = new ProviderItemMerchantId(uuid.toString());
        ProviderItemMerchant providerItemMerchant = new ProviderItemMerchant();
        ProviderItemMerchantDTO dto = new ProviderItemMerchantDTO();
        when(mapper.mapProviderItemMerchantToDto(providerItemMerchant)).thenReturn(dto);
        controller.updateProviderItemMerchant(id, providerItemMerchant);
        verify(mapper).mapProviderItemMerchantToDto(providerItemMerchant);
        verify(service).updateProviderItemMerchant(dto);
    }

    @Test(expectedExceptions = ProviderItemMerchantNotFoundException.class)
    public void testUpdateProviderItemMerchantNotFound() throws ProviderItemMerchantNotFoundException, RegisterNotFound {
        Mockito.doThrow(new RegisterNotFound("Mock Exception")).when(service).updateProviderItemMerchant(any());
        controller.updateProviderItemMerchant(new ProviderItemMerchantId(), new ProviderItemMerchant());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testUpdateProviderItemMerchantThrowable() throws ProviderItemMerchantNotFoundException, RegisterNotFound {
        Mockito.doThrow(new NullPointerException("Mock Exception")).when(service).updateProviderItemMerchant(any());
        controller.updateProviderItemMerchant(new ProviderItemMerchantId(), new ProviderItemMerchant());
    }

}
