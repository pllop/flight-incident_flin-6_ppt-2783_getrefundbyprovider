package com.edreamsodigeo.customersupport.cancellation.api.verifier;

import com.edreamsodigeo.customersupport.cancellation.v1.model.DiscountType;
import com.edreamsodigeo.customersupport.cancellation.v1.model.EmdId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscountId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.VoucherId;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

public class ProviderItemDiscountVerifierTest {
    private ProviderItemDiscountVerifier providerItemDiscountVerifier;

    @BeforeMethod
    public void setUp() {
        providerItemDiscountVerifier = new ProviderItemDiscountVerifier();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyFlexibleTicketIllegalArgumentFlexibleTicket() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.FLEXIBLE_TICKET);
        providerItemDiscount.setEmdId(emdId);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyFlexibleTicketIllegalArgumentVoucherId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.FLEXIBLE_TICKET);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setEmdId(emdId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyFlexibleTicketIllegalArgumentEmdId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.FLEXIBLE_TICKET);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyVoucherRefundIllegalArgumentVoucherId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.VOUCHER_REFUND);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setEmdId(emdId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyVoucherRefundIllegalArgumentFlexibleTicketId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.VOUCHER_REFUND);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscount.setEmdId(emdId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyVoucherRefundIllegalArgumentEmdId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.VOUCHER_REFUND);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyTopUpVoucherIllegalArgumentVoucherId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.TOP_UP_VOUCHER);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setEmdId(emdId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyTopUpVoucherIllegalArgumentEmdId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.TOP_UP_VOUCHER);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyTopUpVoucherIllegalArgumentFlexibleTicketId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.TOP_UP_VOUCHER);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscount.setEmdId(emdId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyEmdIllegalArgumentEmdId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.EMD);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyEmdIllegalArgumentFlexibleTicketId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.EMD);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscount.setEmdId(emdId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void verifyEmdIllegalArgumentVoucherId() {
        UUID uuid = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.EMD);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setEmdId(emdId);
        providerItemDiscountVerifier.verify(providerItemDiscount);
    }

}
