package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantRefundService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.ProviderItemNonMerchantApiMapper;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.ProviderItemNonMerchantNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemNonMerchant;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemNonMerchantId;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class ProviderItemNonMerchantRefundControllerTest {

    private ProviderItemNonMerchantRefundController controller;

    @Mock
    private ProviderItemNonMerchantApiMapper mapper;

    @Mock
    private ProviderItemNonMerchantRefundService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new ProviderItemNonMerchantRefundController(mapper, service);
    }

    @Test
    public void testGetProviderItemNonMerchant() throws RegisterNotFound, ProviderItemNonMerchantNotFoundException {
        UUID uuid = UUID.randomUUID();
        ProviderItemNonMerchantId id = new ProviderItemNonMerchantId(uuid.toString());
        uuid = id.getId();
        ProviderItemNonMerchantDTO dto = new ProviderItemNonMerchantDTO();
        ProviderItemNonMerchant response = new ProviderItemNonMerchant();
        when(service.getProviderItemNonMerchant(uuid)).thenReturn(dto);
        when(mapper.mapDtoToProviderItemNonMerchant(dto)).thenReturn(response);

        ProviderItemNonMerchant result = controller.getProviderItemNonMerchant(id);
        assertSame(result, response);
        verify(service).getProviderItemNonMerchant(uuid);
        verify(mapper).mapDtoToProviderItemNonMerchant(dto);
    }


    @Test(expectedExceptions = ProviderItemNonMerchantNotFoundException.class)
    public void testGetProviderItemNonMerchantNotFound() throws RegisterNotFound, ProviderItemNonMerchantNotFoundException {
        when(service.getProviderItemNonMerchant(any())).thenThrow(new RegisterNotFound("Mock Excpetion"));
        try {
            controller.getProviderItemNonMerchant(new ProviderItemNonMerchantId(UUID.randomUUID().toString()));
        } catch (ProviderItemNonMerchantNotFoundException e) {
            verify(service).getProviderItemNonMerchant(any());
            throw e;
        }
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testGetProviderItemNonMerchantThrowable() throws RegisterNotFound, ProviderItemNonMerchantNotFoundException {
        when(service.getProviderItemNonMerchant(any())).thenThrow(new NullPointerException("Mock Excpetion"));
        controller.getProviderItemNonMerchant(new ProviderItemNonMerchantId(UUID.randomUUID().toString()));
    }

    @Test
    public void testAddProviderItemNonMerchant() throws DataBaseException {
        ProviderItemNonMerchant providerItemNonMerchant = new ProviderItemNonMerchant();
        ProviderItemNonMerchantDTO dto = new ProviderItemNonMerchantDTO();
        when(mapper.mapProviderItemNonMerchantToDto(providerItemNonMerchant)).thenReturn(dto);
        when(mapper.mapDtoToProviderItemNonMerchant(dto)).thenReturn(providerItemNonMerchant);
        when(service.addProviderItemNonMerchant(dto)).thenReturn(dto);
        ProviderItemNonMerchant result = controller.addProviderItemNonMerchant(providerItemNonMerchant);
        assertSame(result, providerItemNonMerchant);
        verify(mapper).mapDtoToProviderItemNonMerchant(dto);
        verify(mapper).mapProviderItemNonMerchantToDto(providerItemNonMerchant);
        verify(service).addProviderItemNonMerchant(dto);
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddProviderItemManualPersistenceException() throws DataBaseException {
        when(service.addProviderItemNonMerchant(any())).thenThrow(new PersistenceException("Mock Exception"));
        controller.addProviderItemNonMerchant(new ProviderItemNonMerchant());
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddProviderItemManualEJBException() throws DataBaseException {
        when(service.addProviderItemNonMerchant(any())).thenThrow(new EJBException("Mock Exception"));
        controller.addProviderItemNonMerchant(new ProviderItemNonMerchant());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testAddProviderItemManualThrowable() throws DataBaseException {
        when(service.addProviderItemNonMerchant(any())).thenThrow(new NullPointerException("Mock Exception"));
        controller.addProviderItemNonMerchant(new ProviderItemNonMerchant());
    }

    @Test
    public void testUpdateProviderItemMerchant() throws ProviderItemNonMerchantNotFoundException, RegisterNotFound {
        UUID uuid = UUID.randomUUID();
        ProviderItemNonMerchantId id = new ProviderItemNonMerchantId(uuid.toString());
        ProviderItemNonMerchant providerItemNonMerchant = new ProviderItemNonMerchant();
        ProviderItemNonMerchantDTO dto = new ProviderItemNonMerchantDTO();
        when(mapper.mapProviderItemNonMerchantToDto(providerItemNonMerchant)).thenReturn(dto);
        controller.updateProviderItemNonMerchant(id, providerItemNonMerchant);
        verify(mapper).mapProviderItemNonMerchantToDto(providerItemNonMerchant);
        verify(service).updateProviderItemNonMerchant(dto);
    }

    @Test(expectedExceptions = ProviderItemNonMerchantNotFoundException.class)
    public void testUpdateProviderItemMerchantNotFound() throws ProviderItemNonMerchantNotFoundException, RegisterNotFound {
        Mockito.doThrow(new RegisterNotFound("Mock Exception")).when(service).updateProviderItemNonMerchant(any());
        controller.updateProviderItemNonMerchant(new ProviderItemNonMerchantId(), new ProviderItemNonMerchant());
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testUpdateProviderItemMerchantThrowable() throws ProviderItemNonMerchantNotFoundException, RegisterNotFound {
        Mockito.doThrow(new NullPointerException("Mock Exception")).when(service).updateProviderItemNonMerchant(any());
        controller.updateProviderItemNonMerchant(new ProviderItemNonMerchantId(), new ProviderItemNonMerchant());
    }

}
