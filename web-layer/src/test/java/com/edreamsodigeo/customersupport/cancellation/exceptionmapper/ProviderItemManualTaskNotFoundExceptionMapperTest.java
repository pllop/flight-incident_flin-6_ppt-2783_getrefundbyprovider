package com.edreamsodigeo.customersupport.cancellation.exceptionmapper;

import com.edreamsodigeo.customersupport.cancellation.v1.exception.ProviderItemManualTaskNotFoundException;
import com.odigeo.commons.rest.error.SimpleExceptionBean;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ProviderItemManualTaskNotFoundExceptionMapperTest {

    private ProviderItemManualTaskNotFoundExceptionMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new ProviderItemManualTaskNotFoundExceptionMapper();
    }

    @Test
    public void testMapper() {
        String mockMessage = "Mock Message";
        Throwable throwable = new Throwable();
        Response response = mapper.toResponse(new ProviderItemManualTaskNotFoundException(mockMessage, throwable));
        assertEquals(response.getStatus(), Response.Status.NOT_FOUND.getStatusCode());
        assertNotNull(response.getEntity());
        SimpleExceptionBean responseEntity = (SimpleExceptionBean) response.getEntity();
        assertEquals(responseEntity.getMessage(), mockMessage);
    }
}
