package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.flightincident.CashRefundService;
import com.edreamsodigeo.customersupport.flightincident.FlightIncidentService;
import com.edreamsodigeo.customersupport.flightincident.api.FlightIncidentRestController;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.mappers.CashRefundMapper;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.IncidentNotFoundException;
import com.edreamsodigeo.customersupport.flightincident.v1.model.BookingItemCashRefund;
import com.edreamsodigeo.customersupport.flightincident.v1.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.ProviderBookingItemCashRefund;
import com.flextrade.jfixture.annotations.Fixture;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static com.flextrade.jfixture.FixtureAnnotations.initFixtures;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class FlightIncidentRestControllerTest {

    private FlightIncidentRestController controller;

    @Mock
    CashRefundService cashRefundService;
    @Mock
    FlightIncidentService flightIncidentService;
    @Fixture
    private com.edreamsodigeo.customersupport.cashrefund.BookingItemCashRefund modelBI;

    private IncidentId incidentId = new IncidentId((UUIDGenerator.getInstance().generateUUID().toString()));

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        initFixtures(this);
        controller = new FlightIncidentRestController(flightIncidentService, cashRefundService);
    }

    @Test
    public void testGetCashRefundsStatus() throws IncidentNotFoundException, RegisterNotFound {
        modelBI.setProviderBookingItemCashRefunds(Collections.singletonList(modelBI.getProviderBookingItemCashRefunds().get(0)));
        List<BookingItemCashRefund> expectedList = CashRefundMapper.INSTANCE.map(Collections.singletonList(modelBI));
        BookingItemCashRefund expectedBI = expectedList.get(0);
        ProviderBookingItemCashRefund expectedPBI = expectedBI.getProviderBookingItemCashRefunds().get(0);

        when(cashRefundService.getRefundsStatus(new com.edreamsodigeo.customersupport.flightincident.model.IncidentId(incidentId.getId()))).thenReturn(Collections.singletonList(modelBI));

        List<BookingItemCashRefund> resultList = controller.getCashRefundsStatus(incidentId);
        BookingItemCashRefund result = resultList.get(0);
        assertEquals(result.getBookingItemId(), expectedBI.getBookingItemId());
        assertEquals(result.getProviderBookingItemCashRefunds().get(0).getProviderBookingItemId(), expectedPBI.getProviderBookingItemId());
        assertEquals(result.getProviderBookingItemCashRefunds().get(0).getCashRefundStatus(), expectedPBI.getCashRefundStatus());
    }

    @Test(expectedExceptions = IncidentNotFoundException.class)
    public void testGetCashRefundsStatusIncidentNotFound() throws IncidentNotFoundException, RegisterNotFound {
        modelBI.setProviderBookingItemCashRefunds(Collections.singletonList(modelBI.getProviderBookingItemCashRefunds().get(0)));
        List<BookingItemCashRefund> expectedList = CashRefundMapper.INSTANCE.map(Collections.singletonList(modelBI));
        BookingItemCashRefund expectedBI = expectedList.get(0);
        ProviderBookingItemCashRefund expectedPBI = expectedBI.getProviderBookingItemCashRefunds().get(0);

        when(cashRefundService.getRefundsStatus(new com.edreamsodigeo.customersupport.flightincident.model.IncidentId(incidentId.getId()))).thenThrow(new RegisterNotFound("Incident not found"));

        controller.getCashRefundsStatus(incidentId);
    }
}
