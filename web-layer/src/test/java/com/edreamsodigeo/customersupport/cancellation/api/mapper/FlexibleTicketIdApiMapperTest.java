package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.v1.model.FlexibleTicketId;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class FlexibleTicketIdApiMapperTest {
    @Test
    public void testMap() {
        FlexibleTicketIdApiMapper flexibleTicketIdApiMapper = new FlexibleTicketIdApiMapper();
        UUID uuid = new UUID(1L, 2L);
        FlexibleTicketId flexibleTicketId = flexibleTicketIdApiMapper.map(uuid);
        assertEquals(flexibleTicketId.getId(), uuid);
    }

}
