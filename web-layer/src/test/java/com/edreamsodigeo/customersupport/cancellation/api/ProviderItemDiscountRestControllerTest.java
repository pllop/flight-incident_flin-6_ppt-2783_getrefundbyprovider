package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemDiscountDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemDiscountService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.ProviderItemDiscountApiMapper;
import com.edreamsodigeo.customersupport.cancellation.api.verifier.ProviderItemDiscountVerifier;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscountId;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ejb.EJBException;
import javax.persistence.PersistenceException;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ProviderItemDiscountRestControllerTest {
    @Mock
    private ProviderItemDiscountApiMapper providerItemDiscountApiMapper;
    @Mock
    private ProviderItemDiscountService providerItemDiscountService;
    @Mock
    private ProviderItemDiscountVerifier providerItemDiscountVerifier;

    private ProviderItemDiscountRestController providerItemDiscountRestController;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        providerItemDiscountRestController = new ProviderItemDiscountRestController(providerItemDiscountApiMapper, providerItemDiscountService, providerItemDiscountVerifier);
    }

    @Test
    public void testAddProviderItemDiscount() throws DataBaseException {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        doNothing().when(providerItemDiscountVerifier).verify(eq(providerItemDiscount));
        when(providerItemDiscountApiMapper.map(eq(providerItemDiscount))).thenReturn(providerItemDiscountDTO);
        when(providerItemDiscountService.addproviderItemDiscount(eq(providerItemDiscountDTO))).thenReturn(providerItemDiscountDTO);
        when(providerItemDiscountApiMapper.map(eq(providerItemDiscountDTO))).thenReturn(providerItemDiscount);
        providerItemDiscountRestController.addProviderItemDiscount(providerItemDiscount);
        verify(providerItemDiscountApiMapper).map(eq(providerItemDiscount));
        verify(providerItemDiscountService).addproviderItemDiscount(eq(providerItemDiscountDTO));
        verify(providerItemDiscountApiMapper).map(eq(providerItemDiscountDTO));
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddProviderItemDiscountPersistenceException() throws DataBaseException {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        doNothing().when(providerItemDiscountVerifier).verify(eq(providerItemDiscount));
        when(providerItemDiscountApiMapper.map(eq(providerItemDiscount))).thenReturn(providerItemDiscountDTO);
        when(providerItemDiscountService.addproviderItemDiscount(eq(providerItemDiscountDTO))).thenThrow(new PersistenceException());
        providerItemDiscountRestController.addProviderItemDiscount(providerItemDiscount);
    }

    @Test(expectedExceptions = DataBaseException.class)
    public void testAddProviderItemDiscountEJBException() throws DataBaseException {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        doNothing().when(providerItemDiscountVerifier).verify(eq(providerItemDiscount));
        when(providerItemDiscountApiMapper.map(eq(providerItemDiscount))).thenReturn(providerItemDiscountDTO);
        when(providerItemDiscountService.addproviderItemDiscount(eq(providerItemDiscountDTO))).thenThrow(new EJBException());
        providerItemDiscountRestController.addProviderItemDiscount(providerItemDiscount);
    }

    @Test
    public void testGetProviderItemDiscount() throws DataBaseException {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(UUIDGenerator.getInstance().generateUUID());
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        doNothing().when(providerItemDiscountVerifier).verify(eq(providerItemDiscount));
        when(providerItemDiscountService.getProviderItemDiscount(eq(providerItemDiscountId.getId()))).thenReturn(providerItemDiscountDTO);
        when(providerItemDiscountApiMapper.map(eq(providerItemDiscountDTO))).thenReturn(providerItemDiscount);
        ProviderItemDiscount result = providerItemDiscountRestController.getProviderItemDiscount(providerItemDiscountId);
        assertEquals(result, providerItemDiscount);
        verify(providerItemDiscountService).getProviderItemDiscount(eq(providerItemDiscountId.getId()));
        verify(providerItemDiscountApiMapper).map(eq(providerItemDiscountDTO));
    }

}
