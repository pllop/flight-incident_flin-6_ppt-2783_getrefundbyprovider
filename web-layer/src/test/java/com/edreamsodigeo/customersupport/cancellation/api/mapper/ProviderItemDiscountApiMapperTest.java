package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemDiscountDTO;
import com.edreamsodigeo.customersupport.cancellation.v1.model.DiscountType;
import com.edreamsodigeo.customersupport.cancellation.v1.model.EmdId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemDiscountId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.VoucherId;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class ProviderItemDiscountApiMapperTest {

    private ProviderItemDiscountApiMapper providerItemDiscountApiMapper;

    @Mock
    private ProviderItemIdApiMapper providerItemIdApiMapper;
    @Mock
    private ProviderItemDiscountIdApiMapper providerItemDiscountIdApiMapper;
    @Mock
    private FlexibleTicketIdApiMapper flexibleTicketIdApiMapper;
    @Mock
    private VoucherIdApiMapper voucherIdApiMapper;
    @Mock
    private EmdIdApiMapper emdIdApiMapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        providerItemDiscountApiMapper = new ProviderItemDiscountApiMapper(providerItemIdApiMapper, providerItemDiscountIdApiMapper, flexibleTicketIdApiMapper, voucherIdApiMapper, emdIdApiMapper);
    }

    @Test
    public void testMap() {
        UUID uuid = new UUID(1L, 2L);
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        when(providerItemDiscountIdApiMapper.map(eq(uuid))).thenReturn(providerItemDiscountId);
        ProviderItemId providerItemId = new ProviderItemId();
        when(providerItemIdApiMapper.map(eq(uuid))).thenReturn(providerItemId);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        when(flexibleTicketIdApiMapper.map(eq(uuid))).thenReturn(flexibleTicketId);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        when(voucherIdApiMapper.map(eq(1L))).thenReturn(voucherId);
        EmdId emdId = new EmdId();
        emdId.setId(1L);
        when(emdIdApiMapper.map(eq(1L))).thenReturn(emdId);

        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        providerItemDiscountDTO.setId(uuid);
        providerItemDiscountDTO.setProviderItemId(uuid);
        providerItemDiscountDTO.setDiscountType("FLEXIBLE_TICKET");
        providerItemDiscountDTO.setFlexibleTicketId(uuid);
        providerItemDiscountDTO.setVoucherId(1L);
        providerItemDiscountDTO.setEmdId(1L);

        ProviderItemDiscount providerItemDiscount = providerItemDiscountApiMapper.map(providerItemDiscountDTO);
        assertEquals(providerItemDiscount.getId(), providerItemDiscountId);
        assertEquals(providerItemDiscount.getProviderItemId(), providerItemId);
        assertEquals(providerItemDiscount.getDiscountType(), DiscountType.FLEXIBLE_TICKET);
        assertEquals(providerItemDiscount.getFlexibleTicketId(), flexibleTicketId);
        assertEquals(providerItemDiscount.getVoucherId(), voucherId);
        assertEquals(providerItemDiscount.getEmdId(), emdId);
    }

    @Test
    public void testMapNullValues() {
        UUID uuid = new UUID(1L, 2L);
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        when(providerItemDiscountIdApiMapper.map(eq(uuid))).thenReturn(providerItemDiscountId);
        ProviderItemId providerItemId = new ProviderItemId();
        when(providerItemIdApiMapper.map(eq(uuid))).thenReturn(providerItemId);

        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        providerItemDiscountDTO.setId(uuid);
        providerItemDiscountDTO.setProviderItemId(uuid);
        providerItemDiscountDTO.setDiscountType("FLEXIBLE_TICKET");

        ProviderItemDiscount providerItemDiscount = providerItemDiscountApiMapper.map(providerItemDiscountDTO);
        assertEquals(providerItemDiscount.getId(), providerItemDiscountId);
        assertEquals(providerItemDiscount.getProviderItemId(), providerItemId);
        assertEquals(providerItemDiscount.getDiscountType(), DiscountType.FLEXIBLE_TICKET);
        assertNull(providerItemDiscount.getFlexibleTicketId());
        assertNull(providerItemDiscount.getVoucherId());
        assertNull(providerItemDiscount.getEmdId());
    }

    @Test
    public void testMapToDTO() {
        UUID uuid = new UUID(1L, 2L);
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        flexibleTicketId.setId(uuid);
        VoucherId voucherId = new VoucherId();
        voucherId.setId(1L);
        EmdId emdId = new EmdId();
        emdId.setId(1L);

        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.FLEXIBLE_TICKET);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscount.setEmdId(emdId);

        ProviderItemDiscountDTO providerItemDiscountDTO = providerItemDiscountApiMapper.map(providerItemDiscount);

        assertEquals(providerItemDiscountDTO.getId(), uuid);
        assertEquals(providerItemDiscountDTO.getProviderItemId(), uuid);
        assertEquals(providerItemDiscountDTO.getDiscountType(), DiscountType.FLEXIBLE_TICKET.name());
        assertEquals(providerItemDiscountDTO.getFlexibleTicketId(), uuid);
        assertEquals(providerItemDiscountDTO.getVoucherId(), Long.valueOf(1L));
        assertEquals(providerItemDiscountDTO.getEmdId(), Long.valueOf(1L));
    }

    @Test
    public void testMapToDTONullValues() {
        UUID uuid = new UUID(1L, 2L);
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        providerItemDiscountId.setId(uuid);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(uuid);
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        VoucherId voucherId = new VoucherId();
        EmdId emdId = new EmdId();

        providerItemDiscount.setId(providerItemDiscountId);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType(DiscountType.FLEXIBLE_TICKET);
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setVoucherId(voucherId);
        providerItemDiscount.setEmdId(emdId);

        ProviderItemDiscountDTO providerItemDiscountDTO = providerItemDiscountApiMapper.map(providerItemDiscount);

        assertEquals(providerItemDiscountDTO.getId(), uuid);
        assertEquals(providerItemDiscountDTO.getProviderItemId(), uuid);
        assertEquals(providerItemDiscountDTO.getDiscountType(), DiscountType.FLEXIBLE_TICKET.name());
        assertNull(providerItemDiscountDTO.getFlexibleTicketId());
        assertNull(providerItemDiscountDTO.getVoucherId());
        assertNull(providerItemDiscountDTO.getEmdId());
    }

}
