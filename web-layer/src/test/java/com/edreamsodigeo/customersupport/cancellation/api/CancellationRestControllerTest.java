package com.edreamsodigeo.customersupport.cancellation.api;

import com.edreamsodigeo.customersupport.cancellation.CancellationDTO;
import com.edreamsodigeo.customersupport.cancellation.CancellationService;
import com.edreamsodigeo.customersupport.cancellation.api.mapper.CancellationApiMapper;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.CancellationNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class CancellationRestControllerTest {

    private CancellationRestController controller;

    @Mock
    private CancellationApiMapper mapper;
    @Mock
    private CancellationService service;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new CancellationRestController(service, mapper);
    }

    @Test
    public void testGetCancellation() throws CancellationNotFoundException, RegisterNotFound {
        CancellationId id = new CancellationId(UUIDGenerator.getInstance().generateUUID().toString());

        CancellationDTO dto = new CancellationDTO();
        when(service.getCancellation(any())).thenReturn(dto);
        Cancellation response = new Cancellation();
        when(mapper.mapCancellationDTOToCancellation(dto)).thenReturn(response);
        Cancellation result = controller.getCancellation(id);
        assertSame(result, response);
        verify(service).getCancellation(id.getId());
        verify(mapper).mapCancellationDTOToCancellation(dto);
    }

    @Test(expectedExceptions = CancellationNotFoundException.class)
    public void testGetCancellationNotFound() throws RegisterNotFound, CancellationNotFoundException {
        when(service.getCancellation(any())).thenThrow(new RegisterNotFound("Mock Exception"));
        try {
            controller.getCancellation(new CancellationId(UUIDGenerator.getInstance().generateUUID().toString()));
        } catch (CancellationNotFoundException e) {
            verify(service).getCancellation(any());
            throw e;
        }
    }

    @Test
    public void testGetCancellationsByIncident() throws CancellationNotFoundException, RegisterNotFound {
        IncidentId id = new IncidentId(UUIDGenerator.getInstance().generateUUID().toString());

        List<CancellationDTO> dto = Collections.singletonList(new CancellationDTO());
        when(service.getCancellationsByIncident(any())).thenReturn(dto);
        Cancellation response = new Cancellation();
        when(mapper.mapCancellationDTOToCancellation(dto.get(0))).thenReturn(response);
        List<Cancellation> result = controller.getCancellationsByIncident(id);
        assertEquals(result.size(), 1);
        assertSame(result.get(0), response);
        verify(service).getCancellationsByIncident(id.getId());
        verify(mapper).mapCancellationDTOToCancellation(dto.get(0));
    }

    @Test(expectedExceptions = CancellationNotFoundException.class)
    public void testGetCancellationsByIncidentNotFound() throws RegisterNotFound, CancellationNotFoundException {
        when(service.getCancellationsByIncident(any())).thenThrow(new RegisterNotFound("Mock Exception"));
        try {
            controller.getCancellationsByIncident(new IncidentId(UUIDGenerator.getInstance().generateUUID().toString()));
        } catch (CancellationNotFoundException e) {
            verify(service).getCancellationsByIncident(any());
            throw e;
        }
    }

}
