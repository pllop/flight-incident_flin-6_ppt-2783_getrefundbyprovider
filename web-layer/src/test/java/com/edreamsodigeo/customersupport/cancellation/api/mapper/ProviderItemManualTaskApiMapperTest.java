package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskDTO;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemManualTaskId;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class ProviderItemManualTaskApiMapperTest {

    private ProviderItemManualTaskApiMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new ProviderItemManualTaskApiMapper();
    }

    @Test
    public void testMapDtoToCancelledItemManual() {
        ProviderItemManualTaskDTO dto = new ProviderItemManualTaskDTO();
        UUID cancelledItemId = UUID.randomUUID();
        UUID id = UUID.randomUUID();
        Long todId = 1L;

        dto.setTaskOnDemandId(todId);
        dto.setProviderItemId(cancelledItemId);
        dto.setId(id);
        ProviderItemManualTask result = mapper.mapDtoToProviderItemManualTask(dto);
        assertEquals(result.getProviderItemId().getId(), cancelledItemId);
        assertEquals(result.getId().getId(), id);
        assertEquals(result.getTaskOnDemandId(), todId);
    }

    @Test
    public void testMapProviderItemManualTaskToDto() {
        ProviderItemManualTask itemManual = new ProviderItemManualTask();

        Long todId = 1L;
        UUID id = UUID.randomUUID();
        UUID cancelledItemId = UUID.randomUUID();

        itemManual.setTaskOnDemandId(todId);
        itemManual.setId(new ProviderItemManualTaskId(id.toString()));
        itemManual.setProviderItemId(new ProviderItemId(cancelledItemId.toString()));

        ProviderItemManualTaskDTO result = mapper.mapProviderItemManualTaskToDto(itemManual);
        assertEquals(result.getId(), id);
        assertEquals(result.getProviderItemId(), cancelledItemId);
        assertEquals(result.getTaskOnDemandId(), todId);
        assertNull(result.getTimestamp());
    }

    @Test
    public void testMapProviderItemManualTaskToDtoNull() {
        ProviderItemManualTask itemManual = new ProviderItemManualTask();

        itemManual.setProviderItemId(new ProviderItemId());

        ProviderItemManualTaskDTO result = mapper.mapProviderItemManualTaskToDto(itemManual);
        assertNull(result.getId());
    }


}
