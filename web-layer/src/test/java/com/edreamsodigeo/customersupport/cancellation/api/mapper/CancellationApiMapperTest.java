package com.edreamsodigeo.customersupport.cancellation.api.mapper;

import com.edreamsodigeo.customersupport.cancellation.CancellationDTO;
import com.edreamsodigeo.customersupport.cancellation.ItemDTO;
import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemDTO;
import com.edreamsodigeo.customersupport.cancellation.v1.model.BookingItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Item;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Money;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderBookingItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.UserId;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CancellationApiMapperTest {
    private CancellationApiMapper cancellationApiMapper;

    @BeforeMethod
    public void setUp() {
        cancellationApiMapper = new CancellationApiMapper();
    }

    @Test
    public void testMapCancellationToCancellationDTO() {
        Cancellation cancellation = new Cancellation();
        cancellation.setId(new CancellationId(UUIDGenerator.getInstance().generateUUID().toString()));
        cancellation.setIncidentId(new IncidentId(UUIDGenerator.getInstance().generateUUID().toString()));
        cancellation.setUserId(new UserId("userId@email.com"));
        cancellation.setBuyerEmail("buyer@email.com");
        cancellation.setDiscarded(true);
        cancellation.setTimestamp(LocalDateTime.now());

        Item item = new Item();
        item.setId(new ItemId(UUIDGenerator.getInstance().generateUUID().toString()));
        item.setCancellationId(cancellation.getId());
        item.setBookingItemId(new BookingItemId("10"));
        cancellation.setItems(Collections.singletonList(item));

        ProviderItem providerItem = new ProviderItem();
        providerItem.setId(new ProviderItemId(UUIDGenerator.getInstance().generateUUID().toString()));
        providerItem.setProviderBookingItemId(new ProviderBookingItemId("20"));
        providerItem.setResolutionType("RESOLUTION_TYPE");
        providerItem.setTopUpVoucherAmount(new Money(BigDecimal.TEN, "COP"));
        providerItem.setTopUpVoucherMinBasketAmount(new Money(BigDecimal.ONE, "COP"));
        item.setProviderItems(Collections.singletonList(providerItem));

        CancellationDTO cancellationDTO = cancellationApiMapper.mapCancellationToCancellationDTO(cancellation);

        assertNotNull(cancellationDTO);
        assertEquals(cancellationDTO.getId(), cancellation.getId().getId());
        assertEquals(cancellationDTO.getIncidentId(), cancellation.getIncidentId().getId());
        assertEquals(cancellationDTO.getUserId(), cancellation.getUserId().getId());
        assertEquals(cancellationDTO.getBuyerEmail(), cancellation.getBuyerEmail());
        assertEquals(cancellationDTO.isDiscarded(), cancellation.getDiscarded());
        assertEquals(cancellationDTO.getTimestamp(), cancellation.getTimestamp());
        assertEquals(cancellationDTO.getItems().size(), cancellation.getItems().size());

        ItemDTO itemDTO = cancellationDTO.getItems().get(0);
        assertEquals(itemDTO.getId(), item.getId().getId());
        assertEquals(itemDTO.getBookingItemId(), item.getBookingItemId().getId());

        ProviderItemDTO providerItemDTO = itemDTO.getProviderItems().get(0);
        assertEquals(providerItemDTO.getId(), providerItem.getId().getId());
        assertEquals(providerItemDTO.getProviderBookingItemId(), providerItem.getProviderBookingItemId().getId());
        assertEquals(providerItemDTO.getResolutionType(), providerItem.getResolutionType());
        assertEquals(providerItemDTO.getTopUpVoucherAmount().getAmount(), providerItem.getTopUpVoucherAmount().getAmount());
        assertEquals(providerItemDTO.getTopUpVoucherAmount().getCurrency(), providerItem.getTopUpVoucherAmount().getCurrency());
        assertEquals(providerItemDTO.getTopUpVoucherMinBasketAmount().getAmount(), providerItem.getTopUpVoucherMinBasketAmount().getAmount());
        assertEquals(providerItemDTO.getTopUpVoucherMinBasketAmount().getCurrency(), providerItem.getTopUpVoucherMinBasketAmount().getCurrency());
    }

    @Test
    public void testMapCancellationToCancellationDTOWithOnlyRequiredInfo() {
        Cancellation cancellation = new Cancellation();
        cancellation.setIncidentId(new IncidentId(UUIDGenerator.getInstance().generateUUID().toString()));
        cancellation.setDiscarded(true);

        Item item = new Item();
        item.setCancellationId(cancellation.getId());
        item.setBookingItemId(new BookingItemId("10"));
        cancellation.setItems(Collections.singletonList(item));

        ProviderItem providerItem = new ProviderItem();
        providerItem.setProviderBookingItemId(new ProviderBookingItemId("20"));
        providerItem.setResolutionType("RESOLUTION_TYPE");
        item.setProviderItems(Collections.singletonList(providerItem));

        CancellationDTO cancellationDTO = cancellationApiMapper.mapCancellationToCancellationDTO(cancellation);

        assertNotNull(cancellationDTO);
        assertNotNull(cancellationDTO.getIncidentId());

        ItemDTO itemDTO = cancellationDTO.getItems().get(0);
        assertNotNull(itemDTO.getBookingItemId());

        ProviderItemDTO providerItemDTO = itemDTO.getProviderItems().get(0);
        assertNotNull(providerItemDTO.getProviderBookingItemId());
        assertNotNull(providerItemDTO.getResolutionType());
    }

    @Test
    public void testMapCancellationDTOToCancellation() {
        CancellationDTO cancellationDTO = new CancellationDTO();
        cancellationDTO.setId(UUIDGenerator.getInstance().generateUUID());
        cancellationDTO.setIncidentId(UUIDGenerator.getInstance().generateUUID());
        cancellationDTO.setUserId("customer@email.com");
        cancellationDTO.setDiscarded(true);

        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(UUIDGenerator.getInstance().generateUUID());
        itemDTO.setBookingItemId(10L);
        cancellationDTO.setItems(Collections.singletonList(itemDTO));

        ProviderItemDTO providerItemDTO = new ProviderItemDTO();
        providerItemDTO.setId(UUIDGenerator.getInstance().generateUUID());
        providerItemDTO.setProviderBookingItemId(20L);
        providerItemDTO.setResolutionType("RESOLUTION_TYPE");
        providerItemDTO.setTopUpVoucherAmount(new MoneyDTO(BigDecimal.TEN, "EUR"));
        providerItemDTO.setTopUpVoucherMinBasketAmount(new MoneyDTO(BigDecimal.ONE, "USD"));
        itemDTO.setProviderItems(Collections.singletonList(providerItemDTO));

        Cancellation cancellation = cancellationApiMapper.mapCancellationDTOToCancellation(cancellationDTO);

        assertNotNull(cancellation);
        assertEquals(cancellation.getId().getId(), cancellationDTO.getId());
        assertEquals(cancellation.getIncidentId().getId(), cancellationDTO.getIncidentId());
        assertEquals(cancellation.getUserId().getId(), cancellationDTO.getUserId());
        assertEquals(cancellation.getBuyerEmail(), cancellationDTO.getBuyerEmail());
        assertEquals(cancellation.getDiscarded(), cancellationDTO.isDiscarded());
        assertEquals(cancellation.getTimestamp(), cancellationDTO.getTimestamp());
        assertEquals(cancellation.getItems().size(), cancellationDTO.getItems().size());

        Item item = cancellation.getItems().get(0);
        assertEquals(item.getId().getId(), itemDTO.getId());
        assertEquals(item.getBookingItemId().getId(), itemDTO.getBookingItemId());

        ProviderItem providerItem = item.getProviderItems().get(0);
        assertEquals(providerItem.getProviderBookingItemId().getId(), providerItemDTO.getProviderBookingItemId());
        assertEquals(providerItem.getResolutionType(), providerItemDTO.getResolutionType());
        assertEquals(providerItem.getTopUpVoucherAmount().getAmount(), providerItemDTO.getTopUpVoucherAmount().getAmount());
        assertEquals(providerItem.getTopUpVoucherAmount().getCurrency(), providerItemDTO.getTopUpVoucherAmount().getCurrency());
        assertEquals(providerItem.getTopUpVoucherMinBasketAmount().getAmount(), providerItemDTO.getTopUpVoucherMinBasketAmount().getAmount());
    }

    @Test
    public void testMapCancellationDTOToCancellationWithOnlyRequiredInfo() {
        CancellationDTO cancellationDTO = new CancellationDTO();
        cancellationDTO.setId(UUIDGenerator.getInstance().generateUUID());
        cancellationDTO.setIncidentId(UUIDGenerator.getInstance().generateUUID());
        cancellationDTO.setDiscarded(true);
        cancellationDTO.setTimestamp(LocalDateTime.now());

        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(UUIDGenerator.getInstance().generateUUID());
        itemDTO.setBookingItemId(10L);
        cancellationDTO.setItems(Collections.singletonList(itemDTO));

        ProviderItemDTO providerItemDTO = new ProviderItemDTO();
        providerItemDTO.setId(UUIDGenerator.getInstance().generateUUID());
        providerItemDTO.setProviderBookingItemId(20L);
        providerItemDTO.setResolutionType("RESOLUTION_TYPE");
        itemDTO.setProviderItems(Collections.singletonList(providerItemDTO));

        Cancellation cancellation = cancellationApiMapper.mapCancellationDTOToCancellation(cancellationDTO);

        assertNotNull(cancellation);
        assertNotNull(cancellation.getId());
        assertNotNull(cancellation.getIncidentId());
        cancellation.getDiscarded();
        assertNotNull(cancellation.getTimestamp());

        Item item = cancellation.getItems().get(0);
        assertNotNull(item.getId());
        assertNotNull(item.getBookingItemId());

        ProviderItem providerItem = item.getProviderItems().get(0);
        assertNotNull(providerItem.getId());
        assertNotNull(providerItem.getProviderBookingItemId());
        assertNotNull(providerItem.getResolutionType());
    }
}
