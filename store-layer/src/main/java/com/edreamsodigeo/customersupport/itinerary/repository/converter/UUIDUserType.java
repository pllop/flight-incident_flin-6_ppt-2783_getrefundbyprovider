package com.edreamsodigeo.customersupport.itinerary.repository.converter;

import com.odigeo.commons.uuid.UUIDSerializer;
import org.apache.commons.lang.ArrayUtils;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.type.StandardBasicTypes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

public class UUIDUserType extends CustomUserType {
    private static final int[] SQL_TYPES = new int[]{Types.BINARY};

    @Override
    public int[] sqlTypes() {
        return SQL_TYPES.clone();
    }

    @Override
    public Class<?> returnedClass() {
        return UUID.class;
    }

    @Override
    public boolean equals(Object x, Object y) {
        if (x == y) {
            return true;
        }
        if (x == null || y == null) {
            return false;
        }
        UUID dtx = (UUID) x;
        UUID dty = (UUID) y;
        return dtx.equals(dty);
    }

    @Override
    public int hashCode(Object object) {
        return object.hashCode();
    }


    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner)
            throws SQLException {
        byte[] bytes = (byte[]) StandardBasicTypes.BINARY.nullSafeGet(resultSet, names, session, owner);
        if (bytes == null || ArrayUtils.isEmpty(bytes)) {
            return null;
        }
        return UUIDSerializer.fromBytes(bytes);
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SessionImplementor session)
            throws SQLException {
        if (value == null) {
            StandardBasicTypes.BINARY.nullSafeSet(preparedStatement, null, index, session);
        } else {
            UUID uuid = (UUID) value;
            byte[] bytes = UUIDSerializer.toBytes(uuid);
            StandardBasicTypes.BINARY.nullSafeSet(preparedStatement, bytes, index, session);
        }
    }

    @Override
    public Object fromXMLString(String string) {
        return UUID.fromString(string);
    }

}

