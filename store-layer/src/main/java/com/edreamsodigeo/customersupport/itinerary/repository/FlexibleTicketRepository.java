package com.edreamsodigeo.customersupport.itinerary.repository;

import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicketId;

public interface FlexibleTicketRepository {

    void insert(FlexibleTicket flexibleTicket);

    void update(FlexibleTicket flexibleTicket);

    FlexibleTicket findById(FlexibleTicketId flexibleTicketId);

}
