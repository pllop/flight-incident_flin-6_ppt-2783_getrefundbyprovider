package com.edreamsodigeo.customersupport.itinerary.repository.impl;

import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.itinerary.repository.FlexibleTicketRepository;

import javax.persistence.EntityManager;

public class JpaFlexibleTicketRepository implements FlexibleTicketRepository {

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void insert(FlexibleTicket flexibleTicket) {
        flexibleTicket.fillKeys();
        entityManager.persist(flexibleTicket);
    }

    @Override
    public void update(FlexibleTicket flexibleTicket) {
        entityManager.merge(flexibleTicket);
    }

    @Override
    public FlexibleTicket findById(FlexibleTicketId flexibleTicketId) {
        return entityManager.find(FlexibleTicket.class, flexibleTicketId);
    }
}
