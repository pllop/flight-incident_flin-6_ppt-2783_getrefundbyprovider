package com.edreamsodigeo.customersupport.itinerary.exception;

public class RegisterNotFoundException extends Exception {
    public RegisterNotFoundException(String s) {
        super(s);
    }

    public RegisterNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
