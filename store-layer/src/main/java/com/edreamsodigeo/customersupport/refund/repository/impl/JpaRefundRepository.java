package com.edreamsodigeo.customersupport.refund.repository.impl;

import com.edreamsodigeo.customersupport.refund.model.ProviderRefund;
import com.edreamsodigeo.customersupport.refund.model.ProviderRefundId;
import com.edreamsodigeo.customersupport.refund.model.Refund;
import com.edreamsodigeo.customersupport.refund.model.RefundId;
import com.edreamsodigeo.customersupport.refund.repository.RefundRepository;
import com.odigeo.commons.uuid.UUIDGenerator;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

public class JpaRefundRepository implements RefundRepository {

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void insert(Refund refund) {
        fillKeysOnRefund(refund);
        entityManager.persist(refund);
    }

    private void fillKeysOnRefund(Refund refund) {
        if (refund.getRefundId() == null || refund.getRefundId().getId() == null) {
            refund.setRefundId(new RefundId(UUIDGenerator.getInstance().generateUUID()));
        }
        fillKeysOnProviderRefunds(refund.getProviderRefunds());
    }

    @Override
    public void update(Refund refund) {
        fillKeysOnProviderRefunds(refund.getProviderRefunds());
        entityManager.merge(refund);
    }

    private void fillKeysOnProviderRefunds(List<ProviderRefund> providerRefunds) {
        if (providerRefunds != null) {
            providerRefunds.forEach(providerRefund -> {
                if (Objects.isNull(providerRefund.getId())) {
                    providerRefund.setId(new ProviderRefundId(UUIDGenerator.getInstance().generateUUID()));
                }
            });
        }
    }

    @Override
    public Refund findById(RefundId refundId) {
        return entityManager.find(Refund.class, refundId);
    }
}
