package com.edreamsodigeo.customersupport.refund.repository;

import com.edreamsodigeo.customersupport.refund.model.Refund;
import com.edreamsodigeo.customersupport.refund.model.RefundId;

public interface RefundRepository {

    void insert(Refund refund);

    void update(Refund refund);

    Refund findById(RefundId refundId);
}
