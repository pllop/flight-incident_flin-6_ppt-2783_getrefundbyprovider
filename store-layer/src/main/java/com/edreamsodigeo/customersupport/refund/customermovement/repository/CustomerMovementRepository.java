package com.edreamsodigeo.customersupport.refund.customermovement.repository;

import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovement;

public interface CustomerMovementRepository {
    void insert(CustomerMovement customerMovement);
}
