package com.edreamsodigeo.customersupport.refund.customermovement.repository.impl;

import com.edreamsodigeo.customersupport.refund.customermovement.repository.CustomerMovementRepository;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovement;
import javax.persistence.EntityManager;

public class JpaCustomerMovementRepository implements CustomerMovementRepository {

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void insert(CustomerMovement customerMovement) {
        entityManager.persist(customerMovement);
    }
}
