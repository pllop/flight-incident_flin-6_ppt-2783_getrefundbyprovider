package com.edreamsodigeo.customersupport.flightincident.exception;

public class RegisterNotFound extends Exception {
    public RegisterNotFound(String s) {
        super(s);
    }

    public RegisterNotFound(String s, Throwable throwable) {
        super(s, throwable);
    }
}
