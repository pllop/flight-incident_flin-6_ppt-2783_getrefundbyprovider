package com.edreamsodigeo.customersupport.flightincident.exception;

public class InsertFailed extends Exception {
    public InsertFailed(String s) {
        super(s);
    }

    public InsertFailed(String s, Throwable throwable) {
        super(s, throwable);
    }
}
