package com.edreamsodigeo.customersupport.flightincident.exception;

public class DatabaseInconsistency extends Exception {
    public DatabaseInconsistency(String s) {
        super(s);
    }
}
