package com.edreamsodigeo.customersupport.flightincident.repository.impl;

import com.edreamsodigeo.customersupport.flightincident.model.RefundReceived;
import com.edreamsodigeo.customersupport.flightincident.repository.RefundReceivedRepository;

import javax.persistence.EntityManager;

public class JpaRefundReceivedRepository implements RefundReceivedRepository {

    private EntityManager entityManger;


    @Override
    public void save(RefundReceived refundReceived) {
        entityManger.persist(refundReceived);
    }

    @Override
    public void setEntityManager(EntityManager entityManager) {
        this.entityManger = entityManager;
    }
}
