package com.edreamsodigeo.customersupport.flightincident.exception;

public class DuplicateRegister extends Exception {
    public DuplicateRegister(String s) {
        super(s);
    }

    public DuplicateRegister(String s, Throwable throwable) {
        super(s, throwable);
    }
}
