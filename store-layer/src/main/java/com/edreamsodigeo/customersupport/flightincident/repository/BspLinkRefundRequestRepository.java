package com.edreamsodigeo.customersupport.flightincident.repository;

import com.edreamsodigeo.customersupport.flightincident.model.BspLinkRefundRequest;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.List;

public interface BspLinkRefundRequestRepository {

    void save(BspLinkRefundRequest request);

    BspLinkRefundRequest findByTicketId(Long ticketId);

    BspLinkRefundRequest findByProviderBookingItemId(Long providerBookingItemId);

    List<BspLinkRefundRequest> findAllBetweenTimestamps(Instant start, Instant end);

    void setEntityManager(EntityManager entityManger);
}
