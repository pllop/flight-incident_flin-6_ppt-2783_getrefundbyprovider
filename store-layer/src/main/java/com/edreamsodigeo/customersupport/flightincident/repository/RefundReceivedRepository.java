package com.edreamsodigeo.customersupport.flightincident.repository;

import com.edreamsodigeo.customersupport.flightincident.model.RefundReceived;

import javax.persistence.EntityManager;

public interface RefundReceivedRepository {

    void save(RefundReceived refundReceived);

    void setEntityManager(EntityManager entityManager);
}
