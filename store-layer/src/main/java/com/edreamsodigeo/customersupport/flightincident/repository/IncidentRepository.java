package com.edreamsodigeo.customersupport.flightincident.repository;

import com.edreamsodigeo.customersupport.flightincident.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;

import java.util.List;

public interface IncidentRepository {

    Incident incidentOfIncidentId(IncidentId incidentId);

    List<Incident> incidentsOfBookingId(Long bookingId);

    List<Incident> incidentsOfBookingIdAndProviderBookingItemId(Long bookingId, Long providerBookingItemId);

    void save(Incident aIncident);

    List<Incident> incidentsWithoutCancellation(int pageNumber);
}
