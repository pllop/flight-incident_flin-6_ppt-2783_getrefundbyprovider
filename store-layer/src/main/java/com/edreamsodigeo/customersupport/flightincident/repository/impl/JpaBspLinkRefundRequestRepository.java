package com.edreamsodigeo.customersupport.flightincident.repository.impl;

import com.edreamsodigeo.customersupport.flightincident.model.BspLinkRefundRequest;
import com.edreamsodigeo.customersupport.flightincident.repository.BspLinkRefundRequestRepository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.util.List;

public class JpaBspLinkRefundRequestRepository implements BspLinkRefundRequestRepository {
    private EntityManager entityManager;

    @Override
    public BspLinkRefundRequest findByTicketId(Long ticketId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BspLinkRefundRequest> query = criteriaBuilder.createQuery(BspLinkRefundRequest.class);
        Root<BspLinkRefundRequest> from = query.from(BspLinkRefundRequest.class);
        query.select(from).where(criteriaBuilder.equal(from.get("ticketId"), ticketId));
        return entityManager.createQuery(query).getSingleResult();
    }

    @Override
    public BspLinkRefundRequest findByProviderBookingItemId(Long providerBookingItemId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BspLinkRefundRequest> query = criteriaBuilder.createQuery(BspLinkRefundRequest.class);
        Root<BspLinkRefundRequest> from = query.from(BspLinkRefundRequest.class);
        query.select(from).where(criteriaBuilder.equal(from.get("providerBookingItemId"), providerBookingItemId));
        return entityManager.createQuery(query).getSingleResult();
    }

    @Override
    public List<BspLinkRefundRequest> findAllBetweenTimestamps(Instant start, Instant end) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BspLinkRefundRequest> query = criteriaBuilder.createQuery(BspLinkRefundRequest.class);
        Root<BspLinkRefundRequest> from = query.from(BspLinkRefundRequest.class);
        query.select(query.from(BspLinkRefundRequest.class)).where(criteriaBuilder.between(from.get("requestInstant"), start, end));
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public void save(BspLinkRefundRequest request) {
        entityManager.persist(request);
    }


    @Override
    public void setEntityManager(EntityManager entityManger) {
        this.entityManager = entityManger;
    }
}
