package com.edreamsodigeo.customersupport.flightincident.repository.impl;

import com.edreamsodigeo.customersupport.flightincident.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.repository.IncidentRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class JpaIncidentRepository implements IncidentRepository {

    private EntityManager entityManager;

    private int pageSize = 5000;

    @Override
    public Incident incidentOfIncidentId(IncidentId incidentId) {
        return entityManager.find(Incident.class, incidentId);
    }

    @Override
    public List<Incident> incidentsOfBookingId(Long bookingId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Incident> criteriaQuery = criteriaBuilder.createQuery(Incident.class);
        Root<Incident> root = criteriaQuery.from(Incident.class);
        criteriaQuery.select(root)
                .where(criteriaBuilder.equal(root.get("bookingId"), bookingId));
        return new ArrayList<>(entityManager.createQuery(criteriaQuery).getResultList());
    }

    @Override
    public List<Incident> incidentsOfBookingIdAndProviderBookingItemId(Long bookingId, Long providerBookingItemId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Incident> criteriaQuery = criteriaBuilder.createQuery(Incident.class);
        Root<Incident> root = criteriaQuery.from(Incident.class);
        criteriaQuery.select(root)
                .where(criteriaBuilder.and(
                        criteriaBuilder.equal(root.get("bookingId"), bookingId),
                        criteriaBuilder.equal(root.get("providerBookingItemId"), providerBookingItemId)
                ));
        return new ArrayList<>(entityManager.createQuery(criteriaQuery).getResultList());
    }

    @Override
    public void save(Incident aIncident) {
        entityManager.persist(aIncident);
    }

    @Override
    public List<Incident> incidentsWithoutCancellation(int pageNumber) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Incident> criteriaQuery = criteriaBuilder.createQuery(Incident.class);
        Root<Incident> root = criteriaQuery.from(Incident.class);
        criteriaQuery.select(root)
                .where(criteriaBuilder.isNull(root.get("cancellationId")));
        TypedQuery<Incident> query = entityManager.createQuery(criteriaQuery);
        query.setFirstResult((pageNumber - 1) * pageSize);
        query.setMaxResults(pageSize);
        return new ArrayList<>(query.getResultList());
    }


    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
