package com.edreamsodigeo.customersupport.flightincident.configuration;

import com.edreamsodigeo.customersupport.cancellation.repository.CancellationRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemDiscountRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemManualTaskRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemMerchantRefundRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemNonMerchantRefundRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaCancellationRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemDiscountRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemManualTaskRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemMerchantRefundRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemNonMerchantRefundRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.BspLinkRefundRequestRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.IncidentRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.RefundReceivedRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaBspLinkRefundRequestRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaIncidentRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaRefundReceivedRepository;
import com.edreamsodigeo.customersupport.itinerary.repository.FlexibleTicketRepository;
import com.edreamsodigeo.customersupport.itinerary.repository.impl.JpaFlexibleTicketRepository;
import com.edreamsodigeo.customersupport.refund.customermovement.repository.CustomerMovementRepository;
import com.edreamsodigeo.customersupport.refund.customermovement.repository.impl.JpaCustomerMovementRepository;
import com.edreamsodigeo.customersupport.refund.repository.RefundRepository;
import com.edreamsodigeo.customersupport.refund.repository.impl.JpaRefundRepository;
import com.google.inject.AbstractModule;

/**
 * This class configures bindings for dependency injection.
 */
public class StoreLayerModule extends AbstractModule {

    @Override
    public void configure() {
        configureResourceLocator();
    }

    private void configureResourceLocator() {
        bind(IncidentRepository.class).to(JpaIncidentRepository.class);
        bind(CancellationRepository.class).to(JpaCancellationRepository.class);
        bind(ProviderItemDiscountRepository.class).to(JpaProviderItemDiscountRepository.class);
        bind(FlexibleTicketRepository.class).to(JpaFlexibleTicketRepository.class);
        bind(RefundRepository.class).to(JpaRefundRepository.class);
        bind(ProviderItemManualTaskRepository.class).to(JpaProviderItemManualTaskRepository.class);
        bind(ProviderItemMerchantRefundRepository.class).to(JpaProviderItemMerchantRefundRepository.class);
        bind(ProviderItemNonMerchantRefundRepository.class).to(JpaProviderItemNonMerchantRefundRepository.class);
        bind(CustomerMovementRepository.class).to(JpaCustomerMovementRepository.class);
        bind(BspLinkRefundRequestRepository.class).to(JpaBspLinkRefundRequestRepository.class);
        bind(RefundReceivedRepository.class).to(JpaRefundReceivedRepository.class);
    }

}
