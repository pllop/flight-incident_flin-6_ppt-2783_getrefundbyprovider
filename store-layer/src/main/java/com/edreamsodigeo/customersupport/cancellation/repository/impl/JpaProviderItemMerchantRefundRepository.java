package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefundId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemMerchantRefundRepository;
import com.odigeo.commons.uuid.UUIDGenerator;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class JpaProviderItemMerchantRefundRepository implements ProviderItemMerchantRefundRepository {

    private EntityManager entityManager;

    @Override
    public void insert(ProviderItemMerchantRefund providerItemMerchantRefund) {
        if (providerItemMerchantRefund.getId() == null || providerItemMerchantRefund.getId().getId() == null) {
            providerItemMerchantRefund.setId(new ProviderItemMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));
        }
        entityManager.persist(providerItemMerchantRefund);
    }

    @Override
    public void update(ProviderItemMerchantRefund providerItemMerchantRefund) {
        if (providerItemMerchantRefund.getId() == null || providerItemMerchantRefund.getId().getId() == null) {
            providerItemMerchantRefund.setId(new ProviderItemMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));
        }
        entityManager.merge(providerItemMerchantRefund);
    }

    @Override
    public ProviderItemMerchantRefund findById(ProviderItemMerchantRefundId providerItemMerchantRefundId) {
        return entityManager.find(ProviderItemMerchantRefund.class, providerItemMerchantRefundId);
    }

    @Override
    public ProviderItemMerchantRefund findByProviderItemId(ProviderItemId providerItemId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProviderItemMerchantRefund> criteria = builder.createQuery(ProviderItemMerchantRefund.class);
        Root<ProviderItemMerchantRefund> from = criteria.from(ProviderItemMerchantRefund.class);
        criteria.select(from).where(builder.equal(from.get("providerItemId"), providerItemId));
        return entityManager.createQuery(criteria).getSingleResult();
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
