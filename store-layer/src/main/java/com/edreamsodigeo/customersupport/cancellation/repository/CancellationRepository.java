package com.edreamsodigeo.customersupport.cancellation.repository;

import com.edreamsodigeo.customersupport.cancellation.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.model.Item;

import java.util.List;

public interface CancellationRepository {
    void insert(Cancellation cancellation);

    void update(Cancellation cancellation);

    Cancellation findById(CancellationId cancellationId);

    List<Cancellation> findByIncidentId(IncidentId incidentId);

    void delete(Item item);
}
