package com.edreamsodigeo.customersupport.cancellation.repository;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTaskId;

public interface ProviderItemManualTaskRepository {
    void save(ProviderItemManualTask providerItemManualTask);

    ProviderItemManualTask findById(ProviderItemManualTaskId providerItemManualTaskId);
}
