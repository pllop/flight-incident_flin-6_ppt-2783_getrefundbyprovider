package com.edreamsodigeo.customersupport.cancellation.repository;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefundId;

public interface ProviderItemMerchantRefundRepository {
    void insert(ProviderItemMerchantRefund providerItemMerchantRefund);

    void update(ProviderItemMerchantRefund providerItemMerchantRefund);

    ProviderItemMerchantRefund findById(ProviderItemMerchantRefundId providerItemMerchantRefundId);

    ProviderItemMerchantRefund findByProviderItemId(ProviderItemId providerItemId);

}
