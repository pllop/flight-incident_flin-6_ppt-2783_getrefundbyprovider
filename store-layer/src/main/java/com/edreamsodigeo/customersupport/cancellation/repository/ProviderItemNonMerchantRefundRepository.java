package com.edreamsodigeo.customersupport.cancellation.repository;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefundId;

public interface ProviderItemNonMerchantRefundRepository {
    void insert(ProviderItemNonMerchantRefund providerItemNonMerchantRefund);

    void update(ProviderItemNonMerchantRefund providerItemNonMerchantRefund);

    ProviderItemNonMerchantRefund findById(ProviderItemNonMerchantRefundId providerItemNonMerchantRefundId);

    ProviderItemNonMerchantRefund findByProviderItemId(ProviderItemId providerItemId);

}
