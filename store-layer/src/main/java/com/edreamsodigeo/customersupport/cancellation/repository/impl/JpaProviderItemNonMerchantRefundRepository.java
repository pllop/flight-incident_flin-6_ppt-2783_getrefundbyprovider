package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefundId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemNonMerchantRefundRepository;
import com.odigeo.commons.uuid.UUIDGenerator;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class JpaProviderItemNonMerchantRefundRepository implements ProviderItemNonMerchantRefundRepository {

    private EntityManager entityManager;

    @Override
    public void insert(ProviderItemNonMerchantRefund providerItemNonMerchantRefund) {
        if (providerItemNonMerchantRefund.getId() == null || providerItemNonMerchantRefund.getId().getId() == null) {
            providerItemNonMerchantRefund.setId(new ProviderItemNonMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));
        }
        entityManager.persist(providerItemNonMerchantRefund);
    }

    @Override
    public void update(ProviderItemNonMerchantRefund providerItemNonMerchantRefund) {
        if (providerItemNonMerchantRefund.getId() == null || providerItemNonMerchantRefund.getId().getId() == null) {
            providerItemNonMerchantRefund.setId(new ProviderItemNonMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));
        }
        entityManager.merge(providerItemNonMerchantRefund);
    }

    @Override
    public ProviderItemNonMerchantRefund findById(ProviderItemNonMerchantRefundId providerItemNonMerchantRefundId) {
        return entityManager.find(ProviderItemNonMerchantRefund.class, providerItemNonMerchantRefundId);
    }

    @Override
    public ProviderItemNonMerchantRefund findByProviderItemId(ProviderItemId providerItemId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProviderItemNonMerchantRefund> criteria = builder.createQuery(ProviderItemNonMerchantRefund.class);
        Root<ProviderItemNonMerchantRefund> from = criteria.from(ProviderItemNonMerchantRefund.class);
        criteria.select(from).where(builder.equal(from.get("providerItemId"), providerItemId));
        return entityManager.createQuery(criteria).getSingleResult();
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
