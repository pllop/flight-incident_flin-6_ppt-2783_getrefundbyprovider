package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTaskId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemManualTaskRepository;
import com.odigeo.commons.uuid.UUIDGenerator;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

public class JpaProviderItemManualTaskRepository implements ProviderItemManualTaskRepository {

    private EntityManager entityManager;

    @Override
    public void save(ProviderItemManualTask providerItemManualTask) {
        if (providerItemManualTask.getId() == null || providerItemManualTask.getId().getId() == null) {
            providerItemManualTask.setId(new ProviderItemManualTaskId(UUIDGenerator.getInstance().generateUUID()));
        }
        providerItemManualTask.setTimestamp(LocalDateTime.now());
        entityManager.persist(providerItemManualTask);
    }

    @Override
    public ProviderItemManualTask findById(ProviderItemManualTaskId providerItemManualTaskId) {
        return entityManager.find(ProviderItemManualTask.class, providerItemManualTaskId);
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
