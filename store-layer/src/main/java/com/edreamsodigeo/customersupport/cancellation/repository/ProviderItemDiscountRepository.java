package com.edreamsodigeo.customersupport.cancellation.repository;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscountId;

public interface ProviderItemDiscountRepository {
    void insert(ProviderItemDiscount providerItemDiscount);
    ProviderItemDiscount findById(ProviderItemDiscountId providerItemDiscountId);
}
