package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscountId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemDiscountRepository;
import com.odigeo.commons.uuid.UUIDGenerator;

import javax.persistence.EntityManager;

public class JpaProviderItemDiscountRepository implements ProviderItemDiscountRepository {

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void insert(ProviderItemDiscount providerItemDiscount) {
        fillKeysOnProviderItemDiscount(providerItemDiscount);
        entityManager.persist(providerItemDiscount);
    }

    @Override
    public ProviderItemDiscount findById(ProviderItemDiscountId providerItemDiscountId) {
        return entityManager.find(ProviderItemDiscount.class, providerItemDiscountId);
    }

    private void fillKeysOnProviderItemDiscount(ProviderItemDiscount providerItemDiscount) {
        if (providerItemDiscount.getId() == null || providerItemDiscount.getId().getId() == null) {
            providerItemDiscount.setId(new ProviderItemDiscountId(UUIDGenerator.getInstance().generateUUID()));
        }
    }
}
