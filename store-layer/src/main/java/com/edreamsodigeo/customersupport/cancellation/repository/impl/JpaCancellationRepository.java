package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.model.Item;
import com.edreamsodigeo.customersupport.cancellation.model.ItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.repository.CancellationRepository;
import com.odigeo.commons.uuid.UUIDGenerator;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class JpaCancellationRepository implements CancellationRepository {

    private EntityManager entityManager;

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void insert(Cancellation cancellation) {
        fillKeysOnCancellation(cancellation);
        cancellation.setTimestamp(LocalDateTime.now());
        entityManager.persist(cancellation);
    }

    private void fillKeysOnCancellation(Cancellation cancellation) {
        if (cancellation.getId() == null || cancellation.getId().getId() == null) {
            cancellation.setId(new CancellationId(UUIDGenerator.getInstance().generateUUID()));
        }
        fillKeysOnItems(cancellation.getItems());
    }

    @Override
    public void update(Cancellation cancellation) {
        fillKeysOnItems(cancellation.getItems());
        cancellation.setTimestamp(LocalDateTime.now());
        entityManager.merge(cancellation);
    }

    private void fillKeysOnItems(List<Item> items) {
        if (items != null) {
            items.forEach(item -> {
                if (Objects.isNull(item.getId())) {
                    item.setId(new ItemId(UUIDGenerator.getInstance().generateUUID()));
                }
                fillKeysOnProviderItems(item.getProviderItems());
            });
        }
    }

    private void fillKeysOnProviderItems(List<ProviderItem> providerItems) {
        if (providerItems != null) {
            providerItems.forEach(providerItem -> {
                if (Objects.isNull(providerItem.getId())) {
                    providerItem.setId(new ProviderItemId(UUIDGenerator.getInstance().generateUUID()));
                }
            });
        }
    }

    @Override
    public Cancellation findById(CancellationId cancellationId) {
        return entityManager.find(Cancellation.class, cancellationId);
    }

    @Override
    public List<Cancellation> findByIncidentId(IncidentId incidentId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Cancellation> criteria = builder.createQuery(Cancellation.class);
        Root<Cancellation> from = criteria.from(Cancellation.class);
        criteria.select(from).where(builder.equal(from.get("incidentId"), incidentId));
        return entityManager.createQuery(criteria).getResultList();
    }

    @Override
    public void delete(Item item) {
        entityManager.remove(item);
    }
}
