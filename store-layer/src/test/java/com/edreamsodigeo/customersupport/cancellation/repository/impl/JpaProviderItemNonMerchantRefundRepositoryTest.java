package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefundId;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class JpaProviderItemNonMerchantRefundRepositoryTest {
    private JpaProviderItemNonMerchantRefundRepository providerItemNonMerchantRefundRepository;

    @Mock
    private EntityManager entityManager;

    @BeforeMethod
    private void setUp() {
        initMocks(this);
        providerItemNonMerchantRefundRepository = new JpaProviderItemNonMerchantRefundRepository();
        providerItemNonMerchantRefundRepository.setEntityManager(entityManager);
    }

    @Test
    public void saveOk() {
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = new ProviderItemNonMerchantRefund();
        providerItemNonMerchantRefund.setId(new ProviderItemNonMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));
        doNothing().when(entityManager).persist(any());

        providerItemNonMerchantRefundRepository.insert(providerItemNonMerchantRefund);

        assertNotNull(providerItemNonMerchantRefund.getId());
        verify(entityManager).persist(eq(providerItemNonMerchantRefund));
    }

    @Test
    public void saveOkWithNullProviderItemNonMerchantRefundId() {
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = new ProviderItemNonMerchantRefund();
        doNothing().when(entityManager).persist(any());

        providerItemNonMerchantRefundRepository.insert(providerItemNonMerchantRefund);

        assertNotNull(providerItemNonMerchantRefund.getId());
        verify(entityManager).persist(eq(providerItemNonMerchantRefund));
    }

    @Test
    public void saveOkWithEmptyProviderItemNonMerchantRefundId() {
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = new ProviderItemNonMerchantRefund();
        providerItemNonMerchantRefund.setId(new ProviderItemNonMerchantRefundId());
        doNothing().when(entityManager).persist(any());

        providerItemNonMerchantRefundRepository.insert(providerItemNonMerchantRefund);

        assertNotNull(providerItemNonMerchantRefund.getId());
        verify(entityManager).persist(eq(providerItemNonMerchantRefund));
    }

    @Test
    public void updateOk() {
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = new ProviderItemNonMerchantRefund();
        providerItemNonMerchantRefund.setId(new ProviderItemNonMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));

        providerItemNonMerchantRefundRepository.update(providerItemNonMerchantRefund);

        assertNotNull(providerItemNonMerchantRefund.getId());
        verify(entityManager).merge(eq(providerItemNonMerchantRefund));
    }

    @Test
    public void updateOkWithNullProviderItemNonMerchantRefundId() {
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = new ProviderItemNonMerchantRefund();

        providerItemNonMerchantRefundRepository.update(providerItemNonMerchantRefund);

        assertNotNull(providerItemNonMerchantRefund.getId());
        verify(entityManager).merge(eq(providerItemNonMerchantRefund));
    }

    @Test
    public void updateOkWithEmptyProviderItemNonMerchantRefundId() {
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = new ProviderItemNonMerchantRefund();
        providerItemNonMerchantRefund.setId(new ProviderItemNonMerchantRefundId());

        providerItemNonMerchantRefundRepository.update(providerItemNonMerchantRefund);

        assertNotNull(providerItemNonMerchantRefund.getId());
        verify(entityManager).merge(eq(providerItemNonMerchantRefund));
    }

    @Test
    public void getFindById() {
        ProviderItemNonMerchantRefundId providerItemNonMerchantRefundId = new ProviderItemNonMerchantRefundId();
        when(entityManager.find(any(), any())).thenReturn(new ProviderItemNonMerchantRefund());

        ProviderItemNonMerchantRefund providerItemMerchantRefund = providerItemNonMerchantRefundRepository.findById(providerItemNonMerchantRefundId);

        assertNotNull(providerItemMerchantRefund);
    }

    @Test
    public void testFindByProviderItemId() {
        CriteriaBuilder builder = Mockito.mock(CriteriaBuilder.class);
        CriteriaQuery<ProviderItemNonMerchantRefund> criteria = Mockito.mock(CriteriaQuery.class);
        Root<ProviderItemNonMerchantRefund> from = Mockito.mock(Root.class);
        when(entityManager.getCriteriaBuilder()).thenReturn(builder);
        when(builder.createQuery(ProviderItemNonMerchantRefund.class)).thenReturn(criteria);
        when(criteria.from(ProviderItemNonMerchantRefund.class)).thenReturn(from);
        TypedQuery<ProviderItemNonMerchantRefund> query = Mockito.mock(TypedQuery.class);
        when(entityManager.createQuery(criteria)).thenReturn(query);
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = new ProviderItemNonMerchantRefund();
        when(query.getSingleResult()).thenReturn(providerItemNonMerchantRefund);
        when(criteria.select(any())).thenReturn(criteria);
        when(criteria.where()).thenReturn(criteria);

        UUID providerItemUuid = UUID.randomUUID();
        ProviderItemId providerItemId = new ProviderItemId(providerItemUuid);
        ProviderItemNonMerchantRefund result = providerItemNonMerchantRefundRepository.findByProviderItemId(providerItemId);
        assertSame(result, providerItemNonMerchantRefund);

        verify(entityManager).getCriteriaBuilder();
        verify(builder).createQuery(ProviderItemNonMerchantRefund.class);
        verify(criteria).from(ProviderItemNonMerchantRefund.class);
        verify(criteria).select(from);
        verify(entityManager).createQuery(criteria);
        verify(query).getSingleResult();
    }

}
