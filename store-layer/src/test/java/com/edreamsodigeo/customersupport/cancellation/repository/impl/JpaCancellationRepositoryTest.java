package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.model.Item;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItem;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class JpaCancellationRepositoryTest {
    private JpaCancellationRepository cancellationRepository;

    @Mock
    private EntityManager entityManager;

    @BeforeMethod
    private void setUp() {
        initMocks(this);
        cancellationRepository = new JpaCancellationRepository();
        cancellationRepository.setEntityManager(entityManager);
    }

    @Test
    public void saveOk() {
        Cancellation cancellation = new Cancellation();
        cancellation.setId(new CancellationId(UUIDGenerator.getInstance().generateUUID()));
        doNothing().when(entityManager).persist(any());

        cancellationRepository.insert(cancellation);

        assertNotNull(cancellation.getId());
        assertNotNull(cancellation.getTimestamp());
        verify(entityManager).persist(eq(cancellation));
    }

    @Test
    public void saveOkWithNullCancellationId() {
        Cancellation cancellation = new Cancellation();
        doNothing().when(entityManager).persist(any());

        cancellationRepository.insert(cancellation);

        assertNotNull(cancellation.getId());
        assertNotNull(cancellation.getTimestamp());
        verify(entityManager).persist(eq(cancellation));
    }

    @Test
    public void saveOkWithEmptyCancellationId() {
        Cancellation cancellation = new Cancellation();
        cancellation.setId(new CancellationId());
        doNothing().when(entityManager).persist(any());

        cancellationRepository.insert(cancellation);

        assertNotNull(cancellation.getId());
        assertNotNull(cancellation.getTimestamp());
        verify(entityManager).persist(eq(cancellation));
    }

    @Test
    public void testUpdateOk() {
        Cancellation cancellation = new Cancellation();
        cancellation.setId(new CancellationId(UUIDGenerator.getInstance().generateUUID()));
        cancellation.setItems(Collections.singletonList(new Item()));
        cancellation.getItems().get(0).setProviderItems(Collections.singletonList(new ProviderItem()));
        doNothing().when(entityManager).persist(any());

        cancellationRepository.update(cancellation);

        assertNotNull(cancellation.getId());
        assertNotNull(cancellation.getTimestamp());
        assertNotNull(cancellation.getItems().get(0).getId());
        assertNotNull(cancellation.getItems().get(0).getProviderItems().get(0).getId());
        verify(entityManager).merge(eq(cancellation));
    }

    @Test
    public void testFindById() {
        CancellationId cancellationId = new CancellationId();
        when(entityManager.find(any(), any())).thenReturn(new Cancellation());

        Cancellation cancellation = cancellationRepository.findById(cancellationId);

        assertNotNull(cancellation);
    }

    @Test
    public void testFindByIncidentId() {
        CriteriaBuilder builder = Mockito.mock(CriteriaBuilder.class);
        CriteriaQuery<Cancellation> criteria = Mockito.mock(CriteriaQuery.class);
        Root<Cancellation> from = Mockito.mock(Root.class);
        when(entityManager.getCriteriaBuilder()).thenReturn(builder);
        when(builder.createQuery(Cancellation.class)).thenReturn(criteria);
        when(criteria.from(Cancellation.class)).thenReturn(from);
        TypedQuery<Cancellation> query = Mockito.mock(TypedQuery.class);
        when(entityManager.createQuery(criteria)).thenReturn(query);
        List<Cancellation> resultList = Collections.emptyList();
        when(query.getResultList()).thenReturn(resultList);
        when(criteria.select(any())).thenReturn(criteria);
        when(criteria.where()).thenReturn(criteria);

        UUID incidentUuid = UUID.randomUUID();
        IncidentId incidentId = new IncidentId(incidentUuid);
        List<Cancellation> result = cancellationRepository.findByIncidentId(incidentId);
        assertSame(result, resultList);

        verify(entityManager).getCriteriaBuilder();
        verify(builder).createQuery(Cancellation.class);
        verify(criteria).from(Cancellation.class);
        verify(criteria).select(from);
        verify(entityManager).createQuery(criteria);
        verify(query).getResultList();
    }

}
