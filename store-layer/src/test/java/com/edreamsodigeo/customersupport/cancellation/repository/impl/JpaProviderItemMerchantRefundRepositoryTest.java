package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefundId;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertSame;

public class JpaProviderItemMerchantRefundRepositoryTest {
    private JpaProviderItemMerchantRefundRepository providerItemMerchantRefundRepository;

    @Mock
    private EntityManager entityManager;

    @BeforeMethod
    private void setUp() {
        initMocks(this);
        providerItemMerchantRefundRepository = new JpaProviderItemMerchantRefundRepository();
        providerItemMerchantRefundRepository.setEntityManager(entityManager);
    }

    @Test
    public void saveOk() {
        ProviderItemMerchantRefund providerItemMerchantRefund = new ProviderItemMerchantRefund();
        providerItemMerchantRefund.setId(new ProviderItemMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));
        doNothing().when(entityManager).persist(any());

        providerItemMerchantRefundRepository.insert(providerItemMerchantRefund);

        assertNotNull(providerItemMerchantRefund.getId());
        verify(entityManager).persist(eq(providerItemMerchantRefund));
    }

    @Test
    public void saveOkWithNullProviderItemMerchantRefundId() {
        ProviderItemMerchantRefund providerItemMerchantRefund = new ProviderItemMerchantRefund();
        doNothing().when(entityManager).persist(any());

        providerItemMerchantRefundRepository.insert(providerItemMerchantRefund);

        assertNotNull(providerItemMerchantRefund.getId());
        verify(entityManager).persist(eq(providerItemMerchantRefund));
    }

    @Test
    public void saveOkWithEmptyProviderItemMerchantRefundId() {
        ProviderItemMerchantRefund providerItemMerchantRefund = new ProviderItemMerchantRefund();
        providerItemMerchantRefund.setId(new ProviderItemMerchantRefundId());
        doNothing().when(entityManager).persist(any());

        providerItemMerchantRefundRepository.insert(providerItemMerchantRefund);

        assertNotNull(providerItemMerchantRefund.getId());
        verify(entityManager).persist(eq(providerItemMerchantRefund));
    }

    @Test
    public void updateOk() {
        ProviderItemMerchantRefund providerItemMerchantRefund = new ProviderItemMerchantRefund();
        providerItemMerchantRefund.setId(new ProviderItemMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));

        providerItemMerchantRefundRepository.update(providerItemMerchantRefund);

        assertNotNull(providerItemMerchantRefund.getId());
        verify(entityManager).merge(eq(providerItemMerchantRefund));
    }

    @Test
    public void updateOkWithNullProviderItemMerchantRefundId() {
        ProviderItemMerchantRefund providerItemMerchantRefund = new ProviderItemMerchantRefund();
        doNothing().when(entityManager).persist(any());

        providerItemMerchantRefundRepository.update(providerItemMerchantRefund);

        assertNotNull(providerItemMerchantRefund.getId());
        verify(entityManager).merge(eq(providerItemMerchantRefund));
    }

    @Test
    public void updateOkWithEmptyProviderItemMerchantRefundId() {
        ProviderItemMerchantRefund providerItemMerchantRefund = new ProviderItemMerchantRefund();
        providerItemMerchantRefund.setId(new ProviderItemMerchantRefundId());

        providerItemMerchantRefundRepository.update(providerItemMerchantRefund);

        assertNotNull(providerItemMerchantRefund.getId());
        verify(entityManager).merge(eq(providerItemMerchantRefund));
    }

    @Test
    public void getFindById() {
        ProviderItemMerchantRefundId providerItemMerchantRefundId = new ProviderItemMerchantRefundId();
        when(entityManager.find(any(), any())).thenReturn(new ProviderItemMerchantRefund());

        ProviderItemMerchantRefund providerItemMerchantRefund = providerItemMerchantRefundRepository.findById(providerItemMerchantRefundId);

        assertNotNull(providerItemMerchantRefund);
    }

    @Test
    public void testFindByProviderItemId() {
        CriteriaBuilder builder = Mockito.mock(CriteriaBuilder.class);
        CriteriaQuery<ProviderItemMerchantRefund> criteria = Mockito.mock(CriteriaQuery.class);
        Root<ProviderItemMerchantRefund> from = Mockito.mock(Root.class);
        when(entityManager.getCriteriaBuilder()).thenReturn(builder);
        when(builder.createQuery(ProviderItemMerchantRefund.class)).thenReturn(criteria);
        when(criteria.from(ProviderItemMerchantRefund.class)).thenReturn(from);
        TypedQuery<ProviderItemMerchantRefund> query = Mockito.mock(TypedQuery.class);
        when(entityManager.createQuery(criteria)).thenReturn(query);
        ProviderItemMerchantRefund resultProviderItemMerchantRefund = new ProviderItemMerchantRefund();
        when(query.getSingleResult()).thenReturn(resultProviderItemMerchantRefund);
        when(criteria.select(any())).thenReturn(criteria);
        when(criteria.where()).thenReturn(criteria);

        UUID providerItemUuid = UUID.randomUUID();
        ProviderItemId providerItemId = new ProviderItemId(providerItemUuid);
        ProviderItemMerchantRefund result = providerItemMerchantRefundRepository.findByProviderItemId(providerItemId);
        assertSame(result, resultProviderItemMerchantRefund);

        verify(entityManager).getCriteriaBuilder();
        verify(builder).createQuery(ProviderItemMerchantRefund.class);
        verify(criteria).from(ProviderItemMerchantRefund.class);
        verify(criteria).select(from);
        verify(entityManager).createQuery(criteria);
        verify(query).getSingleResult();
    }

}
