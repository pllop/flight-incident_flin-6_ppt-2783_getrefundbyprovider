package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTaskId;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNotSame;

public class JpaProviderItemManualTaskRepositoryTest {

    private JpaProviderItemManualTaskRepository repository;

    @Mock
    private EntityManager em;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repository = new JpaProviderItemManualTaskRepository();
        repository.setEntityManager(em);
    }

    @Test
    public void testSaveFullId() {
        ProviderItemManualTask entity = new ProviderItemManualTask();
        ProviderItemManualTaskId id = new ProviderItemManualTaskId(UUID.randomUUID());
        LocalDateTime timestamp = LocalDateTime.now();
        entity.setId(id);
        entity.setTimestamp(timestamp);
        repository.save(entity);
        verify(em).persist(entity);
        assertNotSame(entity.getTimestamp(), timestamp);
    }

    @Test
    public void testSavePartialId() {
        ProviderItemManualTask entity = new ProviderItemManualTask();
        ProviderItemManualTaskId id = new ProviderItemManualTaskId();
        entity.setId(id);
        repository.save(entity);
        verify(em).persist(entity);
        assertNotNull(entity.getId().getId());
    }

    @Test
    public void testSaveNoId() {
        ProviderItemManualTask entity = new ProviderItemManualTask();
        repository.save(entity);
        verify(em).persist(entity);
        assertNotNull(entity.getId());
        assertNotNull(entity.getId().getId());
    }
}
