package com.edreamsodigeo.customersupport.cancellation.repository.impl;

import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscountId;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.util.UUID;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class JpaProviderItemDiscountRepositoryTest {
    @Mock
    private EntityManager entityManager;

    private JpaProviderItemDiscountRepository jpaProviderItemDiscountRepository;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        jpaProviderItemDiscountRepository = new JpaProviderItemDiscountRepository();
        jpaProviderItemDiscountRepository.setEntityManager(entityManager);
    }

    @Test
    public void testInsertIdFilled() {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId id = new ProviderItemDiscountId();
        UUID uuid = new UUID(1L, 2L);
        id.setId(uuid);
        providerItemDiscount.setId(id);
        doNothing().when(entityManager).persist(eq(providerItemDiscount));
        jpaProviderItemDiscountRepository.insert(providerItemDiscount);
        verify(entityManager).persist(eq(providerItemDiscount));
    }

    @Test
    public void testInsertIdFilledNullUUID() {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId id = new ProviderItemDiscountId();
        providerItemDiscount.setId(id);
        doNothing().when(entityManager).persist(eq(providerItemDiscount));
        jpaProviderItemDiscountRepository.insert(providerItemDiscount);
        verify(entityManager).persist(eq(providerItemDiscount));
    }

    @Test
    public void testInsertIdFilledNullId() {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId();
        when(entityManager.find(eq(ProviderItemDiscount.class), eq(providerItemDiscountId))).thenReturn(providerItemDiscount);
        ProviderItemDiscount result = jpaProviderItemDiscountRepository.findById(providerItemDiscountId);
        assertEquals(result, providerItemDiscount);
        verify(entityManager).find(eq(ProviderItemDiscount.class), eq(providerItemDiscountId));
    }

    @Test
    public void testFindById() {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId id = new ProviderItemDiscountId();
        UUID uuid = new UUID(1L, 2L);
        id.setId(uuid);
        providerItemDiscount.setId(id);
        doNothing().when(entityManager).persist(eq(providerItemDiscount));
        jpaProviderItemDiscountRepository.insert(providerItemDiscount);
        verify(entityManager).persist(eq(providerItemDiscount));
    }
}
