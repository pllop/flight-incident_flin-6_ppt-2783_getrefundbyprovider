package com.edreamsodigeo.customersupport.test.utils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public abstract class BeanTest<T> {

    @Test
    public void testBean() throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        testBean(getBean());
    }

    @Test
    public void testBeans() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        for (T bean : getBeans()) {
            testBean(bean);
        }
    }

    @Test
    public void testEquals() {
        if (checkEquals()) {
            Object bean1 = getBean();
            Object bean2 = getBean();
            Object someObject = new Random();

            Assert.assertTrue(bean1.equals(bean1));
            Assert.assertTrue(bean1.equals(bean2));
            Assert.assertFalse(bean1.equals(null));
            Assert.assertFalse(bean1.equals(someObject));
        }
    }

    @Test
    public void testHashCode() {
        if (checkHashCode()) {
            Object bean1 = getBean();
            Object bean2 = getBean();
            Object someObject = new Random();

            Assert.assertEquals(bean1.hashCode(), bean2.hashCode());
            Assert.assertNotSame(bean1.hashCode(), someObject.hashCode());
        }
    }

    @Test
    public void testToString() {
        if (checkToString()) {
            Object bean1 = getBean();
            Object bean2 = getBean();
            Object someObject = new Random();

            Assert.assertEquals(bean1.toString(), bean2.toString());
            Assert.assertNotSame(bean1.toString(), someObject.toString());
        }
    }

    private void testBean(Object bean) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        Map beanMap = PropertyUtils.describe(bean);
        Map<String, Object> beanProperties = new HashMap<>();
        Set<Map.Entry> entries = beanMap.entrySet();
        for (Map.Entry entry : entries) {
            String property = (String) entry.getKey();
            Object value = PropertyUtils.getSimpleProperty(bean, property);
            beanProperties.put(property, value);
        }
        BeanUtils.populate(bean, beanProperties);
        Assert.assertEquals(beanMap, PropertyUtils.describe(bean));
    }

    protected abstract T getBean();

    protected List<T> getBeans() {
        return Arrays.asList(getBean());
    }

    protected abstract boolean checkEquals();

    protected abstract boolean checkHashCode();

    protected boolean checkToString() {
        return false;
    }
}
