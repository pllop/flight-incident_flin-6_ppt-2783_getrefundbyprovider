package com.edreamsodigeo.customersupport.refund.customermovement.repository.impl;

import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovement;
import com.edreamsodigeo.customersupport.refund.customermovement.model.RefundId;
import com.odigeo.commons.uuid.UUIDGenerator;
import javax.persistence.EntityManager;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class JpaCustomerMovementRepositoryTest {
    private JpaCustomerMovementRepository jpaMovementRepository;

    @Mock
    private EntityManager entityManager;

    @BeforeMethod
    private void setUp() {
        initMocks(this);
        jpaMovementRepository = new JpaCustomerMovementRepository();
        jpaMovementRepository.setEntityManager(entityManager);
    }

    @Test
    public void saveOk() {
        CustomerMovement customerMovement = new CustomerMovement();
        customerMovement.setRefundId(new RefundId(UUIDGenerator.getInstance().generateUUID()));
        doNothing().when(entityManager).persist(any());

        jpaMovementRepository.insert(customerMovement);
        assertNotNull(customerMovement.getRefundId());
        verify(entityManager).persist(eq(customerMovement));
    }
}
