package com.edreamsodigeo.customersupport.refund.repository.impl;

import com.edreamsodigeo.customersupport.refund.model.ProviderRefund;
import com.edreamsodigeo.customersupport.refund.model.Refund;
import com.edreamsodigeo.customersupport.refund.model.RefundId;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class JpaRefundRepositoryTest {

    @Mock
    private EntityManager entityManager;

    @BeforeClass
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInsert() {
        JpaRefundRepository jpaRefundRepository = new JpaRefundRepository();
        jpaRefundRepository.setEntityManager(entityManager);

        Refund refund = new Refund();
        jpaRefundRepository.insert(refund);

        verify(entityManager).persist(refund);
        assertNotNull(refund.getRefundId());
    }

    @Test
    public void testUpdate() {
        JpaRefundRepository jpaRefundRepository = new JpaRefundRepository();
        jpaRefundRepository.setEntityManager(entityManager);

        Refund refund = new Refund();
        refund.setProviderRefunds(Collections.singletonList(new ProviderRefund()));
        jpaRefundRepository.update(refund);

        verify(entityManager).merge(refund);
        assertNotNull(refund.getProviderRefunds().get(0).getId());
    }

    @Test
    public void testFindById() {
        UUID id = UUID.fromString("abdb61be-dac9-486f-99ed-e2b9d376fc4d");
        Refund refund = new Refund();
        when(entityManager.find(eq(Refund.class), any(RefundId.class))).thenReturn(refund);

        JpaRefundRepository jpaRefundRepository = new JpaRefundRepository();
        jpaRefundRepository.setEntityManager(entityManager);

        Refund response = jpaRefundRepository.findById(new RefundId(id));

        ArgumentCaptor<RefundId> refundIdArgumentCaptor = ArgumentCaptor.forClass(RefundId.class);

        verify(entityManager).find(eq(Refund.class), refundIdArgumentCaptor.capture());
        assertEquals(refundIdArgumentCaptor.getValue().getId(), id);
        assertEquals(refund, response);
    }
}
