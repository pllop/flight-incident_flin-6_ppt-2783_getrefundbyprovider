package com.edreamsodigeo.customersupport.flightincident.exception;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class InsertFailedTest {

    @Test
    public void testConstructor() {
        InsertFailed insertFailed = new InsertFailed("ERROR");
        assertNotNull(insertFailed);

        insertFailed = new InsertFailed("ERROR", new Exception());
        assertNotNull(insertFailed);
    }
}
