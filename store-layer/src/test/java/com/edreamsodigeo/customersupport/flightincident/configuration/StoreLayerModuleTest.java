package com.edreamsodigeo.customersupport.flightincident.configuration;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.flightincident.repository.IncidentRepository;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class StoreLayerModuleTest {

    @Test
    public void testServiceLocatorBinding() {
        ConfigurationEngine.init(new StoreLayerModule());
        IncidentRepository incidentRepository = ConfigurationEngine.getInstance(IncidentRepository.class);
        assertNotNull(incidentRepository);
    }

}
