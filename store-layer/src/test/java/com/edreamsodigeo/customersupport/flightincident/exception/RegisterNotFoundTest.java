package com.edreamsodigeo.customersupport.flightincident.exception;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class RegisterNotFoundTest {
    @Test
    public void testConstructor() {
        RegisterNotFound registerNotFound = new RegisterNotFound("ERROR");
        assertNotNull(registerNotFound);

        registerNotFound = new RegisterNotFound("ERROR", new Exception());
        assertNotNull(registerNotFound);
    }
}
