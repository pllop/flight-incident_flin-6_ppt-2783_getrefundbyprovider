package com.edreamsodigeo.customersupport.flightincident.repository.impl;

import com.edreamsodigeo.customersupport.flightincident.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class JpaIncidentRepositoryTest {
    private JpaIncidentRepository incidentRepository;

    @Mock
    private EntityManager entityManager;
    @Mock
    private CriteriaBuilder criteriaBuilder;
    @Mock
    private CriteriaQuery<Incident> criteriaQuery;
    @Mock
    private Root<Incident> root;
    @Mock
    private Predicate predicate;
    @Mock
    private TypedQuery<Incident> typedQuery;

    @BeforeMethod
    private void setUp() {
        initMocks(this);
        incidentRepository = new JpaIncidentRepository();
        incidentRepository.setEntityManager(entityManager);
    }

    @Test
    public void addOk() {
        Incident incident = new Incident();
        doNothing().when(entityManager).persist(any());
        incidentRepository.save(incident);
        verify(entityManager).persist(eq(incident));
    }

    @Test
    public void incidentOfIncidentIdOK() {
        Incident incident = new Incident();
        IncidentId incidentId = new IncidentId();
        when(entityManager.find(eq(Incident.class), eq(incidentId))).thenReturn(incident);
        assertEquals(incidentRepository.incidentOfIncidentId(incidentId), incident);
    }

    @Test
    public void incidentsOfDescriptorOK() {
        Incident incident = new Incident();

        doQueryingMocking();
        when(criteriaBuilder.equal(any(), anyString())).thenReturn(predicate);
        when(typedQuery.getResultList()).thenReturn(Collections.singletonList(incident));

        assertEquals(incidentRepository.incidentsOfBookingId(123L), Collections.singletonList(incident));
    }

    @Test
    public void incidentsWithoutCancellationOK() {
        Incident incident = new Incident();
        doQueryingMocking();
        when(criteriaBuilder.equal(any(), anyString())).thenReturn(predicate);
        when(typedQuery.getResultList()).thenReturn(Collections.singletonList(incident));
        assertEquals(incidentRepository.incidentsWithoutCancellation(1), Collections.singletonList(incident));
    }

    @Test
    public void incidentsOfBookingIdAndProviderBookingItemIdOK() {
        Incident incident = new Incident();
        doQueryingMocking();
        when(criteriaBuilder.equal(any(), anyString())).thenReturn(predicate);
        when(typedQuery.getResultList()).thenReturn(Collections.singletonList(incident));
        assertEquals(incidentRepository.incidentsOfBookingIdAndProviderBookingItemId(1L, 1L), Collections.singletonList(incident));
    }


    private void doQueryingMocking() {
        when(entityManager.getCriteriaBuilder()).thenReturn(criteriaBuilder);
        when(criteriaBuilder.createQuery(Incident.class)).thenReturn(criteriaQuery);
        when(criteriaQuery.from(Incident.class)).thenReturn(root);
        when(criteriaQuery.select(eq(root))).thenReturn(criteriaQuery);
        when(criteriaQuery.where(any(Predicate.class))).thenReturn(criteriaQuery);
        when(entityManager.createQuery(any(CriteriaQuery.class))).thenReturn(typedQuery);
    }

}
