package com.edreamsodigeo.customersupport.flightincident.repository.impl;

import com.edreamsodigeo.customersupport.flightincident.model.BspLinkRefundRequest;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class JpaBspLinkRefundRequestRepositoryTest {
    private JpaBspLinkRefundRequestRepository repository;

    @Mock
    private EntityManager entityManager;
    @Mock
    private CriteriaBuilder criteriaBuilder;
    @Mock
    private CriteriaQuery<BspLinkRefundRequest> criteriaQuery;
    @Mock
    private Root<BspLinkRefundRequest> root;
    @Mock
    private Predicate predicate;
    @Mock
    private TypedQuery<BspLinkRefundRequest> typedQuery;

    @BeforeMethod
    private void setUp() {
        initMocks(this);
        repository = new JpaBspLinkRefundRequestRepository();
        repository.setEntityManager(entityManager);
    }

    @Test
    public void testFindByTicketId() {
        final long ticketId = 1234L;
        BspLinkRefundRequest request = new BspLinkRefundRequest();

        doQueryingMocking();
        when(criteriaBuilder.equal(any(), any(BspLinkRefundRequest.class))).thenReturn(predicate);
        when(typedQuery.getSingleResult()).thenReturn(request);

        assertEquals(repository.findByTicketId(ticketId), request);
    }

    @Test
    public void testFindByProviderBookingItemId() {
        final long providerBookingItemId = 1234L;
        BspLinkRefundRequest request = new BspLinkRefundRequest();

        doQueryingMocking();
        when(criteriaBuilder.equal(any(), any(BspLinkRefundRequest.class))).thenReturn(predicate);
        when(typedQuery.getSingleResult()).thenReturn(request);

        assertEquals(repository.findByProviderBookingItemId(providerBookingItemId), request);
    }

    @Test
    public void testFindAllBetweenTimestamps() {
        BspLinkRefundRequest request = new BspLinkRefundRequest();

        doQueryingMocking();
        when(criteriaBuilder.equal(any(), any(BspLinkRefundRequest.class))).thenReturn(predicate);
        when(typedQuery.getResultList()).thenReturn(Collections.singletonList(request));

        assertEquals(repository.findAllBetweenTimestamps(Instant.now(), Instant.now()), Collections.singletonList(request));
    }

    private void doQueryingMocking() {
        when(entityManager.getCriteriaBuilder()).thenReturn(criteriaBuilder);
        when(criteriaBuilder.createQuery(BspLinkRefundRequest.class)).thenReturn(criteriaQuery);
        when(criteriaQuery.from(BspLinkRefundRequest.class)).thenReturn(root);
        when(criteriaQuery.select(eq(root))).thenReturn(criteriaQuery);
        when(criteriaQuery.where(any(Predicate.class))).thenReturn(criteriaQuery);
        when(entityManager.createQuery(any(CriteriaQuery.class))).thenReturn(typedQuery);
    }
}
