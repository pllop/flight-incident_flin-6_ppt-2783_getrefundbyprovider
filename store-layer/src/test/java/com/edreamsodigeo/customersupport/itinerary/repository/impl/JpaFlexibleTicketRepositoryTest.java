package com.edreamsodigeo.customersupport.itinerary.repository.impl;

import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicketId;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class JpaFlexibleTicketRepositoryTest {
    private JpaFlexibleTicketRepository repository;

    @Mock
    private EntityManager entityManager;

    @BeforeMethod
    private void setUp() {
        initMocks(this);
        repository = new JpaFlexibleTicketRepository();
        repository.setEntityManager(entityManager);
    }

    @Test
    public void saveOk() {
        FlexibleTicket flexibleTicket = new FlexibleTicket();
        flexibleTicket.setId(new FlexibleTicketId(UUIDGenerator.getInstance().generateUUID()));
        doNothing().when(entityManager).persist(any());

        repository.insert(flexibleTicket);

        assertNotNull(flexibleTicket.getId());
        verify(entityManager).persist(eq(flexibleTicket));
    }

    @Test
    public void saveOkWithNullFlexibleTicketId() {
        FlexibleTicket flexibleTicket = new FlexibleTicket();
        doNothing().when(entityManager).persist(any());

        repository.insert(flexibleTicket);

        assertNotNull(flexibleTicket.getId());
        verify(entityManager).persist(eq(flexibleTicket));
    }

    @Test
    public void saveOkWithEmptyFlexibleTicketId() {
        FlexibleTicket flexibleTicket = new FlexibleTicket();
        flexibleTicket.setId(new FlexibleTicketId());
        doNothing().when(entityManager).persist(any());

        repository.insert(flexibleTicket);

        assertNotNull(flexibleTicket.getId());
        verify(entityManager).persist(eq(flexibleTicket));
    }

    @Test
    public void saveFindById() {
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        when(entityManager.find(any(), any())).thenReturn(new FlexibleTicket());
        FlexibleTicket flexibleTicket = repository.findById(flexibleTicketId);
        assertNotNull(flexibleTicket);
    }

    @Test
    public void update() {
        FlexibleTicket flexibleTicket = new FlexibleTicket();
        flexibleTicket.setId(new FlexibleTicketId());
        repository.insert(flexibleTicket);
        flexibleTicket.setCurrencyCode("EUR");
        repository.update(flexibleTicket);
        verify(entityManager).merge(eq(flexibleTicket));
    }
}
