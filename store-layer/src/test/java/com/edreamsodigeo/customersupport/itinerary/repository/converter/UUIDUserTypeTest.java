package com.edreamsodigeo.customersupport.itinerary.repository.converter;

import com.edreamsodigeo.customersupport.test.utils.BeanTest;

public class UUIDUserTypeTest extends BeanTest<UUIDUserType> {

    @Override
    protected UUIDUserType getBean() {
        return new UUIDUserType();
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
