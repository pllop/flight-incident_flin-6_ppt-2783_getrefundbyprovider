package com.edreamsodigeo.customersupport.itinerary.exception;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class RegisterNotFoundExceptionTest {
    @Test
    public void testConstructor() {
        RegisterNotFoundException registerNotFound = new RegisterNotFoundException("ERROR");
        assertNotNull(registerNotFound);

        registerNotFound = new RegisterNotFoundException("ERROR", new Exception());
        assertNotNull(registerNotFound);
    }
}
