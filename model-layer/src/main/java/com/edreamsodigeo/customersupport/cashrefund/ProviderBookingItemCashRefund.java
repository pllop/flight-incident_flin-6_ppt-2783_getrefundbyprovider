package com.edreamsodigeo.customersupport.cashrefund;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD")
@SuppressFBWarnings(value = "EI_EXPOSE_REP")
public class ProviderBookingItemCashRefund {

    private Long providerBookingItemId;
    private CashRefundStatus cashRefundStatus;
    private CancellationClassification cancellationClassification;

    public ProviderBookingItemCashRefund(Long providerBookingItemId, CancellationClassification cancellationClassification) {
        this.providerBookingItemId = providerBookingItemId;
        this.cancellationClassification = cancellationClassification;
        markCreated();
    }

    public void markCreated() {
        cashRefundStatus = CashRefundStatus.REFUND_CREATED;
    }

    public void markRequested() {
        cashRefundStatus = CashRefundStatus.REFUND_REQUESTED_TO_AIRLINE;
    }

    public void markApproved() {
        cashRefundStatus = CashRefundStatus.APPROVED_BY_AIRLINE;
    }

    public void markRejected() {
        cashRefundStatus = CashRefundStatus.REJECTED_BY_AIRLINE;
    }

    public void markReceived() {
        cashRefundStatus = CashRefundStatus.REFUND_RECEIVED;
    }

    public void markPartiallyCashedOut() {
        cashRefundStatus = CashRefundStatus.PARTIAL_REFUND_CASHED_OUT;
    }

    public void markCashedOut() {
        cashRefundStatus = CashRefundStatus.REFUND_CASHED_OUT;
    }

}
