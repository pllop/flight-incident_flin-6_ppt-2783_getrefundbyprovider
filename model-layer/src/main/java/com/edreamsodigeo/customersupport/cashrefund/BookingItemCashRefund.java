package com.edreamsodigeo.customersupport.cashrefund;

import java.util.List;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("PMD")
@SuppressFBWarnings(value = "EI_EXPOSE_REP")
public class BookingItemCashRefund {

    private Long bookingItemId;
    private List<ProviderBookingItemCashRefund> providerBookingItemCashRefunds;
}
