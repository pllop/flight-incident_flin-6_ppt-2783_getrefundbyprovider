package com.edreamsodigeo.customersupport.cashrefund;

public enum CashRefundStatus {

    REFUND_CREATED,
    REFUND_REQUESTED_TO_AIRLINE,
    APPROVED_BY_AIRLINE,
    REJECTED_BY_AIRLINE,
    REFUND_RECEIVED,
    PARTIAL_REFUND_CASHED_OUT,
    REFUND_CASHED_OUT;
}
