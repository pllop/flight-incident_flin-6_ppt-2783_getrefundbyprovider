package com.edreamsodigeo.customersupport.cashrefund;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Locale;

@Data
@NoArgsConstructor
@SuppressWarnings("PMD")
@SuppressFBWarnings(value = "EI_EXPOSE_REP")
public class CancellationClassification {

    private String classification;

    private static final String ATC_AUTOMATIC = "ATC_AUTOMATIC";
    private static final String ATC_MANUAL = "ATC_MANUAL";
    private static final String BSP_LINK = "BSP_LINK";

    public CancellationClassification(String classification) {
        this.classification = classification.toUpperCase(Locale.ENGLISH);
    }

    public boolean classifiedAsCashRefund() {
        return Arrays.asList(ATC_AUTOMATIC, ATC_MANUAL, BSP_LINK).contains(classification);
    }

    public boolean isBSPLink() {
        return classification.equals(BSP_LINK);
    }
}
