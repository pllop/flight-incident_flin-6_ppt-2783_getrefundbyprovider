package com.edreamsodigeo.customersupport.flightincident.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(name = "REFUND_RECEIVED")
public class RefundReceived {

    @EmbeddedId
    private RefundReceivedId id;

    @OneToMany()
    @Column(name = "REFUND_REQUEST_ID")
    private RefundRequest refundRequest;

    @Column(name = "PROVIDER_AMOUNT_REFUNDED")
    private BigDecimal providerAmountRefunded;

    @Column(name = "REFUND_TIMESTAMP")
    private Instant refundInstant;

    @Column(nullable = false, name = "PROVIDER_BOOKING_ITEM_ID")
    private Long providerBookingItemId;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "METHOD")
    private RefundMethod method;

    @Column(name = "TICKET_NUMBER", unique = true)
    private Long ticketId;

    @Column(nullable = false, name = "PROVIDER_REFUND_REQUEST_ID")
    private Long providerRefundRequestId;


    public RefundReceivedId getId() {
        return id;
    }

    public void setId(RefundReceivedId id) {
        this.id = id;
    }

    public RefundRequest getRefundRequest() {
        return refundRequest;
    }

    public void setRefundRequest(RefundRequest refundRequest) {
        this.refundRequest = refundRequest;
    }

    public BigDecimal getProviderAmountRefunded() {
        return providerAmountRefunded;
    }

    public void setProviderAmountRefunded(BigDecimal providerAmountRefunded) {
        this.providerAmountRefunded = providerAmountRefunded;
    }

    public Instant getRefundInstant() {
        return refundInstant;
    }

    public void setRefundInstant(Instant refundInstant) {
        this.refundInstant = refundInstant;
    }

    public Long getProviderBookingItemId() {
        return providerBookingItemId;
    }

    public void setProviderBookingItemId(Long providerBookingItemId) {
        this.providerBookingItemId = providerBookingItemId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getProviderRefundRequestId() {
        return providerRefundRequestId;
    }

    public void setProviderRefundRequestId(Long providerRefundRequestId) {
        this.providerRefundRequestId = providerRefundRequestId;
    }

    public RefundMethod getMethod() {
        return method;
    }

    public void setMethod(RefundMethod method) {
        this.method = method;
    }
}
