package com.edreamsodigeo.customersupport.flightincident.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "METHOD")
@Table(name = "REFUND_REQUEST")
public abstract class RefundRequest {

    @EmbeddedId
    private RefundRequestId id;

    @Column(nullable = false, name = "PROVIDER_BOOKING_ITEM_ID")
    private Long providerBookingItemId;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "STATUS")
    private RefundStatus status;

    @Column(name = "REQUESTED_AMOUNT")
    private BigDecimal expectedRefundAmount;

    @Column(name = "ACCEPTED_AMOUNT")
    private BigDecimal acceptedRefundAmount;

    @Column(name = "REQUEST_TIMESTAMP")
    private Instant requestInstant;

    @Column(name = "PROVIDER_RESPONSE_TIMESTAMP")
    private Instant airlineResponseInstant;


    public RefundRequestId getId() {
        return id;
    }

    public void setId(RefundRequestId id) {
        this.id = id;
    }

    public Long getProviderBookingItemId() {
        return providerBookingItemId;
    }

    public void setProviderBookingItemId(Long providerBookingItemId) {
        this.providerBookingItemId = providerBookingItemId;
    }

    public RefundStatus getStatus() {
        return status;
    }

    public void setStatus(RefundStatus status) {
        this.status = status;
    }

    public BigDecimal getExpectedRefundAmount() {
        return expectedRefundAmount;
    }

    public void setExpectedRefundAmount(BigDecimal expectedRefundAmount) {
        this.expectedRefundAmount = expectedRefundAmount;
    }

    public BigDecimal getAcceptedRefundAmount() {
        return acceptedRefundAmount;
    }

    public void setAcceptedRefundAmount(BigDecimal acceptedRefundAmount) {
        this.acceptedRefundAmount = acceptedRefundAmount;
    }

    public Instant getRequestInstant() {
        return requestInstant;
    }

    public void setRequestInstant(Instant requestInstant) {
        this.requestInstant = requestInstant;
    }

    public Instant getAirlineResponseInstant() {
        return airlineResponseInstant;
    }

    public void setAirlineResponseInstant(Instant airlineResponseInstant) {
        this.airlineResponseInstant = airlineResponseInstant;
    }
}
