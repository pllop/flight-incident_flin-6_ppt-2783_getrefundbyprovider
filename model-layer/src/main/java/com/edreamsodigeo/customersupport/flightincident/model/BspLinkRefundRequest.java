package com.edreamsodigeo.customersupport.flightincident.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("BSPLINK")
public class BspLinkRefundRequest extends RefundRequest {
    @Column(name = "TICKET_NUMBER", unique = true)
    private Long ticketId;

    @Column(name = "PROVIDER_REFUND_REQUEST_ID")
    private Long providerRefundRequestId;


    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getProviderRefundRequestId() {
        return providerRefundRequestId;
    }

    public void setProviderRefundRequestId(Long providerRefundRequestId) {
        this.providerRefundRequestId = providerRefundRequestId;
    }
}
