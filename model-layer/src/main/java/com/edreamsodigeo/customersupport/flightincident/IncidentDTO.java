package com.edreamsodigeo.customersupport.flightincident;

import java.time.LocalDateTime;
import java.util.UUID;

public class IncidentDTO {

    private UUID id;
    private Long bookingId;
    private Long providerBookingItemId;
    private String flightNumber;
    private LocalDateTime departureLocalDate;
    private String status;
    private UUID cancellationId;
    private ConsentDTO consentDTO;

    @SuppressWarnings("CPD-START")
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public Long getProviderBookingItemId() {
        return providerBookingItemId;
    }

    public void setProviderBookingItemId(Long providerBookingItemId) {
        this.providerBookingItemId = providerBookingItemId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public LocalDateTime getDepartureLocalDate() {
        return departureLocalDate;
    }

    public void setDepartureLocalDate(LocalDateTime departureLocalDate) {
        this.departureLocalDate = departureLocalDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UUID getCancellationId() {
        return cancellationId;
    }

    public void setCancellationId(UUID cancellationId) {
        this.cancellationId = cancellationId;
    }

    public ConsentDTO getConsentDTO() {
        return consentDTO;
    }
    @SuppressWarnings("CPD-END")
    public void setConsentDTO(ConsentDTO consentDTO) {
        this.consentDTO = consentDTO;
    }

}
