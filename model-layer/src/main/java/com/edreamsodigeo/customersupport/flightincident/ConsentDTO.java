package com.edreamsodigeo.customersupport.flightincident;

public class ConsentDTO {

    private Boolean flexibleTicketAccepted;
    private Boolean voucherAccepted;
    private Boolean cashRefundAccepted;
    private Boolean emdAccepted;

    public ConsentDTO(Boolean flexibleTicketAccepted, Boolean voucherAccepted, Boolean cashRefundAccepted, Boolean emdAccepted) {
        this.flexibleTicketAccepted = flexibleTicketAccepted;
        this.voucherAccepted = voucherAccepted;
        this.cashRefundAccepted = cashRefundAccepted;
        this.emdAccepted = emdAccepted;
    }

    public Boolean getFlexibleTicketAccepted() {
        return flexibleTicketAccepted;
    }

    public void setFlexibleTicketAccepted(Boolean flexibleTicketAccepted) {
        this.flexibleTicketAccepted = flexibleTicketAccepted;
    }

    public Boolean getVoucherAccepted() {
        return voucherAccepted;
    }

    public void setVoucherAccepted(Boolean voucherAccepted) {
        this.voucherAccepted = voucherAccepted;
    }

    public Boolean getCashRefundAccepted() {
        return cashRefundAccepted;
    }

    public void setCashRefundAccepted(Boolean cashRefundAccepted) {
        this.cashRefundAccepted = cashRefundAccepted;
    }

    public Boolean getEmdAccepted() {
        return emdAccepted;
    }

    public void setEmdAccepted(Boolean emdAccepted) {
        this.emdAccepted = emdAccepted;
    }
}
