package com.edreamsodigeo.customersupport.flightincident.model;

public enum OptionValue {
    OPEN_TICKET_CONSENT, EDO_VOUCHER_CONSENT, EMD_CONSENT, CASHOUT_CONSENT, GENERIC
}
