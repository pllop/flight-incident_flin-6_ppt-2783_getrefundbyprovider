package com.edreamsodigeo.customersupport.flightincident.model;

public enum RefundMethod {
    BSP_LINK,
    ATC,
    DIRECT
}
