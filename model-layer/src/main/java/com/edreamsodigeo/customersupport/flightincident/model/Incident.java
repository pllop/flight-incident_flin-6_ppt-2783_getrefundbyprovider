package com.edreamsodigeo.customersupport.flightincident.model;


import org.hibernate.annotations.Type;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "INCIDENT")
@Access(AccessType.FIELD)
public class Incident implements Cloneable {

    @EmbeddedId
    private IncidentId id;

    @Column(nullable = false, name = "BOOKING_ID")
    private Long bookingId;

    @Column(nullable = false, name = "PROVIDER_BOOKING_ITEM_ID")
    private Long providerBookingItemId;

    @Column(name = "FLIGHT_NUMBER")
    private String flightNumber;

    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.LocalDateTimeUserType")
    @Column(name = "DEPARTURE_LOCAL_DATE")
    private LocalDateTime departureLocalDate;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "STATUS")
    private Status status;

    @Embedded
    private CancellationId cancellationId;

    @Type(type = "true_false")
    @Column(name = "FLEXIBLE_TICKET_ACCEPTED")
    private Boolean flexibleTicketAccepted;

    @Type(type = "true_false")
    @Column(name = "VOUCHER_ACCEPTED")
    private Boolean voucherAccepted;

    @Type(type = "true_false")
    @Column(name = "CASH_REFUND_ACCEPTED")
    private Boolean cashRefundAccepted;

    @Type(type = "true_false")
    @Column(name = "EMD_ACCEPTED")
    private Boolean emdAccepted;

    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.LocalDateTimeUserType")
    @Column(nullable = false, name = "TIMESTAMP")
    private LocalDateTime timestamp;

    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.LocalDateTimeUserType")
    @Column(nullable = false, name = "LAST_UPDATE")
    private LocalDateTime lastUpdate;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "INCIDENT_ID", referencedColumnName = "ID")
    private List<ConsentCommsLog> consentCommsLogs;

    public IncidentId getId() {
        return id;
    }

    public void setId(IncidentId id) {
        this.id = id;
    }

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public Long getProviderBookingItemId() {
        return providerBookingItemId;
    }

    public void setProviderBookingItemId(Long providerBookingItemId) {
        this.providerBookingItemId = providerBookingItemId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public LocalDateTime getDepartureLocalDate() {
        return departureLocalDate;
    }

    public void setDepartureLocalDate(LocalDateTime departureLocalDate) {
        this.departureLocalDate = departureLocalDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public CancellationId getCancellationId() {
        return cancellationId;
    }

    public void setCancellationId(CancellationId cancellationId) {
        this.cancellationId = cancellationId;
    }

    public Boolean getFlexibleTicketAccepted() {
        return flexibleTicketAccepted;
    }

    public void setFlexibleTicketAccepted(Boolean flexibleTicketAccepted) {
        this.flexibleTicketAccepted = flexibleTicketAccepted;
    }

    public Boolean getVoucherAccepted() {
        return voucherAccepted;
    }

    public void setVoucherAccepted(Boolean voucherAccepted) {
        this.voucherAccepted = voucherAccepted;
    }

    public Boolean getCashRefundAccepted() {
        return cashRefundAccepted;
    }

    public void setCashRefundAccepted(Boolean cashRefundAccepted) {
        this.cashRefundAccepted = cashRefundAccepted;
    }

    public Boolean getEmdAccepted() {
        return emdAccepted;
    }

    public void setEmdAccepted(Boolean emdAccepted) {
        this.emdAccepted = emdAccepted;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<ConsentCommsLog> getConsentCommsLogs() {
        return consentCommsLogs;
    }

    public void setConsentCommsLogs(List<ConsentCommsLog> consentCommsLogs) {
        this.consentCommsLogs = consentCommsLogs;
    }

    @Override
    public Incident clone() {
        try {
            return (Incident) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }


}
