package com.edreamsodigeo.customersupport.flightincident.model;

public enum RefundStatus {
    REQUESTED,
    ACCEPTED,
    REJECTED
}
