package com.edreamsodigeo.customersupport.flightincident.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "CONSENT_COMM_OPTION")
@Access(AccessType.FIELD)
public class ConsentCommOption implements Cloneable {

    @EmbeddedId
    private ConsentCommOptionId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMM_ID")
    private ConsentCommsLog consentCommsLog;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "COMM_OPTION_ID", referencedColumnName = "ID")
    private List<ConsentCommsRespLog> consentCommsRespLogs;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "OPTION_VALUE")
    private OptionValue optionValue;

    @Override
    public ConsentCommOption clone() {
        try {
            return (ConsentCommOption) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public ConsentCommOptionId getId() {
        return id;
    }

    public void setId(ConsentCommOptionId id) {
        this.id = id;
    }


    public OptionValue getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(OptionValue optionValue) {
        this.optionValue = optionValue;
    }

    public List<ConsentCommsRespLog> getConsentCommsRespLogs() {
        return consentCommsRespLogs;
    }

    public void setConsentCommsRespLogs(List<ConsentCommsRespLog> consentCommsRespLogs) {
        this.consentCommsRespLogs = consentCommsRespLogs;
    }

    public ConsentCommsLog getConsentCommsLog() {
        return consentCommsLog;
    }

    public void setConsentCommsLog(ConsentCommsLog consentCommsLog) {
        this.consentCommsLog = consentCommsLog;
    }
}
