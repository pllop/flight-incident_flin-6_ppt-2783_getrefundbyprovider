package com.edreamsodigeo.customersupport.flightincident.model;

import org.hibernate.annotations.Type;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "CONSENT_COMMS_RESP_LOG")
@Access(AccessType.FIELD)
public class ConsentCommsRespLog implements Cloneable {

    @EmbeddedId
    private ConsentCommsRespLogId id;


    @Type(type = "true_false")
    @Column(nullable = false, name = "RESPONSE")
    private Boolean response;


    @Column(nullable = false, name = "RESPONSE_CHANNEL")
    private String responseChannel;

    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.LocalDateTimeUserType")
    @Column(nullable = false, name = "RESPONSE_DATE")
    private LocalDateTime responseDate;

    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.LocalDateTimeUserType")
    @Column(nullable = false, name = "TIMESTAMP")
    private LocalDateTime timestamp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMM_OPTION_ID")
    private ConsentCommOption consentCommOption;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMM_ID")
    private ConsentCommsLog consentCommsLog;

    @Override
    public ConsentCommsRespLog clone() {
        try {
            return (ConsentCommsRespLog) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }


    public ConsentCommsRespLogId getId() {
        return id;
    }

    public void setId(ConsentCommsRespLogId id) {
        this.id = id;
    }


    public Boolean getResponse() {
        return response;
    }

    public void setResponse(Boolean response) {
        this.response = response;
    }

    public String getResponseChannel() {
        return responseChannel;
    }

    public void setResponseChannel(String responseChannel) {
        this.responseChannel = responseChannel;
    }

    public LocalDateTime getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(LocalDateTime responseDate) {
        this.responseDate = responseDate;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public ConsentCommOption getConsentCommOption() {
        return consentCommOption;
    }

    public void setConsentCommOption(ConsentCommOption consentCommOption) {
        this.consentCommOption = consentCommOption;
    }

    public ConsentCommsLog getConsentCommsLog() {
        return consentCommsLog;
    }

    public void setConsentCommsLog(ConsentCommsLog consentCommsLog) {
        this.consentCommsLog = consentCommsLog;
    }
}
