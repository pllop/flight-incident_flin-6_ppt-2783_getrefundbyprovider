package com.edreamsodigeo.customersupport.flightincident.model;

public enum Status {
    WAITING_FOR_CUSTOMER_CHOICE,
    CUSTOMER_CANCELLATION_CHOSEN;
}
