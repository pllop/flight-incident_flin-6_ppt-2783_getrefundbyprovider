package com.edreamsodigeo.customersupport.flightincident.model;

import org.hibernate.annotations.Type;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "CONSENT_COMMS_LOG")
@Access(AccessType.FIELD)
public class ConsentCommsLog implements Cloneable {
    @EmbeddedId
    private ConsentCommsLogId id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "TYPE")
    private OptionValue type;

    @Column(nullable = false, name = "MESSAGE")
    private String message;

    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.LocalDateTimeUserType")
    @Column(nullable = false, name = "TIMESTAMP")
    private LocalDateTime timestamp;


    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "COMM_ID", referencedColumnName = "ID")
    private List<ConsentCommOption> consentCommOptions;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INCIDENT_ID")
    private Incident incident;

    @Override
    public ConsentCommsLog clone() {
        try {
            return (ConsentCommsLog) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }


    public ConsentCommsLogId getId() {
        return id;
    }

    public void setId(ConsentCommsLogId id) {
        this.id = id;
    }


    public OptionValue getType() {
        return type;
    }

    public void setType(OptionValue type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public List<ConsentCommOption> getConsentCommOptions() {
        return consentCommOptions;
    }

    public void setConsentCommOptions(List<ConsentCommOption> consentCommOptions) {
        this.consentCommOptions = consentCommOptions;
    }

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }
}
