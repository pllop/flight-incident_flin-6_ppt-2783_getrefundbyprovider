package com.edreamsodigeo.customersupport.itinerary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoneyDTO {
    BigDecimal amount;
    String currencyCode;
}
