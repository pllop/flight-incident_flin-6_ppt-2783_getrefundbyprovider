package com.edreamsodigeo.customersupport.itinerary;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.UUID;

@Data
@Builder
public class FlexibleTicketDTO {
    @NonNull
    UUID id;
    Long customerId;
    Long providerBookingItemId;
    String status;
    MoneyDTO monetaryValue;
}
