package com.edreamsodigeo.customersupport.itinerary.model;

import com.odigeo.commons.uuid.UUIDGenerator;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.math.BigDecimal;


@Entity
@Table(name = "FLEXIBLE_TICKET")
@Access(AccessType.FIELD)
@Getter
@Setter
public class FlexibleTicket {

    @EmbeddedId
    private FlexibleTicketId id;

    @Column(nullable = false, name = "CUSTOMER_ID")
    private Long customerId;

    @Column(nullable = false, name = "PROVIDER_BOOKING_ITEM_ID")
    private Long providerBookingItemId;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private Status status;

    @Column(name = "MONETARY_VALUE")
    private BigDecimal monetaryValue;

    @Column(name = "CURRENCY_CODE")
    private String currencyCode;

    public void fillKeys() {
        if (this.getId() == null || this.getId().getId() == null) {
            this.setId(new FlexibleTicketId(UUIDGenerator.getInstance().generateUUID()));
        }
    }

}
