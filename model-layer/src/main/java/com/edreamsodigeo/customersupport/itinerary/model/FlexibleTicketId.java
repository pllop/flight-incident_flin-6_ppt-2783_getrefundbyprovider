package com.edreamsodigeo.customersupport.itinerary.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlexibleTicketId implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "ID")
    @Type(type = "com.edreamsodigeo.customersupport.itinerary.repository.converter.UUIDUserType")
    private UUID id;

}
