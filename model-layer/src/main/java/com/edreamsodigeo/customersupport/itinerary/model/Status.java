package com.edreamsodigeo.customersupport.itinerary.model;

public enum Status {
    AVAILABLE,
    CANCELLED,
    REDEEMED,
    REDEMPTION_REQUESTED
}
