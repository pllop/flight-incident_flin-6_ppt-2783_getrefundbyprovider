package com.edreamsodigeo.customersupport.refund.customermovement;

import java.math.BigDecimal;

public class MovementMoneyDTO {
    private String currency;
    private BigDecimal amount;

    public MovementMoneyDTO() {
    }

    public MovementMoneyDTO(BigDecimal amount, String currency) {
        this.currency = currency;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
