package com.edreamsodigeo.customersupport.refund.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PROVIDER_REFUND")
@Access(AccessType.FIELD)
public class ProviderRefund implements Cloneable {

    @EmbeddedId
    private ProviderRefundId id;

    @Column(name = "PROV_BOOKING_ITEM_ID")
    private Long provBookingItemId;

    @Column(name = "EXPECTED_PROV_REFUND_AMOUNT")
    private Double expectedProvRefundAmount;

    @Column(name = "EXPECTED_PROV_REFUND_CURRENCY")
    private String expectedProvRefundCurrency;

    @Override
    public ProviderRefund clone() {
        try {
            return (ProviderRefund) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public ProviderRefundId getId() {
        return id;
    }

    public void setId(ProviderRefundId id) {
        this.id = id;
    }

    public Long getProvBookingItemId() {
        return provBookingItemId;
    }

    public void setProvBookingItemId(Long provBookingItemId) {
        this.provBookingItemId = provBookingItemId;
    }

    public Double getExpectedProvRefundAmount() {
        return expectedProvRefundAmount;
    }

    public void setExpectedProvRefundAmount(Double expectedProvRefundAmount) {
        this.expectedProvRefundAmount = expectedProvRefundAmount;
    }

    public String getExpectedProvRefundCurrency() {
        return expectedProvRefundCurrency;
    }

    public void setExpectedProvRefundCurrency(String expectedProvRefundCurrency) {
        this.expectedProvRefundCurrency = expectedProvRefundCurrency;
    }
}
