package com.edreamsodigeo.customersupport.refund.customermovement.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


@Entity
@Table(name = "CUSTOMER_MOVEMENT")
@Access(AccessType.FIELD)
public class CustomerMovement implements Cloneable, Serializable {
    @EmbeddedId
    private CustomerMovementId id;

    @Embedded
    private RefundId refundId;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "CURRENCY_RATE_PROV_CUSTOMER")
    private BigDecimal currencyRateProviderCustomer;

    @Column(name = "AMOUNT_IN_EUR")
    private BigDecimal amountInEur;

    @Column(name = "CURRENCY_RATE_PROVIDER_EUR")
    private BigDecimal currencyRateProviderEur;

    @Column(name = "PSP")
    private String psp;

    @Column(name = "PSP_ACCOUNT")
    private String pspAccount;

    @Column(name = "STATUS")
    private String status;

    @Embedded
    private CustomerMovementWavePspId customerMovementWavePspId;

    @Override
    public CustomerMovement clone() {
        try {
            return (CustomerMovement) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public CustomerMovementId getId() {
        return id;
    }

    public void setId(CustomerMovementId id) {
        this.id = id;
    }

    public RefundId getRefundId() {
        return refundId;
    }

    public void setRefundId(RefundId refundId) {
        this.refundId = refundId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getCurrencyRateProviderCustomer() {
        return currencyRateProviderCustomer;
    }

    public void setCurrencyRateProviderCustomer(BigDecimal currencyRateProviderCustomer) {
        this.currencyRateProviderCustomer = currencyRateProviderCustomer;
    }

    public BigDecimal getAmountInEur() {
        return amountInEur;
    }

    public void setAmountInEur(BigDecimal amountInEur) {
        this.amountInEur = amountInEur;
    }

    public BigDecimal getCurrencyRateProviderEur() {
        return currencyRateProviderEur;
    }

    public void setCurrencyRateProviderEur(BigDecimal currencyRateProviderEur) {
        this.currencyRateProviderEur = currencyRateProviderEur;
    }

    public String getPsp() {
        return psp;
    }

    public void setPsp(String psp) {
        this.psp = psp;
    }

    public String getPspAccount() {
        return pspAccount;
    }

    public void setPspAccount(String pspAccount) {
        this.pspAccount = pspAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CustomerMovementWavePspId getCustomerMovementWavePspId() {
        return customerMovementWavePspId;
    }

    public void setCustomerMovementWavePspId(CustomerMovementWavePspId customerMovementWavePspId) {
        this.customerMovementWavePspId = customerMovementWavePspId;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            CustomerMovement that = (CustomerMovement) o;
            return (new EqualsBuilder()).append(this.id, that.id).append(this.refundId, that.refundId).append(this.amount, that.amount).append(this.currency, that.currency).append(this.currencyRateProviderCustomer, that.currencyRateProviderCustomer).append(this.amountInEur, that.amountInEur).append(this.currencyRateProviderEur, that.currencyRateProviderEur).append(this.psp, that.psp).append(this.pspAccount, that.pspAccount).append(this.status, that.status).append(this.customerMovementWavePspId, that.customerMovementWavePspId).isEquals();
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (new HashCodeBuilder(17, 37)).append(this.id).append(this.refundId).append(this.amount).append(this.currency).append(this.currencyRateProviderCustomer).append(this.amountInEur).append(this.currencyRateProviderEur).append(this.psp).append(this.pspAccount).append(this.status).append(this.customerMovementWavePspId).toHashCode();
    }
}
