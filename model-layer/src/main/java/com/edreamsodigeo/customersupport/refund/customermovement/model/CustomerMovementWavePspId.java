package com.edreamsodigeo.customersupport.refund.customermovement.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.hibernate.annotations.Type;

@Embeddable
public class CustomerMovementWavePspId implements Serializable {
    @SuppressWarnings("CPD-START")
    private static final long serialVersionUID = 1L;

    @Column(name = "CUSTOMER_MOVEMENT_WAVE_PSP_ID")
    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.UUIDUserType")
    private UUID id;

    public CustomerMovementWavePspId() {
    }

    public CustomerMovementWavePspId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CustomerMovementWavePspId customerMovementWavePspId = (CustomerMovementWavePspId) o;
        return Objects.equals(id, customerMovementWavePspId.id);
    }

    @SuppressWarnings("CPD-END")
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
