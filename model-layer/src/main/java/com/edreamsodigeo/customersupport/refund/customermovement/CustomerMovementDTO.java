package com.edreamsodigeo.customersupport.refund.customermovement;


import java.math.BigDecimal;
import java.util.UUID;

public class CustomerMovementDTO {
    private UUID id;
    private UUID refundId;
    private MovementMoneyDTO amount;
    private String currencyCode;
    private BigDecimal currencyRateProviderCustomer;
    private MovementMoneyDTO amountInEur;
    private BigDecimal currencyRateProviderEur;
    private String psp;
    private String pspAccount;
    private String status;
    private UUID customerMovementWavePspId;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getRefundId() {
        return refundId;
    }

    public void setRefundId(UUID refundId) {
        this.refundId = refundId;
    }

    public MovementMoneyDTO getAmount() {
        return amount;
    }

    public void setAmount(MovementMoneyDTO amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getCurrencyRateProviderCustomer() {
        return currencyRateProviderCustomer;
    }

    public void setCurrencyRateProviderCustomer(BigDecimal currencyRateProviderCustomer) {
        this.currencyRateProviderCustomer = currencyRateProviderCustomer;
    }

    public MovementMoneyDTO getAmountInEur() {
        return amountInEur;
    }

    public void setAmountInEur(MovementMoneyDTO amountInEur) {
        this.amountInEur = amountInEur;
    }

    public BigDecimal getCurrencyRateProviderEur() {
        return currencyRateProviderEur;
    }

    public void setCurrencyRateProviderEur(BigDecimal currencyRateProviderEur) {
        this.currencyRateProviderEur = currencyRateProviderEur;
    }
    public UUID getCustomerMovementWavePspId() {
        return customerMovementWavePspId;
    }

    public void setCustomerMovementWavePspId(UUID customerMovementWavePspId) {
        this.customerMovementWavePspId = customerMovementWavePspId;
    }

    public String getPsp() {
        return psp;
    }

    public void setPsp(String psp) {
        this.psp = psp;
    }

    public String getPspAccount() {
        return pspAccount;
    }

    public void setPspAccount(String pspAccount) {
        this.pspAccount = pspAccount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
