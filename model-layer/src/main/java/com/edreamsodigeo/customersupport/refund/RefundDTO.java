package com.edreamsodigeo.customersupport.refund;

import java.util.List;
import java.util.UUID;

public class RefundDTO {

    private UUID id;
    private List<ProviderRefundDTO> providerRefunds;
    private String customerRefundMethod;
    private Double customerRefundAmount;
    private String customerRefundCurrency;
    private Long bookingItemId;
    private String status;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<ProviderRefundDTO> getProviderRefunds() {
        return providerRefunds;
    }

    public void setProviderRefunds(List<ProviderRefundDTO> providerRefunds) {
        this.providerRefunds = providerRefunds;
    }

    public String getCustomerRefundMethod() {
        return customerRefundMethod;
    }

    public void setCustomerRefundMethod(String customerRefundMethod) {
        this.customerRefundMethod = customerRefundMethod;
    }

    public Double getCustomerRefundAmount() {
        return customerRefundAmount;
    }

    public void setCustomerRefundAmount(Double customerRefundAmount) {
        this.customerRefundAmount = customerRefundAmount;
    }

    public String getCustomerRefundCurrency() {
        return customerRefundCurrency;
    }

    public void setCustomerRefundCurrency(String customerRefundCurrency) {
        this.customerRefundCurrency = customerRefundCurrency;
    }

    public Long getBookingItemId() {
        return bookingItemId;
    }

    public void setBookingItemId(Long bookingItemId) {
        this.bookingItemId = bookingItemId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
