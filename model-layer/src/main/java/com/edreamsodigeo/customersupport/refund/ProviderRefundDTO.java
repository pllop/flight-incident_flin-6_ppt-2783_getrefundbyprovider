package com.edreamsodigeo.customersupport.refund;

import java.util.UUID;

public class ProviderRefundDTO {

    private UUID id;
    private Long providerBookingItemId;
    private Double expectedProviderRefundAmount;
    private String expectedProviderRefundCurrency;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getProviderBookingItemId() {
        return providerBookingItemId;
    }

    public void setProviderBookingItemId(Long providerBookingItemId) {
        this.providerBookingItemId = providerBookingItemId;
    }

    public Double getExpectedProviderRefundAmount() {
        return expectedProviderRefundAmount;
    }

    public void setExpectedProviderRefundAmount(Double expectedProviderRefundAmount) {
        this.expectedProviderRefundAmount = expectedProviderRefundAmount;
    }

    public String getExpectedProviderRefundCurrency() {
        return expectedProviderRefundCurrency;
    }

    public void setExpectedProviderRefundCurrency(String expectedProviderRefundCurrency) {
        this.expectedProviderRefundCurrency = expectedProviderRefundCurrency;
    }
}
