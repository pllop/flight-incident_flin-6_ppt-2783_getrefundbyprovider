package com.edreamsodigeo.customersupport.refund.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "REFUND")
@Access(AccessType.FIELD)
public class Refund implements Cloneable {

    @EmbeddedId
    private RefundId refundId;

    @Column(name = "CUSTOMER_REFUND_METHOD")
    private String customerRefundMethod;

    @Column(name = "CUSTOMER_REFUND_AMOUNT")
    private Double customerRefundAmount;

    @Column(name = "CUSTOMER_REFUND_CURRENCY")
    private String customerRefundCurrency;

    @Column(name = "BOOKING_ITEM_ID")
    private Long bookingItemId;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "REFUND_ID", referencedColumnName = "ID", nullable = false)
    private List<ProviderRefund> providerRefunds;

    @Column(name = "STATUS")
    private String status;

    @Override
    public Refund clone() {
        try {
            return (Refund) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public RefundId getRefundId() {
        return refundId;
    }

    public void setRefundId(RefundId refundId) {
        this.refundId = refundId;
    }

    public String getCustomerRefundMethod() {
        return customerRefundMethod;
    }

    public void setCustomerRefundMethod(String customerRefundMethod) {
        this.customerRefundMethod = customerRefundMethod;
    }

    public Double getCustomerRefundAmount() {
        return customerRefundAmount;
    }

    public void setCustomerRefundAmount(Double customerRefundAmount) {
        this.customerRefundAmount = customerRefundAmount;
    }

    public String getCustomerRefundCurrency() {
        return customerRefundCurrency;
    }

    public void setCustomerRefundCurrency(String customerRefundCurrency) {
        this.customerRefundCurrency = customerRefundCurrency;
    }

    public Long getBookingItemId() {
        return bookingItemId;
    }

    public void setBookingItemId(Long bookingItemId) {
        this.bookingItemId = bookingItemId;
    }

    public List<ProviderRefund> getProviderRefunds() {
        return providerRefunds;
    }

    public void setProviderRefunds(List<ProviderRefund> providerRefunds) {
        this.providerRefunds = providerRefunds;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
