package com.edreamsodigeo.customersupport.cancellation.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "ITEM")
@Access(AccessType.FIELD)
public class Item implements Cloneable {
    @EmbeddedId
    private ItemId id;

    @Column(nullable = false, name = "BOOKING_ITEM_ID")
    private Long bookingItemId;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ITEM_ID", referencedColumnName = "ID", nullable = false)
    private List<ProviderItem> providerItems;

    @Override
    public Item clone() {
        try {
            return (Item) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public ItemId getId() {
        return id;
    }

    public void setId(ItemId id) {
        this.id = id;
    }

    public Long getBookingItemId() {
        return bookingItemId;
    }

    public void setBookingItemId(Long bookingItemId) {
        this.bookingItemId = bookingItemId;
    }

    public List<ProviderItem> getProviderItems() {
        return providerItems;
    }

    public void setProviderItems(List<ProviderItem> providerItems) {
        this.providerItems = providerItems;
    }
}
