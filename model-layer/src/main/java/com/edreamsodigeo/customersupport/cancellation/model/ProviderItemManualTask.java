package com.edreamsodigeo.customersupport.cancellation.model;

import org.hibernate.annotations.Type;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "PROV_ITEM_MANUAL_TASKS")
@Access(AccessType.FIELD)
public class ProviderItemManualTask implements Cloneable {

    @EmbeddedId
    private ProviderItemManualTaskId id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROV_ITEM_ID", nullable = false)
    private ProviderItem providerItem;

    @Column(name = "TOD_ID")
    private Long taskOnDemandId;

    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.LocalDateTimeUserType")
    @Column(nullable = false, name = "TIMESTAMP")
    private LocalDateTime timestamp;

    public ProviderItemManualTaskId getId() {
        return id;
    }

    public void setId(ProviderItemManualTaskId id) {
        this.id = id;
    }

    public ProviderItem getProviderItem() {
        return providerItem;
    }

    public void setProviderItem(ProviderItem providerItem) {
        this.providerItem = providerItem;
    }

    public Long getTaskOnDemandId() {
        return taskOnDemandId;
    }

    public void setTaskOnDemandId(Long taskOnDemandId) {
        this.taskOnDemandId = taskOnDemandId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public ProviderItemManualTask clone() {
        try {
            return (ProviderItemManualTask) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
