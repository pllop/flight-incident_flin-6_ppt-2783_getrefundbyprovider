package com.edreamsodigeo.customersupport.cancellation;

import java.time.LocalDateTime;
import java.util.UUID;

public class ProviderItemManualTaskDTO {

    private UUID id;
    private UUID providerItemId;
    private Long taskOnDemandId;
    private LocalDateTime timestamp;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getProviderItemId() {
        return providerItemId;
    }

    public void setProviderItemId(UUID providerItemId) {
        this.providerItemId = providerItemId;
    }

    public Long getTaskOnDemandId() {
        return taskOnDemandId;
    }

    public void setTaskOnDemandId(Long taskOnDemandId) {
        this.taskOnDemandId = taskOnDemandId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
