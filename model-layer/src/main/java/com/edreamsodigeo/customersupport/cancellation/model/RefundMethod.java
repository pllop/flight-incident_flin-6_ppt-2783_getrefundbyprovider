package com.edreamsodigeo.customersupport.cancellation.model;

public enum RefundMethod {
    CASH,
    WALLET;
}
