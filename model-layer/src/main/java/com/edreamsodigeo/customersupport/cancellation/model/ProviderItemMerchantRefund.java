package com.edreamsodigeo.customersupport.cancellation.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "PROV_ITEM_MERCHANT_REFUND")
@Access(AccessType.FIELD)
public class ProviderItemMerchantRefund implements Cloneable {

    @EmbeddedId
    private ProviderItemMerchantRefundId id;

    @AttributeOverride(name = "id", column = @Column(name = "PROV_ITEM_ID"))
    private ProviderItemId providerItemId;

    @Column(name = "CUSTOMER_REFUND_METHOD")
    private RefundMethod customerRefundMethod;

    @Column(name = "CUSTOMER_REFUND_AMOUNT")
    private BigDecimal customerRefundAmount;

    @Column(name = "CUSTOMER_REFUND_CURRENCY")
    private String customerRefundCurrency;

    @Column(name = "EXPECTED_PROV_REFUND_AMOUNT")
    private BigDecimal expectedProviderRefundAmount;

    @Column(name = "EXPECTED_PROV_REFUND_CURRENCY")
    private String expectedProviderRefundCurrency;

    @Embedded
    private RefundId refundId;

    public ProviderItemMerchantRefundId getId() {
        return id;
    }

    public void setId(ProviderItemMerchantRefundId id) {
        this.id = id;
    }

    public ProviderItemId getProviderItemId() {
        return providerItemId;
    }

    public void setProviderItemId(ProviderItemId providerItemId) {
        this.providerItemId = providerItemId;
    }

    public RefundMethod getCustomerRefundMethod() {
        return customerRefundMethod;
    }

    public void setCustomerRefundMethod(RefundMethod customerRefundMethod) {
        this.customerRefundMethod = customerRefundMethod;
    }

    public BigDecimal getCustomerRefundAmount() {
        return customerRefundAmount;
    }

    public void setCustomerRefundAmount(BigDecimal customerRefundAmount) {
        this.customerRefundAmount = customerRefundAmount;
    }

    public BigDecimal getExpectedProviderRefundAmount() {
        return expectedProviderRefundAmount;
    }

    public void setExpectedProviderRefundAmount(BigDecimal expectedProviderRefundAmount) {
        this.expectedProviderRefundAmount = expectedProviderRefundAmount;
    }

    public String getExpectedProviderRefundCurrency() {
        return expectedProviderRefundCurrency;
    }

    public void setExpectedProviderRefundCurrency(String expectedProviderRefundCurrency) {
        this.expectedProviderRefundCurrency = expectedProviderRefundCurrency;
    }

    public String getCustomerRefundCurrency() {
        return customerRefundCurrency;
    }

    public void setCustomerRefundCurrency(String customerRefundCurrency) {
        this.customerRefundCurrency = customerRefundCurrency;
    }

    public RefundId getRefundId() {
        return refundId;
    }

    public void setRefundId(RefundId refundId) {
        this.refundId = refundId;
    }

    @Override
    public ProviderItemMerchantRefund clone() {
        try {
            return (ProviderItemMerchantRefund) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
