package com.edreamsodigeo.customersupport.cancellation;

import java.math.BigDecimal;

public class MoneyDTO {
    private String currency;
    private BigDecimal amount;

    public MoneyDTO() {
    }

    public MoneyDTO(BigDecimal amount, String currency) {
        this.currency = currency;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
