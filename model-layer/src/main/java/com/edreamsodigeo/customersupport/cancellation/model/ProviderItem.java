package com.edreamsodigeo.customersupport.cancellation.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "PROV_ITEM")
@Access(AccessType.FIELD)
public class ProviderItem implements Cloneable {
    @EmbeddedId
    private ProviderItemId id;

    @Column(nullable = false, name = "PROVIDER_BOOKING_ITEM_ID")
    private Long providerBookingItemId;

    @Column(name = "RESOLUTION_TYPE")
    private String resolutionType;

    @Column(name = "TOP_UP_VOUCHER_AMOUNT")
    private BigDecimal topUpVoucherAmount;

    @Column(name = "TOP_UP_VOUCHER_MIN_BKT_AMOUNT")
    private BigDecimal topUpVoucherMinBasketAmount;

    @Column(name = "TOP_UP_VOUCHER_CURRENCY")
    private String topUpVoucherCurrency;

    @Override
    public ProviderItem clone() {
        try {
            return (ProviderItem) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public ProviderItemId getId() {
        return id;
    }

    public void setId(ProviderItemId id) {
        this.id = id;
    }

    public Long getProviderBookingItemId() {
        return providerBookingItemId;
    }

    public void setProviderBookingItemId(Long providerBookingItemId) {
        this.providerBookingItemId = providerBookingItemId;
    }

    public String getResolutionType() {
        return resolutionType;
    }

    public void setResolutionType(String resolutionType) {
        this.resolutionType = resolutionType;
    }

    public BigDecimal getTopUpVoucherAmount() {
        return topUpVoucherAmount;
    }

    public void setTopUpVoucherAmount(BigDecimal topUpVoucherAmount) {
        this.topUpVoucherAmount = topUpVoucherAmount;
    }

    public BigDecimal getTopUpVoucherMinBasketAmount() {
        return topUpVoucherMinBasketAmount;
    }

    public void setTopUpVoucherMinBasketAmount(BigDecimal topUpVoucherMinBasketAmount) {
        this.topUpVoucherMinBasketAmount = topUpVoucherMinBasketAmount;
    }

    public String getTopUpVoucherCurrency() {
        return topUpVoucherCurrency;
    }

    public void setTopUpVoucherCurrency(String topUpVoucherCurrency) {
        this.topUpVoucherCurrency = topUpVoucherCurrency;
    }
}
