package com.edreamsodigeo.customersupport.cancellation.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PROV_ITEM_DISCOUNT")
@Access(AccessType.FIELD)
public class ProviderItemDiscount implements Cloneable {
    @EmbeddedId
    private ProviderItemDiscountId id;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "PROV_ITEM_ID"))
    private ProviderItemId providerItemId;

    @Column(name = "DISCOUNT_TYPE")
    private String discountType;

    @Embedded
    private FlexibleTicketId flexibleTicketId;

    @Column(name = "VOUCHER_ID")
    private Long voucherId;

    @Column(name = "EMD_ID")
    private Long emdId;

    @Override
    public ProviderItemDiscount clone() {
        try {
            return (ProviderItemDiscount) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public ProviderItemDiscountId getId() {
        return id;
    }

    public void setId(ProviderItemDiscountId id) {
        this.id = id;
    }

    public ProviderItemId getProviderItemId() {
        return providerItemId;
    }

    public void setProviderItemId(ProviderItemId providerItemId) {
        this.providerItemId = providerItemId;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public FlexibleTicketId getFlexibleTicketId() {
        return flexibleTicketId;
    }

    public void setFlexibleTicketId(FlexibleTicketId flexibleTicketId) {
        this.flexibleTicketId = flexibleTicketId;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public Long getEmdId() {
        return emdId;
    }

    public void setEmdId(Long emdId) {
        this.emdId = emdId;
    }
}
