package com.edreamsodigeo.customersupport.cancellation.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "PROV_ITEM_NON_MERCHANT_REFUND")
@Access(AccessType.FIELD)
public class ProviderItemNonMerchantRefund implements Cloneable {

    @EmbeddedId
    private ProviderItemNonMerchantRefundId id;

    @AttributeOverride(name = "id", column = @Column(name = "PROV_ITEM_ID"))
    private ProviderItemId providerItemId;

    @Column(name = "EXPECTED_PROV_REFUND_AMOUNT")
    private BigDecimal expectedProviderRefundAmount;

    @Column(name = "EXPECTED_PROV_REFUND_CURRENCY")
    private String expectedProviderRefundCurrency;

    @Column(name = "REFUNDER")
    private String refunder;


    public ProviderItemNonMerchantRefundId getId() {
        return id;
    }

    public void setId(ProviderItemNonMerchantRefundId id) {
        this.id = id;
    }

    public ProviderItemId getProviderItemId() {
        return providerItemId;
    }

    public void setProviderItemId(ProviderItemId providerItemId) {
        this.providerItemId = providerItemId;
    }

    public BigDecimal getExpectedProviderRefundAmount() {
        return expectedProviderRefundAmount;
    }

    public void setExpectedProviderRefundAmount(BigDecimal expectedProviderRefundAmount) {
        this.expectedProviderRefundAmount = expectedProviderRefundAmount;
    }

    public String getExpectedProviderRefundCurrency() {
        return expectedProviderRefundCurrency;
    }

    public void setExpectedProviderRefundCurrency(String expectedProviderRefundCurrency) {
        this.expectedProviderRefundCurrency = expectedProviderRefundCurrency;
    }

    public String getRefunder() {
        return refunder;
    }

    public void setRefunder(String refunder) {
        this.refunder = refunder;
    }

    @Override
    public ProviderItemNonMerchantRefund clone() {
        try {
            return (ProviderItemNonMerchantRefund) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
