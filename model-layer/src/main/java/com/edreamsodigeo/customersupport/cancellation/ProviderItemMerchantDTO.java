package com.edreamsodigeo.customersupport.cancellation;

import com.edreamsodigeo.customersupport.cancellation.model.RefundMethod;

import java.util.UUID;

public class ProviderItemMerchantDTO {

    private UUID id;
    private UUID providerItemId;
    private RefundMethod customerRefundMethod;
    private MoneyDTO customerRefundAmount;
    private MoneyDTO providerRefundAmountExpected;
    private UUID refundId;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getProviderItemId() {
        return providerItemId;
    }

    public void setProviderItemId(UUID providerItemId) {
        this.providerItemId = providerItemId;
    }

    public RefundMethod getCustomerRefundMethod() {
        return customerRefundMethod;
    }

    public void setCustomerRefundMethod(RefundMethod customerRefundMethod) {
        this.customerRefundMethod = customerRefundMethod;
    }

    public MoneyDTO getCustomerRefundAmount() {
        return customerRefundAmount;
    }

    public void setCustomerRefundAmount(MoneyDTO customerRefundAmount) {
        this.customerRefundAmount = customerRefundAmount;
    }

    public MoneyDTO getProviderRefundAmountExpected() {
        return providerRefundAmountExpected;
    }

    public void setProviderRefundAmountExpected(MoneyDTO providerRefundAmountExpected) {
        this.providerRefundAmountExpected = providerRefundAmountExpected;
    }

    public UUID getRefundId() {
        return refundId;
    }

    public void setRefundId(UUID refundId) {
        this.refundId = refundId;
    }
}
