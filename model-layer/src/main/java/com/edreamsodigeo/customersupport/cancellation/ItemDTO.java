package com.edreamsodigeo.customersupport.cancellation;

import java.util.List;
import java.util.UUID;

public class ItemDTO {
    private UUID id;
    private Long bookingItemId;
    private List<ProviderItemDTO> providerItems;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getBookingItemId() {
        return bookingItemId;
    }

    public void setBookingItemId(Long bookingItemId) {
        this.bookingItemId = bookingItemId;
    }

    public List<ProviderItemDTO> getProviderItems() {
        return providerItems;
    }

    public void setProviderItems(List<ProviderItemDTO> providerItems) {
        this.providerItems = providerItems;
    }
}
