package com.edreamsodigeo.customersupport.cancellation;

import java.util.UUID;

public class ProviderItemDiscountDTO {
    private UUID id;
    private UUID providerItemId;
    private String discountType;
    private UUID flexibleTicketId;
    private Long voucherId;
    private Long emdId;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getProviderItemId() {
        return providerItemId;
    }

    public void setProviderItemId(UUID providerItemId) {
        this.providerItemId = providerItemId;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public UUID getFlexibleTicketId() {
        return flexibleTicketId;
    }

    public void setFlexibleTicketId(UUID flexibleTicketId) {
        this.flexibleTicketId = flexibleTicketId;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public Long getEmdId() {
        return emdId;
    }

    public void setEmdId(Long emdId) {
        this.emdId = emdId;
    }
}
