package com.edreamsodigeo.customersupport.cancellation;

import java.util.UUID;

public class ProviderItemNonMerchantDTO {

    private UUID id;
    private UUID providerItemId;
    private MoneyDTO providerRefundAmountExpected;
    private String refunder;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getProviderItemId() {
        return providerItemId;
    }

    public void setProviderItemId(UUID providerItemId) {
        this.providerItemId = providerItemId;
    }

    public MoneyDTO getProviderRefundAmountExpected() {
        return providerRefundAmountExpected;
    }

    public void setProviderRefundAmountExpected(MoneyDTO providerRefundAmountExpected) {
        this.providerRefundAmountExpected = providerRefundAmountExpected;
    }

    public String getRefunder() {
        return refunder;
    }

    public void setRefunder(String refunder) {
        this.refunder = refunder;
    }
}
