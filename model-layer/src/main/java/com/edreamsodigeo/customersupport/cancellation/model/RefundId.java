package com.edreamsodigeo.customersupport.cancellation.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class RefundId implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "REFUND_ID")
    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.UUIDUserType")
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public RefundId() {
    }

    public RefundId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RefundId refundId = (RefundId) o;
        return Objects.equals(id, refundId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
