package com.edreamsodigeo.customersupport.cancellation.model;

import org.hibernate.annotations.Type;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "CANCELLATION")
@Access(AccessType.FIELD)
public class Cancellation implements Cloneable {
    @EmbeddedId
    private CancellationId id;

    @Embedded
    private IncidentId incidentId;

    @Type(type = "true_false")
    @Column(nullable = false, name = "IS_DISCARDED")
    private Boolean discarded;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "BUYER_EMAIL")
    private String buyerEmail;

    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.LocalDateTimeUserType")
    @Column(nullable = false, name = "TIMESTAMP")
    private LocalDateTime timestamp;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "CANCELLATION_ID", referencedColumnName = "ID", nullable = false)
    private List<Item> items;

    @Override
    public Cancellation clone() {
        try {
            return (Cancellation) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public CancellationId getId() {
        return id;
    }

    public void setId(CancellationId id) {
        this.id = id;
    }

    public IncidentId getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(IncidentId incidentId) {
        this.incidentId = incidentId;
    }

    public Boolean getDiscarded() {
        return discarded;
    }

    public void setDiscarded(Boolean discarded) {
        this.discarded = discarded;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }
}
