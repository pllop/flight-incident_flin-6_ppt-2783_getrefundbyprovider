package com.edreamsodigeo.customersupport.cancellation.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class FlexibleTicketId implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "FLEXIBLE_TICKET_ID")
    @Type(type = "com.edreamsodigeo.customersupport.flightincident.repository.converter.UUIDUserType")
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public FlexibleTicketId() {
    }

    public FlexibleTicketId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FlexibleTicketId flexibleTicketId = (FlexibleTicketId) o;
        return Objects.equals(id, flexibleTicketId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
