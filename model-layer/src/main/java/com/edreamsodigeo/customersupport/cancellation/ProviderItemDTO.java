package com.edreamsodigeo.customersupport.cancellation;

import java.util.UUID;

public class ProviderItemDTO {
    private UUID id;
    private Long providerBookingItemId;
    private String resolutionType;
    private MoneyDTO topUpVoucherAmount;
    private MoneyDTO topUpVoucherMinBasketAmount;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getProviderBookingItemId() {
        return providerBookingItemId;
    }

    public void setProviderBookingItemId(Long providerBookingItemId) {
        this.providerBookingItemId = providerBookingItemId;
    }

    public String getResolutionType() {
        return resolutionType;
    }

    public void setResolutionType(String resolutionType) {
        this.resolutionType = resolutionType;
    }

    public MoneyDTO getTopUpVoucherAmount() {
        return topUpVoucherAmount;
    }

    public void setTopUpVoucherAmount(MoneyDTO topUpVoucherAmount) {
        this.topUpVoucherAmount = topUpVoucherAmount;
    }

    public MoneyDTO getTopUpVoucherMinBasketAmount() {
        return topUpVoucherMinBasketAmount;
    }

    public void setTopUpVoucherMinBasketAmount(MoneyDTO topUpVoucherMinBasketAmount) {
        this.topUpVoucherMinBasketAmount = topUpVoucherMinBasketAmount;
    }
}
