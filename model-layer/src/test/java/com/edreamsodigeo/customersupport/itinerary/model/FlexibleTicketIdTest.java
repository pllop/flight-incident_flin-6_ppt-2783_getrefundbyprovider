package com.edreamsodigeo.customersupport.itinerary.model;

import com.edreamsodigeo.customersupport.test.utils.BeanTest;

import java.util.UUID;


public class FlexibleTicketIdTest extends BeanTest<FlexibleTicketId> {

    @Override
    protected FlexibleTicketId getBean() {
        return new FlexibleTicketId(UUID.fromString("94e35b37-45f4-49ed-a7cd-8f82ac2f7d2d"));
    }

    @Override
    protected boolean checkEquals() {
        return true;
    }

    @Override
    protected boolean checkHashCode() {
        return true;
    }
}
