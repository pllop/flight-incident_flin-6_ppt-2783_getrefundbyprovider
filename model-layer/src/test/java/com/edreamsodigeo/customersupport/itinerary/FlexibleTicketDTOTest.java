package com.edreamsodigeo.customersupport.itinerary;


import com.edreamsodigeo.customersupport.itinerary.model.Status;
import com.edreamsodigeo.customersupport.test.utils.BeanTest;

import java.util.UUID;

public class FlexibleTicketDTOTest extends BeanTest<FlexibleTicketDTO> {

    @Override
    protected FlexibleTicketDTO getBean() {
        return new FlexibleTicketDTO.FlexibleTicketDTOBuilder()
                .id(UUID.fromString("94e35b37-45f4-49ed-a7cd-8f82ac2f7d2d"))
                .providerBookingItemId(12345L)
                .customerId(6789L)
                .status(Status.AVAILABLE.toString())
                .monetaryValue(new MoneyDTO())
                .build();
    }

    protected boolean checkEquals() {
        return true;
    }

    protected boolean checkHashCode() {
        return true;
    }

    protected boolean checkToString() {
        return true;
    }
}
