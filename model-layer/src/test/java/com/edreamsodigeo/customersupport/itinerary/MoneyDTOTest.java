package com.edreamsodigeo.customersupport.itinerary;

import com.edreamsodigeo.customersupport.test.utils.BeanTest;

import java.math.BigDecimal;


public class MoneyDTOTest extends BeanTest<MoneyDTO> {

    @Override
    protected MoneyDTO getBean() {
        return new MoneyDTO(BigDecimal.valueOf(12345L), "CURR");
    }

    @Override
    protected boolean checkEquals() {
        return true;
    }

    @Override
    protected boolean checkHashCode() {
        return true;
    }
}
