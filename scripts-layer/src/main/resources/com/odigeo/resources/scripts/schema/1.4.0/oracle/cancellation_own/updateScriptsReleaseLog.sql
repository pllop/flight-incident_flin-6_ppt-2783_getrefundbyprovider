UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.4.0' where ID ='flin-17/01_table_cancellation.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.4.0' where ID ='flin-17/02_table_cancelled_item.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.4.0' where ID ='rollback/R_01_table_cancellation.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.4.0' where ID ='rollback/R_02_table_cancelled_item.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.4.0',systimestamp,'flight-incident');
