CREATE TABLE CANCELLATION_OWN.CANCELLED_ITEM_MANUAL(
  ID RAW(16) NOT NULL,
  CANCELLED_ITEM_ID RAW(16) NOT NULL,
  TOD_ID NUMBER(12,0),
  TIMESTAMP DATE NOT NULL,
  CONSTRAINT CANCELLED_ITEM_MANUAL_PK PRIMARY KEY (ID) ENABLE,
  CONSTRAINT CANCELLED_ITEM_FK
    FOREIGN KEY (CANCELLED_ITEM_ID)
    REFERENCES CANCELLED_ITEM(ID)
);
