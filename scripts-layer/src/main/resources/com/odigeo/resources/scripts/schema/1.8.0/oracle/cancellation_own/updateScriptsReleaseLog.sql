UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.8.0' where ID ='flin-29/01_alter_table_cancelled_item_manual.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.8.0' where ID ='flin-29/02_table_item.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.8.0' where ID ='flin-29/03_table_provider_item.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.8.0' where ID ='flin-30/01_table_provider_item_manual_task.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.8.0' where ID ='rollback/R_02_table_provider_item_manual_task.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.8.0',systimestamp,'flight-incident');
