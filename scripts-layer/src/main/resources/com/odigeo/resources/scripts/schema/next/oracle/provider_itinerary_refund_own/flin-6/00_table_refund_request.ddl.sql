create table PROVIDER_ITINERARY_REFUND_OWN.refund_request (
    ID RAW(16) NOT NULL,
    PROVIDER_BOOKING_ITEM_ID INTEGER NOT NULL,
    METHOD VARCHAR2(10) NOT NULL,
    STATUS VARCHAR2(30) NOT NULL,
    REQUESTED_AMOUNT NUMBER(10,2),
    ACCEPTED_AMOUNT NUMBER(10,2),
    TICKET_NUMBER INTEGER,
    PROVIDER_REFUND_REQUEST_ID INTEGER,
    REQUEST_TIMESTAMP TIMESTAMP,
    PROVIDER_RESPONSE_TIMESTAMP TIMESTAMP,
    CONSTRAINT REFUND_REQUEST_PK PRIMARY KEY (ID) ENABLE,
    CONSTRAINT REFUND_REQUEST_TN_UNIQUE UNIQUE(TICKET_NUMBER),
    CONSTRAINT REFUND_REQUEST_PRR_ID_UNIQUE UNIQUE(PROVIDER_REFUND_REQUEST_ID)
);
