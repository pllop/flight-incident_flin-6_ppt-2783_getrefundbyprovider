UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.10.0' where ID ='asq-1534/01_table_prov_item_discount.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.10.0' where ID ='rollback/R01_drop_table_prov_item_discount.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.10.0' where ID ='flin-34/01_cancelled_item_manual.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.10.0' where ID ='flin-34/02_cancelled_item.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.10.0' where ID ='flin-34/03_prov_item.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.10.0',systimestamp,'flight-incident');
