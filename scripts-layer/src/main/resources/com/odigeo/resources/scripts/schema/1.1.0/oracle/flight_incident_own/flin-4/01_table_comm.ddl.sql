  CREATE TABLE FLIGHT_INCIDENT_OWN.CONSENT_COMMS_LOG(	
  ID RAW(16) NOT NULL, 
  INCIDENT_ID RAW(16) NOT NULL, 
  TYPE VARCHAR2(20) NOT NULL, 
  MESSAGE VARCHAR2(250 BYTE), 
  TIMESTAMP DATE NOT NULL, 
  CONSTRAINT COMMS_KEY PRIMARY KEY (ID));
  