UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.9.4' where ID ='asq-1523/01_table_prov_item_merchant_refund.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.9.4' where ID ='asq-1523/02_table_prov_item_non_merchant_refund.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.9.4' where ID ='rollback/R01_drop_table_prov_item_merchant_refund.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.9.4' where ID ='rollback/R02_drop_table_prov_item_non_merchant_refund.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.9.4',systimestamp,'flight-incident');
