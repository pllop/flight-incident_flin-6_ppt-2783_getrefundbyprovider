UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.1.0' where ID ='flin-4/01_table_comm.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.1.0' where ID ='flin-4/02_table_option.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.1.0' where ID ='flin-4/03_table_resp.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.1.0' where ID ='rollback/R_01_table_comm.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.1.0' where ID ='rollback/R_02_table_option.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.1.0' where ID ='rollback/R_03_table_resp.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.1.0',systimestamp,'flight-incident');
