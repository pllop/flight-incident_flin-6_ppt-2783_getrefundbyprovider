package com.edreamsodigeo.customersupport.flightincident;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.v1.CancellationResource;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.CancellationNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Item;
import com.edreamsodigeo.customersupport.cashrefund.BookingItemCashRefund;
import com.edreamsodigeo.customersupport.cashrefund.CancellationClassification;
import com.edreamsodigeo.customersupport.cashrefund.ProviderBookingItemCashRefund;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.repository.IncidentRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaIncidentRepository;
import com.odigeo.product.itinerary.provideritineraryrefundapi.BspLinkRefundRequest;
import com.odigeo.product.itinerary.provideritineraryrefundapi.DataBaseException;
import com.odigeo.product.itinerary.provideritineraryrefundapi.ProviderItineraryRefundService;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Stateless
@Local(CashRefundService.class)
public class CashRefundServiceBean implements CashRefundService {

    private final Logger logger = Logger.getLogger(CashRefundServiceBean.class);

    private IncidentRepository incidentRepository;
    private CancellationResource cancellationResource;
    private ProviderItineraryRefundService providerItineraryRefundService;

    // TODO: ugly "feature flag"... Remove this flag when FLIN-6 has been implemented and deployed
    private static boolean enableProviderItineraryRefundServiceIntegration = false;

    @PersistenceContext(unitName = "flight-incident")
    private EntityManager entityManager;

    @PostConstruct
    public void postConstruct() {
        incidentRepository = ConfigurationEngine.getInstance(IncidentRepository.class);
        ((JpaIncidentRepository) incidentRepository).setEntityManager(entityManager);

        cancellationResource = ConfigurationEngine.getInstance(CancellationResource.class);
        providerItineraryRefundService = ConfigurationEngine.getInstance(ProviderItineraryRefundService.class);
    }

    private void checkRefundRequested(ProviderBookingItemCashRefund providerBookingItemCashRefund) {
        if (!enableProviderItineraryRefundServiceIntegration) {
            return;
        }
        // for now, the provider-itinerary-refund API only exposes information about requests sent via BSPLink.
        // This method will be extended in the future, once support for other refund request methods gets added
        if (!providerBookingItemCashRefund.getCancellationClassification().isBSPLink()) {
            return;
        }
        try {
            BspLinkRefundRequest refundRequest = providerItineraryRefundService.getBspLinkRequestByProviderBookingItemId(providerBookingItemCashRefund.getProviderBookingItemId());
            switch (refundRequest.getStatus()) {
            case REQUESTED:
                providerBookingItemCashRefund.markRequested();
                break;
            case REJECTED:
                providerBookingItemCashRefund.markRejected();
                break;
            case ACCEPTED:
                providerBookingItemCashRefund.markApproved();
                break;
            }
        } catch (DataBaseException e) {
            logger.warn(String.format("Error encountered while checking the status of refund request for provider item %s", providerBookingItemCashRefund.getProviderBookingItemId()));
        }
    }

    private void updateRefundStatus(ProviderBookingItemCashRefund providerBookingItemCashRefund) {
        checkRefundRequested(providerBookingItemCashRefund);
    }

    private List<Item> getIncidentCashRefundCancelledItems(Incident incident) {
        CancellationId cancellationId = new CancellationId(incident.getCancellationId().getId().toString());
        try {
            Cancellation cancellation = cancellationResource.getCancellation(cancellationId);
            return cancellation.getItems()
                    .stream()
                    .filter(c -> c.getProviderItems().stream().anyMatch(p -> new CancellationClassification(p.getResolutionType().toUpperCase()).classifiedAsCashRefund()))
                    .collect(Collectors.toList());
        } catch (CancellationNotFoundException e) {
            logger.warn(String.format("Cancellation id %s linked to incident %s not found in cancellation module", cancellationId.getAsString(), incident.getId().getId()));
            return Collections.emptyList();
        } catch (Exception e) {
            logger.warn(String.format("Unexpected exception while querying cancellation %s from cancellation module: %s", cancellationId.getAsString(), e));
            return Collections.emptyList();
        }
    }

    @Override
    public List<BookingItemCashRefund> getRefundsStatus(IncidentId incidentId) throws RegisterNotFound {
        Incident incident = incidentRepository.incidentOfIncidentId(incidentId);
        if (Objects.isNull(incident)) {
            throw new RegisterNotFound(String.format("Incident with id %s not found", incidentId.getId()));
        }
        if (Objects.isNull(incident.getCancellationId())) {
            return Collections.emptyList();
        }
        List<Item> cashRefundCancelledItems = getIncidentCashRefundCancelledItems(incident);

        return cashRefundCancelledItems.stream().map(c -> {
            BookingItemCashRefund bookingItemCashRefund = new BookingItemCashRefund();
            bookingItemCashRefund.setBookingItemId(c.getBookingItemId().getId());
            bookingItemCashRefund.setProviderBookingItemCashRefunds(
                c.getProviderItems().stream()
                .filter(p -> new CancellationClassification(p.getResolutionType()).classifiedAsCashRefund())
                .map(p -> {
                    ProviderBookingItemCashRefund providerBookingItemCashRefund = new ProviderBookingItemCashRefund(
                            p.getProviderBookingItemId().getId(), new CancellationClassification(p.getResolutionType())
                    );
                    updateRefundStatus(providerBookingItemCashRefund);
                    return providerBookingItemCashRefund;
                })
                .collect(Collectors.toList())
            );
            return bookingItemCashRefund;
        }).collect(Collectors.toList());
    }

}
