package com.edreamsodigeo.customersupport.flightincident.email.sender;

import com.edreamsodigeo.customersupport.flightincident.email.render.EmailType;
import com.odigeo.crm.mailer.EmailApi;
import com.odigeo.crm.mailer.SendEmailRequest;

import javax.inject.Inject;

public class EmailSender {

    private final EmailApi emailApi;

    @Inject
    public EmailSender(EmailApi emailApi) {
        this.emailApi = emailApi;
    }

    public void sendCancellationEmail(com.edreamsodigeo.customersupport.flightincident.model.Incident incident) {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .bookingId(incident.getBookingId())
                .templateName(EmailType.CANCELLATION.getTemplateName())
                .build();
        emailApi.sendEmail(sendEmailRequest);
    }
}
