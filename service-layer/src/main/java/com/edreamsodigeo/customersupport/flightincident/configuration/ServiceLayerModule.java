package com.edreamsodigeo.customersupport.flightincident.configuration;

import com.edreamsodigeo.customersupport.cancellation.CancellationService;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemDiscountService;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskService;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantRefundService;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantRefundService;
import com.edreamsodigeo.customersupport.flightincident.CashRefundService;
import com.edreamsodigeo.customersupport.flightincident.FlightIncidentService;
import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketService;
import com.edreamsodigeo.customersupport.refund.RefundService;
import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementService;
import com.edreamsodigeo.customersupport.flightincident.RefundRequestService;
import com.google.inject.AbstractModule;
import com.odigeo.configuration.ServiceLocator;
import com.odigeo.configuration.UnavailableServiceException;
import org.apache.log4j.Logger;

public class ServiceLayerModule extends AbstractModule {

    private static final Logger LOGGER = Logger.getLogger(ServiceLayerModule.class);

    @Override
    public void configure() {
        bind(ServiceLocator.class).toInstance(JeeServiceLocator.getInstance());
        configureJeeServices();
    }

    private void configureJeeServices() {
        configureJeeService(FlightIncidentService.class);
        configureJeeService(CashRefundService.class);
        configureJeeService(CancellationService.class);
        configureJeeService(FlexibleTicketService.class);
        configureJeeService(ProviderItemManualTaskService.class);
        configureJeeService(ProviderItemDiscountService.class);
        configureJeeService(ProviderItemMerchantRefundService.class);
        configureJeeService(ProviderItemNonMerchantRefundService.class);
        configureJeeService(RefundService.class);
        configureJeeService(CustomerMovementService.class);
        configureJeeService(RefundRequestService.class);
        LOGGER.info("All services have been configured");
    }

    private <T> void configureJeeService(Class<T> clazz) {
        configureJeeService(clazz, clazz);
    }

    private <T> void configureJeeService(Class<T> clazz, Class<? extends T> providerClazz) {
        try {
            bind(clazz).toProvider(JeeServiceProvider.getInstance(providerClazz));
        } catch (UnavailableServiceException usex) {
            LOGGER.error("Unable to create provider " + providerClazz + " for class " + clazz, usex);
        }
    }


}
