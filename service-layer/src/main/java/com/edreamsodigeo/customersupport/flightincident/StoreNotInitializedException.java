package com.edreamsodigeo.customersupport.flightincident;

public class StoreNotInitializedException extends RuntimeException {
    public StoreNotInitializedException(String s) {
        super(s);
    }
}
