package com.edreamsodigeo.customersupport.flightincident.email.render;

public enum EmailType {

    CANCELLATION("emailCancellation");

    private final String templateName;

    EmailType(String emailCancellation) {
        templateName = emailCancellation;
    }

    public String getTemplateName() {
        return templateName;
    }
}
