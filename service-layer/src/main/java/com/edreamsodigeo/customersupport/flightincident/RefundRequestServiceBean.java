package com.edreamsodigeo.customersupport.flightincident;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.flightincident.exception.DatabaseInconsistency;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.model.BspLinkRefundRequest;
import com.edreamsodigeo.customersupport.flightincident.model.RefundReceived;
import com.edreamsodigeo.customersupport.flightincident.model.RefundStatus;
import com.edreamsodigeo.customersupport.flightincident.repository.BspLinkRefundRequestRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.RefundReceivedRepository;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;

@Stateless
@Local(RefundRequestService.class)
public class RefundRequestServiceBean implements RefundRequestService {

    private final Logger logger = Logger.getLogger(RefundRequestServiceBean.class);

    @PersistenceContext(unitName = "provider-itinerary-refund")
    private EntityManager entityManager;

    private BspLinkRefundRequestRepository bspLinkRefundRequestRepository;
    private RefundReceivedRepository refundReceivedRepository;

    @PostConstruct
    public void postConstruct() {
        this.bspLinkRefundRequestRepository = ConfigurationEngine.getInstance(BspLinkRefundRequestRepository.class);
        this.refundReceivedRepository = ConfigurationEngine.getInstance(RefundReceivedRepository.class);
        this.bspLinkRefundRequestRepository.setEntityManager(entityManager);
        this.refundReceivedRepository.setEntityManager(entityManager);
    }

    @Override
    public void saveBspLinkRequest(BspLinkRefundRequest refundRequest) {
        bspLinkRefundRequestRepository.save(refundRequest);
    }

    @Override
    public void updateBspLinkAcceptedRequest(Long ticketId, BigDecimal amountAccepted, Instant acceptedTimestamp) throws RegisterNotFound {
        BspLinkRefundRequest refundRequest = bspLinkRefundRequestRepository.findByTicketId(ticketId);
        if (refundRequest == null) {
            throw new RegisterNotFound("Could not find BspLinkRequest with ticket number: " + ticketId);
        }
        refundRequest.setAcceptedRefundAmount(amountAccepted);
        refundRequest.setAirlineResponseInstant(acceptedTimestamp);
        refundRequest.setStatus(RefundStatus.ACCEPTED);
    }

    @Override
    public void updateBspLinkRejectedRequest(Long ticketId, Instant rejectionTimestamp) throws RegisterNotFound {
        BspLinkRefundRequest refundRequest = bspLinkRefundRequestRepository.findByTicketId(ticketId);
        if (refundRequest == null) {
            throw new RegisterNotFound("Could not find BspLinkRequest with ticket number: " + ticketId);
        }
        refundRequest.setAirlineResponseInstant(rejectionTimestamp);
        refundRequest.setStatus(RefundStatus.REJECTED);
    }

    @Override
    public BspLinkRefundRequest getBspLinkRequest(Long ticketId) throws RegisterNotFound {
        BspLinkRefundRequest refundRequest = bspLinkRefundRequestRepository.findByTicketId(ticketId);
        if (refundRequest == null) {
            throw new RegisterNotFound("Could not find BspLinkRequest with ticket number: " + ticketId);
        }
        return refundRequest;
    }

    @Override
    public BspLinkRefundRequest getBspLinkRequestByProviderBookingItemId(Long providerBookingItemId) throws RegisterNotFound {
        BspLinkRefundRequest refundRequest = bspLinkRefundRequestRepository.findByProviderBookingItemId(providerBookingItemId);
        if (refundRequest == null) {
            throw new RegisterNotFound("Could not find BspLinkRequest with provider booking item id: " + providerBookingItemId);
        }
        return refundRequest;
    }

    @Override
    public List<BspLinkRefundRequest> getBspLinkRequests(ZonedDateTime startDate, ZonedDateTime endDate) {
        return bspLinkRefundRequestRepository.findAllBetweenTimestamps(startDate.toInstant(), endDate.toInstant());
    }

    @Override
    public void saveBspLinkRefundReceived(Long ticketId, RefundReceived refundReceived) throws DatabaseInconsistency {
        UserTransaction tx = (UserTransaction) entityManager.getTransaction();
        try {
            tx.begin();
            BspLinkRefundRequest refundRequest = bspLinkRefundRequestRepository.findByTicketId(ticketId);
            if (refundRequest != null) {
                refundReceived.setRefundRequest(refundRequest);

                if (RefundStatus.REQUESTED.equals(refundRequest.getStatus())) {
                    refundRequest.setStatus(RefundStatus.ACCEPTED);
                    refundRequest.setAcceptedRefundAmount(refundReceived.getProviderAmountRefunded());
                } else {
                    throw new DatabaseInconsistency("The request has status: " + refundRequest.getStatus().name());
                }
            }

            refundReceivedRepository.save(refundReceived);
            tx.commit();
        } catch (Exception e) {
            logger.error("Error while trying save refund received request for ticketId" + ticketId, e);
            performRollback(tx);
        }
    }

    private void performRollback(UserTransaction tx) throws DatabaseInconsistency {
        try {
            if (tx.getStatus() == Status.STATUS_ACTIVE || tx.getStatus() == Status.STATUS_MARKED_ROLLBACK) {
                tx.rollback();
            }
        } catch (SystemException ex) {
            throw new DatabaseInconsistency("Error while trying to perform the transaction rollback for ticketId: ");
        }
    }
}
