package com.edreamsodigeo.customersupport.flightincident;

import com.edreamsodigeo.customersupport.cashrefund.BookingItemCashRefund;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;

import java.util.List;

public interface CashRefundService {

    List<BookingItemCashRefund> getRefundsStatus(IncidentId incidentId) throws RegisterNotFound;

}
