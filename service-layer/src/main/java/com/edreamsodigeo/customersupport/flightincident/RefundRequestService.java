package com.edreamsodigeo.customersupport.flightincident;

import com.edreamsodigeo.customersupport.flightincident.exception.DatabaseInconsistency;
import com.edreamsodigeo.customersupport.flightincident.exception.InsertFailed;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.model.BspLinkRefundRequest;
import com.edreamsodigeo.customersupport.flightincident.model.RefundReceived;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;

public interface RefundRequestService {

    void saveBspLinkRequest(BspLinkRefundRequest refundRequest) throws InsertFailed;

    void updateBspLinkAcceptedRequest(Long ticketId, BigDecimal amountAccepted, Instant acceptedTimestamp) throws RegisterNotFound;

    void updateBspLinkRejectedRequest(Long ticketId, Instant rejectionTimestamp) throws RegisterNotFound;

    BspLinkRefundRequest getBspLinkRequest(Long ticketId) throws RegisterNotFound;

    BspLinkRefundRequest getBspLinkRequestByProviderBookingItemId(Long providerBookingItemId) throws RegisterNotFound;

    List<BspLinkRefundRequest> getBspLinkRequests(ZonedDateTime startDate, ZonedDateTime endDate);

    void saveBspLinkRefundReceived(Long ticketId, RefundReceived refundReceived) throws RegisterNotFound, DatabaseInconsistency;
}
