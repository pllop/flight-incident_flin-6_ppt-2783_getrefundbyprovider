package com.edreamsodigeo.customersupport.flightincident;

import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;

import java.util.List;
import java.util.UUID;

public interface FlightIncidentService {

    List<Incident> getIncidents(Long bookingId) throws RegisterNotFound;

    Incident getIncident(UUID incidentId) throws RegisterNotFound;

    void updateConsent(UUID incidentId, ConsentDTO consentDTO, String channel) throws RegisterNotFound;

    void addIncident(IncidentDTO incidentDTO);

    void updateIncident(IncidentDTO incidentDTO) throws RegisterNotFound;

    void addCancellationID(UUID incidentId, UUID cancellationId) throws RegisterNotFound;

    List<Incident> getIncidentsWithoutCancellation(int pageNumber);
}
