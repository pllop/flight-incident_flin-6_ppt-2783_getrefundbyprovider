package com.edreamsodigeo.customersupport.flightincident;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.v1.CancellationResource;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.CancellationNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItem;
import com.edreamsodigeo.customersupport.flightincident.email.render.EmailType;
import com.edreamsodigeo.customersupport.flightincident.email.sender.EmailSender;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.mapper.IncidentMapper;
import com.edreamsodigeo.customersupport.flightincident.model.CancellationId;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommOption;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommOptionId;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsLog;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsLogId;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsRespLog;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsRespLogId;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.model.OptionValue;
import com.edreamsodigeo.customersupport.flightincident.model.Status;
import com.edreamsodigeo.customersupport.flightincident.repository.IncidentRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaIncidentRepository;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import com.google.common.base.Strings;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;


@Stateless
@Local(FlightIncidentService.class)
public class FlightIncidentServiceBean implements FlightIncidentService {
    private static final Logger logger = Logger.getLogger(FlightIncidentServiceBean.class);

    private IncidentRepository incidentRepository;

    @PersistenceContext(unitName = "flight-incident")
    private EntityManager entityManager;
    private IncidentMapper incidentMapper;

    private EmailSender emailSender;

    @PostConstruct
    public void postConstruct() {
        incidentRepository = ConfigurationEngine.getInstance(IncidentRepository.class);
        emailSender = ConfigurationEngine.getInstance(EmailSender.class);
        ((JpaIncidentRepository) incidentRepository).setEntityManager(entityManager);
        incidentMapper = ConfigurationEngine.getInstance(IncidentMapper.class);
    }

    @Override
    public List<Incident> getIncidents(Long bookingId) throws RegisterNotFound {
        List<com.edreamsodigeo.customersupport.flightincident.model.Incident> incidents = incidentRepository.incidentsOfBookingId(bookingId);
        if (incidents.isEmpty()) {
            throw new RegisterNotFound("No incidents found for given booking id: " + bookingId);
        }
        return incidentMapper.mapIncident(incidents);
    }

    @Override
    public Incident getIncident(UUID incidentId) throws RegisterNotFound {
        com.edreamsodigeo.customersupport.flightincident.model.Incident incident = incidentRepository.incidentOfIncidentId(new IncidentId(incidentId));
        if (Objects.isNull(incident)) {
            throw new RegisterNotFound("No incident found for given incident id: " + incidentId);
        }
        return incidentMapper.mapIncident(incident);
    }

    @Override
    public void updateConsent(UUID incidentId, ConsentDTO consentDTO, String channel) throws RegisterNotFound {
        com.edreamsodigeo.customersupport.flightincident.model.Incident incident = incidentRepository.incidentOfIncidentId(new IncidentId(incidentId));
        checkIncident(incidentId, incident);
        incident.setEmdAccepted(consentDTO.getEmdAccepted());
        incident.setFlexibleTicketAccepted(consentDTO.getFlexibleTicketAccepted());
        incident.setVoucherAccepted(consentDTO.getVoucherAccepted());
        incident.setCashRefundAccepted(consentDTO.getCashRefundAccepted());
        incident.setStatus(computeStatus(consentDTO));
        incident.setLastUpdate(LocalDateTime.now());
        addConsentLog(incident.getConsentCommsLogs(), consentDTO, channel);
        if (!Objects.isNull(incident.getCancellationId())) {
            updateCancellationModule(incident.getCancellationId());
            incident.setCancellationId(null);
        }
        incidentRepository.save(incident);
    }

    private void checkIncident(UUID incidentId, com.edreamsodigeo.customersupport.flightincident.model.Incident incident) throws RegisterNotFound {
        if (Objects.isNull(incident)) {
            throw new RegisterNotFound("No incidents found for given incident id: " + incidentId);
        }
    }

    private void updateCancellationModule(CancellationId cancellationId) throws RegisterNotFound {
        CancellationResource cancellationResource = ConfigurationEngine.getInstance(CancellationResource.class);
        try {
            cancellationResource.discardCancellation(new com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId(cancellationId.getId().toString()));
        } catch (CancellationNotFoundException e) {
            throw new RegisterNotFound(e.getMessage());
        }

    }

    private Status computeStatus(ConsentDTO consentDTO) {
        return Stream.of(consentDTO.getCashRefundAccepted(), consentDTO.getFlexibleTicketAccepted(), consentDTO.getVoucherAccepted(), consentDTO.getEmdAccepted()).anyMatch(Objects::nonNull) ? Status.CUSTOMER_CANCELLATION_CHOSEN : Status.WAITING_FOR_CUSTOMER_CHOICE;
    }

    private void addConsentLog(List<ConsentCommsLog> consentCommsLogs, ConsentDTO consentDTO, String channel) {
        if (!consentCommsLogs.isEmpty()) {
            ConsentCommsLog consentCommsLog = consentCommsLogs.get(0);
            List<OptionValue> optionValues = extractOptions(consentDTO);
            for (ConsentCommOption consentCommOption : consentCommsLog.getConsentCommOptions()) {
                Optional<OptionValue> first = optionValues.stream().filter(optionValue -> optionValue.equals(consentCommOption.getOptionValue())).findFirst();
                first.ifPresent(optionValue -> setCommResp(consentCommsLog, consentCommOption, optionValue, getBooleanFromOptionValue(consentDTO, optionValue), channel));
            }
        }
    }

    private void setCommResp(ConsentCommsLog consentCommsLogs, ConsentCommOption consentCommOption, OptionValue value, boolean b, String channel) {
        if (consentCommOption.getOptionValue().equals(value)) {
            buildConsentCommResp(consentCommsLogs, consentCommOption, b, channel);
        }
    }

    private void buildConsentCommResp(ConsentCommsLog consentCommsLogs, ConsentCommOption consentCommOption, boolean b, String channel) {
        ConsentCommsRespLog consentCommsRespLog = new ConsentCommsRespLog();
        consentCommsRespLog.setId(new ConsentCommsRespLogId(UUIDGenerator.getInstance().generateUUID()));
        consentCommsRespLog.setResponse(b);
        consentCommsRespLog.setResponseChannel(Strings.isNullOrEmpty(channel) ? "OF/Native" : channel);
        consentCommsRespLog.setResponseDate(LocalDateTime.now());
        consentCommsRespLog.setTimestamp(LocalDateTime.now());
        consentCommsRespLog.setConsentCommsLog(consentCommsLogs);
        consentCommsRespLog.setConsentCommOption(consentCommOption);
        consentCommOption.getConsentCommsRespLogs().add(consentCommsRespLog);
    }

    private Boolean getBooleanFromOptionValue(ConsentDTO consentDTO, OptionValue optionValue) {
        switch (optionValue) {
        case CASHOUT_CONSENT:
            return consentDTO.getCashRefundAccepted();
        case EDO_VOUCHER_CONSENT:
            return consentDTO.getVoucherAccepted();
        case EMD_CONSENT:
            return consentDTO.getEmdAccepted();
        case OPEN_TICKET_CONSENT:
            return consentDTO.getFlexibleTicketAccepted();
        }
        return false;
    }

    private List<OptionValue> extractOptions(ConsentDTO consentDTO) {
        List<OptionValue> optionValues = new ArrayList<>();
        if (consentDTO.getEmdAccepted() != null) {
            optionValues.add(OptionValue.EMD_CONSENT);
        }
        if (consentDTO.getFlexibleTicketAccepted() != null) {
            optionValues.add(OptionValue.OPEN_TICKET_CONSENT);
        }
        if (consentDTO.getVoucherAccepted() != null) {
            optionValues.add(OptionValue.EDO_VOUCHER_CONSENT);
        }
        if (consentDTO.getCashRefundAccepted() != null) {
            optionValues.add(OptionValue.CASHOUT_CONSENT);
        }

        return optionValues;
    }

    @Override
    public void addIncident(IncidentDTO incidentDTO) {
        incidentRepository.save(incidentMapper.mapIncidentDTOtoIncident(incidentDTO));
    }

    @Override
    public void updateIncident(IncidentDTO incidentDTO) throws RegisterNotFound {
        List<com.edreamsodigeo.customersupport.flightincident.model.Incident> incidents = incidentRepository.incidentsOfBookingIdAndProviderBookingItemId(incidentDTO.getBookingId(), incidentDTO.getProviderBookingItemId());
        if (incidents.isEmpty()) {
            throw new RegisterNotFound("No incidents found for given booking id: " + incidentDTO.getBookingId() + " and provider booking item id: " + incidentDTO.getProviderBookingItemId());
        }
        for (com.edreamsodigeo.customersupport.flightincident.model.Incident incident : incidents) {
            incident.setFlexibleTicketAccepted(incidentDTO.getConsentDTO().getFlexibleTicketAccepted());
            incident.setVoucherAccepted(incidentDTO.getConsentDTO().getVoucherAccepted());
            incident.setCashRefundAccepted(incidentDTO.getConsentDTO().getCashRefundAccepted());
            incident.setEmdAccepted(incidentDTO.getConsentDTO().getEmdAccepted());
            incident.setStatus(Status.valueOf(incidentDTO.getStatus()));
            incident.setLastUpdate(LocalDateTime.now());
            incidentRepository.save(incident);
        }
    }

    @Override
    public void addCancellationID(UUID incidentId, UUID cancellationId) throws RegisterNotFound {
        com.edreamsodigeo.customersupport.flightincident.model.Incident incident = incidentRepository.incidentOfIncidentId(new IncidentId(incidentId));
        checkIncident(incidentId, incident);
        boolean shouldSendMail = incident.getStatus().equals(Status.WAITING_FOR_CUSTOMER_CHOICE) && incident.getConsentCommsLogs().isEmpty();
        incident.setCancellationId(new CancellationId(cancellationId));
        if (shouldSendMail) {
            Cancellation cancellation = retrieveCancellation(cancellationId);
            createCommsLog(incident, cancellation);
        }
        incident.setLastUpdate(LocalDateTime.now());
        incidentRepository.save(incident);

        if (shouldSendMail) {
            try {
                emailSender.sendCancellationEmail(incident);
            } catch (Exception e) {
                logger.error("EMAIL NOT SENT: " + incident.getBookingId() + ":" + EmailType.CANCELLATION.getTemplateName());
            }
        }

    }

    private Cancellation retrieveCancellation(UUID cancellationId) throws RegisterNotFound {
        CancellationResource cancellationResource = ConfigurationEngine.getInstance(CancellationResource.class);
        try {
            return cancellationResource.getCancellation(new com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId(cancellationId.toString()));
        } catch (CancellationNotFoundException e) {
            throw new RegisterNotFound(e.getMessage());
        }
    }

    public List<Incident> getIncidentsWithoutCancellation(int pageNumber) {
        List<com.edreamsodigeo.customersupport.flightincident.model.Incident> incidents = incidentRepository.incidentsWithoutCancellation(pageNumber);
        return incidentMapper.mapIncidentsToIds(incidents);
    }

    private void createCommsLog(com.edreamsodigeo.customersupport.flightincident.model.Incident incident, Cancellation cancellation) {
        ConsentCommsLog consentCommsLog = new ConsentCommsLog();
        consentCommsLog.setId(new ConsentCommsLogId(UUIDGenerator.getInstance().generateUUID()));
        consentCommsLog.setIncident(incident);
        consentCommsLog.setTimestamp(LocalDateTime.now());
        OptionValue optionValue = mapResolutionToOptionValue(getResolutionType(incident, cancellation));
        consentCommsLog.setType(optionValue);
        consentCommsLog.setConsentCommOptions(createCommOptions(optionValue, consentCommsLog));
        incident.setConsentCommsLogs(Collections.singletonList(consentCommsLog));
    }

    private String getResolutionType(com.edreamsodigeo.customersupport.flightincident.model.Incident incident, Cancellation cancellation) {
        return cancellation.getItems().stream().flatMap(p -> p.getProviderItems().stream()).filter(i -> i.getProviderBookingItemId().getId().equals(incident.getProviderBookingItemId())).findFirst().orElse(new ProviderItem()).getResolutionType();
    }

    private OptionValue mapResolutionToOptionValue(String resolutionType) {
        switch (resolutionType) {
        case "ATC_AUTOMATIC":
        case "ATC_MANUAL":
        case "BSP_LINK":
            return OptionValue.CASHOUT_CONSENT;
        case "OPEN_TICKET_WITH_CONSENT":
        case "OPEN_TICKET_WITHOUT_CONSENT":
            return OptionValue.OPEN_TICKET_CONSENT;
        case "MANUAL":
        case "FREE_REBOOKING_MANUAL":
        case "TF_AUTOMATIC":
        case "TF_BSM":
        case "COMMERCIAL_RELATIONSHIP":
        case "CUSTOMER_DIRECT_RESOLUTION":
        case "AIRLINE_DIRECT_RESOLUTION":
        case "BANKRUPTCY":
        case "RESOLVED":
            return OptionValue.GENERIC;
        }
        return null;
    }

    private List<ConsentCommOption> createCommOptions(OptionValue type, ConsentCommsLog consentCommsLog) {
        List<ConsentCommOption> consentCommOptions = new ArrayList<>();
        if (!OptionValue.CASHOUT_CONSENT.equals(type)) {
            ConsentCommOption consentCommOption = createConsentCommOption(type, consentCommsLog);
            consentCommOptions.add(consentCommOption);
        }
        ConsentCommOption optionCashout = createConsentCommOption(OptionValue.CASHOUT_CONSENT, consentCommsLog);
        consentCommOptions.add(optionCashout);
        return consentCommOptions;
    }

    private ConsentCommOption createConsentCommOption(OptionValue type, ConsentCommsLog consentCommsLog) {
        ConsentCommOption consentCommOption = new ConsentCommOption();
        consentCommOption.setConsentCommsLog(consentCommsLog);
        consentCommOption.setId(new ConsentCommOptionId(UUIDGenerator.getInstance().generateUUID()));
        consentCommOption.setOptionValue(type);
        return consentCommOption;
    }
}
