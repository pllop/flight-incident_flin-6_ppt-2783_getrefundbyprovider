package com.edreamsodigeo.customersupport.flightincident.mapper;

import com.edreamsodigeo.customersupport.flightincident.IncidentDTO;
import com.edreamsodigeo.customersupport.flightincident.v1.model.BookingId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.CancellationId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Consent;
import com.edreamsodigeo.customersupport.flightincident.v1.model.ConsentCommOption;
import com.edreamsodigeo.customersupport.flightincident.v1.model.ConsentCommOptionId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.ConsentCommsLog;
import com.edreamsodigeo.customersupport.flightincident.v1.model.ConsentCommsLogId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.ConsentCommsRespLog;
import com.edreamsodigeo.customersupport.flightincident.v1.model.ConsentCommsRespLogId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.v1.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.OptionValue;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Status;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class IncidentMapper {

    public List<Incident> mapIncident(List<com.edreamsodigeo.customersupport.flightincident.model.Incident> incidentsByBookingId) {
        List<Incident> incidents = new ArrayList<>();
        for (com.edreamsodigeo.customersupport.flightincident.model.Incident incidentDTO : incidentsByBookingId) {
            incidents.add(mapIncident(incidentDTO));
        }
        return incidents;
    }

    public Incident mapIncident(com.edreamsodigeo.customersupport.flightincident.model.Incident incidentDTO) {
        Incident incident = new Incident();
        setIdsToIncident(incidentDTO, incident);
        CancellationId cancellationId = new CancellationId();
        cancellationId.setId(incidentDTO.getCancellationId() != null ? incidentDTO.getCancellationId().getId() : null);
        incident.setCancellationId(incidentDTO.getCancellationId() != null ? cancellationId : null);
        Consent consent = new Consent();
        consent.setCashRefundAccepted(incidentDTO.getCashRefundAccepted());
        consent.setFlexibleTicketAccepted(incidentDTO.getFlexibleTicketAccepted());
        consent.setVoucherAccepted(incidentDTO.getVoucherAccepted());
        consent.setEmdAccepted(incidentDTO.getEmdAccepted());
        incident.setConsent(consent);
        incident.setDepartureLocalDate(incidentDTO.getDepartureLocalDate() != null ? incidentDTO.getDepartureLocalDate().toLocalDate() : null);
        incident.setDepartureLocalDateTime(incidentDTO.getDepartureLocalDate());
        incident.setFlightNumber(incidentDTO.getFlightNumber());
        incident.setStatus(Status.valueOf(incidentDTO.getStatus().name()));
        List<ConsentCommsLog> consentCommsLogs = new ArrayList<>();
        for (com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsLog consentCommsLog : incidentDTO.getConsentCommsLogs()) {
            consentCommsLogs.add(mapConsentCommsLog(consentCommsLog));
        }
        incident.setConsentCommsLogs(consentCommsLogs);
        return incident;
    }

    private ConsentCommsLog mapConsentCommsLog(com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsLog consentCommsLog) {
        ConsentCommsLog consentCommOptionResult = new ConsentCommsLog();
        ConsentCommsLogId consentCommOptionId = new ConsentCommsLogId();
        consentCommOptionId.setId(consentCommsLog.getId().getId());
        consentCommOptionResult.setId(consentCommOptionId);
        consentCommOptionResult.setMessage(consentCommsLog.getMessage());
        consentCommOptionResult.setTimestamp(LocalDate.from(consentCommsLog.getTimestamp()));
        consentCommOptionResult.setTime(LocalDateTime.from(consentCommsLog.getTimestamp()));
        consentCommOptionResult.setType(OptionValue.valueOf(consentCommsLog.getType().name()));
        List<ConsentCommOption> consentCommOptions = new ArrayList<>();
        for (com.edreamsodigeo.customersupport.flightincident.model.ConsentCommOption consentCommOption : consentCommsLog.getConsentCommOptions()) {
            consentCommOptions.add(mapConsentCommOption(consentCommOption));
        }
        consentCommOptionResult.setConsentCommOptions(consentCommOptions);
        return consentCommOptionResult;
    }

    private ConsentCommOption mapConsentCommOption(com.edreamsodigeo.customersupport.flightincident.model.ConsentCommOption consentCommOption) {
        ConsentCommOption consentCommOptionResult = new ConsentCommOption();
        ConsentCommOptionId consentCommOptionId = new ConsentCommOptionId();
        consentCommOptionId.setId(consentCommOption.getId().getId());
        consentCommOptionResult.setId(consentCommOptionId);
        consentCommOptionResult.setOption(OptionValue.valueOf(consentCommOption.getOptionValue().name()));
        List<ConsentCommsRespLog> consentCommsRespLogs = new ArrayList<>();
        for (com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsRespLog consentCommsRespLog : consentCommOption.getConsentCommsRespLogs()) {
            consentCommsRespLogs.add(mapConsentCommsRespLog(consentCommsRespLog));
        }
        consentCommOptionResult.setConsentCommsRespLogs(consentCommsRespLogs);
        return consentCommOptionResult;
    }

    private ConsentCommsRespLog mapConsentCommsRespLog(com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsRespLog consentCommsRespLog) {
        ConsentCommsRespLog consentCommsRespLogResult = new ConsentCommsRespLog();
        ConsentCommsRespLogId consentCommsRespLogId = new ConsentCommsRespLogId();
        consentCommsRespLogId.setId(consentCommsRespLog.getId().getId());
        consentCommsRespLogResult.setId(consentCommsRespLogId);
        consentCommsRespLogResult.setResponse(consentCommsRespLog.getResponse());
        consentCommsRespLogResult.setResponseChannel(consentCommsRespLog.getResponseChannel());
        consentCommsRespLogResult.setResponseDate(consentCommsRespLog.getResponseDate() != null ? consentCommsRespLog.getResponseDate().toLocalDate() : null);
        consentCommsRespLogResult.setResponseDateTime(consentCommsRespLog.getResponseDate());
        consentCommsRespLogResult.setTimestamp(consentCommsRespLog.getTimestamp() != null ? consentCommsRespLog.getTimestamp().toLocalDate() : null);
        consentCommsRespLogResult.setTime(consentCommsRespLog.getTimestamp());

        return consentCommsRespLogResult;
    }

    public com.edreamsodigeo.customersupport.flightincident.model.Incident mapIncidentDTOtoIncident(IncidentDTO incidentDTO) {
        com.edreamsodigeo.customersupport.flightincident.model.Incident incident = new com.edreamsodigeo.customersupport.flightincident.model.Incident();
        incident.setId(new com.edreamsodigeo.customersupport.flightincident.model.IncidentId(incidentDTO.getId()));
        incident.setBookingId(incidentDTO.getBookingId());
        incident.setProviderBookingItemId(incidentDTO.getProviderBookingItemId());
        incident.setFlightNumber(incidentDTO.getFlightNumber());
        incident.setDepartureLocalDate(incidentDTO.getDepartureLocalDate());
        incident.setStatus(com.edreamsodigeo.customersupport.flightincident.model.Status.valueOf(incidentDTO.getStatus()));
        incident.setCancellationId(new com.edreamsodigeo.customersupport.flightincident.model.CancellationId(incidentDTO.getCancellationId()));
        incident.setFlexibleTicketAccepted(incidentDTO.getConsentDTO().getFlexibleTicketAccepted());
        incident.setVoucherAccepted(incidentDTO.getConsentDTO().getVoucherAccepted());
        incident.setCashRefundAccepted(incidentDTO.getConsentDTO().getCashRefundAccepted());
        incident.setEmdAccepted(incidentDTO.getConsentDTO().getEmdAccepted());
        incident.setTimestamp(LocalDateTime.now());
        incident.setLastUpdate(LocalDateTime.now());
        return incident;
    }

    public List<Incident> mapIncidentsToIds(List<com.edreamsodigeo.customersupport.flightincident.model.Incident> incidents) {
        List<Incident> incidentsIds = new ArrayList<>();
        for (com.edreamsodigeo.customersupport.flightincident.model.Incident incidentDTO : incidents) {
            Incident incident = new Incident();
            setIdsToIncident(incidentDTO, incident);
            incidentsIds.add(incident);
        }
        return incidentsIds;
    }

    private void setIdsToIncident(com.edreamsodigeo.customersupport.flightincident.model.Incident source, Incident target) {
        IncidentId id = new IncidentId();
        id.setId(source.getId().getId());
        target.setId(id);
        BookingId bookingId = new BookingId();
        bookingId.setId(source.getBookingId());
        target.setBookingId(bookingId);
        target.setProviderBookingItemId(source.getProviderBookingItemId());
    }
}
