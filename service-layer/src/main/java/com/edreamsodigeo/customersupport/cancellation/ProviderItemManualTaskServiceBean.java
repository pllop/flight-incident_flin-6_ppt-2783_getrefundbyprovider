package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.ProviderItemManualTaskMapper;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTaskId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemManualTaskRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemManualTaskRepository;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;
import java.util.UUID;

@Stateless
@Local(ProviderItemManualTaskService.class)
public class ProviderItemManualTaskServiceBean implements ProviderItemManualTaskService {

    private ProviderItemManualTaskRepository repository;
    private ProviderItemManualTaskMapper mapper;

    @PersistenceContext(unitName = "cancellation")
    private EntityManager entityManager;

    public ProviderItemManualTaskServiceBean() {
        repository = ConfigurationEngine.getInstance(ProviderItemManualTaskRepository.class);
        mapper = ConfigurationEngine.getInstance(ProviderItemManualTaskMapper.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaProviderItemManualTaskRepository) repository).setEntityManager(entityManager);
    }

    @Override
    public ProviderItemManualTaskDTO getProviderItemManualTask(UUID uuid) throws RegisterNotFound {
        return mapper.mapProviderItemManualTaskToDto(getFromRepository(uuid));
    }

    @Override
    public ProviderItemManualTaskDTO addProviderItemManualTask(ProviderItemManualTaskDTO providerItemManualTaskDTO) {
        ProviderItemManualTask providerItemManualTask = mapper.mapDtoToProviderItemManualTask(providerItemManualTaskDTO);
        repository.save(providerItemManualTask);
        return mapper.mapProviderItemManualTaskToDto(providerItemManualTask);
    }

    @Override
    public void updateProviderItemManualTask(UUID uuid, ProviderItemManualTaskDTO providerItemManualTaskDTO) throws RegisterNotFound {
        ProviderItemManualTask detached = mapper.mapDtoToProviderItemManualTask(providerItemManualTaskDTO);
        ProviderItemManualTask managed = getFromRepository(uuid);
        managed.getProviderItem().getId().setId(detached.getProviderItem().getId().getId());
        managed.setTaskOnDemandId(detached.getTaskOnDemandId());
        repository.save(managed);
    }

    private ProviderItemManualTask getFromRepository(UUID id) throws RegisterNotFound {
        ProviderItemManualTask result = repository.findById(new ProviderItemManualTaskId(id));
        if (Objects.isNull(result)) {
            throw new RegisterNotFound("No CancelledItemManual found for given id: " + id);
        }
        return result;
    }
}
