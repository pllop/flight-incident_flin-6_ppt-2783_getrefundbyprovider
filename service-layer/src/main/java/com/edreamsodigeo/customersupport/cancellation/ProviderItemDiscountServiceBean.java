package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.ProviderItemDiscountMapper;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscountId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemDiscountRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemDiscountRepository;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.UUID;

@Stateless
@Local(ProviderItemDiscountService.class)
public class ProviderItemDiscountServiceBean implements ProviderItemDiscountService {

    @PersistenceContext(unitName = "cancellation")
    private EntityManager entityManager;

    private final ProviderItemDiscountRepository providerItemDiscountRepository;
    private final ProviderItemDiscountMapper providerItemDiscountMapper;

    public ProviderItemDiscountServiceBean() {
        providerItemDiscountRepository = ConfigurationEngine.getInstance(ProviderItemDiscountRepository.class);
        providerItemDiscountMapper = ConfigurationEngine.getInstance(ProviderItemDiscountMapper.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaProviderItemDiscountRepository) providerItemDiscountRepository).setEntityManager(entityManager);
    }

    @Override
    public ProviderItemDiscountDTO addproviderItemDiscount(ProviderItemDiscountDTO providerItemDiscountDTO) {
        ProviderItemDiscount providerItemDiscount = providerItemDiscountMapper.map(providerItemDiscountDTO);
        providerItemDiscountRepository.insert(providerItemDiscount);
        return providerItemDiscountMapper.map(providerItemDiscount);
    }

    @Override
    public ProviderItemDiscountDTO getProviderItemDiscount(UUID id) {
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId(id);
        return providerItemDiscountMapper.map(providerItemDiscountRepository.findById(providerItemDiscountId));
    }
}
