package com.edreamsodigeo.customersupport.cancellation;

import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;

import java.util.UUID;

public interface ProviderItemMerchantRefundService {
    ProviderItemMerchantDTO getProviderItemMerchant(UUID uuid) throws RegisterNotFound;

    ProviderItemMerchantDTO getProviderItemMerchantByProviderItem(UUID uuid) throws RegisterNotFound;

    ProviderItemMerchantDTO addProviderItemMerchant(ProviderItemMerchantDTO providerItemMerchantDTO);

    void updateProviderItemMerchant(ProviderItemMerchantDTO providerItemMerchantDTO) throws RegisterNotFound;
}
