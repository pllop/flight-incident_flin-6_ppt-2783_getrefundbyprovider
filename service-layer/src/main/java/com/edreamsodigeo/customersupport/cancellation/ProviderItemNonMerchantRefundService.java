package com.edreamsodigeo.customersupport.cancellation;

import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;

import java.util.UUID;

public interface ProviderItemNonMerchantRefundService {
    ProviderItemNonMerchantDTO getProviderItemNonMerchant(UUID uuid) throws RegisterNotFound;

    ProviderItemNonMerchantDTO getProviderItemNonMerchantByProviderItem(UUID uuid) throws RegisterNotFound;

    ProviderItemNonMerchantDTO addProviderItemNonMerchant(ProviderItemNonMerchantDTO providerItemNonMerchantDTO);

    void updateProviderItemNonMerchant(ProviderItemNonMerchantDTO providerItemNonMerchantDTO) throws RegisterNotFound;
}
