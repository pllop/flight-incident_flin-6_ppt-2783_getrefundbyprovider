package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefundId;
import com.edreamsodigeo.customersupport.cancellation.model.RefundId;

public class ProviderItemMerchantRefundMapper {


    public ProviderItemMerchantDTO mapProviderItemMerchantToDto(ProviderItemMerchantRefund providerItemMerchant) {
        ProviderItemMerchantDTO result = new ProviderItemMerchantDTO();
        result.setId(providerItemMerchant.getId().getId());
        result.setProviderItemId(providerItemMerchant.getProviderItemId().getId());
        result.setCustomerRefundAmount(new MoneyDTO(
                providerItemMerchant.getCustomerRefundAmount(),
                providerItemMerchant.getCustomerRefundCurrency()));
        result.setProviderRefundAmountExpected(new MoneyDTO(
                providerItemMerchant.getExpectedProviderRefundAmount(),
                providerItemMerchant.getCustomerRefundCurrency()));
        result.setCustomerRefundMethod(providerItemMerchant.getCustomerRefundMethod());
        result.setRefundId(providerItemMerchant.getRefundId().getId());
        return result;
    }

    public ProviderItemMerchantRefund mapDtoToProviderItemMerchant(ProviderItemMerchantDTO dto) {
        ProviderItemMerchantRefund result = new ProviderItemMerchantRefund();
        ProviderItemMerchantRefundId id = new ProviderItemMerchantRefundId();
        id.setId(dto.getId());
        result.setId(id);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(dto.getProviderItemId());
        result.setProviderItemId(providerItemId);
        result.setCustomerRefundAmount(dto.getCustomerRefundAmount().getAmount());
        result.setCustomerRefundCurrency(dto.getCustomerRefundAmount().getCurrency());
        result.setExpectedProviderRefundAmount(dto.getProviderRefundAmountExpected().getAmount());
        result.setExpectedProviderRefundCurrency(dto.getProviderRefundAmountExpected().getCurrency());
        result.setCustomerRefundMethod(dto.getCustomerRefundMethod());
        RefundId refundId = new RefundId();
        refundId.setId(dto.getRefundId());
        result.setRefundId(refundId);
        return result;
    }

}
