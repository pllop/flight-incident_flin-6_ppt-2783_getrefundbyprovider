package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.CancellationMapper;
import com.edreamsodigeo.customersupport.cancellation.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.model.Item;
import com.edreamsodigeo.customersupport.cancellation.repository.CancellationRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaCancellationRepository;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Stateless
@Local(CancellationService.class)
public class CancellationServiceBean implements CancellationService {
    private CancellationRepository cancellationRepository;

    @PersistenceContext(unitName = "cancellation")
    private EntityManager entityManager;

    private CancellationMapper cancellationMapper;

    public CancellationServiceBean() {
        cancellationRepository = ConfigurationEngine.getInstance(CancellationRepository.class);
        cancellationMapper = ConfigurationEngine.getInstance(CancellationMapper.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaCancellationRepository) cancellationRepository).setEntityManager(entityManager);
    }

    @Override
    public CancellationDTO getCancellation(UUID cancellationId) throws RegisterNotFound {
        Cancellation cancellation = getCancellationFromRepository(cancellationId);
        return cancellationMapper.mapCancellationToCancellationDTO(cancellation);
    }

    @Override
    public List<CancellationDTO> getCancellationsByIncident(UUID incidentId) throws RegisterNotFound {
        List<Cancellation> result = cancellationRepository.findByIncidentId(new IncidentId(incidentId));
        if (null == result || result.isEmpty()) {
            throw new RegisterNotFound("No Cancellations found for given incident id: " + incidentId);
        }
        return result.stream().map(cancellationMapper::mapCancellationToCancellationDTO).collect(Collectors.toList());
    }

    @Override
    public CancellationDTO addCancellation(CancellationDTO cancellationDTO) {
        Cancellation cancellation = cancellationMapper.mapCancellationDTOToCancellation(cancellationDTO);
        cancellationRepository.insert(cancellation);
        return cancellationMapper.mapCancellationToCancellationDTO(cancellation);
    }

    @Override
    public void updateCancellation(CancellationDTO cancellationDTO) throws RegisterNotFound {
        Cancellation cancellationInStore = getCancellationFromRepository(cancellationDTO.getId());

        Cancellation cancellation = cancellationMapper.mapCancellationDTOToCancellation(cancellationDTO);
        cancellationInStore.setIncidentId(cancellation.getIncidentId());
        cancellationInStore.setDiscarded(cancellation.getDiscarded());
        cancellationInStore.setUserId(cancellation.getUserId());
        cancellationInStore.setBuyerEmail(cancellation.getBuyerEmail());

        setProviderItems(cancellationInStore.getItems(), cancellation.getItems());

        cancellationInStore.getItems().clear();
        cancellationInStore.getItems().addAll(cancellation.getItems());

        cancellationRepository.update(cancellationInStore);
    }

    private void setProviderItems(List<Item> itemsInStore, List<Item> newItems) {
        itemsInStore.forEach(itemInStore -> newItems.stream()
                .filter(newItem -> itemInStore.getId().equals(newItem.getId()))
                .forEach(newItem -> {
                    itemInStore.getProviderItems().clear();
                    itemInStore.getProviderItems().addAll(newItem.getProviderItems());
                })
        );
    }

    @Override
    public void discardCancellation(UUID cancellationId) throws RegisterNotFound {
        Cancellation cancellationToUpdate = getCancellationFromRepository(cancellationId);
        cancellationToUpdate.setDiscarded(true);
        cancellationRepository.update(cancellationToUpdate);
    }

    private Cancellation getCancellationFromRepository(UUID cancellationId) throws RegisterNotFound {
        Cancellation cancellationToUpdate = cancellationRepository.findById(new CancellationId(cancellationId));
        if (Objects.isNull(cancellationToUpdate)) {
            throw new RegisterNotFound("No Cancellation found for given cancellation id: " + cancellationId);
        }
        return cancellationToUpdate;
    }
}
