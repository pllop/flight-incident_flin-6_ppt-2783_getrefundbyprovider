package com.edreamsodigeo.customersupport.cancellation;

import java.util.UUID;

public interface ProviderItemDiscountService {
    ProviderItemDiscountDTO addproviderItemDiscount(ProviderItemDiscountDTO providerItemDiscountDTO);
    ProviderItemDiscountDTO getProviderItemDiscount(UUID providerItemDiscountDTO);
}
