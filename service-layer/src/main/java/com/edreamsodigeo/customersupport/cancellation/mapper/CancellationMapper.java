package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.CancellationDTO;
import com.edreamsodigeo.customersupport.cancellation.ItemDTO;
import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemDTO;
import com.edreamsodigeo.customersupport.cancellation.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.model.Item;
import com.edreamsodigeo.customersupport.cancellation.model.ItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CancellationMapper {
    public Cancellation mapCancellationDTOToCancellation(CancellationDTO cancellationDTO) {
        Cancellation cancellation = new Cancellation();

        if (Objects.nonNull(cancellationDTO.getId())) {
            cancellation.setId(new CancellationId(cancellationDTO.getId()));
        }
        cancellation.setIncidentId(new IncidentId(cancellationDTO.getIncidentId()));
        cancellation.setDiscarded(cancellationDTO.isDiscarded());
        cancellation.setUserId(cancellationDTO.getUserId());
        cancellation.setBuyerEmail(cancellationDTO.getBuyerEmail());
        cancellation.setTimestamp(cancellationDTO.getTimestamp());
        cancellation.setItems(mapItemDTOToItem(cancellationDTO.getItems()));
        return cancellation;
    }

    private List<Item> mapItemDTOToItem(List<ItemDTO> itemsDTO) {
        return itemsDTO.stream().map(this::mapItemDTOToItem).collect(Collectors.toList());
    }

    private Item mapItemDTOToItem(ItemDTO itemDTO) {
        Item item = new Item();

        if (Objects.nonNull(itemDTO.getId())) {
            item.setId(new ItemId(itemDTO.getId()));
        }
        item.setBookingItemId(itemDTO.getBookingItemId());
        item.setProviderItems(mapProviderItemsDTOToProviderItems(itemDTO.getProviderItems()));
        return item;
    }

    private List<ProviderItem> mapProviderItemsDTOToProviderItems(List<ProviderItemDTO> providerItemsDTO) {
        return providerItemsDTO.stream().map(this::mapProviderItemDTOToProviderItem).collect(Collectors.toList());
    }

    private ProviderItem mapProviderItemDTOToProviderItem(ProviderItemDTO providerItemDTO) {
        ProviderItem providerItem = new ProviderItem();

        if (Objects.nonNull(providerItemDTO.getId())) {
            providerItem.setId(new ProviderItemId(providerItemDTO.getId()));
        }
        providerItem.setProviderBookingItemId(providerItemDTO.getProviderBookingItemId());
        providerItem.setResolutionType(providerItemDTO.getResolutionType());
        if (Objects.nonNull(providerItemDTO.getTopUpVoucherAmount())) {
            providerItem.setTopUpVoucherAmount(providerItemDTO.getTopUpVoucherAmount().getAmount());
            providerItem.setTopUpVoucherCurrency(providerItemDTO.getTopUpVoucherAmount().getCurrency());
        }
        if (Objects.nonNull(providerItemDTO.getTopUpVoucherMinBasketAmount())) {
            providerItem.setTopUpVoucherMinBasketAmount(providerItemDTO.getTopUpVoucherMinBasketAmount().getAmount());
        }
        return providerItem;
    }

    public CancellationDTO mapCancellationToCancellationDTO(Cancellation cancellation) {
        CancellationDTO cancellationDTO = new CancellationDTO();

        cancellationDTO.setId(cancellation.getId().getId());
        cancellationDTO.setIncidentId(cancellation.getIncidentId().getId());
        cancellationDTO.setDiscarded(cancellation.getDiscarded());
        cancellationDTO.setUserId(cancellation.getUserId());
        cancellationDTO.setBuyerEmail(cancellation.getBuyerEmail());
        cancellationDTO.setTimestamp(cancellation.getTimestamp());
        if (Objects.nonNull(cancellation.getItems())) {
            cancellationDTO.setItems(mapItemsToItemsDTO(cancellation.getItems()));
        }
        return cancellationDTO;
    }

    private List<ItemDTO> mapItemsToItemsDTO(List<Item> items) {
        return items.stream().map(this::mapItemToItemDTO).collect(Collectors.toList());
    }

    private ItemDTO mapItemToItemDTO(Item item) {
        ItemDTO itemDTO = new ItemDTO();

        itemDTO.setId(item.getId().getId());
        itemDTO.setBookingItemId(item.getBookingItemId());
        if (Objects.nonNull(item.getProviderItems())) {
            itemDTO.setProviderItems(mapProviderItemsToProviderItemsDTO(item.getProviderItems()));
        }
        return itemDTO;
    }

    private List<ProviderItemDTO> mapProviderItemsToProviderItemsDTO(List<ProviderItem> providerItems) {
        return providerItems.stream().map(this::mapProviderItemToProviderItemDTO).collect(Collectors.toList());
    }

    private ProviderItemDTO mapProviderItemToProviderItemDTO(ProviderItem providerItem) {
        ProviderItemDTO providerItemDTO = new ProviderItemDTO();

        providerItemDTO.setId(providerItem.getId().getId());
        providerItemDTO.setProviderBookingItemId(providerItem.getProviderBookingItemId());
        providerItemDTO.setResolutionType(providerItem.getResolutionType());
        providerItemDTO.setTopUpVoucherAmount(new MoneyDTO(providerItem.getTopUpVoucherAmount(), providerItem.getTopUpVoucherCurrency()));
        providerItemDTO.setTopUpVoucherMinBasketAmount(new MoneyDTO(providerItem.getTopUpVoucherMinBasketAmount(), providerItem.getTopUpVoucherCurrency()));
        return providerItemDTO;
    }
}
