package com.edreamsodigeo.customersupport.cancellation;

import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;

import java.util.List;
import java.util.UUID;

public interface CancellationService {
    CancellationDTO getCancellation(UUID cancellationId) throws RegisterNotFound;

    List<CancellationDTO> getCancellationsByIncident(UUID incidentId) throws RegisterNotFound;

    CancellationDTO addCancellation(CancellationDTO cancellationDTO);

    void updateCancellation(CancellationDTO cancellationDTO) throws RegisterNotFound;

    void discardCancellation(UUID cancellationId) throws RegisterNotFound;
}
