package com.edreamsodigeo.customersupport.cancellation;

import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;

import java.util.UUID;

public interface ProviderItemManualTaskService {
    ProviderItemManualTaskDTO getProviderItemManualTask(UUID uuid) throws RegisterNotFound;

    ProviderItemManualTaskDTO addProviderItemManualTask(ProviderItemManualTaskDTO providerItemManualTaskDTO);

    void updateProviderItemManualTask(UUID uuid, ProviderItemManualTaskDTO providerItemManualTaskDTO) throws RegisterNotFound;
}
