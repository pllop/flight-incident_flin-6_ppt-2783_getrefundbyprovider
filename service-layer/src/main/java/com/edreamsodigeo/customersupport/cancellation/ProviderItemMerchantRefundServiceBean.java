package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.ProviderItemMerchantRefundMapper;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefundId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemMerchantRefundRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemMerchantRefundRepository;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;
import java.util.UUID;

@Stateless
@Local(ProviderItemMerchantRefundService.class)
public class ProviderItemMerchantRefundServiceBean implements ProviderItemMerchantRefundService {

    private ProviderItemMerchantRefundRepository repository;
    private ProviderItemMerchantRefundMapper mapper;

    @PersistenceContext(unitName = "cancellation")
    private EntityManager entityManager;

    public ProviderItemMerchantRefundServiceBean() {
        repository = ConfigurationEngine.getInstance(ProviderItemMerchantRefundRepository.class);
        mapper = ConfigurationEngine.getInstance(ProviderItemMerchantRefundMapper.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaProviderItemMerchantRefundRepository) repository).setEntityManager(entityManager);
    }

    @Override
    public ProviderItemMerchantDTO getProviderItemMerchant(UUID uuid) throws RegisterNotFound {
        return mapper.mapProviderItemMerchantToDto(getFromRepository(uuid));
    }

    @Override
    public ProviderItemMerchantDTO getProviderItemMerchantByProviderItem(UUID uuid) throws RegisterNotFound {
        ProviderItemMerchantRefund result = repository.findByProviderItemId(new ProviderItemId(uuid));
        if (Objects.isNull(result)) {
            throw new RegisterNotFound("No ProviderItemMerchantRefund found for given id: " + uuid);
        }
        return mapper.mapProviderItemMerchantToDto(result);
    }

    @Override
    public ProviderItemMerchantDTO addProviderItemMerchant(ProviderItemMerchantDTO providerItemMerchantDTO) {
        ProviderItemMerchantRefund providerItemMerchantRefund = mapper.mapDtoToProviderItemMerchant(providerItemMerchantDTO);
        repository.insert(providerItemMerchantRefund);
        return mapper.mapProviderItemMerchantToDto(providerItemMerchantRefund);
    }

    @Override
    public void updateProviderItemMerchant(ProviderItemMerchantDTO providerItemMerchantDTO) throws RegisterNotFound {
        ProviderItemMerchantRefund providerItemMerchantRefundToUpdate = getFromRepository(providerItemMerchantDTO.getId());

        ProviderItemMerchantRefund providerItemMerchantRefund = mapper.mapDtoToProviderItemMerchant(providerItemMerchantDTO);
        providerItemMerchantRefundToUpdate.setProviderItemId(providerItemMerchantRefund.getProviderItemId());
        providerItemMerchantRefundToUpdate.setExpectedProviderRefundAmount(providerItemMerchantRefund.getExpectedProviderRefundAmount());
        providerItemMerchantRefundToUpdate.setExpectedProviderRefundCurrency(providerItemMerchantRefund.getExpectedProviderRefundCurrency());
        providerItemMerchantRefundToUpdate.setCustomerRefundAmount(providerItemMerchantRefund.getCustomerRefundAmount());
        providerItemMerchantRefundToUpdate.setCustomerRefundCurrency(providerItemMerchantRefund.getCustomerRefundCurrency());
        providerItemMerchantRefundToUpdate.setCustomerRefundMethod(providerItemMerchantRefund.getCustomerRefundMethod());
        providerItemMerchantRefundToUpdate.setRefundId(providerItemMerchantRefund.getRefundId());

        repository.update(providerItemMerchantRefundToUpdate);
    }

    private ProviderItemMerchantRefund getFromRepository(UUID id) throws RegisterNotFound {
        ProviderItemMerchantRefund result = repository.findById(new ProviderItemMerchantRefundId(id));
        if (Objects.isNull(result)) {
            throw new RegisterNotFound("No ProviderItemMerchantRefund found for given id: " + id);
        }
        return result;
    }

}
