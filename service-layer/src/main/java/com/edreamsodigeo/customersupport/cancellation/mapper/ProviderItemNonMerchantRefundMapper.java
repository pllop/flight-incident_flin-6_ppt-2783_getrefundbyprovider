package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefundId;

public class ProviderItemNonMerchantRefundMapper {


    public ProviderItemNonMerchantDTO mapProviderItemNonMerchantToDto(ProviderItemNonMerchantRefund providerItemNonMerchantRefund) {
        ProviderItemNonMerchantDTO result = new ProviderItemNonMerchantDTO();
        result.setId(providerItemNonMerchantRefund.getId().getId());
        result.setProviderItemId(providerItemNonMerchantRefund.getProviderItemId().getId());
        result.setProviderRefundAmountExpected(new MoneyDTO(
                providerItemNonMerchantRefund.getExpectedProviderRefundAmount(),
                providerItemNonMerchantRefund.getExpectedProviderRefundCurrency()));
        result.setRefunder(providerItemNonMerchantRefund.getRefunder());
        return result;
    }

    public ProviderItemNonMerchantRefund mapDtoToProviderItemNonMerchant(ProviderItemNonMerchantDTO dto) {
        ProviderItemNonMerchantRefund result = new ProviderItemNonMerchantRefund();
        ProviderItemNonMerchantRefundId id = new ProviderItemNonMerchantRefundId();
        id.setId(dto.getId());
        result.setId(id);
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(dto.getProviderItemId());
        result.setProviderItemId(providerItemId);
        result.setExpectedProviderRefundAmount(dto.getProviderRefundAmountExpected().getAmount());
        result.setExpectedProviderRefundCurrency(dto.getProviderRefundAmountExpected().getCurrency());
        result.setRefunder(dto.getRefunder());
        return result;
    }

}
