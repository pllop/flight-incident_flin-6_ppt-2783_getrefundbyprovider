package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemDiscountDTO;
import com.edreamsodigeo.customersupport.cancellation.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscountId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;

import java.util.Objects;

public class ProviderItemDiscountMapper {
    public ProviderItemDiscount map(ProviderItemDiscountDTO providerItemDiscountDTO) {
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        providerItemDiscount.setId(new ProviderItemDiscountId(providerItemDiscountDTO.getId()));
        providerItemDiscount.setProviderItemId(new ProviderItemId(providerItemDiscountDTO.getProviderItemId()));
        providerItemDiscount.setDiscountType(providerItemDiscountDTO.getDiscountType());
        providerItemDiscount.setFlexibleTicketId(new FlexibleTicketId(providerItemDiscountDTO.getFlexibleTicketId()));
        providerItemDiscount.setVoucherId(providerItemDiscountDTO.getVoucherId());
        providerItemDiscount.setEmdId(providerItemDiscountDTO.getEmdId());
        return providerItemDiscount;
    }

    public ProviderItemDiscountDTO map(ProviderItemDiscount providerItemDiscount) {
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        providerItemDiscountDTO.setId(providerItemDiscount.getId().getId());
        providerItemDiscountDTO.setProviderItemId(providerItemDiscount.getProviderItemId().getId());
        providerItemDiscountDTO.setDiscountType(providerItemDiscount.getDiscountType());
        if (Objects.nonNull(providerItemDiscount.getFlexibleTicketId())) {
            providerItemDiscountDTO.setFlexibleTicketId(providerItemDiscount.getFlexibleTicketId().getId());
        }
        providerItemDiscountDTO.setVoucherId(providerItemDiscount.getVoucherId());
        providerItemDiscountDTO.setEmdId(providerItemDiscount.getEmdId());
        return providerItemDiscountDTO;
    }


}
