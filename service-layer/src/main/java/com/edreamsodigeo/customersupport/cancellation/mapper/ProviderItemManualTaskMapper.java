package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskDTO;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTaskId;

public class ProviderItemManualTaskMapper {

    public ProviderItemManualTask mapDtoToProviderItemManualTask(ProviderItemManualTaskDTO dto) {
        ProviderItemManualTask result = new ProviderItemManualTask();

        result.setId(new ProviderItemManualTaskId(dto.getId()));
        result.setTaskOnDemandId(dto.getTaskOnDemandId());
        ProviderItem providerItem = new ProviderItem();
        providerItem.setId(new ProviderItemId(dto.getProviderItemId()));
        result.setProviderItem(providerItem);
        result.setTimestamp(dto.getTimestamp());
        return result;
    }

    public ProviderItemManualTaskDTO mapProviderItemManualTaskToDto(ProviderItemManualTask providerItemManualTask) {
        ProviderItemManualTaskDTO result = new ProviderItemManualTaskDTO();
        result.setId(providerItemManualTask.getId().getId());
        result.setProviderItemId(providerItemManualTask.getProviderItem().getId().getId());
        result.setTaskOnDemandId(providerItemManualTask.getTaskOnDemandId());
        result.setTimestamp(providerItemManualTask.getTimestamp());
        return result;
    }

}
