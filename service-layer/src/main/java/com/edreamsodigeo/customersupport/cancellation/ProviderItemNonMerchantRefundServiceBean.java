package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.ProviderItemNonMerchantRefundMapper;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefundId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemNonMerchantRefundRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemNonMerchantRefundRepository;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;
import java.util.UUID;

@Stateless
@Local(ProviderItemNonMerchantRefundService.class)
public class ProviderItemNonMerchantRefundServiceBean implements ProviderItemNonMerchantRefundService {

    private ProviderItemNonMerchantRefundRepository repository;
    private ProviderItemNonMerchantRefundMapper mapper;

    @PersistenceContext(unitName = "cancellation")
    private EntityManager entityManager;

    public ProviderItemNonMerchantRefundServiceBean() {
        repository = ConfigurationEngine.getInstance(ProviderItemNonMerchantRefundRepository.class);
        mapper = ConfigurationEngine.getInstance(ProviderItemNonMerchantRefundMapper.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaProviderItemNonMerchantRefundRepository) repository).setEntityManager(entityManager);
    }

    @Override
    public ProviderItemNonMerchantDTO getProviderItemNonMerchant(UUID uuid) throws RegisterNotFound {
        return mapper.mapProviderItemNonMerchantToDto(getFromRepository(uuid));
    }

    @Override
    public ProviderItemNonMerchantDTO getProviderItemNonMerchantByProviderItem(UUID uuid) throws RegisterNotFound {
        ProviderItemNonMerchantRefund result = repository.findByProviderItemId(new ProviderItemId(uuid));
        if (Objects.isNull(result)) {
            throw new RegisterNotFound("No ProviderItemMerchantRefund found for given id: " + uuid);
        }
        return mapper.mapProviderItemNonMerchantToDto(result);
    }

    @Override
    public ProviderItemNonMerchantDTO addProviderItemNonMerchant(ProviderItemNonMerchantDTO providerItemNonMerchantDTO) {
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = mapper.mapDtoToProviderItemNonMerchant(providerItemNonMerchantDTO);
        repository.insert(providerItemNonMerchantRefund);
        return mapper.mapProviderItemNonMerchantToDto(providerItemNonMerchantRefund);
    }

    @Override
    public void updateProviderItemNonMerchant(ProviderItemNonMerchantDTO providerItemNonMerchantDTO) throws RegisterNotFound {
        ProviderItemNonMerchantRefund providerItemNonMerchantRefundToUpdate = getFromRepository(providerItemNonMerchantDTO.getId());

        ProviderItemNonMerchantRefund providerItemMerchantRefund = mapper.mapDtoToProviderItemNonMerchant(providerItemNonMerchantDTO);
        providerItemNonMerchantRefundToUpdate.setProviderItemId(providerItemMerchantRefund.getProviderItemId());
        providerItemNonMerchantRefundToUpdate.setExpectedProviderRefundAmount(providerItemMerchantRefund.getExpectedProviderRefundAmount());
        providerItemNonMerchantRefundToUpdate.setExpectedProviderRefundCurrency(providerItemMerchantRefund.getExpectedProviderRefundCurrency());
        providerItemNonMerchantRefundToUpdate.setRefunder(providerItemMerchantRefund.getRefunder());

        repository.update(providerItemNonMerchantRefundToUpdate);
    }

    private ProviderItemNonMerchantRefund getFromRepository(UUID id) throws RegisterNotFound {
        ProviderItemNonMerchantRefund result = repository.findById(new ProviderItemNonMerchantRefundId(id));
        if (Objects.isNull(result)) {
            throw new RegisterNotFound("No ProviderItemNonMerchantRefund found for given id: " + id);
        }
        return result;
    }

}
