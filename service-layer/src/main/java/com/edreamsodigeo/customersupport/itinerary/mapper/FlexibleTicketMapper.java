package com.edreamsodigeo.customersupport.itinerary.mapper;

import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketDTO;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicketId;
import com.google.inject.ImplementedBy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.UUID;

@Mapper
@ImplementedBy(FlexibleTicketMapperImpl.class)
public interface FlexibleTicketMapper {

    @Mapping(target = "monetaryValue.amount", source = "monetaryValue")
    @Mapping(target = "monetaryValue.currencyCode", source = "currencyCode")
    FlexibleTicketDTO mapFlexibleTicketToFlexibleTicketDTO(FlexibleTicket flexibleTicket);

    @Mapping(target = "monetaryValue", source = "monetaryValue.amount")
    @Mapping(target = "currencyCode", source = "monetaryValue.currencyCode")
    FlexibleTicket mapFlexibleTicketDTOToFlexibleTicket(FlexibleTicketDTO flexibleTicketDTO);

    default UUID mapFlexibleTicketIdToUUID(FlexibleTicketId value) {
        return value.getId();
    }

    default FlexibleTicketId mapUUIDToFlexibleTicketId(UUID value) {
        return new FlexibleTicketId(value);
    }

}
