package com.edreamsodigeo.customersupport.itinerary;

import com.edreamsodigeo.customersupport.itinerary.exception.RegisterNotFoundException;

import java.util.UUID;

public interface FlexibleTicketService {

    FlexibleTicketDTO getFlexibleTicket(UUID flexibleTicketId) throws RegisterNotFoundException;

    void addFlexibleTicket(FlexibleTicketDTO flexibleTicketDTO);

    void updateFlexibleTicket(FlexibleTicketDTO flexibleTicketDTO) throws RegisterNotFoundException;

}
