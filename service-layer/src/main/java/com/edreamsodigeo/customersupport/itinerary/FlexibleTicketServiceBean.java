package com.edreamsodigeo.customersupport.itinerary;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.itinerary.exception.RegisterNotFoundException;
import com.edreamsodigeo.customersupport.itinerary.mapper.FlexibleTicketMapper;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.itinerary.repository.FlexibleTicketRepository;
import com.edreamsodigeo.customersupport.itinerary.repository.impl.JpaFlexibleTicketRepository;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;
import java.util.UUID;

@Stateless
@Local(FlexibleTicketService.class)
public class FlexibleTicketServiceBean implements FlexibleTicketService {

    private FlexibleTicketRepository flexibleTicketRepository;
    private FlexibleTicketMapper flexibleTicketMapper;

    @PersistenceContext(unitName = "itinerary")
    private EntityManager entityManager;

    public FlexibleTicketServiceBean() {
        flexibleTicketRepository = ConfigurationEngine.getInstance(FlexibleTicketRepository.class);
        flexibleTicketMapper = ConfigurationEngine.getInstance(FlexibleTicketMapper.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaFlexibleTicketRepository) flexibleTicketRepository).setEntityManager(entityManager);
    }

    @Override
    public FlexibleTicketDTO getFlexibleTicket(UUID flexibleTicketId) throws RegisterNotFoundException {
        final FlexibleTicket flexibleTicket = flexibleTicketRepository.findById(new FlexibleTicketId(flexibleTicketId));
        if (Objects.isNull(flexibleTicket)) {
            throw new RegisterNotFoundException("No flexibleTicket found for given flexibleTicket id: " + flexibleTicketId);
        }
        return flexibleTicketMapper.mapFlexibleTicketToFlexibleTicketDTO(flexibleTicket);
    }

    @Override
    public void addFlexibleTicket(FlexibleTicketDTO flexibleTicketDTO) {
        flexibleTicketRepository.insert(
                flexibleTicketMapper.mapFlexibleTicketDTOToFlexibleTicket(flexibleTicketDTO));
    }

    @Override
    public void updateFlexibleTicket(FlexibleTicketDTO flexibleTicketDTO) {
        flexibleTicketRepository.update(
                flexibleTicketMapper.mapFlexibleTicketDTOToFlexibleTicket(flexibleTicketDTO));
    }

}
