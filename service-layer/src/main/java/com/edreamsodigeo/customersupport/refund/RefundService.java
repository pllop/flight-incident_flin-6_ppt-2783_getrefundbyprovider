package com.edreamsodigeo.customersupport.refund;

import java.util.UUID;

public interface RefundService {

    RefundDTO getRefundById(UUID id);

    RefundDTO addRefund(RefundDTO refundDTO);

    void updateRefund(RefundDTO refundDTO);
}
