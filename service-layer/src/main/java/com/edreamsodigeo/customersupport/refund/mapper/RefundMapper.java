package com.edreamsodigeo.customersupport.refund.mapper;

import com.edreamsodigeo.customersupport.refund.ProviderRefundDTO;
import com.edreamsodigeo.customersupport.refund.RefundDTO;
import com.edreamsodigeo.customersupport.refund.model.ProviderRefund;
import com.edreamsodigeo.customersupport.refund.model.ProviderRefundId;
import com.edreamsodigeo.customersupport.refund.model.Refund;
import com.edreamsodigeo.customersupport.refund.model.RefundId;

import java.util.List;
import java.util.stream.Collectors;

public class RefundMapper {

    public Refund mapRefundDTOToRefund(RefundDTO refundDTO) {

        Refund refund = new Refund();
        if (refundDTO == null) {
            return refund;
        }

        refund.setRefundId(new RefundId(refundDTO.getId()));

        if (refundDTO.getProviderRefunds() != null) {
            refund.setProviderRefunds(mapProviderRefundsDTOToProviderRefunds(refundDTO.getProviderRefunds()));
        }

        refund.setCustomerRefundMethod(refundDTO.getCustomerRefundMethod());
        refund.setCustomerRefundAmount(refundDTO.getCustomerRefundAmount());
        refund.setCustomerRefundCurrency(refundDTO.getCustomerRefundCurrency());
        refund.setBookingItemId(refundDTO.getBookingItemId());
        refund.setStatus(refundDTO.getStatus());

        return refund;
    }

    public RefundDTO mapRefundToRefundDTO(Refund refund) {
        RefundDTO refundDTO = new RefundDTO();

        if (refund == null) {
            return refundDTO;
        }

        if (refund.getRefundId() != null) {
            refundDTO.setId(refund.getRefundId().getId());
        }

        if (refund.getProviderRefunds() != null) {
            refundDTO.setProviderRefunds(mapProviderRefundsToProviderRefundsDTO(refund.getProviderRefunds()));
        }

        refundDTO.setCustomerRefundMethod(refund.getCustomerRefundMethod());
        refundDTO.setCustomerRefundAmount(refund.getCustomerRefundAmount());
        refundDTO.setCustomerRefundCurrency(refund.getCustomerRefundCurrency());
        refundDTO.setBookingItemId(refund.getBookingItemId());
        refundDTO.setStatus(refund.getStatus());

        return refundDTO;
    }

    private List<ProviderRefundDTO> mapProviderRefundsToProviderRefundsDTO(List<ProviderRefund> providerRefunds) {
        return providerRefunds.stream().map(this::mapProviderRefundToProviderRefundDTO).collect(Collectors.toList());
    }

    private ProviderRefundDTO mapProviderRefundToProviderRefundDTO(ProviderRefund providerRefund) {
        ProviderRefundDTO providerRefundDTO = new ProviderRefundDTO();
        providerRefundDTO.setId(providerRefund.getId().getId());
        providerRefundDTO.setProviderBookingItemId(providerRefund.getProvBookingItemId());
        providerRefundDTO.setExpectedProviderRefundAmount(providerRefund.getExpectedProvRefundAmount());
        providerRefundDTO.setExpectedProviderRefundCurrency(providerRefund.getExpectedProvRefundCurrency());

        return providerRefundDTO;
    }

    private List<ProviderRefund> mapProviderRefundsDTOToProviderRefunds(List<ProviderRefundDTO> providerRefundDTOS) {
        return providerRefundDTOS.stream().map(this::mapProviderRefundDTOToProviderRefund).collect(Collectors.toList());
    }

    private ProviderRefund mapProviderRefundDTOToProviderRefund(ProviderRefundDTO providerRefundDTO) {
        ProviderRefund providerRefund = new ProviderRefund();
        providerRefund.setId(new ProviderRefundId(providerRefundDTO.getId()));
        providerRefund.setProvBookingItemId(providerRefundDTO.getProviderBookingItemId());
        providerRefund.setExpectedProvRefundAmount(providerRefundDTO.getExpectedProviderRefundAmount());
        providerRefund.setExpectedProvRefundCurrency(providerRefundDTO.getExpectedProviderRefundCurrency());

        return providerRefund;
    }
}
