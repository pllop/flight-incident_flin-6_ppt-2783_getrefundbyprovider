package com.edreamsodigeo.customersupport.refund.customermovement;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.refund.customermovement.repository.impl.JpaCustomerMovementRepository;
import com.edreamsodigeo.customersupport.refund.customermovement.mapper.CustomerMovementMapper;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovement;
import com.edreamsodigeo.customersupport.refund.customermovement.repository.CustomerMovementRepository;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@Local(CustomerMovementService.class)
public class CustomerMovementServiceBean implements CustomerMovementService {

    private CustomerMovementRepository customerMovementRepository;

    @PersistenceContext(unitName = "refund-engine")
    private EntityManager entityManager;

    private CustomerMovementMapper customerMovementMapper;

    public CustomerMovementServiceBean() {
        customerMovementRepository = ConfigurationEngine.getInstance(CustomerMovementRepository.class);
        customerMovementMapper = ConfigurationEngine.getInstance(CustomerMovementMapper.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaCustomerMovementRepository) customerMovementRepository).setEntityManager(entityManager);
    }

    @Override
    public UUID createCustomerMovement(CustomerMovementDTO customerMovementDto) throws RegisterNotFound {
        CustomerMovement customerMovement = customerMovementMapper.mapCustomerMovementDTOtoCustomerMovement(customerMovementDto);
        customerMovementRepository.insert(customerMovement);
        return customerMovementMapper.mapCustomerMovementToCustomerMovementDTO(customerMovement).getId();
    }
}
