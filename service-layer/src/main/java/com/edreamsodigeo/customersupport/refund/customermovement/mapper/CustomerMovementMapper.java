package com.edreamsodigeo.customersupport.refund.customermovement.mapper;

import com.edreams.util.money.MoneySettings;
import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementDTO;
import com.edreamsodigeo.customersupport.refund.customermovement.MovementMoneyDTO;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovement;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovementId;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovementWavePspId;
import com.edreamsodigeo.customersupport.refund.customermovement.model.RefundId;

public class CustomerMovementMapper {
    public CustomerMovement mapCustomerMovementDTOtoCustomerMovement(CustomerMovementDTO customerMovementDTO) {
        CustomerMovement customerMovement = new CustomerMovement();
        customerMovement.setId(new CustomerMovementId(customerMovementDTO.getId()));
        customerMovement.setRefundId(new RefundId(customerMovementDTO.getRefundId()));
        customerMovement.setAmount(customerMovementDTO.getAmount().getAmount());
        customerMovement.setCurrency(customerMovementDTO.getCurrencyCode());
        customerMovement.setCurrencyRateProviderCustomer(customerMovementDTO.getCurrencyRateProviderCustomer());
        customerMovement.setAmountInEur(customerMovementDTO.getAmountInEur().getAmount());
        customerMovement.setCurrencyRateProviderEur(customerMovementDTO.getCurrencyRateProviderEur());
        customerMovement.setPsp(customerMovementDTO.getPsp());
        customerMovement.setPspAccount(customerMovementDTO.getPspAccount());
        customerMovement.setStatus(customerMovementDTO.getStatus());
        customerMovement.setCustomerMovementWavePspId(new CustomerMovementWavePspId(customerMovementDTO.getCustomerMovementWavePspId()));
        return customerMovement;
    }

    public CustomerMovementDTO mapCustomerMovementToCustomerMovementDTO(CustomerMovement customerMovement) {
        CustomerMovementDTO customerMovementDTO = new CustomerMovementDTO();
        customerMovementDTO.setId(customerMovement.getId().getId());
        customerMovementDTO.setRefundId(customerMovement.getRefundId().getId());
        customerMovementDTO.setAmount(new MovementMoneyDTO(customerMovement.getAmount(), customerMovement.getCurrency()));
        customerMovementDTO.setCurrencyCode(customerMovement.getCurrency());
        customerMovementDTO.setCurrencyRateProviderCustomer(customerMovement.getCurrencyRateProviderCustomer());
        customerMovementDTO.setAmountInEur(new MovementMoneyDTO(customerMovement.getAmountInEur(), MoneySettings.DEFAULT_CURRENCY.getCurrencyCode()));
        customerMovementDTO.setCurrencyRateProviderEur(customerMovement.getCurrencyRateProviderEur());
        customerMovementDTO.setPsp(customerMovement.getPsp());
        customerMovementDTO.setPspAccount(customerMovement.getPspAccount());
        customerMovementDTO.setStatus(customerMovement.getStatus());
        customerMovementDTO.setCustomerMovementWavePspId(customerMovement.getCustomerMovementWavePspId().getId());
        return customerMovementDTO;
    }
}
