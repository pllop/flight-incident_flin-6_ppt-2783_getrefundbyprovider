package com.edreamsodigeo.customersupport.refund;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.refund.mapper.RefundMapper;
import com.edreamsodigeo.customersupport.refund.model.Refund;
import com.edreamsodigeo.customersupport.refund.model.RefundId;
import com.edreamsodigeo.customersupport.refund.repository.RefundRepository;
import com.edreamsodigeo.customersupport.refund.repository.impl.JpaRefundRepository;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.UUID;

@Stateless
@Local(RefundService.class)
public class RefundServiceBean implements RefundService {

    @PersistenceContext(unitName = "refund-engine")
    private EntityManager entityManager;

    private RefundMapper refundMapper;
    private RefundRepository refundRepository;

    public RefundServiceBean() {
        this.refundRepository = ConfigurationEngine.getInstance(RefundRepository.class);
        this.refundMapper = ConfigurationEngine.getInstance(RefundMapper.class);
    }

    @PostConstruct
    public void postConstruct() {
        ((JpaRefundRepository) refundRepository).setEntityManager(entityManager);
    }

    @Override
    public RefundDTO getRefundById(UUID id) {
        return refundMapper.mapRefundToRefundDTO(refundRepository.findById(new RefundId(id)));
    }

    @Override
    public RefundDTO addRefund(RefundDTO refundDTO) {
        Refund refund = refundMapper.mapRefundDTOToRefund(refundDTO);
        refundRepository.insert(refund);
        return refundMapper.mapRefundToRefundDTO(refund);
    }

    @Override
    public void updateRefund(RefundDTO refundDTO) {
        refundRepository.update(refundMapper.mapRefundDTOToRefund(refundDTO));
    }
}
