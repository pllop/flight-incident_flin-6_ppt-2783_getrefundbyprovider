package com.edreamsodigeo.customersupport.refund.customermovement;

import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import java.util.UUID;

public interface CustomerMovementService {
    UUID createCustomerMovement(CustomerMovementDTO customerMovementDto) throws RegisterNotFound;
}
