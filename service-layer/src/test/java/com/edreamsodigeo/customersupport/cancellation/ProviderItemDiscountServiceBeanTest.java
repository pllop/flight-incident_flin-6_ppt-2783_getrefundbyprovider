package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.ProviderItemDiscountMapper;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscountId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemDiscountRepository;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ProviderItemDiscountServiceBeanTest {
    @Mock
    private ProviderItemDiscountRepository providerItemDiscountRepository;
    @Mock
    private ProviderItemDiscountMapper providerItemDiscountMapper;

    private ProviderItemDiscountServiceBean providerItemDiscountServiceBean;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(binder -> {
            binder.bind(ProviderItemDiscountRepository.class).toInstance(providerItemDiscountRepository);
            binder.bind(ProviderItemDiscountMapper.class).toInstance(providerItemDiscountMapper);
        });
        providerItemDiscountServiceBean = new ProviderItemDiscountServiceBean();
    }

    @Test
    public void testAddproviderItemDiscount() {
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        when(providerItemDiscountMapper.map(eq(providerItemDiscountDTO))).thenReturn(providerItemDiscount);
        doNothing().when(providerItemDiscountRepository).insert(eq(providerItemDiscount));
        when(providerItemDiscountMapper.map(eq(providerItemDiscount))).thenReturn(providerItemDiscountDTO);
        providerItemDiscountServiceBean.addproviderItemDiscount(providerItemDiscountDTO);
        verify(providerItemDiscountMapper).map(eq(providerItemDiscountDTO));
        verify(providerItemDiscountRepository).insert(eq(providerItemDiscount));
        verify(providerItemDiscountMapper).map(eq(providerItemDiscount));
    }

    @Test
    public void testGetProviderItemDiscount() {
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        UUID id = UUIDGenerator.getInstance().generateUUID();
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        ProviderItemDiscountId providerItemDiscountId = new ProviderItemDiscountId(id);
        when(providerItemDiscountMapper.map(eq(providerItemDiscount))).thenReturn(providerItemDiscountDTO);
        when(providerItemDiscountRepository.findById(eq(providerItemDiscountId))).thenReturn(providerItemDiscount);
        ProviderItemDiscountDTO result = providerItemDiscountServiceBean.getProviderItemDiscount(id);
        assertEquals(result, providerItemDiscountDTO);
        verify(providerItemDiscountMapper).map(eq(providerItemDiscount));
        verify(providerItemDiscountRepository).findById(eq(providerItemDiscountId));
    }
}
