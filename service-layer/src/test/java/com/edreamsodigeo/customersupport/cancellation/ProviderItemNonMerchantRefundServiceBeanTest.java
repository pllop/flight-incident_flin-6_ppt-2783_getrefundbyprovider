package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.ProviderItemNonMerchantRefundMapper;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefundId;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemNonMerchantRefundRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemNonMerchantRefundRepository;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;
import java.math.BigDecimal;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class ProviderItemNonMerchantRefundServiceBeanTest {

    private ProviderItemNonMerchantRefundServiceBean serviceBean;

    @Mock
    private JpaProviderItemNonMerchantRefundRepository providerItemNonMerchantRefundRepository;

    @Mock
    private ProviderItemNonMerchantRefundMapper providerItemNonMerchantRefundMapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(ProviderItemNonMerchantRefundMapper.class).toInstance(providerItemNonMerchantRefundMapper);
            t.bind(ProviderItemNonMerchantRefundRepository.class).toInstance(providerItemNonMerchantRefundRepository);
        });
        serviceBean = new ProviderItemNonMerchantRefundServiceBean();
    }

    @Test
    public void testGetProviderItemNonMerchantRefundOK() throws RegisterNotFound {
        ProviderItemNonMerchantRefundId providerItemMerchantRefundId = new ProviderItemNonMerchantRefundId(UUIDGenerator.getInstance().generateUUID());
        when(providerItemNonMerchantRefundRepository.findById(providerItemMerchantRefundId)).thenReturn(createProviderItemNonMerchantRefund());

        serviceBean.postConstruct();
        serviceBean.getProviderItemNonMerchant(providerItemMerchantRefundId.getId());

        verify(providerItemNonMerchantRefundRepository).findById(providerItemMerchantRefundId);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetProviderItemNonMerchantRefundRegisterNotFound() throws RegisterNotFound {
        when(providerItemNonMerchantRefundRepository.findById(any())).thenReturn(null);

        serviceBean.postConstruct();
        serviceBean.getProviderItemNonMerchant(UUIDGenerator.getInstance().generateUUID());
    }

    @Test
    public void testAddProviderItemNonMerchantRefund() {
        when(providerItemNonMerchantRefundMapper.mapDtoToProviderItemNonMerchant(any())).thenReturn(new ProviderItemNonMerchantRefund());

        serviceBean.postConstruct();
        serviceBean.addProviderItemNonMerchant(new ProviderItemNonMerchantDTO());

        verify(providerItemNonMerchantRefundRepository).insert(any());
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testAddProviderItemNonMerchantRefundWithError() {
        when(providerItemNonMerchantRefundMapper.mapDtoToProviderItemNonMerchant(any())).thenReturn(new ProviderItemNonMerchantRefund());
        doThrow(new NoResultException()).when(providerItemNonMerchantRefundRepository).insert(any());

        serviceBean.postConstruct();
        serviceBean.addProviderItemNonMerchant(new ProviderItemNonMerchantDTO());

        verify(providerItemNonMerchantRefundRepository).insert(any());
    }

    @Test
    public void testUpdateProviderItemNonMerchantRefundOK() throws RegisterNotFound {
        ProviderItemNonMerchantDTO providerItemNonMerchantDTO = new ProviderItemNonMerchantDTO();
        ProviderItemNonMerchantRefund providerItemNonMerchantRefundRequest = createProviderItemNonMerchantRefund();
        ProviderItemNonMerchantRefund providerItemNonMerchantRefundInStore = createProviderItemNonMerchantRefund();

        providerItemNonMerchantRefundInStore.setId(new ProviderItemNonMerchantRefundId(providerItemNonMerchantDTO.getId()));
        providerItemNonMerchantRefundRequest.setId(providerItemNonMerchantRefundInStore.getId());

        when(providerItemNonMerchantRefundRepository.findById(any())).thenReturn(providerItemNonMerchantRefundInStore);
        when(providerItemNonMerchantRefundMapper.mapDtoToProviderItemNonMerchant(any())).thenReturn(providerItemNonMerchantRefundRequest);

        serviceBean.postConstruct();
        serviceBean.updateProviderItemNonMerchant(providerItemNonMerchantDTO);

        ArgumentCaptor<ProviderItemNonMerchantRefund> argumentProviderItemMerchantRefund = ArgumentCaptor.forClass(ProviderItemNonMerchantRefund.class);
        verify(providerItemNonMerchantRefundRepository).update(argumentProviderItemMerchantRefund.capture());
        assertEquals(providerItemNonMerchantRefundRequest.getProviderItemId().getId(), argumentProviderItemMerchantRefund.getValue().getProviderItemId().getId());
        assertEquals(providerItemNonMerchantRefundRequest.getExpectedProviderRefundAmount(), argumentProviderItemMerchantRefund.getValue().getExpectedProviderRefundAmount());
        assertEquals(providerItemNonMerchantRefundRequest.getExpectedProviderRefundCurrency(), argumentProviderItemMerchantRefund.getValue().getExpectedProviderRefundCurrency());
        assertEquals(providerItemNonMerchantRefundRequest.getRefunder(), argumentProviderItemMerchantRefund.getValue().getRefunder());
    }

    private ProviderItemNonMerchantRefund createProviderItemNonMerchantRefund() {
        ProviderItemNonMerchantRefund itemInStore = new ProviderItemNonMerchantRefund();
        itemInStore.setId(new ProviderItemNonMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));
        itemInStore.setProviderItemId(new ProviderItemId(UUIDGenerator.getInstance().generateUUID()));
        itemInStore.setExpectedProviderRefundAmount(new BigDecimal(Math.random()));
        itemInStore.setExpectedProviderRefundCurrency("EUR");
        itemInStore.setRefunder("TEST-REFUNDER");

        return itemInStore;
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testUpdateProviderItemNonMerchantRefundRegisterNotFound() throws RegisterNotFound {
        when(providerItemNonMerchantRefundRepository.findById(any())).thenReturn(null);

        serviceBean.postConstruct();
        serviceBean.updateProviderItemNonMerchant(new ProviderItemNonMerchantDTO());

        verify(providerItemNonMerchantRefundRepository).update(any());
    }

    @Test
    public void testGetProviderItemNonMerchantRefundByProviderItem() throws RegisterNotFound {
        ProviderItemNonMerchantDTO dto = new ProviderItemNonMerchantDTO();
        ProviderItemNonMerchantRefund entityItem = new ProviderItemNonMerchantRefund();
        when(providerItemNonMerchantRefundRepository.findByProviderItemId(any())).thenReturn(entityItem);
        when(providerItemNonMerchantRefundMapper.mapProviderItemNonMerchantToDto(entityItem)).thenReturn(dto);
        ProviderItemNonMerchantDTO result = serviceBean.getProviderItemNonMerchantByProviderItem(UUIDGenerator.getInstance().generateUUID());
        assertSame(result, dto);
        verify(providerItemNonMerchantRefundRepository).findByProviderItemId(any());
        verify(providerItemNonMerchantRefundMapper).mapProviderItemNonMerchantToDto(entityItem);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetProviderItemNonMerchantRefundByProviderItemNullCollection() throws RegisterNotFound {
        when(providerItemNonMerchantRefundRepository.findByProviderItemId(any())).thenReturn(null);
        try {
            serviceBean.getProviderItemNonMerchantByProviderItem(UUIDGenerator.getInstance().generateUUID());
        } catch (RegisterNotFound e) {
            verify(providerItemNonMerchantRefundRepository).findByProviderItemId(any());
            throw e;
        }
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetProviderItemNonMerchantRefundByProviderItemEmptyCollection() throws RegisterNotFound {
        when(providerItemNonMerchantRefundRepository.findByProviderItemId(any())).thenReturn(null);
        try {
            serviceBean.getProviderItemNonMerchantByProviderItem(UUIDGenerator.getInstance().generateUUID());
        } catch (RegisterNotFound e) {
            verify(providerItemNonMerchantRefundRepository).findByProviderItemId(any());
            throw e;
        }
    }
}
