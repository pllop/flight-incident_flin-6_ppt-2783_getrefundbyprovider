package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.CancellationMapper;
import com.edreamsodigeo.customersupport.cancellation.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.model.Item;
import com.edreamsodigeo.customersupport.cancellation.model.ItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.repository.CancellationRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaCancellationRepository;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.apache.commons.lang.RandomStringUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.collections.Lists;

import javax.persistence.NoResultException;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class CancellationServiceBeanTest {

    private CancellationServiceBean serviceBean;

    @Mock
    private JpaCancellationRepository cancellationRepository;

    @Mock
    private CancellationMapper cancellationMapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(CancellationMapper.class).toInstance(cancellationMapper);
            t.bind(CancellationRepository.class).toInstance(cancellationRepository);
        });
        serviceBean = new CancellationServiceBean();
    }

    @Test
    public void testGetCancellationOK() throws RegisterNotFound {
        CancellationId cancellationId = new CancellationId(UUIDGenerator.getInstance().generateUUID());
        when(cancellationRepository.findById(cancellationId)).thenReturn(createCancellation());

        serviceBean.postConstruct();
        serviceBean.getCancellation(cancellationId.getId());

        verify(cancellationRepository).findById(cancellationId);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetCancellationRegisterNotFound() throws RegisterNotFound {
        when(cancellationRepository.findById(any())).thenReturn(null);

        serviceBean.postConstruct();
        serviceBean.getCancellation(UUIDGenerator.getInstance().generateUUID());
    }

    @Test
    public void testAddCancellation() {
        when(cancellationMapper.mapCancellationDTOToCancellation(any())).thenReturn(new Cancellation());

        serviceBean.postConstruct();
        serviceBean.addCancellation(new CancellationDTO());

        verify(cancellationRepository).insert(any());
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testAddCancellationWithError() {
        when(cancellationMapper.mapCancellationDTOToCancellation(any())).thenReturn(new Cancellation());
        doThrow(new NoResultException()).when(cancellationRepository).insert(any());

        serviceBean.postConstruct();
        serviceBean.addCancellation(new CancellationDTO());

        verify(cancellationRepository).insert(any());
    }

    @Test
    public void testUpdateCancellationOK() throws RegisterNotFound {
        CancellationDTO cancellationDTO = createCancellationDTO();
        Cancellation cancellationRequest = createCancellation();
        Cancellation cancellationInStore = createCancellation();

        cancellationInStore.getItems().get(0).setId(new ItemId(cancellationDTO.getItems().get(0).getId()));
        cancellationRequest.getItems().get(0).setId(cancellationInStore.getItems().get(0).getId());

        when(cancellationRepository.findById(any())).thenReturn(cancellationInStore);
        when(cancellationMapper.mapCancellationDTOToCancellation(any())).thenReturn(cancellationRequest);

        serviceBean.postConstruct();
        serviceBean.updateCancellation(cancellationDTO);

        ArgumentCaptor<Cancellation> argumentCancellation = ArgumentCaptor.forClass(Cancellation.class);
        verify(cancellationRepository).update(argumentCancellation.capture());
        assertEquals(argumentCancellation.getValue().getIncidentId().getId(), cancellationRequest.getIncidentId().getId());
        assertEquals(argumentCancellation.getValue().getDiscarded(), cancellationRequest.getDiscarded());
        assertEquals(argumentCancellation.getValue().getUserId(), cancellationRequest.getUserId());
        assertEquals(argumentCancellation.getValue().getBuyerEmail(), cancellationRequest.getBuyerEmail());
        assertEquals(argumentCancellation.getValue().getItems().size(), cancellationRequest.getItems().size());
        assertEquals(argumentCancellation.getValue().getItems().get(0).getId(), cancellationRequest.getItems().get(0).getId());
        assertEquals(argumentCancellation.getValue().getItems().get(0).getProviderItems().size(), cancellationRequest.getItems().get(0).getProviderItems().size());
        assertEquals(argumentCancellation.getValue().getItems().get(0).getProviderItems().get(0).getId(), cancellationRequest.getItems().get(0).getProviderItems().get(0).getId());

        verify(cancellationRepository, times(0)).delete(any());
    }

    private CancellationDTO createCancellationDTO() {
        CancellationDTO cancellationDTO = new CancellationDTO();
        cancellationDTO.setItems(Collections.singletonList(createCancelledItemDTO()));
        return cancellationDTO;
    }

    private ItemDTO createCancelledItemDTO() {
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(UUIDGenerator.getInstance().generateUUID());
        itemDTO.setProviderItems(Collections.singletonList(createProviderItemDTO()));
        return itemDTO;
    }

    private ProviderItemDTO createProviderItemDTO() {
        ProviderItemDTO providerItemDTO = new ProviderItemDTO();
        providerItemDTO.setId(UUIDGenerator.getInstance().generateUUID());
        return providerItemDTO;
    }

    @Test
    public void testUpdateCancellationWithItemsToRemove() throws RegisterNotFound {
        CancellationDTO cancellationDTO = createCancellationDTO();
        Cancellation cancellationRequest = createCancellation();
        Cancellation cancellationInStore = createCancellation();

        when(cancellationRepository.findById(any())).thenReturn(cancellationInStore);
        when(cancellationMapper.mapCancellationDTOToCancellation(any())).thenReturn(cancellationRequest);

        serviceBean.postConstruct();
        serviceBean.updateCancellation(cancellationDTO);

        ArgumentCaptor<Cancellation> argumentCancellation = ArgumentCaptor.forClass(Cancellation.class);
        verify(cancellationRepository).update(argumentCancellation.capture());
        assertEquals(argumentCancellation.getValue().getIncidentId().getId(), cancellationRequest.getIncidentId().getId());
        assertEquals(argumentCancellation.getValue().getDiscarded(), cancellationRequest.getDiscarded());
        assertEquals(argumentCancellation.getValue().getUserId(), cancellationRequest.getUserId());
        assertEquals(argumentCancellation.getValue().getBuyerEmail(), cancellationRequest.getBuyerEmail());
        assertEquals(argumentCancellation.getValue().getItems().size(), cancellationRequest.getItems().size());
        assertEquals(argumentCancellation.getValue().getItems().get(0).getId(), cancellationRequest.getItems().get(0).getId());
        assertEquals(argumentCancellation.getValue().getItems().get(0).getProviderItems().size(), cancellationRequest.getItems().get(0).getProviderItems().size());
        assertEquals(argumentCancellation.getValue().getItems().get(0).getProviderItems().get(0).getId(), cancellationRequest.getItems().get(0).getProviderItems().get(0).getId());
    }

    private Cancellation createCancellation() {
        Cancellation cancellation = new Cancellation();
        cancellation.setId(new CancellationId(UUIDGenerator.getInstance().generateUUID()));
        cancellation.setIncidentId(new IncidentId(UUIDGenerator.getInstance().generateUUID()));
        cancellation.setDiscarded(false);
        cancellation.setUserId(RandomStringUtils.randomAlphanumeric(10));
        cancellation.setBuyerEmail(RandomStringUtils.randomAlphanumeric(10));

        cancellation.setItems(Lists.newArrayList(Collections.singletonList(createItem())));

        return cancellation;
    }

    private Item createItem() {
        Item item = new Item();
        item.setId(new ItemId(UUIDGenerator.getInstance().generateUUID()));
        item.setProviderItems(Lists.newArrayList(Collections.singletonList(createProviderItem())));
        return item;
    }

    private ProviderItem createProviderItem() {
        ProviderItem providerItem = new ProviderItem();
        providerItem.setId(new ProviderItemId(UUIDGenerator.getInstance().generateUUID()));
        return providerItem;
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testUpdateCancellationRegisterNotFound() throws RegisterNotFound {
        when(cancellationRepository.findById(any())).thenReturn(null);

        serviceBean.postConstruct();
        serviceBean.updateCancellation(new CancellationDTO());

        verify(cancellationRepository).update(any());
    }

    @Test
    public void testDiscardCancellationOK() throws RegisterNotFound {
        when(cancellationRepository.findById(any())).thenReturn(createCancellation());

        serviceBean.postConstruct();
        serviceBean.discardCancellation(UUIDGenerator.getInstance().generateUUID());

        ArgumentCaptor<Cancellation> argumentCancellation = ArgumentCaptor.forClass(Cancellation.class);
        verify(cancellationRepository).update(argumentCancellation.capture());
        assertTrue(argumentCancellation.getValue().getDiscarded());
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testDiscardCancellationRegisterNotFound() throws RegisterNotFound {
        when(cancellationRepository.findById(any())).thenReturn(null);

        serviceBean.postConstruct();
        serviceBean.discardCancellation(UUIDGenerator.getInstance().generateUUID());
    }

    @Test
    public void testGetCancellationsByIncident() throws RegisterNotFound {
        CancellationDTO dto = new CancellationDTO();
        Cancellation entity = new Cancellation();
        List<Cancellation> entityList = Collections.singletonList(entity);
        when(cancellationRepository.findByIncidentId(any())).thenReturn(entityList);
        when(cancellationMapper.mapCancellationToCancellationDTO(entity)).thenReturn(dto);
        List<CancellationDTO> result = serviceBean.getCancellationsByIncident(UUIDGenerator.getInstance().generateUUID());
        assertEquals(result.size(), 1);
        assertSame(result.get(0), dto);
        verify(cancellationRepository).findByIncidentId(any());
        verify(cancellationMapper).mapCancellationToCancellationDTO(entity);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetCancellationsByIncidentNullCollection() throws RegisterNotFound {
        when(cancellationRepository.findByIncidentId(any())).thenReturn(null);
        try {
            serviceBean.getCancellationsByIncident(UUIDGenerator.getInstance().generateUUID());
        } catch (RegisterNotFound e) {
            verify(cancellationRepository).findByIncidentId(any());
            throw e;
        }
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetCancellationsByIncidentEmptyCollection() throws RegisterNotFound {
        when(cancellationRepository.findByIncidentId(any())).thenReturn(Collections.emptyList());
        try {
            serviceBean.getCancellationsByIncident(UUIDGenerator.getInstance().generateUUID());
        } catch (RegisterNotFound e) {
            verify(cancellationRepository).findByIncidentId(any());
            throw e;
        }
    }
}
