package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.CancellationDTO;
import com.edreamsodigeo.customersupport.cancellation.ItemDTO;
import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemDTO;
import com.edreamsodigeo.customersupport.cancellation.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.model.CancellationId;
import com.edreamsodigeo.customersupport.cancellation.model.IncidentId;
import com.edreamsodigeo.customersupport.cancellation.model.Item;
import com.edreamsodigeo.customersupport.cancellation.model.ItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CancellationMapperTest {
    private CancellationMapper cancellationMapper;

    @BeforeMethod
    public void setUp() {
        cancellationMapper = new CancellationMapper();
    }

    @Test
    public void testMapCancellationDTO() {
        CancellationDTO cancellationDTO = new CancellationDTO();
        cancellationDTO.setId(UUIDGenerator.getInstance().generateUUID());
        cancellationDTO.setIncidentId(UUIDGenerator.getInstance().generateUUID());
        cancellationDTO.setUserId("customer@email.com");
        cancellationDTO.setDiscarded(true);

        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(UUIDGenerator.getInstance().generateUUID());
        itemDTO.setBookingItemId(10L);
        cancellationDTO.setItems(Collections.singletonList(itemDTO));

        ProviderItemDTO providerItemDTO = new ProviderItemDTO();
        providerItemDTO.setId(UUIDGenerator.getInstance().generateUUID());
        providerItemDTO.setProviderBookingItemId(20L);
        providerItemDTO.setResolutionType("RESOLUTION_TYPE");
        providerItemDTO.setTopUpVoucherAmount(new MoneyDTO(BigDecimal.TEN, "EUR"));
        providerItemDTO.setTopUpVoucherMinBasketAmount(new MoneyDTO(BigDecimal.ONE, "USD"));
        itemDTO.setProviderItems(Collections.singletonList(providerItemDTO));

        Cancellation cancellation = cancellationMapper.mapCancellationDTOToCancellation(cancellationDTO);

        assertNotNull(cancellation);
        assertEquals(cancellation.getId().getId(), cancellationDTO.getId());
        assertEquals(cancellation.getIncidentId().getId(), cancellationDTO.getIncidentId());
        assertEquals(cancellation.getUserId(), cancellationDTO.getUserId());
        assertEquals(cancellation.getDiscarded(), (Boolean) cancellationDTO.isDiscarded());
        assertEquals(cancellation.getTimestamp(), cancellationDTO.getTimestamp());
        assertEquals(cancellation.getItems().size(), cancellationDTO.getItems().size());

        Item item = cancellation.getItems().get(0);
        assertEquals(item.getId().getId(), itemDTO.getId());
        assertEquals(item.getBookingItemId(), itemDTO.getBookingItemId());

        ProviderItem providerItem = item.getProviderItems().get(0);
        assertEquals(providerItem.getProviderBookingItemId(), providerItemDTO.getProviderBookingItemId());
        assertEquals(providerItem.getResolutionType(), providerItemDTO.getResolutionType());
        assertEquals(providerItem.getTopUpVoucherAmount(), providerItemDTO.getTopUpVoucherAmount().getAmount());
        assertEquals(providerItem.getTopUpVoucherCurrency(), providerItemDTO.getTopUpVoucherAmount().getCurrency());
        assertEquals(providerItem.getTopUpVoucherMinBasketAmount(), providerItemDTO.getTopUpVoucherMinBasketAmount().getAmount());
    }

    @Test
    public void testMapCancellationDTOWithOnlyRequiredInfo() {
        CancellationDTO cancellationDTO = new CancellationDTO();
        cancellationDTO.setIncidentId(UUIDGenerator.getInstance().generateUUID());

        ItemDTO itemDTO = new ItemDTO();
        cancellationDTO.setItems(Collections.singletonList(itemDTO));

        ProviderItemDTO providerItemDTO = new ProviderItemDTO();
        itemDTO.setProviderItems(Collections.singletonList(providerItemDTO));

        Cancellation cancellation = cancellationMapper.mapCancellationDTOToCancellation(cancellationDTO);

        assertNotNull(cancellation);

        Item item = cancellation.getItems().get(0);
        assertNotNull(item);

        ProviderItem providerItem = item.getProviderItems().get(0);
        assertNotNull(providerItem);
    }

    @Test
    public void testMapCancellation() {
        Cancellation cancellation = new Cancellation();
        cancellation.setId(new CancellationId(UUIDGenerator.getInstance().generateUUID()));
        cancellation.setIncidentId(new IncidentId(UUIDGenerator.getInstance().generateUUID()));
        cancellation.setUserId("customer@email.com");
        cancellation.setDiscarded(true);
        cancellation.setTimestamp(LocalDateTime.now());

        Item item = new Item();
        item.setId(new ItemId(UUIDGenerator.getInstance().generateUUID()));
        item.setBookingItemId(10L);
        cancellation.setItems(Collections.singletonList(item));

        ProviderItem providerItem = new ProviderItem();
        providerItem.setId(new ProviderItemId(UUIDGenerator.getInstance().generateUUID()));
        providerItem.setProviderBookingItemId(20L);
        providerItem.setResolutionType("RESOLUTION_TYPE");
        providerItem.setTopUpVoucherAmount(BigDecimal.TEN);
        providerItem.setTopUpVoucherMinBasketAmount(BigDecimal.ONE);
        providerItem.setTopUpVoucherCurrency("COP");
        item.setProviderItems(Collections.singletonList(providerItem));

        CancellationDTO cancellationDTO = cancellationMapper.mapCancellationToCancellationDTO(cancellation);

        assertNotNull(cancellationDTO);
        assertEquals(cancellationDTO.getId(), cancellation.getId().getId());
        assertEquals(cancellationDTO.getIncidentId(), cancellation.getIncidentId().getId());
        assertEquals(cancellationDTO.getUserId(), cancellation.getUserId());
        assertEquals((Boolean) cancellationDTO.isDiscarded(), cancellation.getDiscarded());
        assertEquals(cancellationDTO.getTimestamp(), cancellation.getTimestamp());
        assertEquals(cancellationDTO.getItems().size(), cancellation.getItems().size());

        ItemDTO itemDTO = cancellationDTO.getItems().get(0);
        assertEquals(itemDTO.getId(), item.getId().getId());
        assertEquals(itemDTO.getBookingItemId(), item.getBookingItemId());

        ProviderItemDTO providerItemDTO = itemDTO.getProviderItems().get(0);
        assertEquals(providerItemDTO.getProviderBookingItemId(), providerItem.getProviderBookingItemId());
        assertEquals(providerItemDTO.getResolutionType(), providerItem.getResolutionType());
        assertEquals(providerItemDTO.getTopUpVoucherAmount().getAmount(), providerItem.getTopUpVoucherAmount());
        assertEquals(providerItemDTO.getTopUpVoucherAmount().getCurrency(), providerItem.getTopUpVoucherCurrency());
        assertEquals(providerItemDTO.getTopUpVoucherMinBasketAmount().getAmount(), providerItem.getTopUpVoucherMinBasketAmount());
        assertEquals(providerItemDTO.getTopUpVoucherMinBasketAmount().getCurrency(), providerItem.getTopUpVoucherCurrency());
    }

    @Test
    public void testMapCancellationWithOnlyRequiredInfo() {
        Cancellation cancellation = new Cancellation();
        cancellation.setId(new CancellationId(UUIDGenerator.getInstance().generateUUID()));
        cancellation.setIncidentId(new IncidentId(UUIDGenerator.getInstance().generateUUID()));
        cancellation.setDiscarded(false);

        Item item = new Item();
        item.setId(new ItemId(UUIDGenerator.getInstance().generateUUID()));
        cancellation.setItems(Collections.singletonList(item));

        ProviderItem providerItem = new ProviderItem();
        providerItem.setId(new ProviderItemId(UUIDGenerator.getInstance().generateUUID()));
        providerItem.setProviderBookingItemId(20L);
        item.setProviderItems(Collections.singletonList(providerItem));

        CancellationDTO cancellationDTO = cancellationMapper.mapCancellationToCancellationDTO(cancellation);

        assertNotNull(cancellationDTO);
        assertEquals(cancellationDTO.getId(), cancellation.getId().getId());
        assertEquals(cancellationDTO.getIncidentId(), cancellation.getIncidentId().getId());
        assertEquals((Boolean) cancellationDTO.isDiscarded(), cancellation.getDiscarded());
        assertEquals(cancellationDTO.getItems().size(), cancellation.getItems().size());

        ItemDTO itemDTO = cancellationDTO.getItems().get(0);
        assertEquals(itemDTO.getId(), item.getId().getId());
    }
}
