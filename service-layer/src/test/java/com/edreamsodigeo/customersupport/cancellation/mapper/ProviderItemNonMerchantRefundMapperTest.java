package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemNonMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemNonMerchantRefundId;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class ProviderItemNonMerchantRefundMapperTest {

    private ProviderItemNonMerchantRefundMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new ProviderItemNonMerchantRefundMapper();
    }

    @Test
    public void testMapDtoToProviderItemNonMerchantRefund() {
        UUID id = UUID.randomUUID();
        UUID providerItemId = UUID.randomUUID();
        UUID refundId = UUID.randomUUID();
        ProviderItemNonMerchantDTO dto = new ProviderItemNonMerchantDTO();
        dto.setId(id);
        dto.setProviderItemId(providerItemId);
        dto.setProviderRefundAmountExpected(new MoneyDTO(BigDecimal.TEN, "EUR"));
        dto.setRefunder("TEST-REFUNDER");
        ProviderItemNonMerchantRefund result = mapper.mapDtoToProviderItemNonMerchant(dto);
        assertEquals(result.getId().getId(), id);
        assertEquals(result.getProviderItemId().getId(), providerItemId);
        assertEquals(result.getExpectedProviderRefundAmount(), BigDecimal.TEN);
        assertEquals(result.getExpectedProviderRefundCurrency(), "EUR");
        assertEquals(result.getRefunder(), "TEST-REFUNDER");
    }

    @Test
    public void testMapProviderItemNonMerchantRefund() {
        UUID uuid = UUID.randomUUID();
        ProviderItemNonMerchantRefund providerItemNonMerchantRefund = new ProviderItemNonMerchantRefund();
        ProviderItemNonMerchantRefundId providerItemNonMerchantRefundId = new ProviderItemNonMerchantRefundId();
        providerItemNonMerchantRefundId.setId(uuid);
        providerItemNonMerchantRefund.setId(providerItemNonMerchantRefundId);

        UUID providerItemUUId = UUID.randomUUID();
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(providerItemUUId);
        providerItemNonMerchantRefund.setProviderItemId(providerItemId);

        providerItemNonMerchantRefund.setExpectedProviderRefundAmount(BigDecimal.TEN);
        providerItemNonMerchantRefund.setExpectedProviderRefundCurrency("EUR");
        providerItemNonMerchantRefund.setRefunder("TEST-REFUND");


        ProviderItemNonMerchantDTO result = mapper.mapProviderItemNonMerchantToDto(providerItemNonMerchantRefund);
        assertEquals(result.getId(), uuid);
        assertEquals(result.getProviderItemId(), providerItemUUId);
        assertEquals(result.getProviderRefundAmountExpected().getAmount(), BigDecimal.TEN);
        assertEquals(result.getProviderRefundAmountExpected().getCurrency(), "EUR");
        assertEquals(result.getRefunder(), "TEST-REFUND");
    }

}
