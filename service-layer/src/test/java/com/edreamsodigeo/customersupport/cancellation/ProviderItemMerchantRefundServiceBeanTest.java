package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.ProviderItemMerchantRefundMapper;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefundId;
import com.edreamsodigeo.customersupport.cancellation.model.RefundId;
import com.edreamsodigeo.customersupport.cancellation.model.RefundMethod;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemMerchantRefundRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemMerchantRefundRepository;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;
import java.math.BigDecimal;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

public class ProviderItemMerchantRefundServiceBeanTest {

    private ProviderItemMerchantRefundServiceBean serviceBean;

    @Mock
    private JpaProviderItemMerchantRefundRepository providerItemMerchantRefundRepository;

    @Mock
    private ProviderItemMerchantRefundMapper providerItemMerchantRefundMapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(ProviderItemMerchantRefundMapper.class).toInstance(providerItemMerchantRefundMapper);
            t.bind(ProviderItemMerchantRefundRepository.class).toInstance(providerItemMerchantRefundRepository);
        });
        serviceBean = new ProviderItemMerchantRefundServiceBean();
    }

    @Test
    public void testGetProviderItemMerchantRefundOK() throws RegisterNotFound {
        ProviderItemMerchantRefundId providerItemMerchantRefundId = new ProviderItemMerchantRefundId(UUIDGenerator.getInstance().generateUUID());
        when(providerItemMerchantRefundRepository.findById(providerItemMerchantRefundId)).thenReturn(createProviderItemMerchantRefund());

        serviceBean.postConstruct();
        serviceBean.getProviderItemMerchant(providerItemMerchantRefundId.getId());

        verify(providerItemMerchantRefundRepository).findById(providerItemMerchantRefundId);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetProviderItemMerchantRefundRegisterNotFound() throws RegisterNotFound {
        when(providerItemMerchantRefundRepository.findById(any())).thenReturn(null);

        serviceBean.postConstruct();
        serviceBean.getProviderItemMerchant(UUIDGenerator.getInstance().generateUUID());
    }

    @Test
    public void testAddProviderItemMerchantRefund() {
        when(providerItemMerchantRefundMapper.mapDtoToProviderItemMerchant(any())).thenReturn(new ProviderItemMerchantRefund());

        serviceBean.postConstruct();
        serviceBean.addProviderItemMerchant(new ProviderItemMerchantDTO());

        verify(providerItemMerchantRefundRepository).insert(any());
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testAddProviderItemMerchantRefundWithError() {
        when(providerItemMerchantRefundMapper.mapDtoToProviderItemMerchant(any())).thenReturn(new ProviderItemMerchantRefund());
        doThrow(new NoResultException()).when(providerItemMerchantRefundRepository).insert(any());

        serviceBean.postConstruct();
        serviceBean.addProviderItemMerchant(new ProviderItemMerchantDTO());

        verify(providerItemMerchantRefundRepository).insert(any());
    }

    @Test
    public void testUpdateProviderItemMerchantRefundOK() throws RegisterNotFound {
        ProviderItemMerchantDTO providerItemMerchantDTO = new ProviderItemMerchantDTO();
        ProviderItemMerchantRefund providerItemMerchantRefundRequest = createProviderItemMerchantRefund();
        ProviderItemMerchantRefund providerItemMerchantRefundInStore = createProviderItemMerchantRefund();

        providerItemMerchantRefundInStore.setId(new ProviderItemMerchantRefundId(providerItemMerchantDTO.getId()));
        providerItemMerchantRefundRequest.setId(providerItemMerchantRefundInStore.getId());

        when(providerItemMerchantRefundRepository.findById(any())).thenReturn(providerItemMerchantRefundInStore);
        when(providerItemMerchantRefundMapper.mapDtoToProviderItemMerchant(any())).thenReturn(providerItemMerchantRefundRequest);

        serviceBean.postConstruct();
        serviceBean.updateProviderItemMerchant(providerItemMerchantDTO);

        ArgumentCaptor<ProviderItemMerchantRefund> argumentProviderItemMerchantRefund = ArgumentCaptor.forClass(ProviderItemMerchantRefund.class);
        verify(providerItemMerchantRefundRepository).update(argumentProviderItemMerchantRefund.capture());
        assertEquals(providerItemMerchantRefundRequest.getProviderItemId().getId(), argumentProviderItemMerchantRefund.getValue().getProviderItemId().getId());
        assertEquals(providerItemMerchantRefundRequest.getRefundId().getId(), argumentProviderItemMerchantRefund.getValue().getRefundId().getId());
        assertEquals(providerItemMerchantRefundRequest.getCustomerRefundMethod(), argumentProviderItemMerchantRefund.getValue().getCustomerRefundMethod());
        assertEquals(providerItemMerchantRefundRequest.getCustomerRefundCurrency(), argumentProviderItemMerchantRefund.getValue().getCustomerRefundCurrency());
        assertEquals(providerItemMerchantRefundRequest.getCustomerRefundAmount(), argumentProviderItemMerchantRefund.getValue().getCustomerRefundAmount());
        assertEquals(providerItemMerchantRefundRequest.getExpectedProviderRefundAmount(), argumentProviderItemMerchantRefund.getValue().getExpectedProviderRefundAmount());
        assertEquals(providerItemMerchantRefundRequest.getExpectedProviderRefundCurrency(), argumentProviderItemMerchantRefund.getValue().getExpectedProviderRefundCurrency());
    }

    private ProviderItemMerchantRefund createProviderItemMerchantRefund() {
        ProviderItemMerchantRefund providerItemMerchantRefundInStore = new ProviderItemMerchantRefund();
        providerItemMerchantRefundInStore.setId(new ProviderItemMerchantRefundId(UUIDGenerator.getInstance().generateUUID()));
        providerItemMerchantRefundInStore.setProviderItemId(new ProviderItemId(UUIDGenerator.getInstance().generateUUID()));
        providerItemMerchantRefundInStore.setRefundId(new RefundId(UUIDGenerator.getInstance().generateUUID()));
        providerItemMerchantRefundInStore.setExpectedProviderRefundAmount(new BigDecimal(Math.random()));
        providerItemMerchantRefundInStore.setExpectedProviderRefundCurrency("EUR");
        providerItemMerchantRefundInStore.setCustomerRefundAmount(new BigDecimal(Math.random()));
        providerItemMerchantRefundInStore.setCustomerRefundCurrency("EUR");
        providerItemMerchantRefundInStore.setCustomerRefundMethod(RefundMethod.CASH);

        return providerItemMerchantRefundInStore;
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testUpdateProviderItemMerchantRefundRegisterNotFound() throws RegisterNotFound {
        when(providerItemMerchantRefundRepository.findById(any())).thenReturn(null);

        serviceBean.postConstruct();
        serviceBean.updateProviderItemMerchant(new ProviderItemMerchantDTO());

        verify(providerItemMerchantRefundRepository).update(any());
    }

    @Test
    public void testGetProviderItemMerchantRefundByProviderItem() throws RegisterNotFound {
        ProviderItemMerchantDTO dto = new ProviderItemMerchantDTO();
        ProviderItemMerchantRefund entityItem = new ProviderItemMerchantRefund();
        when(providerItemMerchantRefundRepository.findByProviderItemId(any())).thenReturn(entityItem);
        when(providerItemMerchantRefundMapper.mapProviderItemMerchantToDto(entityItem)).thenReturn(dto);
        ProviderItemMerchantDTO result = serviceBean.getProviderItemMerchantByProviderItem(UUIDGenerator.getInstance().generateUUID());
        assertSame(result, dto);
        verify(providerItemMerchantRefundRepository).findByProviderItemId(any());
        verify(providerItemMerchantRefundMapper).mapProviderItemMerchantToDto(entityItem);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetProviderItemMerchantRefundByProviderItemNullCollection() throws RegisterNotFound {
        when(providerItemMerchantRefundRepository.findByProviderItemId(any())).thenReturn(null);
        try {
            serviceBean.getProviderItemMerchantByProviderItem(UUIDGenerator.getInstance().generateUUID());
        } catch (RegisterNotFound e) {
            verify(providerItemMerchantRefundRepository).findByProviderItemId(any());
            throw e;
        }
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetProviderItemMerchantRefundByProviderItemEmptyCollection() throws RegisterNotFound {
        when(providerItemMerchantRefundRepository.findByProviderItemId(any())).thenReturn(null);
        try {
            serviceBean.getProviderItemMerchantByProviderItem(UUIDGenerator.getInstance().generateUUID());
        } catch (RegisterNotFound e) {
            verify(providerItemMerchantRefundRepository).findByProviderItemId(any());
            throw e;
        }
    }
}
