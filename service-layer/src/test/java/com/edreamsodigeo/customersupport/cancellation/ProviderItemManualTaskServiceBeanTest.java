package com.edreamsodigeo.customersupport.cancellation;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.mapper.ProviderItemManualTaskMapper;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.repository.ProviderItemManualTaskRepository;
import com.edreamsodigeo.customersupport.cancellation.repository.impl.JpaProviderItemManualTaskRepository;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertSame;

public class ProviderItemManualTaskServiceBeanTest {

    private ProviderItemManualTaskServiceBean bean;

    @Mock
    private ProviderItemManualTaskMapper mapper;

    @Mock
    private ProviderItemManualTaskRepository repository;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(ProviderItemManualTaskMapper.class).toInstance(mapper);
            t.bind(ProviderItemManualTaskRepository.class).toInstance(repository);
        });
        bean = new ProviderItemManualTaskServiceBean();
    }

    @Test
    public void testGetProviderItemManual() throws RegisterNotFound {
        ProviderItemManualTask itemManual = new ProviderItemManualTask();
        when(repository.findById(any())).thenReturn(itemManual);
        ProviderItemManualTaskDTO dto = new ProviderItemManualTaskDTO();
        when(mapper.mapProviderItemManualTaskToDto(itemManual)).thenReturn(dto);
        ProviderItemManualTaskDTO result = bean.getProviderItemManualTask(null);
        assertSame(result, dto);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetProviderItemManualNotFound() throws RegisterNotFound {
        when(repository.findById(any())).thenReturn(null);
        bean.getProviderItemManualTask(null);
    }

    @Test
    public void testAddProviderItemManualTask() {
        ProviderItemManualTaskDTO dto = new ProviderItemManualTaskDTO();
        ProviderItemManualTaskDTO resultDto = new ProviderItemManualTaskDTO();
        ProviderItemManualTask itemManual = new ProviderItemManualTask();
        when(mapper.mapDtoToProviderItemManualTask(dto)).thenReturn(itemManual);
        when(mapper.mapProviderItemManualTaskToDto(itemManual)).thenReturn(resultDto);
        ProviderItemManualTaskDTO result = bean.addProviderItemManualTask(dto);
        assertSame(result, resultDto);
        verify(mapper).mapDtoToProviderItemManualTask(dto);
        verify(mapper).mapProviderItemManualTaskToDto(itemManual);
        verify(repository).save(itemManual);
    }

    @Test
    public void testUpdateProviderItemManualTask() throws RegisterNotFound {
        ProviderItemManualTaskDTO dto = new ProviderItemManualTaskDTO();

        UUID uuid = UUID.randomUUID();
        Long detachedTodId = 2L;
        ProviderItem detachedProviderItem = new ProviderItem();
        UUID detachedProviderItemIdUuid = UUID.randomUUID();
        ProviderItemId detachedProviderItemId = new ProviderItemId(detachedProviderItemIdUuid);
        detachedProviderItem.setId(detachedProviderItemId);
        ProviderItemManualTask detachedItemManual = new ProviderItemManualTask();
        detachedItemManual.setTaskOnDemandId(detachedTodId);
        detachedItemManual.setProviderItem(detachedProviderItem);

        ProviderItem managedProviderItem = new ProviderItem();
        managedProviderItem.setId(new ProviderItemId());
        Long managedTodId = 1L;
        ProviderItemManualTask managedItemManual = new ProviderItemManualTask();
        managedItemManual.setProviderItem(managedProviderItem);
        managedItemManual.setTaskOnDemandId(managedTodId);

        when(mapper.mapDtoToProviderItemManualTask(dto)).thenReturn(detachedItemManual);
        when(repository.findById(any())).thenReturn(managedItemManual);

        bean.updateProviderItemManualTask(uuid, dto);

        verify(repository).save(managedItemManual);
        verify(mapper).mapDtoToProviderItemManualTask(dto);
        verify(repository).findById(any());

        assertSame(managedItemManual.getTaskOnDemandId(), detachedTodId);
        assertSame(managedItemManual.getProviderItem().getId().getId(), detachedProviderItemIdUuid);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testUpdateProviderItemManualTaskNotFound() throws RegisterNotFound {
        when(repository.findById(any())).thenReturn(null);
        bean.updateProviderItemManualTask(null, null);
    }

    @Test
    public void testPostConstruct() {
        JpaProviderItemManualTaskRepository jpaRespository = Mockito.mock(JpaProviderItemManualTaskRepository.class);
        ConfigurationEngine.init(t -> {
            t.bind(ProviderItemManualTaskMapper.class).toInstance(mapper);
            t.bind(ProviderItemManualTaskRepository.class).toInstance(jpaRespository);
        });
        new ProviderItemManualTaskServiceBean().postConstruct();
        verify(jpaRespository).setEntityManager(any());
    }

}
