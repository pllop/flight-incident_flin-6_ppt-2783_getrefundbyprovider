package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.MoneyDTO;
import com.edreamsodigeo.customersupport.cancellation.ProviderItemMerchantDTO;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefund;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemMerchantRefundId;
import com.edreamsodigeo.customersupport.cancellation.model.RefundId;
import com.edreamsodigeo.customersupport.cancellation.model.RefundMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class ProviderItemMerchantRefundMapperTest {

    private ProviderItemMerchantRefundMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new ProviderItemMerchantRefundMapper();
    }

    @Test
    public void testMapDtoToProviderItemMerchantRefund() {
        UUID id = UUID.randomUUID();
        UUID providerItemId = UUID.randomUUID();
        UUID refundId = UUID.randomUUID();
        ProviderItemMerchantDTO dto = new ProviderItemMerchantDTO();
        dto.setId(id);
        dto.setProviderItemId(providerItemId);
        dto.setCustomerRefundAmount(new MoneyDTO(BigDecimal.ONE, "EUR"));
        dto.setCustomerRefundMethod(RefundMethod.CASH);
        dto.setProviderRefundAmountExpected(new MoneyDTO(BigDecimal.TEN, "EUR"));
        dto.setRefundId(refundId);
        ProviderItemMerchantRefund result = mapper.mapDtoToProviderItemMerchant(dto);
        assertEquals(result.getId().getId(), id);
        assertEquals(result.getProviderItemId().getId(), providerItemId);
        assertEquals(result.getRefundId().getId(), refundId);
        assertEquals(result.getCustomerRefundAmount(), BigDecimal.ONE);
        assertEquals(result.getCustomerRefundAmount(), BigDecimal.ONE);
        assertEquals(result.getCustomerRefundCurrency(), "EUR");
        assertEquals(result.getExpectedProviderRefundAmount(), BigDecimal.TEN);
        assertEquals(result.getCustomerRefundMethod(), RefundMethod.CASH);
    }

    @Test
    public void testMapProviderItemMerchantRefundToDto() {
        UUID uuid = UUID.randomUUID();
        ProviderItemMerchantRefund providerItemMerchantRefund = new ProviderItemMerchantRefund();
        ProviderItemMerchantRefundId providerItemMerchantRefundId = new ProviderItemMerchantRefundId();
        providerItemMerchantRefundId.setId(uuid);
        providerItemMerchantRefund.setId(providerItemMerchantRefundId);

        UUID providerItemUUId = UUID.randomUUID();
        ProviderItemId providerItemId = new ProviderItemId();
        providerItemId.setId(providerItemUUId);
        providerItemMerchantRefund.setProviderItemId(providerItemId);

        UUID refundUUID = UUID.randomUUID();
        RefundId refundId = new RefundId();
        refundId.setId(refundUUID);
        providerItemMerchantRefund.setRefundId(refundId);

        providerItemMerchantRefund.setCustomerRefundMethod(RefundMethod.CASH);
        providerItemMerchantRefund.setCustomerRefundCurrency("EUR");
        providerItemMerchantRefund.setCustomerRefundAmount(BigDecimal.ONE);
        providerItemMerchantRefund.setExpectedProviderRefundAmount(BigDecimal.TEN);
        providerItemMerchantRefund.setExpectedProviderRefundCurrency("EUR");


        ProviderItemMerchantDTO result = mapper.mapProviderItemMerchantToDto(providerItemMerchantRefund);
        assertEquals(result.getId(), uuid);
        assertEquals(result.getProviderItemId(), providerItemUUId);
        assertEquals(result.getRefundId(), refundUUID);
        assertEquals(result.getCustomerRefundAmount().getCurrency(), "EUR");
        assertEquals(result.getCustomerRefundAmount().getAmount(), BigDecimal.ONE);
        assertEquals(result.getCustomerRefundMethod(), RefundMethod.CASH);
        assertEquals(result.getProviderRefundAmountExpected().getAmount(), BigDecimal.TEN);
        assertEquals(result.getProviderRefundAmountExpected().getCurrency(), "EUR");
    }

}
