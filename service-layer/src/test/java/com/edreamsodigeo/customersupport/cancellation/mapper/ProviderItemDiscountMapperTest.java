package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemDiscountDTO;
import com.edreamsodigeo.customersupport.cancellation.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscount;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemDiscountId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class ProviderItemDiscountMapperTest {

    private ProviderItemDiscountMapper providerItemDiscountMapper;

    @BeforeMethod
    public void setUp() {
        providerItemDiscountMapper = new ProviderItemDiscountMapper();
    }

    @Test
    public void testMapToProviderItemDiscountDTO() {
        UUID uuid = new UUID(1L, 2L);
        ProviderItemDiscountId id = new ProviderItemDiscountId();
        ProviderItemId providerItemId = new ProviderItemId();
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        id.setId(uuid);
        providerItemId.setId(uuid);
        flexibleTicketId.setId(uuid);
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        providerItemDiscount.setId(id);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType("DISCOUNT_TYPE");
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setVoucherId(1L);
        providerItemDiscount.setEmdId(1L);

        ProviderItemDiscountDTO providerItemDiscountDTO = providerItemDiscountMapper.map(providerItemDiscount);

        assertEquals(providerItemDiscountDTO.getId(), uuid);
        assertEquals(providerItemDiscountDTO.getProviderItemId(), uuid);
        assertEquals(providerItemDiscountDTO.getDiscountType(), "DISCOUNT_TYPE");
        assertEquals(providerItemDiscountDTO.getFlexibleTicketId(), uuid);
        assertEquals(providerItemDiscountDTO.getVoucherId(), Long.valueOf(1L));
        assertEquals(providerItemDiscountDTO.getEmdId(), Long.valueOf(1L));
    }

    @Test
    public void testMapToProviderItemDiscountDTONullValues() {
        UUID uuid = new UUID(1L, 2L);
        ProviderItemDiscountId id = new ProviderItemDiscountId();
        ProviderItemId providerItemId = new ProviderItemId();
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId();
        id.setId(uuid);
        providerItemId.setId(uuid);
        ProviderItemDiscount providerItemDiscount = new ProviderItemDiscount();
        providerItemDiscount.setId(id);
        providerItemDiscount.setProviderItemId(providerItemId);
        providerItemDiscount.setDiscountType("DISCOUNT_TYPE");
        providerItemDiscount.setFlexibleTicketId(flexibleTicketId);
        providerItemDiscount.setVoucherId(1L);
        providerItemDiscount.setEmdId(1L);

        ProviderItemDiscountDTO providerItemDiscountDTO = providerItemDiscountMapper.map(providerItemDiscount);

        assertEquals(providerItemDiscountDTO.getId(), uuid);
        assertEquals(providerItemDiscountDTO.getProviderItemId(), uuid);
        assertEquals(providerItemDiscountDTO.getDiscountType(), "DISCOUNT_TYPE");
        assertNull(providerItemDiscountDTO.getFlexibleTicketId());
        assertEquals(providerItemDiscountDTO.getVoucherId(), Long.valueOf(1L));
        assertEquals(providerItemDiscountDTO.getEmdId(), Long.valueOf(1L));
    }

    @Test
    public void testMapToProviderItemDiscount() {
        UUID uuid = new UUID(1L, 2L);
        ProviderItemDiscountDTO providerItemDiscountDTO = new ProviderItemDiscountDTO();
        providerItemDiscountDTO.setId(uuid);
        providerItemDiscountDTO.setProviderItemId(uuid);
        providerItemDiscountDTO.setDiscountType("DISCOUNT_TYPE");
        providerItemDiscountDTO.setFlexibleTicketId(uuid);
        providerItemDiscountDTO.setVoucherId(1L);
        providerItemDiscountDTO.setEmdId(1L);

        ProviderItemDiscount providerItemDiscount = providerItemDiscountMapper.map(providerItemDiscountDTO);

        assertEquals(providerItemDiscount.getId().getId(), uuid);
        assertEquals(providerItemDiscount.getProviderItemId().getId(), uuid);
        assertEquals(providerItemDiscount.getDiscountType(), "DISCOUNT_TYPE");
        assertEquals(providerItemDiscount.getFlexibleTicketId().getId(), uuid);
        assertEquals(providerItemDiscount.getVoucherId(), Long.valueOf(1L));
        assertEquals(providerItemDiscount.getEmdId(), Long.valueOf(1L));
    }
}
