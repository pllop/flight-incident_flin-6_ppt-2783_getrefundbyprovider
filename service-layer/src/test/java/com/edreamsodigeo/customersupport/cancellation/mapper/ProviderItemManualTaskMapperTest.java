package com.edreamsodigeo.customersupport.cancellation.mapper;

import com.edreamsodigeo.customersupport.cancellation.ProviderItemManualTaskDTO;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItem;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemId;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTask;
import com.edreamsodigeo.customersupport.cancellation.model.ProviderItemManualTaskId;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class ProviderItemManualTaskMapperTest {

    private ProviderItemManualTaskMapper mapper;

    @BeforeMethod
    public void setUp() {
        mapper = new ProviderItemManualTaskMapper();
    }

    @Test
    public void testMapDtoToProviderItemManual() {
        UUID id = UUID.randomUUID();
        UUID providerItemId = UUID.randomUUID();
        LocalDateTime timestamp = LocalDateTime.now();
        Long todId = 1L;
        ProviderItemManualTaskDTO dto = new ProviderItemManualTaskDTO();
        dto.setId(id);
        dto.setProviderItemId(providerItemId);
        dto.setTaskOnDemandId(todId);
        dto.setTimestamp(timestamp);
        ProviderItemManualTask result = mapper.mapDtoToProviderItemManualTask(dto);
        assertEquals(result.getTaskOnDemandId(), todId);
        assertEquals(result.getProviderItem().getId().getId(), providerItemId);
        assertEquals(result.getId().getId(), id);
        assertEquals(result.getTimestamp(), timestamp);
    }

    @Test
    public void testMapProviderItemManualToDto() {
        Long todId = 1L;
        LocalDateTime timestamp = LocalDateTime.now();
        UUID uuid = UUID.randomUUID();
        ProviderItem providerItem = new ProviderItem();
        ProviderItemId providerItemId = new ProviderItemId();
        UUID providerItemUuid = UUID.randomUUID();
        providerItemId.setId(providerItemUuid);
        providerItem.setId(providerItemId);
        ProviderItemManualTask itemManual = new ProviderItemManualTask();
        ProviderItemManualTaskId id = new ProviderItemManualTaskId();
        id.setId(uuid);
        itemManual.setTaskOnDemandId(todId);
        itemManual.setId(id);
        itemManual.setProviderItem(providerItem);
        itemManual.setTimestamp(timestamp);
        ProviderItemManualTaskDTO result = mapper.mapProviderItemManualTaskToDto(itemManual);
        assertEquals(result.getTaskOnDemandId(), todId);
        assertEquals(result.getId(), uuid);
        assertEquals(result.getProviderItemId(), providerItemUuid);
        assertEquals(result.getTimestamp(), timestamp);
    }


}
