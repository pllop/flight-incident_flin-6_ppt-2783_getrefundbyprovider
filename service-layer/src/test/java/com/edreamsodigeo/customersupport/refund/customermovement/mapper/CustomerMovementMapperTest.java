package com.edreamsodigeo.customersupport.refund.customermovement.mapper;

import com.edreamsodigeo.customersupport.refund.customermovement.CustomerMovementDTO;
import com.edreamsodigeo.customersupport.refund.customermovement.MovementMoneyDTO;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovement;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovementId;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovementWavePspId;
import com.edreamsodigeo.customersupport.refund.customermovement.model.RefundId;
import java.math.BigDecimal;
import java.util.UUID;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustomerMovementMapperTest {

    public static final MovementMoneyDTO AMOUNT = new MovementMoneyDTO(BigDecimal.valueOf(7), "GBP");
    public static final MovementMoneyDTO AMOUNT_EUR = new MovementMoneyDTO(BigDecimal.valueOf(8), "EUR");
    public static final String ID = "b31f8180-fa5b-494f-8f03-4a996e2eaab4";
    public static final String PSP = "WORLDPAY";
    public static final String PSP_ACCOUNT = "WPEUROLTD";
    public static final String STATUS = "OK";
    private CustomerMovementMapper customerMovementMapper;

    @BeforeMethod
    public void setUp() {
        customerMovementMapper = new CustomerMovementMapper();
    }

    @Test
    public void testMapCustomerMovementDto() {
        CustomerMovementDTO customerMovementDTO = buildCustomerMovementDto();
        CustomerMovement customerMovement = customerMovementMapper.mapCustomerMovementDTOtoCustomerMovement(customerMovementDTO);
        assertNotNull(customerMovement);
        assertEquals(customerMovementDTO.getId(), customerMovement.getId().getId());
        assertEquals(customerMovementDTO.getRefundId(), customerMovement.getRefundId().getId());
        assertEquals(customerMovementDTO.getAmount().getAmount(), customerMovement.getAmount());
        assertEquals(customerMovementDTO.getAmount().getCurrency(), customerMovement.getCurrency());
        assertEquals(customerMovementDTO.getCurrencyCode(), customerMovement.getCurrency());
        assertEquals(customerMovementDTO.getCurrencyRateProviderCustomer(), customerMovement.getCurrencyRateProviderCustomer());
        assertEquals(customerMovementDTO.getAmountInEur().getAmount(), customerMovement.getAmountInEur());
        assertEquals(customerMovementDTO.getCurrencyRateProviderEur(), customerMovement.getCurrencyRateProviderEur());
        assertEquals(customerMovementDTO.getPsp(), customerMovement.getPsp());
        assertEquals(customerMovementDTO.getPspAccount(), customerMovement.getPspAccount());
        assertEquals(customerMovementDTO.getStatus(), customerMovement.getStatus());
        assertEquals(customerMovementDTO.getCustomerMovementWavePspId(), customerMovement.getCustomerMovementWavePspId().getId());
    }

    @Test
    public void testMapCustomerMovementToCustomerMovementDTO() {
        CustomerMovement customerMovement = buildCustomerMovement();
        CustomerMovementDTO customerMovementDTO = customerMovementMapper.mapCustomerMovementToCustomerMovementDTO(customerMovement);
        assertNotNull(customerMovementDTO);
        assertEquals(customerMovement.getId().getId(), customerMovementDTO.getId());
        assertEquals(customerMovement.getRefundId().getId(), customerMovementDTO.getRefundId());
        assertEquals(customerMovement.getAmount(), customerMovementDTO.getAmount().getAmount());
        assertEquals(customerMovement.getCurrency(), customerMovementDTO.getAmount().getCurrency());
        assertEquals(customerMovement.getCurrencyRateProviderCustomer(), customerMovementDTO.getCurrencyRateProviderCustomer());
        assertEquals(customerMovement.getAmountInEur(), customerMovementDTO.getAmountInEur().getAmount());
        assertEquals(customerMovement.getAmountInEur(), customerMovementDTO.getAmountInEur().getAmount());
        assertEquals(customerMovement.getCurrencyRateProviderEur(), customerMovementDTO.getCurrencyRateProviderEur());
        assertEquals(customerMovement.getPsp(), customerMovementDTO.getPsp());
        assertEquals(customerMovement.getPspAccount(), customerMovementDTO.getPspAccount());
        assertEquals(customerMovement.getStatus(), customerMovementDTO.getStatus());
        assertEquals(customerMovement.getCustomerMovementWavePspId().getId(), customerMovementDTO.getCustomerMovementWavePspId());

    }

    private CustomerMovementDTO buildCustomerMovementDto() {
        CustomerMovementDTO customerMovementDTO = new CustomerMovementDTO();
        customerMovementDTO.setId(UUID.fromString(ID));
        customerMovementDTO.setRefundId(UUID.fromString(ID));
        customerMovementDTO.setAmount(AMOUNT);
        customerMovementDTO.setCurrencyCode(AMOUNT.getCurrency());
        customerMovementDTO.setCurrencyRateProviderCustomer(BigDecimal.TEN);
        customerMovementDTO.setAmountInEur(AMOUNT_EUR);
        customerMovementDTO.setCurrencyRateProviderEur(BigDecimal.ONE);
        customerMovementDTO.setPsp(PSP);
        customerMovementDTO.setPspAccount(PSP_ACCOUNT);
        customerMovementDTO.setStatus(STATUS);
        customerMovementDTO.setCustomerMovementWavePspId(UUID.fromString(ID));
        return customerMovementDTO;
    }

    private CustomerMovement buildCustomerMovement() {
        CustomerMovement customerMovement = new CustomerMovement();
        customerMovement.setId(new CustomerMovementId(UUID.fromString(ID)));
        customerMovement.setRefundId(new RefundId(UUID.fromString(ID)));
        customerMovement.setAmount(AMOUNT.getAmount());
        customerMovement.setCurrency(AMOUNT.getCurrency());
        customerMovement.setCurrencyRateProviderCustomer(BigDecimal.TEN);
        customerMovement.setAmountInEur(AMOUNT_EUR.getAmount());
        customerMovement.setCurrencyRateProviderEur(BigDecimal.ONE);
        customerMovement.setPsp(PSP);
        customerMovement.setPspAccount(PSP_ACCOUNT);
        customerMovement.setStatus(STATUS);
        customerMovement.setCustomerMovementWavePspId(new CustomerMovementWavePspId(UUID.fromString(ID)));
        return customerMovement;
    }

}
