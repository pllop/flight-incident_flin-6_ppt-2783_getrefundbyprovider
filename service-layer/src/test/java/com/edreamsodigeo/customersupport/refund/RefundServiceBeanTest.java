package com.edreamsodigeo.customersupport.refund;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.refund.mapper.RefundMapper;
import com.edreamsodigeo.customersupport.refund.model.Refund;
import com.edreamsodigeo.customersupport.refund.model.RefundId;
import com.edreamsodigeo.customersupport.refund.repository.RefundRepository;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class RefundServiceBeanTest {

    @Mock
    private RefundMapper refundMapper;

    @Mock
    private RefundRepository refundRepository;

    RefundServiceBean refundServiceBean;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(RefundMapper.class).toInstance(refundMapper);
            t.bind(RefundRepository.class).toInstance(refundRepository);
        });
        refundServiceBean = new RefundServiceBean();
    }

    @Test
    public void testGetRefundById() {
        UUID id = UUID.fromString("abdb61be-dac9-486f-99ed-e2b9d376fc4d");
        Refund refund = new Refund();
        RefundDTO refundDTO = new RefundDTO();

        when(refundRepository.findById(new RefundId(id))).thenReturn(refund);
        when(refundMapper.mapRefundToRefundDTO(refund)).thenReturn(refundDTO);

        RefundServiceBean refundServiceBean = new RefundServiceBean();
        RefundDTO response = refundServiceBean.getRefundById(id);

        verify(refundRepository).findById(new RefundId(id));
        verify(refundMapper).mapRefundToRefundDTO(refund);
        assertEquals(refundDTO, response);
    }

    @Test
    public void testAddRefund() {
        Refund refund = new Refund();
        RefundDTO refundDTO = new RefundDTO();

        when(refundMapper.mapRefundDTOToRefund(refundDTO)).thenReturn(refund);

        RefundServiceBean refundServiceBean = new RefundServiceBean();
        refundServiceBean.addRefund(refundDTO);

        verify(refundMapper).mapRefundDTOToRefund(refundDTO);
        verify(refundRepository).insert(refund);
    }

    @Test
    public void testUpdateRefund() {
        Refund refund = new Refund();
        RefundDTO refundDTO = new RefundDTO();

        when(refundMapper.mapRefundDTOToRefund(refundDTO)).thenReturn(refund);

        RefundServiceBean refundServiceBean = new RefundServiceBean();
        refundServiceBean.updateRefund(refundDTO);

        verify(refundMapper).mapRefundDTOToRefund(refundDTO);
        verify(refundRepository).update(refund);
    }
}
