package com.edreamsodigeo.customersupport.refund.customermovement;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.refund.customermovement.mapper.CustomerMovementMapper;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovement;
import com.edreamsodigeo.customersupport.refund.customermovement.model.CustomerMovementId;
import com.edreamsodigeo.customersupport.refund.customermovement.repository.CustomerMovementRepository;
import com.edreamsodigeo.customersupport.refund.customermovement.repository.impl.JpaCustomerMovementRepository;
import java.util.UUID;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CustomerMovementServiceBeanTest {
    public static final String ID = "b31f8180-fa5b-494f-8f03-4a996e2eaab4";
    @Mock
    CustomerMovementMapper customerMovementMapper;
    private CustomerMovementServiceBean serviceBean;
    @Mock
    private JpaCustomerMovementRepository movementRepository;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(CustomerMovementMapper.class).toInstance(customerMovementMapper);
            t.bind(CustomerMovementRepository.class).toInstance(movementRepository);
        });
        serviceBean = new CustomerMovementServiceBean();
    }

    @Test
    public void testCreateCustomerMovement() throws RegisterNotFound {
        CustomerMovementDTO dto = new CustomerMovementDTO();
        dto.setId(UUID.fromString(ID));
        CustomerMovement movement = new CustomerMovement();
        movement.setId(new CustomerMovementId(UUID.fromString(ID)));
        when(customerMovementMapper.mapCustomerMovementDTOtoCustomerMovement(dto)).thenReturn(movement);
        when(customerMovementMapper.mapCustomerMovementToCustomerMovementDTO(movement)).thenReturn(dto);
        serviceBean.postConstruct();
        UUID uuid = serviceBean.createCustomerMovement(dto);
        assertEquals(uuid, dto.getId());
        verify(customerMovementMapper).mapCustomerMovementDTOtoCustomerMovement(dto);
        verify(customerMovementMapper).mapCustomerMovementToCustomerMovementDTO(movement);
        verify(movementRepository).insert(movement);
    }
}
