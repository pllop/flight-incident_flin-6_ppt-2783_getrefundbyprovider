package com.edreamsodigeo.customersupport.refund.mapper;

import com.edreamsodigeo.customersupport.refund.ProviderRefundDTO;
import com.edreamsodigeo.customersupport.refund.RefundDTO;
import com.edreamsodigeo.customersupport.refund.model.ProviderRefund;
import com.edreamsodigeo.customersupport.refund.model.ProviderRefundId;
import com.edreamsodigeo.customersupport.refund.model.Refund;
import com.edreamsodigeo.customersupport.refund.model.RefundId;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class RefundMapperTest {

    @Test
    public void testMapRefundDTOToRefund() {
        RefundDTO refundDTO = createRefundDTO();

        RefundMapper refundMapper = new RefundMapper();
        Refund response = refundMapper.mapRefundDTOToRefund(refundDTO);

        assertEquals(refundDTO.getId().toString(), response.getRefundId().getId().toString());
        assertEquals(refundDTO.getProviderRefunds().size(), response.getProviderRefunds().size());
        assertEquals(refundDTO.getProviderRefunds().get(0).getId().toString(), response.getProviderRefunds().get(0).getId().getId().toString());
        assertEquals(refundDTO.getProviderRefunds().get(0).getExpectedProviderRefundCurrency(), response.getProviderRefunds().get(0).getExpectedProvRefundCurrency());
        assertEquals(refundDTO.getProviderRefunds().get(0).getExpectedProviderRefundAmount(), response.getProviderRefunds().get(0).getExpectedProvRefundAmount());
        assertEquals(refundDTO.getProviderRefunds().get(0).getProviderBookingItemId(), response.getProviderRefunds().get(0).getProvBookingItemId());
        assertEquals(refundDTO.getCustomerRefundAmount(), response.getCustomerRefundAmount());
        assertEquals(refundDTO.getBookingItemId(), response.getBookingItemId());
        assertEquals(refundDTO.getCustomerRefundCurrency(), response.getCustomerRefundCurrency());
        assertEquals(refundDTO.getCustomerRefundMethod(), response.getCustomerRefundMethod());
        assertEquals(refundDTO.getStatus(), response.getStatus());
    }

    @Test
    public void testMapRefundDTOToRefundNull() {
        RefundDTO refundDTO = null;

        RefundMapper refundMapper = new RefundMapper();
        Refund response = refundMapper.mapRefundDTOToRefund(refundDTO);

        assertNull(response.getRefundId());
        assertNull(response.getBookingItemId());
        assertNull(response.getCustomerRefundAmount());
        assertNull(response.getCustomerRefundCurrency());
        assertNull(response.getCustomerRefundMethod());
        assertNull(response.getProviderRefunds());
        assertNull(response.getStatus());
    }

    @Test
    public void testMapRefundToRefundDTO() {
        Refund refund = createRefund();

        RefundMapper refundMapper = new RefundMapper();
        RefundDTO response = refundMapper.mapRefundToRefundDTO(refund);

        assertEquals(refund.getRefundId().getId().toString(), response.getId().toString());
        assertEquals(refund.getProviderRefunds().size(), response.getProviderRefunds().size());
        assertEquals(refund.getProviderRefunds().get(0).getId().getId().toString(), response.getProviderRefunds().get(0).getId().toString());
        assertEquals(refund.getProviderRefunds().get(0).getExpectedProvRefundCurrency(), response.getProviderRefunds().get(0).getExpectedProviderRefundCurrency());
        assertEquals(refund.getProviderRefunds().get(0).getExpectedProvRefundAmount(), response.getProviderRefunds().get(0).getExpectedProviderRefundAmount());
        assertEquals(refund.getProviderRefunds().get(0).getProvBookingItemId(), response.getProviderRefunds().get(0).getProviderBookingItemId());
        assertEquals(refund.getCustomerRefundAmount(), response.getCustomerRefundAmount());
        assertEquals(refund.getBookingItemId(), response.getBookingItemId());
        assertEquals(refund.getCustomerRefundCurrency(), response.getCustomerRefundCurrency());
        assertEquals(refund.getCustomerRefundMethod(), response.getCustomerRefundMethod());
        assertEquals(refund.getStatus(), response.getStatus());
    }

    @Test
    public void testMapRefundToRefundDTONull() {
        Refund refund = null;

        RefundMapper refundMapper = new RefundMapper();
        RefundDTO response = refundMapper.mapRefundToRefundDTO(refund);

        assertNull(response.getId());
        assertNull(response.getBookingItemId());
        assertNull(response.getCustomerRefundAmount());
        assertNull(response.getCustomerRefundCurrency());
        assertNull(response.getCustomerRefundMethod());
        assertNull(response.getProviderRefunds());
        assertNull(response.getStatus());
    }

    private Refund createRefund() {
        Refund refund = new Refund();

        refund.setRefundId(new RefundId(UUID.fromString("abdb61be-dac9-486f-99ed-e2b9d376fc4d")));
        refund.setBookingItemId(2L);
        refund.setCustomerRefundAmount(465.79);
        refund.setCustomerRefundCurrency("EUR");
        refund.setCustomerRefundMethod("method");
        refund.setProviderRefunds(createProviderRefunds());
        refund.setStatus("status");

        return refund;
    }

    private List<ProviderRefund> createProviderRefunds() {
        List<ProviderRefund> providerRefunds = new ArrayList<>();

        ProviderRefund providerRefund = new ProviderRefund();
        providerRefund.setId(new ProviderRefundId(UUID.fromString("abdb61be-dac9-65463-99ed-e2b9d376fc4d")));
        providerRefund.setExpectedProvRefundAmount(789.45);
        providerRefund.setExpectedProvRefundCurrency("EUR");
        providerRefund.setProvBookingItemId(129L);

        providerRefunds.add(providerRefund);

        return providerRefunds;
    }

    private RefundDTO createRefundDTO() {
        RefundDTO refundDTO = new RefundDTO();
        refundDTO.setId(UUID.fromString("abdb61be-dac9-486f-99ed-e2b9d376fc4d"));
        refundDTO.setBookingItemId(1L);
        refundDTO.setCustomerRefundAmount(123.345);
        refundDTO.setCustomerRefundCurrency("EUR");
        refundDTO.setCustomerRefundMethod("method");
        refundDTO.setProviderRefunds(createProviderRefundsDTO());
        refundDTO.setStatus("status");

        return refundDTO;
    }

    private List<ProviderRefundDTO> createProviderRefundsDTO() {
        List<ProviderRefundDTO> providerRefundDTOS = new ArrayList<>();

        ProviderRefundDTO providerRefundDTO = new ProviderRefundDTO();
        providerRefundDTO.setId(UUID.fromString("abdb61be-dac9-65463-99ed-e2b9d376fc4d"));
        providerRefundDTO.setExpectedProviderRefundAmount(123.123);
        providerRefundDTO.setExpectedProviderRefundCurrency("EUR");
        providerRefundDTO.setProviderBookingItemId(54L);

        providerRefundDTOS.add(providerRefundDTO);

        return providerRefundDTOS;
    }
}
