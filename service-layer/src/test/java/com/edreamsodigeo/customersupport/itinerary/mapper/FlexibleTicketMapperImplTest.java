package com.edreamsodigeo.customersupport.itinerary.mapper;

import com.edreamsodigeo.customersupport.itinerary.FlexibleTicketDTO;
import com.edreamsodigeo.customersupport.itinerary.MoneyDTO;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.itinerary.model.Status;
import org.mapstruct.factory.Mappers;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;

import static org.testng.Assert.assertNull;

public class FlexibleTicketMapperImplTest {

    private FlexibleTicketMapper mapper = Mappers.getMapper(FlexibleTicketMapper.class);

    @Test
    public void testMapFlexibleTicketToFlexibleTicketDTOWithNullFlexibleTicket() {
        assertNull(mapper.mapFlexibleTicketToFlexibleTicketDTO(null));
    }

    @Test
    public void testMapFlexibleTicketToFlexibleTicketDTOWithAllFields() {
        FlexibleTicket source = new FlexibleTicket();
        final UUID randomUUID = UUID.randomUUID();

        source.setId(new FlexibleTicketId(randomUUID));
        source.setProviderBookingItemId(12345L);
        source.setCustomerId(9876L);
        source.setStatus(Status.REDEEMED);
        source.setMonetaryValue(BigDecimal.valueOf(678.88));
        source.setCurrencyCode("CURR");

        final FlexibleTicketDTO actual = mapper.mapFlexibleTicketToFlexibleTicketDTO(source);

        Assert.assertEquals(actual.getId(), source.getId().getId());
        Assert.assertEquals(actual.getProviderBookingItemId(), source.getProviderBookingItemId());
        Assert.assertEquals(actual.getCustomerId(), source.getCustomerId());
        Assert.assertEquals(actual.getStatus(), source.getStatus().toString());
        Assert.assertEquals(actual.getMonetaryValue().getAmount(), source.getMonetaryValue());
        Assert.assertEquals(actual.getMonetaryValue().getCurrencyCode(), source.getCurrencyCode());
    }

    @Test
    public void testMapFlexibleTicketDTOToFlexibleTicketWithNullFlexibleTicketDTO() {
        assertNull(mapper.mapFlexibleTicketDTOToFlexibleTicket(null));
    }

    @Test
    public void testMapFlexibleTicketDTOToFlexibleTicketWithAllFields() {
        Random rand = new Random(System.currentTimeMillis());
        FlexibleTicketDTO source = FlexibleTicketDTO.builder()
                .id(UUID.randomUUID())
                .providerBookingItemId(rand.nextLong())
                .customerId(rand.nextLong())
                .status(Status.AVAILABLE.toString())
                .monetaryValue(new MoneyDTO(BigDecimal.valueOf(rand.nextLong()), "CURR"))
                .build();

        final FlexibleTicket actual = mapper.mapFlexibleTicketDTOToFlexibleTicket(source);

        Assert.assertEquals(actual.getId().getId(), source.getId());
        Assert.assertEquals(actual.getProviderBookingItemId(), source.getProviderBookingItemId());
        Assert.assertEquals(actual.getCustomerId(), source.getCustomerId());
        Assert.assertEquals(actual.getStatus().toString(), source.getStatus());
        Assert.assertEquals(actual.getMonetaryValue(), source.getMonetaryValue().getAmount());
        Assert.assertEquals(actual.getCurrencyCode(), source.getMonetaryValue().getCurrencyCode());
    }

    @Test
    public void testFlexibleTicketToMoneyDTOWithNullFlexibleTicket() {
        final FlexibleTicketMapperImpl underTest = new FlexibleTicketMapperImpl();
        final MoneyDTO actual = underTest.flexibleTicketToMoneyDTO(null);
        assertNull(actual);
    }

    @Test
    public void testMapFlexibleTicketDTOToFlexibleTicketWithMonetaryValueNull() {
        FlexibleTicketDTO source = FlexibleTicketDTO.builder()
                .id(UUID.randomUUID())
                .monetaryValue(null)
                .build();

        final FlexibleTicket actual = mapper.mapFlexibleTicketDTOToFlexibleTicket(source);

        Assert.assertEquals(actual.getId().getId(), source.getId());
        Assert.assertNull(actual.getMonetaryValue());
    }

}
