package com.edreamsodigeo.customersupport.itinerary;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.itinerary.exception.RegisterNotFoundException;
import com.edreamsodigeo.customersupport.itinerary.mapper.FlexibleTicketMapper;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicket;
import com.edreamsodigeo.customersupport.itinerary.model.FlexibleTicketId;
import com.edreamsodigeo.customersupport.itinerary.repository.FlexibleTicketRepository;
import com.edreamsodigeo.customersupport.itinerary.repository.impl.JpaFlexibleTicketRepository;
import com.odigeo.commons.uuid.UUIDGenerator;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FlexibleTicketServiceBeanTest {


    private FlexibleTicketServiceBean serviceBean;

    @Mock
    private JpaFlexibleTicketRepository repository;

    @Mock
    private FlexibleTicketMapper mapper;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(FlexibleTicketMapper.class).toInstance(mapper);
            t.bind(FlexibleTicketRepository.class).toInstance(repository);
        });
        serviceBean = new FlexibleTicketServiceBean();
        serviceBean.postConstruct();
    }

    @Test
    public void testGetFlexibleTicket() throws RegisterNotFoundException {
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId(UUIDGenerator.getInstance().generateUUID());
        when(repository.findById(flexibleTicketId)).thenReturn(createFlexibleTicket());

        serviceBean.getFlexibleTicket(flexibleTicketId.getId());
        verify(repository).findById(flexibleTicketId);
    }

    @Test(expectedExceptions = RegisterNotFoundException.class)
    public void testGetFlexibleTicketThrowsExceptionWhenResponseIsNull() throws RegisterNotFoundException {
        FlexibleTicketId flexibleTicketId = new FlexibleTicketId(UUIDGenerator.getInstance().generateUUID());
        when(repository.findById(flexibleTicketId)).thenReturn(null);

        serviceBean.getFlexibleTicket(flexibleTicketId.getId());
        verify(repository).findById(flexibleTicketId);
    }

    @Test
    public void testAddFlexibleTicket() {
        FlexibleTicket flexibleTicket = createFlexibleTicket();
        final FlexibleTicketDTO flexibleTicketDTO = Mockito.mock(FlexibleTicketDTO.class);
        when(mapper.mapFlexibleTicketDTOToFlexibleTicket(flexibleTicketDTO)).thenReturn(flexibleTicket);

        serviceBean.addFlexibleTicket(flexibleTicketDTO);
        verify(repository).insert(flexibleTicket);
    }

    @Test
    public void testUpdateFlexibleTicket() {
        FlexibleTicket flexibleTicket = createFlexibleTicket();
        final FlexibleTicketDTO flexibleTicketDTO = Mockito.mock(FlexibleTicketDTO.class);
        when(mapper.mapFlexibleTicketDTOToFlexibleTicket(flexibleTicketDTO)).thenReturn(flexibleTicket);

        flexibleTicketDTO.setCustomerId(new Random().nextLong());
        serviceBean.updateFlexibleTicket(flexibleTicketDTO);
        verify(repository).update(flexibleTicket);
    }

    private FlexibleTicket createFlexibleTicket() {
        FlexibleTicket flexibleTicket = new FlexibleTicket();
        flexibleTicket.setId(new FlexibleTicketId(UUIDGenerator.getInstance().generateUUID()));
        return flexibleTicket;
    }
}
