package com.edreamsodigeo.customersupport.flightincident;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.v1.CancellationResource;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.CancellationNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Item;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderBookingItemId;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItem;
import com.edreamsodigeo.customersupport.flightincident.email.sender.EmailSender;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.mapper.IncidentMapper;
import com.edreamsodigeo.customersupport.flightincident.model.CancellationId;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommOption;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsLog;
import com.edreamsodigeo.customersupport.flightincident.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.model.OptionValue;
import com.edreamsodigeo.customersupport.flightincident.model.Status;
import com.edreamsodigeo.customersupport.flightincident.repository.IncidentRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaIncidentRepository;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class FlightIncidentServiceBeanTest {
    @Mock
    private DataSource dataSource;
    @Mock
    private Connection connection;
    @Mock
    private JpaIncidentRepository incidentRepository;
    @Mock
    private EmailSender emailSender;

    @Mock
    private IncidentMapper incidentMapper;
    @Mock
    private ConsentCommsLog consentCommsLog;

    @Mock
    private CancellationResource cancellationService;

    private ConsentDTO consentDTO;
    private FlightIncidentServiceBean serviceBean;
    private UUID uuid;

    @BeforeMethod
    public void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(IncidentMapper.class).toInstance(incidentMapper);
            t.bind(IncidentRepository.class).toInstance(incidentRepository);
            t.bind(CancellationResource.class).toInstance(cancellationService);
            t.bind(EmailSender.class).toInstance(emailSender);
        });
        serviceBean = new FlightIncidentServiceBean();
        consentDTO = new ConsentDTO(true, true, false, true);
        uuid = new UUID(1, 1);
        when(dataSource.getConnection()).thenReturn(connection);
    }

    @Test
    public void testGetIncidentOk() throws RegisterNotFound {
        UUID uuid = new UUID(1234, 456);
        serviceBean.postConstruct();
        when(incidentRepository.incidentOfIncidentId(new IncidentId(uuid))).thenReturn(new Incident());
        serviceBean.getIncident(uuid);
        verify(incidentRepository).incidentOfIncidentId(new IncidentId(uuid));
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetIncidentKO() throws RegisterNotFound {
        UUID uuid = new UUID(1234, 456);
        serviceBean.postConstruct();
        when(incidentRepository.incidentOfIncidentId(new IncidentId(uuid))).thenReturn(null);
        serviceBean.getIncident(uuid);
        verify(incidentRepository).incidentsOfBookingId(123L);
    }

    @Test
    public void testGetIncidentsOK() throws RegisterNotFound {
        serviceBean.postConstruct();
        when(incidentRepository.incidentsOfBookingId(123L)).thenReturn(Collections.singletonList(new Incident()));
        serviceBean.getIncidents(123L);
        verify(incidentRepository).incidentsOfBookingId(123L);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetIncidentsKO() throws RegisterNotFound {
        serviceBean.postConstruct();
        when(incidentRepository.incidentsOfBookingId(123L)).thenReturn(new ArrayList<>());
        serviceBean.getIncidents(123L);
        verify(incidentRepository).incidentsOfBookingId(123L);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetIncidentsKONotFound() throws RegisterNotFound {
        serviceBean.postConstruct();
        when(incidentRepository.incidentsOfBookingId(123L)).thenReturn(new ArrayList<>());
        serviceBean.getIncidents(123L);
        verify(incidentRepository).incidentsOfBookingId(123L);
    }

    @Test
    public void testUpdateOK() throws RegisterNotFound {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setCancellationId(new CancellationId(uuid));
        ConsentCommOption consentCommOption = new ConsentCommOption();
        consentCommOption.setOptionValue(OptionValue.CASHOUT_CONSENT);
        consentCommOption.setConsentCommsRespLogs(new ArrayList<>());
        when(consentCommsLog.getType()).thenReturn(OptionValue.CASHOUT_CONSENT);
        when(consentCommsLog.getConsentCommOptions()).thenReturn(Collections.singletonList(consentCommOption));
        incident.setConsentCommsLogs(Collections.singletonList(consentCommsLog));
        when(incidentRepository.incidentOfIncidentId(any(IncidentId.class))).thenReturn(incident);
        serviceBean.updateConsent(uuid, consentDTO, "TestChannel");
        verify(incidentRepository).incidentOfIncidentId(new IncidentId(uuid));
    }

    @Test
    public void testUpdateOK2() throws RegisterNotFound {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setCancellationId(new CancellationId(uuid));
        ConsentCommOption consentCommOption = new ConsentCommOption();
        consentCommOption.setOptionValue(OptionValue.OPEN_TICKET_CONSENT);
        consentCommOption.setConsentCommsRespLogs(new ArrayList<>());
        when(consentCommsLog.getType()).thenReturn(OptionValue.OPEN_TICKET_CONSENT);
        when(consentCommsLog.getConsentCommOptions()).thenReturn(Collections.singletonList(consentCommOption));
        incident.setConsentCommsLogs(Collections.singletonList(consentCommsLog));
        when(incidentRepository.incidentOfIncidentId(any(IncidentId.class))).thenReturn(incident);
        serviceBean.updateConsent(uuid, new ConsentDTO(null, null, null, null), "TestChannel");
        verify(incidentRepository).incidentOfIncidentId(new IncidentId(uuid));
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testUpdateKONotFound() throws RegisterNotFound {
        serviceBean.postConstruct();
        when(incidentRepository.incidentOfIncidentId(new IncidentId(uuid))).thenReturn(null);
        serviceBean.updateConsent(uuid, consentDTO, "TestChannel");
        verify(incidentRepository).incidentOfIncidentId(new IncidentId(uuid));
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testUpdateKO() throws RegisterNotFound {
        serviceBean.postConstruct();
        when(incidentRepository.incidentOfIncidentId(any())).thenReturn(null);
        serviceBean.updateConsent(uuid, consentDTO, "TestChannel");
        verify(incidentRepository).incidentOfIncidentId(any());
    }


    @Test
    public void testUpdateIncidentOK() throws RegisterNotFound {
        serviceBean.postConstruct();
        IncidentDTO incidentDTO = new IncidentDTO();
        incidentDTO.setStatus(Status.WAITING_FOR_CUSTOMER_CHOICE.name());
        incidentDTO.setConsentDTO(new ConsentDTO(true, true, null, false));
        when(incidentRepository.incidentsOfBookingIdAndProviderBookingItemId(any(), any())).thenReturn(Collections.singletonList(new Incident()));
        serviceBean.updateIncident(incidentDTO);
        verify(incidentRepository).save(any());
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testUpdateIncidentKONotFound() throws RegisterNotFound {
        serviceBean.postConstruct();
        IncidentDTO incidentDTO = new IncidentDTO();
        when(incidentRepository.incidentsOfBookingIdAndProviderBookingItemId(any(), any())).thenReturn(new ArrayList<>());
        serviceBean.updateIncident(incidentDTO);
        verify(incidentRepository).incidentsOfBookingIdAndProviderBookingItemId(any(), any());
    }


    @Test
    public void testInsertIncidentOK() {
        serviceBean.postConstruct();
        IncidentDTO incidentDTO = new IncidentDTO();
        incidentDTO.setStatus(Status.WAITING_FOR_CUSTOMER_CHOICE.name());
        incidentDTO.setConsentDTO(new ConsentDTO(true, true, null, false));
        serviceBean.addIncident(incidentDTO);
        verify(incidentRepository).save(any());
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testInsertIncidentKO() {
        serviceBean.postConstruct();
        IncidentDTO incidentDTO = new IncidentDTO();
        incidentDTO.setStatus(Status.WAITING_FOR_CUSTOMER_CHOICE.name());
        incidentDTO.setConsentDTO(new ConsentDTO(true, true, null, false));
        doThrow(new NoResultException()).when(incidentRepository).save(any());
        serviceBean.addIncident(incidentDTO);
        verify(incidentRepository).save(any());
    }

    @Test
    public void testAddCancellationIdWithStatusDifferentFromWaitingForCustomerChoice() throws RegisterNotFound {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setStatus(Status.CUSTOMER_CANCELLATION_CHOSEN);
        when(incidentRepository.incidentOfIncidentId(any(IncidentId.class))).thenReturn(incident);
        serviceBean.addCancellationID(uuid, uuid);
        verify(incidentRepository).save(any());
        verifyZeroInteractions(emailSender);
    }


    @Test
    public void testAddCancellationId() throws RegisterNotFound, CancellationNotFoundException {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setStatus(Status.WAITING_FOR_CUSTOMER_CHOICE);
        incident.setConsentCommsLogs(new ArrayList<>());
        incident.setProviderBookingItemId(123L);
        Cancellation cancellation = new Cancellation();
        Item item = new Item();
        ProviderItem providerItem = new ProviderItem();
        providerItem.setProviderBookingItemId(new ProviderBookingItemId("123"));
        providerItem.setResolutionType("MANUAL");
        item.setProviderItems(Collections.singletonList(providerItem));
        cancellation.setItems(Collections.singletonList(item));
        when(cancellationService.getCancellation(any(com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId.class))).thenReturn(cancellation);
        when(incidentRepository.incidentOfIncidentId(any(IncidentId.class))).thenReturn(incident);
        serviceBean.addCancellationID(uuid, uuid);
        verify(incidentRepository).save(any());
        verify(emailSender).sendCancellationEmail(incident);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testAddCancellationIdThrowException() throws RegisterNotFound, CancellationNotFoundException {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setStatus(Status.WAITING_FOR_CUSTOMER_CHOICE);
        incident.setConsentCommsLogs(new ArrayList<>());
        when(cancellationService.getCancellation(any(com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId.class))).thenThrow(new CancellationNotFoundException("message", new Exception()));
        when(incidentRepository.incidentOfIncidentId(any(IncidentId.class))).thenReturn(incident);
        serviceBean.addCancellationID(uuid, uuid);
        verify(incidentRepository).save(any());
        verify(emailSender).sendCancellationEmail(incident);
    }

    @Test
    public void testGetIncidentsWithoutCancellation() {
        serviceBean.postConstruct();
        when(incidentRepository.incidentsWithoutCancellation(1)).thenReturn(Collections.singletonList(new Incident()));
        serviceBean.getIncidentsWithoutCancellation(1);
        verify(incidentRepository).incidentsWithoutCancellation(1);
    }
}
