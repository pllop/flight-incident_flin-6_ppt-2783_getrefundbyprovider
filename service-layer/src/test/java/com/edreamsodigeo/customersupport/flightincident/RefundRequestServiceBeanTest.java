package com.edreamsodigeo.customersupport.flightincident;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.flightincident.model.BspLinkRefundRequest;
import com.edreamsodigeo.customersupport.flightincident.repository.BspLinkRefundRequestRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.RefundReceivedRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaBspLinkRefundRequestRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaRefundReceivedRepository;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RefundRequestServiceBeanTest {
    RefundRequestServiceBean serviceBean;

    @Mock
    private RefundReceivedRepository refundReceivedRepository;
    @Mock
    private BspLinkRefundRequestRepository bspLinkRefundRequestRepository;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(RefundReceivedRepository.class).toInstance(refundReceivedRepository);
            t.bind(BspLinkRefundRequestRepository.class).toInstance(bspLinkRefundRequestRepository);
        });
        serviceBean = new RefundRequestServiceBean();
    }


    @Test
    public void testSaveBspLinkRequest() {
        serviceBean.postConstruct();
        BspLinkRefundRequest request = new BspLinkRefundRequest();
        serviceBean.saveBspLinkRequest(request);
        verify(bspLinkRefundRequestRepository).save(request);
    }


    @Test
    public void testGetBspLinkRequest() {

    }

    @Test
    public void testSaveBspLinkRefundReceived() {
    }
}
