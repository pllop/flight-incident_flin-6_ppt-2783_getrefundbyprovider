package com.edreamsodigeo.customersupport.flightincident.configuration;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.configuration.UnavailableServiceException;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.naming.NamingException;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class JeeServiceLocatorTest extends ContextFactory {

    public static final String MY_APP_JNDI_NAME = "myApp";

    protected ServiceNameResolver serviceNameResolver;

    @BeforeClass
    public void before() throws NamingException {
        MockitoAnnotations.initMocks(this);
        super.before();
        ConfigurationEngine.init();
        serviceNameResolver = ServiceNameResolver.getInstance();
        when(context.lookup(JeeServiceLocator.JNDI_APP_NAME)).thenReturn(JeeServiceLocatorTest.MY_APP_JNDI_NAME);
    }

    @Test
    public void testJndiAppName() throws NamingException {
        assertEquals(JeeServiceLocator.getInstance().getApplicationName(), MY_APP_JNDI_NAME);
    }

    @Test(expectedExceptions = UnavailableServiceException.class)
    public void testNotFoundService() throws NamingException, UnavailableServiceException {
        when(context.lookup(serviceNameResolver.resolveServiceName(String.class, serviceNameResolver.resolveServiceContext(MY_APP_JNDI_NAME)))).thenThrow(new NamingException());
        JeeServiceLocator.getInstance().getService(String.class);
    }

}
