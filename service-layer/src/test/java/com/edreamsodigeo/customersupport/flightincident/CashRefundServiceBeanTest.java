package com.edreamsodigeo.customersupport.flightincident;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.cancellation.v1.CancellationResource;
import com.edreamsodigeo.customersupport.cancellation.v1.exception.CancellationNotFoundException;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Cancellation;
import com.edreamsodigeo.customersupport.cancellation.v1.model.Item;
import com.edreamsodigeo.customersupport.cancellation.v1.model.ProviderItem;
import com.edreamsodigeo.customersupport.cashrefund.BookingItemCashRefund;
import com.edreamsodigeo.customersupport.cashrefund.CashRefundStatus;
import com.edreamsodigeo.customersupport.flightincident.exception.RegisterNotFound;
import com.edreamsodigeo.customersupport.flightincident.model.CancellationId;
import com.edreamsodigeo.customersupport.flightincident.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.repository.IncidentRepository;
import com.edreamsodigeo.customersupport.flightincident.repository.impl.JpaIncidentRepository;
import com.flextrade.jfixture.JFixture;
import com.odigeo.product.itinerary.provideritineraryrefundapi.BspLinkRefundRequest;
import com.odigeo.product.itinerary.provideritineraryrefundapi.DataBaseException;
import com.odigeo.product.itinerary.provideritineraryrefundapi.ProviderItineraryRefundService;
import com.odigeo.product.itinerary.provideritineraryrefundapi.RefundRequestStatus;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.mockito.internal.util.reflection.Whitebox;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class CashRefundServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private Connection connection;
    @Mock
    private JpaIncidentRepository incidentRepository;
    @Mock
    private CancellationResource cancellationService;
    @Mock
    private ProviderItineraryRefundService providerItineraryRefundService;

    private CashRefundServiceBean serviceBean;
    private IncidentId incidentId = new IncidentId(new UUID(1, 1));
    private CancellationId cancellationId = new CancellationId(UUID.fromString("abdb61be-dac9-486f-99ed-e2b9d376fc4d"));

    private JFixture fixture = new JFixture();

    @BeforeMethod
    public void setUp() throws SQLException {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(t -> {
            t.bind(IncidentRepository.class).toInstance(incidentRepository);
            t.bind(CancellationResource.class).toInstance(cancellationService);
            t.bind(ProviderItineraryRefundService.class).toInstance(providerItineraryRefundService);
        });
        serviceBean = new CashRefundServiceBean();
        Whitebox.setInternalState(serviceBean, "enableProviderItineraryRefundServiceIntegration", true);
        when(dataSource.getConnection()).thenReturn(connection);
    }

    @Test(expectedExceptions = RegisterNotFound.class)
    public void testGetRefundsStatusIncidentNotFound() throws RegisterNotFound {
        serviceBean.postConstruct();
        when(incidentRepository.incidentOfIncidentId(incidentId)).thenReturn(null);
        serviceBean.getRefundsStatus(incidentId);
    }

    @Test
    public void testGetRefundsStatusNoCancellationLinkedToIncident() throws RegisterNotFound {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setId(incidentId);

        when(incidentRepository.incidentOfIncidentId(incidentId)).thenReturn(incident);

        List<BookingItemCashRefund> result = serviceBean.getRefundsStatus(incidentId);
        assertEquals(result, Collections.emptyList());
    }

    @Test
    public void testGetRefundsStatusCancellationNotFound() throws RegisterNotFound, CancellationNotFoundException {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setId(incidentId);
        incident.setCancellationId(cancellationId);

        when(incidentRepository.incidentOfIncidentId(incidentId)).thenReturn(incident);
        when(cancellationService.getCancellation(new com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId(incident.getCancellationId().getId().toString())))
                .thenThrow(new CancellationNotFoundException("Error", new RegisterNotFound("Stuff not found")));

        List<BookingItemCashRefund> result = serviceBean.getRefundsStatus(incidentId);
        assertEquals(result, Collections.emptyList());
    }

    @Test
    public void testGetRefundsStatusCancellationUnexpectedException() throws RegisterNotFound, CancellationNotFoundException {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setId(incidentId);
        incident.setCancellationId(cancellationId);

        when(incidentRepository.incidentOfIncidentId(incidentId)).thenReturn(incident);
        when(cancellationService.getCancellation(new com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId(incident.getCancellationId().getId().toString())))
                .thenThrow(new RuntimeException("Error"));

        List<BookingItemCashRefund> result = serviceBean.getRefundsStatus(incidentId);
        assertEquals(result, Collections.emptyList());
    }

    @Test
    public void testGetRefundsStatus() throws RegisterNotFound, CancellationNotFoundException {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setId(incidentId);
        incident.setCancellationId(cancellationId);

        Cancellation cancellation = fixture.create(Cancellation.class);

        Item cancelledItemCashRefund = fixture.create(Item.class);
        ProviderItem providerItemCashRefund = fixture.create(ProviderItem.class);
        providerItemCashRefund.setResolutionType("ATC_AUTOMATIC");
        ProviderItem providerItemNotCashRefund = fixture.create(ProviderItem.class);
        providerItemNotCashRefund.setResolutionType("TF_BSM");
        cancelledItemCashRefund.setProviderItems(Arrays.asList(providerItemCashRefund, providerItemNotCashRefund));

        Item cancelledItemNotCashRefund = fixture.create(Item.class);
        cancelledItemNotCashRefund.setProviderItems(Collections.singletonList(providerItemNotCashRefund));
        cancellation.setItems(Arrays.asList(cancelledItemNotCashRefund, cancelledItemCashRefund));

        when(incidentRepository.incidentOfIncidentId(incidentId)).thenReturn(incident);
        when(cancellationService.getCancellation(new com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId(incident.getCancellationId().getId().toString())))
                .thenReturn(cancellation);

        List<BookingItemCashRefund> result = serviceBean.getRefundsStatus(incidentId);
        assertEquals(result.size(), 1);
        assertEquals(result.get(0).getBookingItemId(), cancelledItemCashRefund.getBookingItemId().getId());
        assertEquals(result.get(0).getProviderBookingItemCashRefunds().get(0).getCashRefundStatus(), CashRefundStatus.REFUND_CREATED);
        assertEquals(result.get(0).getProviderBookingItemCashRefunds().get(0).getProviderBookingItemId(), providerItemCashRefund.getProviderBookingItemId().getId());
    }

    @DataProvider
    public static Object[][] data() {
        return new Object[][] {
                {RefundRequestStatus.ACCEPTED, CashRefundStatus.APPROVED_BY_AIRLINE},
                {RefundRequestStatus.REJECTED, CashRefundStatus.REJECTED_BY_AIRLINE},
                {RefundRequestStatus.REQUESTED, CashRefundStatus.REFUND_REQUESTED_TO_AIRLINE}
        };
    }

    @Test(dataProvider = "data")
    public void testGetRefundsStatusBSPLink(RefundRequestStatus inputRefundRequestStatus, CashRefundStatus expectedCashRefundStatus) throws RegisterNotFound, CancellationNotFoundException, DataBaseException {
        serviceBean.postConstruct();
        Incident incident = new Incident();
        incident.setId(incidentId);
        incident.setCancellationId(cancellationId);

        Cancellation cancellation = fixture.create(Cancellation.class);

        Item cancelledItemCashRefund = fixture.create(Item.class);
        ProviderItem providerItemCashRefund = fixture.create(ProviderItem.class);
        providerItemCashRefund.setResolutionType("BSP_LINK");
        cancelledItemCashRefund.setProviderItems(Collections.singletonList(providerItemCashRefund));
        cancellation.setItems(Collections.singletonList(cancelledItemCashRefund));

        BspLinkRefundRequest bspLinkRefundRequest = fixture.create(BspLinkRefundRequest.class);
        bspLinkRefundRequest.setStatus(inputRefundRequestStatus);

        when(incidentRepository.incidentOfIncidentId(incidentId)).thenReturn(incident);
        when(cancellationService.getCancellation(new com.edreamsodigeo.customersupport.cancellation.v1.model.CancellationId(incident.getCancellationId().getId().toString())))
                .thenReturn(cancellation);
        when(providerItineraryRefundService.getBspLinkRequestByProviderBookingItemId(providerItemCashRefund.getProviderBookingItemId().getId())).thenReturn(bspLinkRefundRequest);

        List<BookingItemCashRefund> result = serviceBean.getRefundsStatus(incidentId);
        assertEquals(result.size(), 1);
        assertEquals(result.get(0).getBookingItemId(), cancelledItemCashRefund.getBookingItemId().getId());
        assertEquals(result.get(0).getProviderBookingItemCashRefunds().get(0).getCashRefundStatus(), expectedCashRefundStatus);
        assertEquals(result.get(0).getProviderBookingItemCashRefunds().get(0).getProviderBookingItemId(), providerItemCashRefund.getProviderBookingItemId().getId());
    }
}
