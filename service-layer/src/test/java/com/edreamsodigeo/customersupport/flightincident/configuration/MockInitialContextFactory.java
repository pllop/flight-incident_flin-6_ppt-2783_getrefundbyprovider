package com.edreamsodigeo.customersupport.flightincident.configuration;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Module;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import java.util.Hashtable;

import static org.mockito.Mockito.when;

public class MockInitialContextFactory implements InitialContextFactory {

    private static final ThreadLocal<Context> CURRENT_CONTEXT = new ThreadLocal<>();

    public static void testAvailableServiceFromConfigurationEngine(Module[] modules, Class<?> type, Context context) throws NamingException {
        setCurrentContext(context);
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, MockInitialContextFactory.class.getName());
        when(context.lookup(JeeServiceLocator.JNDI_APP_NAME)).thenReturn("flight-incident");
        ConfigurationEngine.init(modules);
        ConfigurationEngine.getInstance(type);
        clearCurrentContext();
        System.clearProperty(Context.INITIAL_CONTEXT_FACTORY);
    }

    @Override
    public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
        return CURRENT_CONTEXT.get();
    }

    static void setCurrentContext(Context context) {
        CURRENT_CONTEXT.set(context);
    }

    static void clearCurrentContext() {
        CURRENT_CONTEXT.remove();
    }

}
