package com.edreamsodigeo.customersupport.flightincident;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class StoreNotInitializedExceptionTest {

    private StoreNotInitializedException storeNotInitializedException;

    private static final String EXCEPTION_MESSAGE = "test";


    @Test
    public void testGetDefaultDataSource() {
        storeNotInitializedException = new StoreNotInitializedException(EXCEPTION_MESSAGE);
        assertEquals(storeNotInitializedException.getMessage(), EXCEPTION_MESSAGE);
    }

}
