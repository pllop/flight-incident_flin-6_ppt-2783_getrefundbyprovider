package com.edreamsodigeo.customersupport.flightincident.configuration;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.configuration.ServiceLocator;
import org.testng.annotations.Test;

import javax.naming.NamingException;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class ServiceLayerModuleTest extends ContextFactory {

    @Test
    public void testServiceLocatorBinding() throws NamingException {
        when(context.lookup(JeeServiceLocator.JNDI_APP_NAME)).thenReturn("flight-incident");
        ConfigurationEngine.init(new ServiceLayerModule());
        ServiceLocator serviceLocator = ConfigurationEngine.getInstance(ServiceLocator.class);
        assertNotNull(serviceLocator);
    }

}
