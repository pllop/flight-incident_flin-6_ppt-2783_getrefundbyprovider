package com.edreamsodigeo.customersupport.flightincident.email.sender;

import com.edreamsodigeo.customersupport.flightincident.model.Incident;
import com.odigeo.crm.mailer.EmailApi;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmailSenderTest {

    @Mock
    EmailApi emailApi;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSendCancellationEmail() {
        final Incident incident = Mockito.mock(Incident.class);
        when(incident.getBookingId()).thenReturn(1L);
        getEmailSender().sendCancellationEmail(incident);
        verify(emailApi).sendEmail(any());
    }

    private EmailSender getEmailSender() {
        return new EmailSender(emailApi);
    }
}
