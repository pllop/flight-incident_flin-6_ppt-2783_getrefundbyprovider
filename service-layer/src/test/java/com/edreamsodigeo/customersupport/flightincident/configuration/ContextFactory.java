package com.edreamsodigeo.customersupport.flightincident.configuration;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import static org.mockito.Mockito.mock;

public class ContextFactory {

    protected Context context;

    @BeforeClass
    public void before() throws NamingException {
        context = mock(InitialContext.class);
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, MockInitialContextFactory.class.getName());
        MockInitialContextFactory.setCurrentContext(context);
    }

    @AfterClass
    public void after() {
        System.clearProperty(Context.INITIAL_CONTEXT_FACTORY);
        MockInitialContextFactory.clearCurrentContext();
    }
}
