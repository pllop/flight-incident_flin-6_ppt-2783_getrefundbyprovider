package com.edreamsodigeo.customersupport.flightincident.mapper;

import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommOption;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommOptionId;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsLog;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsLogId;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsRespLog;
import com.edreamsodigeo.customersupport.flightincident.model.ConsentCommsRespLogId;
import com.edreamsodigeo.customersupport.flightincident.model.IncidentId;
import com.edreamsodigeo.customersupport.flightincident.model.OptionValue;
import com.edreamsodigeo.customersupport.flightincident.model.Status;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class IncidentMapperTest {
    private IncidentMapper incidentMapper;

    @BeforeMethod
    public void setUp() {
        incidentMapper = new IncidentMapper();
    }

    @Test
    public void testMapIncident() {
        Incident incident = incidentMapper.mapIncident(createIncident());
        assertNotNull(incident);
    }

    @Test
    public void testMapIncidents() {
        List<Incident> incidents = incidentMapper.mapIncident(Collections.singletonList(createIncident()));
        assertEquals(incidents.size(), 1);
    }

    private com.edreamsodigeo.customersupport.flightincident.model.Incident createIncident() {
        UUID uuid = new UUID(1, 1);
        com.edreamsodigeo.customersupport.flightincident.model.Incident incident = new com.edreamsodigeo.customersupport.flightincident.model.Incident();
        incident.setBookingId(123L);
        incident.setCancellationId(null);
        incident.setDepartureLocalDate(null);
        incident.setFlightNumber("FLIGHTNUMBER");
        incident.setId(new IncidentId(uuid));
        incident.setProviderBookingItemId(123L);
        incident.setStatus(Status.WAITING_FOR_CUSTOMER_CHOICE);
        incident.setVoucherAccepted(null);
        incident.setFlexibleTicketAccepted(null);
        incident.setCashRefundAccepted(null);
        ConsentCommsLog consentCommsLog = new ConsentCommsLog();
        consentCommsLog.setId(new ConsentCommsLogId());
        consentCommsLog.setTimestamp(LocalDateTime.now());
        consentCommsLog.setType(OptionValue.CASHOUT_CONSENT);
        ConsentCommOption consentCommOption = new ConsentCommOption();
        consentCommOption.setId(new ConsentCommOptionId());
        consentCommOption.setOptionValue(OptionValue.CASHOUT_CONSENT);
        ConsentCommsRespLog consentCommsRespLog = new ConsentCommsRespLog();
        consentCommsRespLog.setId(new ConsentCommsRespLogId());
        consentCommsRespLog.setTimestamp(LocalDateTime.now());
        consentCommsRespLog.setResponseDate(LocalDateTime.now());
        consentCommOption.setConsentCommsRespLogs(Collections.singletonList(consentCommsRespLog));
        consentCommsLog.setConsentCommOptions(Collections.singletonList(consentCommOption));
        incident.setConsentCommsLogs(Collections.singletonList(consentCommsLog));
        return incident;
    }
}
