Feature: Flight-incidents calls

  Scenario: getIncident doesn't found
    Given that exists a incident in the database with incident id:
      | incidentId                           |
      | 2d305094-15ed-45d6-9f90-a4f2723daf63 |
    When a call to getIncident is done with incident id:
      | incidentId                           |
      | 29eb8bea-a4bb-4d84-83ee-a50253749e0e |
    Then a "IncidentNotFoundException" is returned

  Scenario: getIncident return an incident
    Given that exists a incident in the database with incident id:
      | incidentId                           |
      | 2d305094-15ed-45d6-9f90-a4f2723daf63 |
    When a call to getIncident is done with incident id:
      | incidentId                           |
      | 2d305094-15ed-45d6-9f90-a4f2723daf63 |
    Then "1" incident/s is/are returned

  Scenario: getIncidents doesn't found incidents
    Given that exists a incident in the database with booking id:
      | bookingId |
      | 123       |
    When a call to getIncidents is done with booking id:
      | bookingId |
      | 456       |
    Then a "IncidentNotFoundException" is returned

  Scenario: getIncidents return a incident
    Given that exists a incident in the database with booking id:
      | bookingId  |
      | 5054916808 |
    When a call to getIncidents is done with booking id:
      | bookingId  |
      | 5054916808 |
    Then "1" incident/s is/are returned

  Scenario: updateConsent
    Given that exists a incident in the database with booking id:
      | bookingId |
      | 123       |
    When a call to updateConsent is done :
      | bookingId | flexibleTicketAccepted | voucherAccepted | cashRefundAccepted | emdAccepted | channel     |
      | 123       | true                   | true            | true               | true        | TestChannel |
    Then retrieving a Incident with bookingId "123" is returned with values:
      | flexibleTicketAccepted | voucherAccepted | cashRefundAccepted | emdAccepted | status                       |
      | true                   | true            | true               | true        | CUSTOMER_CANCELLATION_CHOSEN |

  Scenario: updateConsent to null
    Given that exists a incident in the database with booking id:
      | bookingId |
      | 456       |
    When a call to updateConsent is done :
      | bookingId | channel     |
      | 456       | TestChannel |
    Then retrieving a Incident with bookingId "456" is returned with values:
      | status                      |
      | WAITING_FOR_CUSTOMER_CHOICE |

  Scenario: updateConsent to false
    Given that exists a incident in the database with booking id:
      | bookingId |
      | 789       |
    When a call to updateConsent is done :
      | bookingId | flexibleTicketAccepted | voucherAccepted | cashRefundAccepted | emdAccepted | channel     |
      | 789       | false                  | false           | false              | false       | TestChannel |
    Then retrieving a Incident with bookingId "789" is returned with values:
      | flexibleTicketAccepted | voucherAccepted | cashRefundAccepted | emdAccepted | status                       |
      | false                  | false           | false              | false       | CUSTOMER_CANCELLATION_CHOSEN |
