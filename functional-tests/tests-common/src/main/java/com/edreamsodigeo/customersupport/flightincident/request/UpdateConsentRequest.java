package com.edreamsodigeo.customersupport.flightincident.request;

import com.edreamsodigeo.customersupport.flightincident.v1.model.IncidentId;

public class UpdateConsentRequest {
    private String bookingId;
    private Boolean flexibleTicketAccepted;
    private Boolean voucherAccepted;
    private Boolean cashRefundAccepted;
    private Boolean emdAccepted;
    private String channel;
    private IncidentId incidentId;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Boolean getFlexibleTicketAccepted() {
        return flexibleTicketAccepted;
    }

    public void setFlexibleTicketAccepted(Boolean flexibleTicketAccepted) {
        this.flexibleTicketAccepted = flexibleTicketAccepted;
    }

    public Boolean getVoucherAccepted() {
        return voucherAccepted;
    }

    public void setVoucherAccepted(Boolean voucherAccepted) {
        this.voucherAccepted = voucherAccepted;
    }

    public Boolean getCashRefundAccepted() {
        return cashRefundAccepted;
    }

    public void setCashRefundAccepted(Boolean cashRefundAccepted) {
        this.cashRefundAccepted = cashRefundAccepted;
    }

    public Boolean getEmdAccepted() {
        return emdAccepted;
    }

    public void setEmdAccepted(Boolean emdAccepted) {
        this.emdAccepted = emdAccepted;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public IncidentId getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(IncidentId incidentId) {
        this.incidentId = incidentId;
    }

}
