package com.edreamsodigeo.customersupport.flightincident.request;

public class GetIncidentsRequest {
    private String bookingId;
    private String incidentId;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(String incidentId) {
        this.incidentId = incidentId;
    }
}
