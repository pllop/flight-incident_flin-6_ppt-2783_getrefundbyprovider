package com.edreamsodigeo.customersupport.flightincident.database;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.technology.scriptsexecutor.runner.ScriptRunner;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

@Singleton
public class DatabaseInstaller {

    private static final Logger LOGGER = Logger.getLogger(DatabaseInstaller.class);
    private static final String SCRITPS_CLEAR_DATA_SQL = "clear-data.sql";

    public void installScenario() throws InterruptedException, SQLException, ClassNotFoundException, IOException {
        clearData();
    }

    private void clearData() throws InterruptedException, SQLException, ClassNotFoundException, IOException {
        try (Connection conn = ConfigurationEngine.getInstance(DatabaseConnectionGenerator.class).getOracleAGConnection()) {
            runScriptWithStartAndEndMessages(conn, SCRITPS_CLEAR_DATA_SQL, "Start clear Flight-Incident oracle database data", "End clear Flight-Incident oracle database data", true);
        }
    }

    private void runScriptWithStartAndEndMessages(Connection conn, String scriptName, String startMessage, String endMessage, boolean isLineByLine) throws InterruptedException, SQLException, ClassNotFoundException, IOException {
        LOGGER.info(startMessage);
        runScript(scriptName, conn, isLineByLine);
        LOGGER.info(endMessage);
    }

    private void runScript(String scriptName, Connection conn, boolean isLineByLine) throws InterruptedException, SQLException, ClassNotFoundException, IOException {
        try (InputStream resourceAsStream = DatabaseInstaller.class.getResourceAsStream(scriptName)) {
            runScript(resourceAsStream, conn, isLineByLine);
        }
    }

    private void runScript(InputStream inputStream, Connection conn, boolean isLineByLine) throws InterruptedException, SQLException, ClassNotFoundException, IOException {
        StringWriter stringWriter = new StringWriter();
        try {
            ConfigurationEngine.getInstance(ScriptRunner.class).runScript(inputStream, isLineByLine, conn, stringWriter);
        } finally {
            LOGGER.info("ScriptRunner.runScript output: " + stringWriter.toString());
        }
        if (!conn.getAutoCommit()) {
            conn.commit();
        }
    }
}
