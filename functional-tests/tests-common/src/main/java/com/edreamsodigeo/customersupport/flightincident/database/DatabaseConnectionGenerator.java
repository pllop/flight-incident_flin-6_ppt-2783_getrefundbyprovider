package com.edreamsodigeo.customersupport.flightincident.database;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.technology.docker.ContainerException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.util.Properties;

public class DatabaseConnectionGenerator {
    private static final Logger logger = Logger.getLogger(DatabaseConnectionGenerator.class);
    private static final int GET_CONNECTION_MAX_RETRIES = 3;
    private static final String JDBC_ORACLE_DRIVER = "oracle.jdbc.OracleDriver";
    DatabaseConfiguration databaseConfig = ConfigurationEngine.getInstance(DatabaseConfiguration.class);

    public Connection getOracleAGConnection() throws DBConnectionGenerationException {
        return getOracleConnection(databaseConfig.getUserFlightIncidentDS(), databaseConfig.getPasswordFlightIncidentDS());
    }

    Connection getOracleConnection(String userName, String password) throws DBConnectionGenerationException {
        try {
            return getConnection(databaseConfig.getDbCompleteUrl(), userName, password, JDBC_ORACLE_DRIVER);
        } catch (ClassNotFoundException | SQLException | InterruptedException | ContainerException e) {
            throw new DBConnectionGenerationException(e);
        }
    }

    private Connection getConnection(String urlConnection, String userName, String password, String jdbcDriver) throws ClassNotFoundException, SQLException, InterruptedException {
        Properties connectionProps = new Properties();
        connectionProps.put("user", userName);
        connectionProps.put("password", password);

        Class.forName(jdbcDriver);
        logger.debug(urlConnection + connectionProps);
        Connection conn = getInnerConnectionWithRetry(urlConnection, connectionProps);
        logger.debug("Connected to database with user: " + userName);
        return conn;
    }

    private Connection getInnerConnectionWithRetry(String urlConnection, Properties connectionProps) throws SQLException, InterruptedException {
        Connection conn = null;
        int retries = 0;
        while (conn == null && retries < GET_CONNECTION_MAX_RETRIES) {
            try {
                conn = DriverManager.getConnection(
                        urlConnection,
                        connectionProps);
            } catch (SQLRecoverableException e) {
                retries++;
                Thread.sleep(1000L);
                logger.warn("Could not get Connection retry #" + retries + " - " + e.getMessage() + (e.getCause() != null ? ". Cause " + e.getCause().getMessage() : ""));
            }
        }
        return conn;
    }
}
