package com.edreamsodigeo.customersupport.flightincident.database;

import com.google.inject.Singleton;
import com.odigeo.commons.test.random.Picker;
import com.odigeo.commons.uuid.UUIDSerializer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;
import java.util.UUID;

@Singleton
public class FlightIncidentDatabaseManager {

    private final DatabaseConnectionGenerator databaseConnectionGenerator;
    private static final String INSERT = "Insert into FLIGHT_INCIDENT_OWN.incident (ID,BOOKING_ID,PROVIDER_BOOKING_ITEM_ID,FLIGHT_NUMBER,DEPARTURE_LOCAL_DATE,STATUS,CANCELLATION_ID,FLEXIBLE_TICKET_ACCEPTED,VOUCHER_ACCEPTED,CASH_REFUND_ACCEPTED,EMD_ACCEPTED,TIMESTAMP,LAST_UPDATE) values (?,?,6568728762,'FNUMBER',to_date('05-AUG-20','DD-MON-RR'),'WAITING_FOR_CUSTOMER_CHOICE',null,null,null,null,null,to_date('05-AUG-20','DD-MON-RR'),to_date('20-JUL-20','DD-MON-RR'))";

    public FlightIncidentDatabaseManager() {
        this.databaseConnectionGenerator = new DatabaseConnectionGenerator();
    }


    public void insertIncidentWithBookingId(Long bookingId) {
        try (Connection connection = databaseConnectionGenerator.getOracleAGConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
                preparedStatement.setBytes(1, UUIDSerializer.toBytes(UUID.randomUUID()));
                preparedStatement.setLong(2, bookingId);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertIncidentWithIncidentId(String incidentId) {
        try (Connection connection = databaseConnectionGenerator.getOracleAGConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
                preparedStatement.setBytes(1, UUIDSerializer.toBytes(UUID.fromString(incidentId)));
                preparedStatement.setLong(2, new Picker(new Random()).pickLong(0, 999999999999L));
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
