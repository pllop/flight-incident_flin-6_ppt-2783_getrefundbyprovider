package com.edreamsodigeo.customersupport.flightincident.database;

public class DBConnectionGenerationException extends RuntimeException {
    public DBConnectionGenerationException(Throwable e) {
        super(e);
    }
}
