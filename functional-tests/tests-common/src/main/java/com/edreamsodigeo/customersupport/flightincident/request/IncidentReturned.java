package com.edreamsodigeo.customersupport.flightincident.request;

public class IncidentReturned {
    private Boolean flexibleTicketAccepted;
    private Boolean voucherAccepted;
    private Boolean cashRefundAccepted;
    private Boolean emdAccepted;
    private String status;

    public Boolean getFlexibleTicketAccepted() {
        return flexibleTicketAccepted;
    }

    public void setFlexibleTicketAccepted(Boolean flexibleTicketAccepted) {
        this.flexibleTicketAccepted = flexibleTicketAccepted;
    }

    public Boolean getVoucherAccepted() {
        return voucherAccepted;
    }

    public void setVoucherAccepted(Boolean voucherAccepted) {
        this.voucherAccepted = voucherAccepted;
    }

    public Boolean getCashRefundAccepted() {
        return cashRefundAccepted;
    }

    public void setCashRefundAccepted(Boolean cashRefundAccepted) {
        this.cashRefundAccepted = cashRefundAccepted;
    }

    public Boolean getEmdAccepted() {
        return emdAccepted;
    }

    public void setEmdAccepted(Boolean emdAccepted) {
        this.emdAccepted = emdAccepted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
