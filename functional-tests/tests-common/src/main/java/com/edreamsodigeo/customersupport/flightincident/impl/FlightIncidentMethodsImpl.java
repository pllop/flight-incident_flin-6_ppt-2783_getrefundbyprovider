package com.edreamsodigeo.customersupport.flightincident.impl;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.flightincident.ServerConfiguration;
import com.edreamsodigeo.customersupport.flightincident.gateway.FlightIncidentMethods;
import com.edreamsodigeo.customersupport.flightincident.request.AddIncidentsRequest;
import com.edreamsodigeo.customersupport.flightincident.request.UpdateConsentRequest;
import com.edreamsodigeo.customersupport.flightincident.v1.FlightIncidentResource;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.IncidentNotFoundException;
import com.edreamsodigeo.customersupport.flightincident.v1.model.BookingId;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Consent;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.v1.model.IncidentId;
import com.odigeo.commons.rest.ServiceBuilder;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;

import java.time.LocalDateTime;
import java.util.List;

public class FlightIncidentMethodsImpl implements FlightIncidentMethods {
    private final ServerConfiguration serverConfiguration;
    private final FlightIncidentResource flightIncidentResource;

    public FlightIncidentMethodsImpl() {
        serverConfiguration = ConfigurationEngine.getInstance(ServerConfiguration.class);
        flightIncidentResource = createService(FlightIncidentResource.class, "/flight-incident");
    }

    @Override
    public Incident getIncident(String incidentId) throws IncidentNotFoundException {
        return flightIncidentResource.getIncident(new IncidentId(incidentId));
    }

    @Override
    public List<Incident> getIncidents(String bookingId) throws IncidentNotFoundException {
        return flightIncidentResource.getIncidents(new BookingId(bookingId));
    }

    @Override
    public void updateConsent(UpdateConsentRequest updateConsentRequest) throws IncidentNotFoundException {
        Consent consent = createConsent(updateConsentRequest);
        flightIncidentResource.updateConsent(updateConsentRequest.getIncidentId(), consent, updateConsentRequest.getChannel());
    }

    private Consent createConsent(UpdateConsentRequest updateConsentRequest) {
        Consent consent = new Consent();
        consent.setFlexibleTicketAccepted(updateConsentRequest.getFlexibleTicketAccepted());
        consent.setCashRefundAccepted(updateConsentRequest.getCashRefundAccepted());
        consent.setEmdAccepted(updateConsentRequest.getEmdAccepted());
        consent.setVoucherAccepted(updateConsentRequest.getVoucherAccepted());
        return consent;
    }

    @Override
    public void addIncident(AddIncidentsRequest addIncidentsRequest) throws DataBaseException {
        Incident incident = new Incident();
        incident.setBookingId(new BookingId(addIncidentsRequest.getBookingId().toString()));
        incident.setProviderBookingItemId(addIncidentsRequest.getProviderBookingItemId());
        incident.setDepartureLocalDateTime(LocalDateTime.now());
        Consent consent = new Consent();
        consent.setEmdAccepted(addIncidentsRequest.getEmdAccepted());
        consent.setCashRefundAccepted(addIncidentsRequest.getCashRefundAccepted());
        consent.setVoucherAccepted(addIncidentsRequest.getVoucherAccepted());
        consent.setFlexibleTicketAccepted(addIncidentsRequest.getFlexibleTicketAccepted());
        incident.setConsent(consent);
        flightIncidentResource.addIncident(incident);
    }

    private <service> service createService(Class<service> serviceClass, String appName) {
        final ServiceBuilder<service> flightIncidentServiceBuilder = new ServiceBuilder<>(serviceClass, "http://" + serverConfiguration.getServer() + appName, new SimpleRestErrorsHandler(serviceClass));
        ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration.Builder()
                .socketTimeoutInMillis(1000001)
                .connectionTimeoutInMillis(1000001)
                .maxConcurrentConnections(100)
                .build();
        flightIncidentServiceBuilder.withConnectionConfiguration(connectionConfiguration);
        return flightIncidentServiceBuilder.build();
    }
}
