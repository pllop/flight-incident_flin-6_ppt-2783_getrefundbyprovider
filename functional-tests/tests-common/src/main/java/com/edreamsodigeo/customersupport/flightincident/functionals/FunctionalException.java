package com.edreamsodigeo.customersupport.flightincident.functionals;

public class FunctionalException extends RuntimeException {
    public FunctionalException(Throwable cause) {
        super(cause);
    }
}
