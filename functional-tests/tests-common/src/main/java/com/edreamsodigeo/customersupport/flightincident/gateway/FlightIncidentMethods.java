package com.edreamsodigeo.customersupport.flightincident.gateway;

import com.edreamsodigeo.customersupport.flightincident.request.AddIncidentsRequest;
import com.edreamsodigeo.customersupport.flightincident.request.UpdateConsentRequest;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.IncidentNotFoundException;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;

import java.util.List;

public interface FlightIncidentMethods {
    Incident getIncident(String incidentId) throws IncidentNotFoundException;
    List<Incident> getIncidents(String bookingId) throws IncidentNotFoundException;

    void updateConsent(UpdateConsentRequest updateConsentRequest) throws IncidentNotFoundException;

    void addIncident(AddIncidentsRequest addIncidentsRequest) throws DataBaseException;
}
