package com.edreamsodigeo.customersupport.flightincident;

import com.google.inject.Singleton;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.DockerHostRetriever;

@Singleton
public class ServerConfiguration {

    public static final String ENGINEERING_CONTEXT = "/flight-incident/engineering/";

    private final DockerHostRetriever dockerHostRetriever;
    private final String serviceServer;

    @SuppressWarnings("PMD.AvoidUsingHardCodedIP")
    public ServerConfiguration() throws ContainerException {
        dockerHostRetriever = new DockerHostRetriever();
        serviceServer = dockerHostRetriever.getDockerHost() + ":8080";
    }


    public String getServer() {
        return serviceServer;
    }
}
