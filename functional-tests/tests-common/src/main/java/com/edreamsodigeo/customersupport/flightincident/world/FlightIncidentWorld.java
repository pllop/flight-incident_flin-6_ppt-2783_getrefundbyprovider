package com.edreamsodigeo.customersupport.flightincident.world;

import com.edreamsodigeo.customersupport.flightincident.database.FlightIncidentDatabaseManager;
import com.edreamsodigeo.customersupport.flightincident.gateway.FlightIncidentMethods;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Collections;
import java.util.List;

@ScenarioScoped
public class FlightIncidentWorld {

    private FlightIncidentMethods flightIncidentMethods;
    private FlightIncidentDatabaseManager flightIncidentDatabaseManager;
    private Exception errorResponse;
    private List<Incident> incidentResponse;

    public FlightIncidentMethods getFlightIncidentMethods() {
        return this.flightIncidentMethods;
    }

    public void setFlightIncidentMethods(FlightIncidentMethods flightIncidentMethods) {
        this.flightIncidentMethods = flightIncidentMethods;
    }

    public Exception getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(Exception errorResponse) {
        this.errorResponse = errorResponse;
    }

    public List<Incident> getIncidentResponse() {
        return incidentResponse;
    }

    public void setIncidentResponse(List<Incident> incidentResponse) {
        this.incidentResponse = incidentResponse;
    }

    public void setIncidentResponse(Incident incidentResponse) {
        this.incidentResponse = Collections.singletonList(incidentResponse);
    }


    public FlightIncidentDatabaseManager getFlightIncidentDatabaseManager() {
        return flightIncidentDatabaseManager;
    }

    public void setFlightIncidentDatabaseManager(FlightIncidentDatabaseManager flightIncidentDatabaseManager) {
        this.flightIncidentDatabaseManager = flightIncidentDatabaseManager;
    }
}
