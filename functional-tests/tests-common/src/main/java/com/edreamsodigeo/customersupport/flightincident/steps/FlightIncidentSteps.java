package com.edreamsodigeo.customersupport.flightincident.steps;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.customersupport.flightincident.database.FlightIncidentDatabaseManager;
import com.edreamsodigeo.customersupport.flightincident.impl.FlightIncidentMethodsImpl;
import com.edreamsodigeo.customersupport.flightincident.request.AddIncidentsRequest;
import com.edreamsodigeo.customersupport.flightincident.request.GetIncidentsRequest;
import com.edreamsodigeo.customersupport.flightincident.request.IncidentReturned;
import com.edreamsodigeo.customersupport.flightincident.request.UpdateConsentRequest;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.DataBaseException;
import com.edreamsodigeo.customersupport.flightincident.v1.exception.IncidentNotFoundException;
import com.edreamsodigeo.customersupport.flightincident.v1.model.Incident;
import com.edreamsodigeo.customersupport.flightincident.world.FlightIncidentWorld;
import com.google.inject.Inject;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;
import java.util.Optional;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

@ScenarioScoped
public class FlightIncidentSteps {

    private final FlightIncidentWorld flightIncidentWorld;

    @Inject
    public FlightIncidentSteps(FlightIncidentWorld flightIncidentWorld) {
        this.flightIncidentWorld = flightIncidentWorld;
    }

    @Before
    public void install() {
        flightIncidentWorld.setFlightIncidentMethods(new FlightIncidentMethodsImpl());
        flightIncidentWorld.setFlightIncidentDatabaseManager(ConfigurationEngine.getInstance(FlightIncidentDatabaseManager.class));
    }

    @Given("that exists a incident in the database with incident id:$")
    public void addIncidentToDatabaseByIncidentId(List<GetIncidentsRequest> getIncidentsRequests) {
        flightIncidentWorld.getFlightIncidentDatabaseManager().insertIncidentWithIncidentId(getIncidentsRequests.get(0).getIncidentId());
    }

    @Given("that exists a incident in the database with booking id:$")
    public void addIncidentToDatabaseByBookingId(List<GetIncidentsRequest> getIncidentsRequests) {
        flightIncidentWorld.getFlightIncidentDatabaseManager().insertIncidentWithBookingId(Long.parseLong(getIncidentsRequests.get(0).getBookingId()));
    }

    @When("a call to getIncident is done with incident id:$")
    public void callGetIncident(List<GetIncidentsRequest> getIncidentsRequests) {
        try {
            flightIncidentWorld.setIncidentResponse(this.flightIncidentWorld.getFlightIncidentMethods().getIncident(getIncidentsRequests.get(0).getIncidentId()));
        } catch (IncidentNotFoundException e) {
            flightIncidentWorld.setErrorResponse(e);
        }
    }

    @When("a call to getIncidents is done with booking id:$")
    public void callGetIncidents(List<GetIncidentsRequest> getIncidentsRequests) {
        try {
            flightIncidentWorld.setIncidentResponse(this.flightIncidentWorld.getFlightIncidentMethods().getIncidents(getIncidentsRequests.get(0).getBookingId()));
        } catch (IncidentNotFoundException e) {
            flightIncidentWorld.setErrorResponse(e);
        }
    }

    @When("a call to updateConsent is done :$")
    public void callUpdateConsent(List<UpdateConsentRequest> updateConsentRequests) {
        try {
            List<Incident> incidents = this.flightIncidentWorld.getFlightIncidentMethods().getIncidents(updateConsentRequests.get(0).getBookingId());
            updateConsentRequests.get(0).setIncidentId(incidents.get(0).getId());
            this.flightIncidentWorld.getFlightIncidentMethods().updateConsent(updateConsentRequests.get(0));
        } catch (IncidentNotFoundException e) {
            flightIncidentWorld.setErrorResponse(e);
        }
    }

    @When("a call to addIncidents is done with:$")
    public void callAddIncident(List<AddIncidentsRequest> addIncidentsRequests) {
        try {
            this.flightIncidentWorld.getFlightIncidentMethods().addIncident(addIncidentsRequests.get(0));
        } catch (DataBaseException e) {
            flightIncidentWorld.setErrorResponse(e);
        }
    }

    @Then("^a \"([^\"]*)\" is returned$")
    public void anExceptionIsReturned(String exceptionCaught) {
        assertNotNull(flightIncidentWorld.getErrorResponse());
        Optional.of(flightIncidentWorld.getErrorResponse())
                .map(error -> error.getClass().getSimpleName())
                .ifPresent(error -> assertEquals(error, exceptionCaught));

    }

    @Then("^retrieving a Incident with bookingId \"([^\"]*)\" is returned with values:$")
    public void anIncidentWithValuesIsReturned(String bookingId, List<IncidentReturned> incidentReturneds) throws IncidentNotFoundException {
        Incident incident = this.flightIncidentWorld.getFlightIncidentMethods().getIncidents(bookingId).get(0);
        IncidentReturned incidentReturned = incidentReturneds.get(0);
        assertEquals(incident.getStatus().toString(), incidentReturned.getStatus());
        assertEquals(incident.getConsent().getCashRefundAccepted(), incidentReturned.getCashRefundAccepted());
        assertEquals(incident.getConsent().getEmdAccepted(), incidentReturned.getEmdAccepted());
        assertEquals(incident.getConsent().getFlexibleTicketAccepted(), incidentReturned.getFlexibleTicketAccepted());
        assertEquals(incident.getConsent().getVoucherAccepted(), incidentReturned.getVoucherAccepted());
    }

    @Then("^\"([^\"]*)\" incident/s is/are returned$")
    public void anNumberIncidentIsReturned(String numberIncidents) {
        assertNotNull(flightIncidentWorld.getIncidentResponse());
        assertEquals(flightIncidentWorld.getIncidentResponse().size(), Integer.parseInt(numberIncidents));
    }
}
