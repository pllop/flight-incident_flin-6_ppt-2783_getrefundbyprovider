package com.edreamsodigeo.customersupport.flightincident.database;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.DockerHostRetriever;

@Singleton
@ConfiguredInPropertiesFile
public class DatabaseConfiguration {
    private static final String JDBC_URL_ORACLE_THIN = "jdbc:oracle:thin:@//{host}:{port}/{schema}";
    private static final String JDBC_URL_HOST = "{host}";
    private static final String JDBC_URL_PORT = "{port}";
    private static final String JDBC_URL_SCHEMA = "{schema}";

    private final DockerHostRetriever dockerHostRetriever = new DockerHostRetriever();

    private Integer port;
    private String sid;
    private String userFlightIncidentDS;
    private String passwordFlightIncidentDS;

    public String getHost() throws ContainerException {
        return dockerHostRetriever.getDockerHost();
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getUserFlightIncidentDS() {
        return userFlightIncidentDS;
    }

    public void setUserFlightIncidentDS(String userFlightIncidentDS) {
        this.userFlightIncidentDS = userFlightIncidentDS;
    }

    public String getPasswordFlightIncidentDS() {
        return passwordFlightIncidentDS;
    }

    public void setPasswordFlightIncidentDS(String passwordFlightIncidentDS) {
        this.passwordFlightIncidentDS = passwordFlightIncidentDS;
    }

    public String getDbCompleteUrl() throws ContainerException {
        return JDBC_URL_ORACLE_THIN.replace(JDBC_URL_HOST, getHost()).
                replace(JDBC_URL_PORT, getPort().toString()).
                replace(JDBC_URL_SCHEMA, getSid());
    }
}
