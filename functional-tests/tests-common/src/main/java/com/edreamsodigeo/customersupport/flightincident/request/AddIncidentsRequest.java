package com.edreamsodigeo.customersupport.flightincident.request;


public class AddIncidentsRequest {

    private String id;
    private Long bookingId;
    private Long providerBookingItemId;
    private String flightNumber;
    private String status;
    private String cancellationId;
    private Boolean flexibleTicketAccepted;
    private Boolean voucherAccepted;
    private Boolean cashRefundAccepted;
    private Boolean emdAccepted;

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public Long getProviderBookingItemId() {
        return providerBookingItemId;
    }

    public void setProviderBookingItemId(Long providerBookingItemId) {
        this.providerBookingItemId = providerBookingItemId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getFlexibleTicketAccepted() {
        return flexibleTicketAccepted;
    }

    public void setFlexibleTicketAccepted(Boolean flexibleTicketAccepted) {
        this.flexibleTicketAccepted = flexibleTicketAccepted;
    }

    public Boolean getVoucherAccepted() {
        return voucherAccepted;
    }

    public void setVoucherAccepted(Boolean voucherAccepted) {
        this.voucherAccepted = voucherAccepted;
    }

    public Boolean getCashRefundAccepted() {
        return cashRefundAccepted;
    }

    public void setCashRefundAccepted(Boolean cashRefundAccepted) {
        this.cashRefundAccepted = cashRefundAccepted;
    }

    public Boolean getEmdAccepted() {
        return emdAccepted;
    }

    public void setEmdAccepted(Boolean emdAccepted) {
        this.emdAccepted = emdAccepted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCancellationId() {
        return cancellationId;
    }

    public void setCancellationId(String cancellationId) {
        this.cancellationId = cancellationId;
    }
}
